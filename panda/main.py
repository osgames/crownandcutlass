#! /usr/bin/env python

from pandac.PandaModules import loadPrcFile

loadPrcFile('z_crownandcutlass_config.prc')

import direct.directbase.DirectStart
from direct.showbase.DirectObject import DirectObject

from pandac.PandaModules import FrameBufferProperties
from pandac.PandaModules import WindowProperties
from pandac.PandaModules import Camera
from pandac.PandaModules import NodePath
from pandac.PandaModules import Mat4
from pandac.PandaModules import DirectionalLight
from pandac.PandaModules import Point3
from direct.directtools.DirectGeometry import LineNodePath

from ships import Sloop, Schooner, Fluyt, Brig
from ccmap import Map
from ocean import Ocean
import physics

import sys
import math

class World(DirectObject):
    def __init__(self):
        base.disableMouse()
        self.accept("escape", sys.exit)
        
        self.shipRoot = render.attachNewNode("shipRoot")
        
        self.sloop = Sloop(self.shipRoot)
        self.sloop.setPos(10, 20, 0)
        
        base.camera.setPos(10, 0, 10)
        base.camera.lookAt(self.sloop.getPos())
        
        #self.schooner = Schooner(self.shipRoot)
        #self.schooner.setPos(0, 40, -5)
        
        #self.brig = Brig(self.shipRoot)
        #self.brig.setPos(0, 20, -5)
        
        #self.fluyt = Fluyt(self.shipRoot)
        #self.fluyt.setPos(0, 80, -5)
        
        #dlight = DirectionalLight('my dlight')
        #dlnp = render.attachNewNode(dlight)
        #render.setLight(dlnp)
        
        #self.map = Map()
        
        self.oceanRoot = render.attachNewNode('oceanRoot')
        self.ocean = Ocean(self.oceanRoot)
        
        self.ocean.setScale(100)
        self.ocean.setPos(0, 0, 0)
        
        taskMgr.add(self._updateCamera, 'update_camera')
        
        self.accept('m', self._toggleMouse)
        self.accept('mouse1', self._moveShip)
        self.accept('s', self.sloop.fireCannons)
        self._drawGrid()
        
        # Physics Stuff
        base.enableParticles()
        physics.AddGravity()
        
    def _drawGrid(self):
        lnp = LineNodePath(render, 'grid')
        lnp.drawLines([
            [(100,0,0), (-100,0,0)],
            [(0,100,0), (0,-100,0)],
            [(0,0,100), (0,0,-100)]])
        lnp.create()
        
    def _toggleMouse(self):
        base.oobe() # Out of Body experience
        if self.isAccepting('mouse1'):
            self.ignore('mouse1')
        else:
            self.accept('mouse1', self._moveShip)
        
    def _updateCamera(self, task):
        sloopPos = self.sloop.getPos()
        base.camera.setPos(sloopPos[0], sloopPos[1] - 20, 10)
        base.camera.lookAt(sloopPos)
        return task.cont
        
    def _moveShip(self):
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            pos3d = Point3()
            nearPoint = Point3()
            farPoint = Point3()
            base.camLens.extrude(mpos, nearPoint, farPoint)
            if self.ocean.plane.intersectsLine(pos3d,
               render.getRelativePoint(camera, nearPoint),
               render.getRelativePoint(camera, farPoint)):
                print "Mouse ray intersects ocean at ", pos3d
                self.sloop.sailTo(pos3d)

w = World()

if __name__=='__main__':
    run()
