from utils import *

class CCModel(object):    
    def __init__(self, node, model):      
        self.model = loader.loadModel(model)
        self.model.reparentTo(node)
        self.node = node
        
    def getPos(self):
        return self.node.getPos()
    
    def setPos(self, x, y, z):
        self.node.setPos(x,y,z)
    
    def setScale(self, scale):
        self.node.setScale(scale)
        
    def getHpr(self):
        return self.node.getHpr()
    
    def setHpr(self, h, p, r):
        self.node.setHpr(h, p, r)
    
    def reparentTo(self, node):
        self.model.reparentTo(node)
        self.node = node

