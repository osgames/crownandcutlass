from pandac.PandaModules import GeoMipTerrain
from pandac.PandaModules import Filename
from pandac.PandaModules import Texture
from pandac.PandaModules import TextureStage

class Map(object):
    def __init__(self):
        self._terrain = GeoMipTerrain("Carribean")
        self._terrain.setHeightfield(Filename("heightmap.png"))
        
        self._terrain.setBlockSize(32)
        self._terrain.setFactor(100)
        self._terrain.setFocalPoint(base.camera)
        
        tex0 = loader.loadTexture("land.png")
        tex0.setMagfilter(Texture.FTLinearMipmapLinear)
        
        root = self._terrain.getRoot()
        root.setTexture( TextureStage('tex0'), tex0)

        root.setPos(0,0,-10)
        root.setSz(30)
        root.reparentTo(render)
        
        self._terrain.generate()
        
        taskMgr.add(self.updateMapTask, "update")
        
    def updateMapTask(self, task):
        self._terrain.update()
        return task.cont

