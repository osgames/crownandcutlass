from pandac.PandaModules import ForceNode
from pandac.PandaModules import LinearVectorForce

def AddGravity():
    if not render.find('*/gravity-physics').isEmpty():
        gravityFN = ForceNode('gravity-physics')
        NP = render.attachNewNode(gravityFN)
        force = LinearVectorForce(0, 0, -9.81) # Gravity Acceleration
        gravityFN.addForce(gravityForce)
        base.physicsMgr.addLinearForce(gravityForce)
