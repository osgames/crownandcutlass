
class CCException(Exception):
    pass

class CCAssertException(CCException):
    pass

def check(check_expr, message):
    if not check_expr:
        raise CCAssertException(message)

