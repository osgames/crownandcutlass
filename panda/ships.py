from pandac.PandaModules import Point3
from pandac.PandaModules import ActorNode
from pandac.PandaModules import ForceNode
from pandac.PandaModules import LinearVectorForce
from direct.interval.LerpInterval import LerpHprInterval
from direct.interval.LerpInterval import LerpPosInterval
from direct.interval.MetaInterval import Sequence

from utils import *
from model import CCModel

def CreateShip(model_name):
    class Ship(CCModel):
        _left = 1
        _right = -1
        def __init__(self, node):
            super(Ship, self).__init__(node, model_name)
            self._movement = None
        
        def sailTo(self, toLocation):
            if self._movement and self._movement.isPlaying():
                self._movement.pause()
            n = self.node
            curHpr = n.getHpr()
            n.lookAt(Point3(toLocation))
            finalHpr = n.getHpr()
            n.setHpr(curHpr)
            
            turnLerp = LerpHprInterval(n, 1, finalHpr)
            moveLerp = LerpPosInterval(n, 2, toLocation, blendType='easeInOut')
            
            self._movement = Sequence(turnLerp, moveLerp)
            self._movement.start()
            
        def fireCannons(self):
            n = self.node.getParent()
            cball = CCModel(self.node.getParent(), 'models/cannon_ball')
            an = ActorNode('cannon-ball-physics')
            anp = n.attachNewNode(an)
            
            base.physicsMgr.attachPhysicalNode(an)
            cball.reparentTo(anp)
            cball.setPos(self.getPos()[0], self.getPos()[1], self.getPos()[2])
            an.getPhysicsObject().setMass(22.6796) # about 50 Lbs
            
            fn = ForceNode('cannon-ball-force')
            fnp = n.attachNewNode(fn)
            force = LinearVectorForce(300, 15, 0)
            force.setMassDependent(1)
            fn.addForce(force)
            an.getPhysical(0).addLinearForce(force)
            
    return Ship

Sloop = CreateShip('models/sloop')
Brig = CreateShip('models/brig')
Schooner = CreateShip('models/schooner')
Fluyt = CreateShip('models/fluyt')

