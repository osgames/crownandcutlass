from direct.showbase.ShowBase import Plane
from pandac.PandaModules import Vec3
from pandac.PandaModules import Point3
from pandac.PandaModules import Texture
from pandac.PandaModules import TextureStage

from model import CCModel

class Ocean(CCModel):
    def __init__(self, node):
        super(Ocean, self).__init__(node, 'models/ocean')
        self.plane = Plane(Vec3(0,0,1), Point3(0,0,0))
        #self.model.clearTexture()
        ts = self.model.findTextureStage('*')
        t = loader.loadTexture('models/water.png')
        t.setWrapU(Texture.WMRepeat)
        t.setWrapV(Texture.WMRepeat)
        self.model.setTexture(ts, t, 1)
        
        #ts = TextureStage('ocean-tex')
        #ts.setMode(TextureStage.MReplace)
        #self.model.setTexture(ts, t, 1)
