# - Locate Ogg Vorbis
# This module defines
#  VORBIS_LIBRARY
#  VORBISFILE_LIBRARY
#  VORBIS_FOUND, if false, do not try to link to vorbisfile 
#  VORBIS_INCLUDE_DIR, where to find the headers

FIND_PATH(VORBIS_INCLUDE_DIR vorbis/vorbisfile.h
  /usr/local/include
  /usr/include
  C:/VorbisSDK/include
  /Library/Frameworks
  )

FIND_LIBRARY(VORBIS_LIBRARY 
  NAMES vorbis libvorbis
  PATHS
  /usr/local/lib
  /usr/lib
  C:/VorbisSDK/lib
  /Library/Frameworks
  )
IF(APPLE AND UNIX)
  SET(VORBISFILE_LIBRARY ${VORBIS_LIBRARY})
ELSE(APPLE AND UNIX)
  FIND_LIBRARY(VORBISFILE_LIBRARY 
    NAMES vorbisfile libvorbisfile
    PATHS
    /usr/local/lib
    /usr/lib
    C:/VorbisSDK/lib
    /Library/Frameworks
    )
ENDIF(APPLE AND UNIX)

SET(VORBIS_FOUND 0)
IF(VORBIS_LIBRARY AND VORBISFILE_LIBRARY AND VORBIS_INCLUDE_DIR)
  SET(VORBIS_FOUND 1)
ENDIF(VORBIS_LIBRARY AND VORBISFILE_LIBRARY AND VORBIS_INCLUDE_DIR)

IF(NOT VORBIS_FOUND)
  IF(VORBIS_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "vorbisfile was not found.")
  ELSE(VORBIS_FIND_REQUIRED)
    IF(NOT VORBIS_FIND_QUIETLY)
      MESSAGE(STATUS "vorbisfile was not found.")
    ENDIF(NOT VORBIS_FIND_QUIETLY)
  ENDIF(VORBIS_FIND_REQUIRED)
ENDIF(NOT VORBIS_FOUND)
