# - Locate CEGUI
#
# CEGUI (Crazy Eddie's GUI System) is an MIT-licensed library providing
# windowing and widgets for graphics APIs / engines. The library is object
# orientated, written in C++, and targeted at games developers.  It can be found
# at:
# http://www.cegui.org.uk/
#
# This module defines
#  CEGUI_LIBRARY
#  CEGUI_FOUND, if false, do not try to link to CEGUI
#  CEGUI_INCLUDE_DIR, where to find the headers

FIND_PATH(CEGUI_INCLUDE_DIR CEGUI.h
  /usr/local/include/CEGUI
  /usr/include/CEGUI
  C:/CeguiSDK/include
  C:/OgreSDK/include/CEGUI
  ~/Library/Frameworks
  ~/OgreSDK/Dependencies
  )

FIND_LIBRARY(CEGUI_LIBRARY 
  NAMES
    CEGUIBase
    CEGUI
  PATHS
    /usr/local/lib
    /usr/lib
    C:/CeguiSDK/lib
    C:/OgreSDK/lib
    ~/Library/Frameworks
    ~/OgreSDK/Dependencies
  )

SET(CEGUI_FOUND 0)
IF(CEGUI_LIBRARY)
  SET(CEGUI_FOUND 1)
ENDIF(CEGUI_LIBRARY)

IF(NOT CEGUI_FOUND)
  IF(CEGUI_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "CEGUI was not found.")
  ELSE(CEGUI_FIND_REQUIRED)
    IF(NOT CEGUI_FIND_QUIETLY)
      MESSAGE(STATUS "CEGUI was not found.")
    ENDIF(NOT CEGUI_FIND_QUIETLY)
  ENDIF(CEGUI_FIND_REQUIRED)
ENDIF(NOT CEGUI_FOUND)
