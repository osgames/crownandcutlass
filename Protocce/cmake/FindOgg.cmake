# - Locate Ogg
# This module defines
#  OGG_LIBRARY
#  OGG_FOUND, if false, do not try to link to ogg 
#  OGG_INCLUDE_DIR, where to find the headers

FIND_PATH(OGG_INCLUDE_DIR ogg/ogg.h
  /usr/local/include
  /usr/include
  C:/OggSDK/include
  /Library/Frameworks
  )

FIND_LIBRARY(OGG_LIBRARY 
  NAMES ogg libogg
  PATHS
  /usr/local/lib
  /usr/lib
  C:/OggSDK/lib
  /Library/Frameworks
  )

SET(OGG_FOUND 0)
IF(OGG_LIBRARY AND OGG_INCLUDE_DIR)
  SET(OGG_FOUND 1)
ENDIF(OGG_LIBRARY AND OGG_INCLUDE_DIR)

IF(NOT OGG_FOUND)
  IF(OGG_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "ogg was not found.")
  ELSE(OGG_FIND_REQUIRED)
    IF(NOT OGG_FIND_QUIETLY)
      MESSAGE(STATUS "ogg was not found.")
    ENDIF(NOT OGG_FIND_QUIETLY)
  ENDIF(OGG_FIND_REQUIRED)
ENDIF(NOT OGG_FOUND)
