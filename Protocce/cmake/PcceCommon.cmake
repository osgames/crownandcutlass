# Some common settings for Crown and Cutlass

# Add a common path to a list of filenames
MACRO(PCCE_ADD_PATH_TO_FILES var path_to_add)
  # Detect whether we need to add trailing /
  SET(add_path_regex ".*/$")
  SET(add_path_fixed ${path_to_add})
  IF(NOT ${path_to_add} MATCHES ${add_path_regex})
    SET(add_path_fixed "${add_path_fixed}/")
  ENDIF(NOT ${path_to_add} MATCHES ${add_path_regex})

  # Loop through each file in the list and add the path
  FOREACH(i ${ARGN})
    LIST(APPEND ${var} "${add_path_fixed}${i}")
  ENDFOREACH(i)
ENDMACRO(PCCE_ADD_PATH_TO_FILES)

# Find boost libs, since some systems add a -mt suffix
MACRO(PCCE_FIND_BOOST_LIB var boost_lib_path lib_name)
  FIND_LIBRARY(${var}
    NAMES
      ${lib_name}-mt
      ${lib_name}-xgcc40-mt
      ${lib_name}
    PATHS
      ${boost_lib_path}
  )
  IF(NOT ${var})
    MESSAGE(FATAL_ERROR "Could not find boost lib ${lib_name}")
  ENDIF(NOT ${var})
ENDMACRO(PCCE_FIND_BOOST_LIB)

MACRO(PCCE_FIND_BOOST_LIBS var boost_lib_path)
  # Loop through each file in the list and add the path
  FOREACH(i ${ARGN})
    SET(t "pcce_${i}_lib") 
    PCCE_FIND_BOOST_LIB(${t} ${boost_lib_path} ${i})
    LIST(APPEND ${var} ${${t}})
  ENDFOREACH(i)
ENDMACRO(PCCE_FIND_BOOST_LIBS)

MACRO(PCCE_GET_BOOST_LIBS var boost_lib_path)
  PCCE_FIND_BOOST_LIBS(
    ${var}
    ${boost_lib_path}
    boost_thread
    boost_signals
    ${ARGN}
  )
ENDMACRO(PCCE_GET_BOOST_LIBS)

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Debug)
ENDIF(NOT CMAKE_BUILD_TYPE)

IF(APPLE)
  IF(UNIX)
    SET(PCCE_OSX 1)
  ELSE(UNIX)
    MESSAGE(FATAL_ERROR "Mac OS 9 and earlier is not supported")
  ENDIF(UNIX)
ENDIF(APPLE)

IF(WIN32)
  SET(PCCE_WIN32 1)
ENDIF(WIN32)

IF(PCCE_OSX)
  IF(CMAKE_COMPILER_IS_GNUCXX)
    SET(CMAKE_CXX_FLAGS "-ansi -Wall -no_dead_strip_inits_and_terms")
    SET(CMAKE_CXX_FLAGS_RELEASE "-O2")
    SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
  ENDIF(CMAKE_COMPILER_IS_GNUCXX)
ELSE(PCCE_OSX)
  IF(CMAKE_COMPILER_IS_GNUCXX)
    SET(CMAKE_CXX_FLAGS "-ansi -Wall -Werror")
    SET(CMAKE_CXX_FLAGS_RELEASE "-O2")
    SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
  ENDIF(CMAKE_COMPILER_IS_GNUCXX)
ENDIF(PCCE_OSX)

IF(PCCE_WIN32)
  SET(PCCE_SYS_NAME "win32")
ELSE(PCCE_WIN32)
  IF(PCCE_OSX)
    SET(PCCE_SYS_NAME "osx")
  ELSE(PCCE_OSX)
    SET(PCCE_SYS_NAME "linux")
  ENDIF(PCCE_OSX)
ENDIF(PCCE_WIN32)
IF(NOT PCCE_SYS_NAME)
  MESSAGE(FATAL_ERROR "Could not determine system type")
ENDIF(NOT PCCE_SYS_NAME)

SET(Boost_ADDITIONAL_VERSIONS 1.37)
