#!/usr/bin/python
"""Script to be used in our cmake build system to maintain build numbers.
"""

import os.path
import pickle
import sys
import re

file_build_num_var = '${PCCE_BUILD_NUMBER}'

def inc_build_num(version_dict, version, data_file):
    """Increment the build number in the version_dict and save it to disk"""
    version_dict[version] = version_dict.get(version, 0) + 1
    f = open(data_file, 'w')
    pickle.dump(version_dict, f)
    f.close()

def create_cpp_file(input_file, output_file, version_dict, version):
    """Read the contents of the input file and replace the build number
    token with the actual build number"""
    
    # If this version has not been built before, set build_num to 1
    build_num = version_dict.get(version, 1)
    
    # Copy file and replace file_build_num_var with build_num
    f = open(input_file, 'r')
    contents = f.read()
    f.close()
    
    contents = contents.replace(file_build_num_var, str(build_num))
    f = open(output_file, 'w')
    f.write(contents)
    f.close()

def usage():
    print "Usage: IncBuildNumber.py <Version> [-h] [-i] [-c <Input File> <Output File>]"
    print ""
    print "Version Format:"
    print "  The <Version> argument should be a string formatted like \"0.0.0\"."
    print ""
    print "Flags:"
    print "  -h"
    print "    Print this message and quit."
    print ""
    print "  -i"
    print "    Increments the build number for <Version>.  If this is given with -c,"
    print "    the increment occurs first."
    print ""
    print "  -c"
    print "    Copies the contents of <Input File> to <Output File>, replacing any"
    print "    occurances of \"%s\" with the build number for <Version>." % file_build_num_var
    print ""
    sys.exit(1)

def main():
    """Main function responsible for parsing command line arguments"""
    data_file = 'buildnum.dat'
    
    arg_count = len(sys.argv);
    if (arg_count < 3) or (arg_count > 6):
        usage()
    should_increment = False
    should_copy = False;
    arg_num = 1
    while arg_num < len(sys.argv):
        arg = sys.argv[arg_num]
        if arg == "-i":
            should_increment = True
        elif arg == '-c':
            should_copy = True
            if arg_num + 2 >= arg_count:
                usage()
            arg_num = arg_num + 1
            input_file = sys.argv[arg_num]
            arg_num = arg_num + 1
            output_file = sys.argv[arg_num]
        elif arg == '-h':
            usage()
        else:
            if arg_num > 1:
                print "Unexpected argument \"%s\".  Use \"IncBuildNumber.py -h\" for help" % arg
                sys.exit(1)
        arg_num = arg_num + 1
    
    version = sys.argv[1]
    if re.search('^\d+\.\d+\.\d+$', version) is None:
        print "Version argument must be like '0.0.0'"
        sys.exit(2)
    
    # version_dict is a version_dictionary from version to build number
    if not os.path.exists(data_file):
        version_dict = {}
    else:
        f = open(data_file, 'r')
        version_dict = pickle.load(f)
        f.close()
    
    if should_increment:
        inc_build_num(version_dict, version, data_file)
    
    if should_copy:
        if not os.path.exists(input_file):
            print "IncBuildNumber.py error: %s does not exist" % input_file
            sys.exit(3) 
        
        create_cpp_file(input_file, output_file, version_dict, version)

if __name__ == "__main__":
    main()
