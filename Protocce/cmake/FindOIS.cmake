# - Locate OIS
#
# OIS (Object-Oriented Input System) is an open-source, cross-platform library
# for handling input.  More info can be found at:
# http://www.wreckedgames.com/
#
# This module defines
#  OIS_LIBRARY
#  OIS_FOUND, if false, do not try to link to Ogre 
#  OIS_INCLUDE_DIR, where to find the headers

FIND_PATH(OIS_INCLUDE_DIR OIS.h
  /usr/local/include/OIS
  /usr/include/OIS
  C:/LibOIS/include
  /Library/Frameworks
  ~/OISv1_0_Mac_SDK/
  )

FIND_LIBRARY(OIS_LIBRARY 
  NAMES OIS
  PATHS
  /usr/local/lib
  /usr/lib
  C:/LibOIS/lib
  /Library/Frameworks
  ~/OISv1_0_Mac_SDK/
  )
SET(OIS_FOUND 0)
IF(OIS_LIBRARY AND OIS_INCLUDE_DIR)
  SET(OIS_FOUND 1)
ENDIF(OIS_LIBRARY AND OIS_INCLUDE_DIR)

IF(NOT OIS_FOUND)
  IF(OIS_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "OIS was not found.")
  ELSE(OIS_FIND_REQUIRED)
    IF(NOT OIS_FIND_QUIETLY)
      MESSAGE(STATUS "OIS was not found.")
    ENDIF(NOT OIS_FIND_QUIETLY)
  ENDIF(OIS_FIND_REQUIRED)
ENDIF(NOT OIS_FOUND)
