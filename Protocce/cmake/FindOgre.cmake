# - Locate OGRE
#
# OGRE is an open-source graphics rendering engine written in C++.  More info
# can be found at:
# http://www.ogre3d.org
#
# This module defines
#  OGRE_LIBRARY
#  OGRE_FOUND, if false, do not try to link to OGRE 
#  OGRE_INCLUDE_DIR, where to find the headers
#  OGRE_INCLUDE_EXTRA_OSX_DIR, one step above the OGRE include dir (for OSX)
#  OGRE_CEGUI_FOUND
#  OGRE_CEGUI_INCLUDE_DIR, where to find OgreCEGUIRenderer.h
#  OGRE_CEGUI_LIBRARY, where to find lib for OgreCEGUIRenderer

SET(OGRE_INCLUDE_SEARCH
  /usr/local/include/OGRE
  /usr/include/OGRE
  C:/OgreSDK/include
  ~/OgreSDK/Dependencies
  )
FIND_PATH(OGRE_INCLUDE_DIR Ogre.h
  ${OGRE_INCLUDE_SEARCH}
  )
IF(APPLE AND UNIX)
  FIND_PATH(OGRE_INCLUDE_EXTRA_OSX_DIR Ogre/Ogre.h
    ${OGRE_INCLUDE_SEARCH}
    )
ELSE(APPLE AND UNIX)
  SET(OGRE_INCLUDE_EXTRA_OSX_DIR ${OGRE_INCLUDE_DIR})
ENDIF(APPLE AND UNIX)
  
FIND_PATH(OGRE_CEGUI_INCLUDE_DIR OgreCEGUIRenderer.h
  /usr/local/include/OGRE
  /usr/include/OGRE
  C:/OgreSDK/include
  C:/OgreSDK/samples/include
  ~/OgreSDK/Samples/include
  )
  
FIND_LIBRARY(OGRE_LIBRARY 
  NAMES OgreMain Ogre
  PATHS
  /usr/local/lib
  /usr/lib
  C:/OgreSDK/lib
  ~/OgreSDK/Dependencies
  )

FIND_LIBRARY(OGRE_CEGUI_LIBRARY 
  NAMES OgreGUIRenderer CEGUIOgreRenderer OgreCEGUIRenderer
  PATHS
  /usr/local/lib
  /usr/lib
  C:/OgreSDK/lib
  ~/OgreSDK/Dependencies
  ~/OgreSDK/build
  ~/OgreSDK/build/Release
  )

SET(OGRE_FOUND 0)
IF(OGRE_LIBRARY AND OGRE_INCLUDE_DIR)
  SET(OGRE_FOUND 1)
ENDIF(OGRE_LIBRARY AND OGRE_INCLUDE_DIR)

SET(OGRE_CEGUI_FOUND 0)
IF(OGRE_CEGUI_LIBRARY AND OGRE_CEGUI_INCLUDE_DIR)
  SET(OGRE_CEGUI_FOUND 1)
ENDIF(OGRE_CEGUI_LIBRARY AND OGRE_CEGUI_INCLUDE_DIR)

IF(NOT OGRE_FOUND)
  IF(OGRE_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "OGRE was not found.")
  ELSE(OGRE_FIND_REQUIRED)
    IF(NOT OGRE_FIND_QUIETLY)
      MESSAGE(STATUS "OGRE was not found.")
    ENDIF(NOT OGRE_FIND_QUIETLY)
  ENDIF(OGRE_FIND_REQUIRED)
ENDIF(NOT OGRE_FOUND)
