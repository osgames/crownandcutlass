/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Process List header

#ifndef _PCCE_PROCESSLIST_H_
#define _PCCE_PROCESSLIST_H_

#include <boost/shared_ptr.hpp>
#include "util.h"
#include "IProcess.h"

namespace pcce {
  
  class ProcessList {
  public:
    ProcessList();
    ~ProcessList();
    
    void AddProcess(pcce::tIProcessPtr process);
    
    void ExecuteProcesses(tMillisecond timeDelta, tMillisecond maxTime);
    void ExecuteAllProcesses(tMillisecond timeDelta);
    void ExecuteOneProcess(tMillisecond timeDelta);
    
    unsigned int GetProcessCount();
    
  private:
    class ProcessListItem;
    
    ProcessListItem* mCurrent;
    ProcessListItem* mPrevious;
    ProcessListItem* mLast;
    
    unsigned int mCount;
    
    void DoHandleCurrentItem(tMillisecond timeDelta);
    void DoDeleteCurrentItem();
  };
  
  typedef boost::shared_ptr< ProcessList > tProcessListPtr;
    
}

#endif
