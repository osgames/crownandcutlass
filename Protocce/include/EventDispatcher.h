/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Event Dispatcher class

#ifndef _PCCE_EVENTDISPATCHER_H_
#define _PCCE_EVENTDISPATCHER_H_

#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/signals/signal1.hpp>
#include <boost/thread/mutex.hpp>
#include "IEventFwd.h"
#include "EventDispatcherFwd.h"

namespace pcce {

  class EventDispatcher {
  public:
    EventDispatcher();
    ~EventDispatcher();
    
    /// Registers a tEventHandler to receive a certain type of events
    /**
     * Registers a tEventHandler function to receive certain types of events.
     * The handler will be unregistered for the event 
     */
    tEventConnection AddHandler(const tHashValue eventType, tEventHandler handler);

    /// Dispatches a single event immediately
    void DispatchEvent(tIEventPtr event);
    
    /// Dispatches a list of events immediately
    void DispatchEvents(const tIEventPtrList& events);

    /// Removes unused signals from map
    void RemoveInactiveEventTypes();
    
    /// Returns number of event types with active handlers in the map
    /**
     *  Returns the number of event types that have active handlers.
     *  Note: This is the equivalent of calling RemoveInactiveEventTypes()
     *  followed by EventTypeCount().
     */
    unsigned int ActiveEventTypeCount();
    
    /// Returns number of event types with a signal in the map
    unsigned int EventTypeCount();
    
  private:
    typedef boost::signal1< void, tIEventPtr > tEventSignal;
    typedef boost::shared_ptr< tEventSignal > tEventSignalPtr;
    typedef std::map< tHashValue, tEventSignalPtr > tEventSignalPtrMap;
    typedef std::pair < const tHashValue, tEventSignalPtr > tEventSignalPtrMapPair;

    boost::mutex mMutex;
    tEventSignalPtrMap mMap;

    tEventSignalPtr Signal(const tHashValue eventType);
    void DoCleanSignals(bool skipAllEventsType);
  };

}

#endif
