/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Input Translator Class

#ifndef _PCCE_INPUTTRANSLATOR_H_
#define _PCCE_INPUTTRANSLATOR_H_

#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>
#include <OIS.h>
#include "IEventFwd.h"

namespace pcce {
  
  typedef enum { ksUp, ksDown } tKeyState;
  
  typedef boost::function< tIEventPtr (const OIS::KeyCode KeyCode, const tKeyState KeyState) > tKeyInputToEvent;
  typedef boost::function< tIEventPtr (const OIS::MouseState &ms) > tMouseMoveToEvent;
  typedef boost::function< tIEventPtr (const OIS::MouseState &ms, 
    const OIS::MouseButtonID mbid, const tKeyState state) > tMouseClickToEvent;

  class InputTranslator {
  public:
    void AddKeyHandler(const OIS::KeyCode kc, const tKeyState state, const tKeyInputToEvent InputFunction);
    void RemoveKeyHandler(const OIS::KeyCode kc, const tKeyState state);

    void AddMouseMoveHandler(tMouseMoveToEvent InputFunction);
    void RemoveMouseMoveHandler();
    void AddMouseClickHandler(const OIS::MouseButtonID mbid, const tMouseClickToEvent InputFunction);
    void RemoveMouseClickHandler(const OIS::MouseButtonID mbid);
    
    bool ReceiveMouseMoveInput(const OIS::MouseState &ms);
    bool ReceiveMouseButtonInput(const OIS::MouseState &ms, const OIS::MouseButtonID mbid, const tKeyState state);
    bool ReceiveKeyboardInput(const OIS::KeyCode kc, const tKeyState state);
    
  private:
    typedef std::map< std::pair<OIS::KeyCode, tKeyState>, tKeyInputToEvent > tKeyMap;
    typedef std::map< OIS::MouseButtonID, tMouseClickToEvent > tMouseClickMap;
    tKeyMap mKeyMap;
    tMouseMoveToEvent mMouseMoveEvent;
    tMouseClickMap mMouseClickMap;
  };
  typedef boost::shared_ptr< InputTranslator > tInputTranslatorPtr;
  
  void SetActiveInputTranslator(const tInputTranslatorPtr& translator);
  
  template< class T > tIEventPtr CreateEvent() {
    BOOST_STATIC_ASSERT(
      (boost::is_class< T >::value) && (boost::is_base_of< pcce::IEvent, T >::value));
    tIEventPtr result(new T());
    return result;
  }
  
}
#endif 
