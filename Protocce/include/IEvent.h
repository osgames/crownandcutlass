/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Base Event class

#ifndef _PCCE_IEVENT_H_
#define _PCCE_IEVENT_H_

#include <string>
#include <boost/algorithm/string/case_conv.hpp>
#include "EventSystem.h"
#include "stringutil.h"
#include "IEventFwd.h"

namespace pcce {

  /// Base event class
  /**
   *  This class provides a base for all events.  Events provide the means
   *  for communication between different systems and processes in the engine
   *  code as well as the game logic.  Creating a custom event class is somewhat
   *  unusual, although quite easy.  Custom events must descend from EventType,
   *  which allows us to simulate a "virtual static" method.  It is recommended
   *  that you use the PCCE_EVENT_CLASS macro to declare custom events.
   *  \sa EventType helper class for "virtual static" workaround
   *  \sa PCCE_EVENT_CLASS macro to declare custom events 
   */
  class IEvent {
  public:
    /// Returns an event's type name (for debugging only)
    /**
     *  This method returns the event type name.  That value is only used for
     *  determining the "event type" hash value (see vGetEventType()) and
     *  debugging.  As a result, for the sake of simplicity
     *  it returns a full string, not a reference.
     *  Note: This is implemented in the EventType template, so custom event
     *  classes should not implement it.
     */
    virtual const std::string vGetEventName() const = 0;

    /// Returns an event's type
    /**
     *  This method returns the event type.  That value is calculated by hasing
     *  the event name (see vGetEventName()).  That allows us to avoid a single
     *  event type enumeration, use integers intead of strings for event
     *  listener lookups, and still have a name for debugging.
     *  Note: This is implemented in the EventType template, so custom event
     *  classes should not implement it.
     */
    virtual const tHashValue vGetEventType() = 0;

    virtual ~IEvent() {};
  };

  /// Event type template
  /**
   *  All custom event classes should derive from a class generated by this
   *  template.  This template allows us to have "virtual static" methods.
   *  \sa IEvent for more detailed infomation.
   *  \sa PCCE_EVENT_CLASS for an easy way to declare a custom event
   */ 
  template<class T> class EventType: public IEvent {
  private:
    static tHashValue sHash;
  public:
    /// Static method to get an event's type
    /**
     *  This static method allows a listener to register for an event without
     *  having to actually create an instance of that event.  See IEvent for
     *  more information.
     */
    static const tHashValue sGetEventType() {
      if (sHash == 0) {
        pcce::tHashValue hash;
        std::string s = boost::to_lower_copy(T::sGetEventName());
        PCCE_CHECK(!s.empty(), "Invalid event name");
        hash = HashString(s);
        PCCE_CHECK(
          EventSystemSingleton::Get()->RegisterEventType(hash),
          "Event type duplicated (\"" + s + "\")");
        sHash = hash;
      }
      return sHash;
    }
    const std::string vGetEventName() const {
      return T::sGetEventName();
    }
    const tHashValue vGetEventType() {
      return sGetEventType();
    }
  };

  template<class T> tHashValue EventType<T>::sHash = 0;
  
  /// Macro to properly define an event class
  /**
   *  This macro declares a class "name" that is derived from
   *  "EventType< name >".  All custom events must inherit from EventType.
   *  This macro makes that easier.
   *  \sa pcce::IEvent for more detailed information
   *  \sa pcce::EventType
   */
  #define PCCE_EVENT_CLASS(name) class name: public pcce::EventType< name > 

}

#endif
