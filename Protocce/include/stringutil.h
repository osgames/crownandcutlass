/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// String utility functions

#ifndef _PCCE_STRINGUTIL_H_
#define _PCCE_STRINGUTIL_H_

#include <string>
#include "util.h"
#include "VariantFwd.h"

namespace pcce {
  
  tHashValue HashString(const std::string& s);
  
  /// Very basic format function
  /**
   *  This function provides basic format functionality.  It takes a format
   *  string and a tVariantList (std::list of Variant).  Any '%' characters
   *  in the format string are replaced with the corresponding Variant value as
   *  as string.  You can escape the '%' using a '|'.  If you want a '|' in
   *  the final output, you must use "||".  Use of a single '|' with any other
   *  character will result in an exception.  Extra values are ignored (e.g.
   *  passing 3 Variants with a format string with 1 '%'), while passing too
   *  few values results in an exception.
   *
   *  Note: '|' is used instead of '\' to avoid confusing due to C++ treating
   *  '\' as an escape character in strings.  If we used '\', the string
   *  "% Test\\ %\%" would be written in code as "% Test\\\\ %\\%".  That just
   *  seems ugly.  We are already doing unusual things with our Format, so a
   *  different escape character doesn't seem like that big of an issue.
   *
   *  For example, Format("% Test|| %|%", [ "ABC", 98, 0 ]) will return
   *  "ABC Test| 98%".
   */
  std::string Format(const std::string& formatStr, const tVariantList values); 

  /// Return newline for the current platform
  std::string GetNL();
  
}

#endif
