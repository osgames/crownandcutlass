/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Property Bag Class


#ifndef _PCCE_PROPERTYBAG_H_
#define _PCCE_PROPERTYBAG_H_

#include <string>
#include <map>
#include <boost/shared_ptr.hpp>
#include "config.h"
#include "VariantFwd.h"

namespace pcce {

  class PropertyBag;
  typedef boost::shared_ptr< PropertyBag > tPropertyBagPtr;

  class PropertyBag {
  public:
    PropertyBag();
    virtual ~PropertyBag();

    void AddProperty(const std::string& name);
    void RemoveProperty(const std::string& name);
    bool HasProperty(const std::string& name) const;
    unsigned int GetPropertyCount() const;

    Variant GetValue(const std::string& name) const;
    Variant GetValueDef(const std::string& name, Variant defaultValue) const;
    bool HasValue(const std::string& name) const;
    void SetValue(const std::string& name, Variant value);
    void ClearValue(const std::string& name);

    int GetValueAsInt(const std::string& name) const;
    bool GetValueAsBool(const std::string& name) const;
    unsigned int GetValueAsUnsignedInt(const std::string& name) const;
    long int GetValueAsLongInt(const std::string& name) const;
    unsigned long int GetValueAsUnsignedLongInt(const std::string& name) const;
    tReal GetValueAsReal(const std::string& name) const;
    char GetValueAsChar(const std::string& name) const;
    unsigned char GetValueAsUnsignedChar(const std::string& name) const;
    std::string GetValueAsString(const std::string& name) const;
    tPropertyBagPtr GetValueAsPropertyBagPtr(const std::string& name) const;
       
  private:
    typedef std::map< std::string, Variant > tPropertyMap;
    typedef std::pair < std::string, Variant > tPropertyMapPair;

    tPropertyMap mPropMap;
  public:
    
    class const_iterator {
    public:
      const_iterator(tPropertyMap::const_iterator iter);
      ~const_iterator();
      
      // Assignment and relational operators
      const_iterator& operator=(const const_iterator& other);
      
      bool operator==(const const_iterator& other);
      
      bool operator!=(const const_iterator& other);
      
      // Update state so we go to the next element
      const_iterator& operator++();
      
      const std::string& first();
      
      const Variant& second();
      
  	private:
  	  tPropertyMap::const_iterator mMapIter;
  	};
  	
  	const_iterator begin() const;
  	
  	const_iterator end() const;
  };
}

#endif
