/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// General Exception class

#ifndef _PCCE_EXCEPTION_H_
#define _PCCE_EXCEPTION_H_

// Carbon defines check!  Ugh...
#ifdef check
#undef check
#endif

#include <string>
#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>

namespace pcce {

  /// Base class for all Protocce exceptions
  class Exception {
  public:
    /// Constructor for Exception
    /**
     *  This is the constructor for an Exception.  Generally, you should
     *  use the macro #PCCE_THROW to automatically set the fileName and lineNumber.
     *  \param error Description of the error
     *  \param fileName Name of the file where the error occurred
     *  \param lineNumber Line number where the error occurred
     *  \sa PCCE_THROW()
     */
    Exception(const std::string &error, const std::string &fileName, const unsigned int lineNumber);

    /// Copy constructor for an Exception
    Exception(const Exception& copy);

    /// Assignment operator for an Exception
    Exception& operator=(const Exception& rhs);

    /// Returns the error string
    /**
     *  Note to get the file name, line number, and description of the error,
     *  use GetFullError().
     *  \return Description of the error
     */
    const std::string& GetError() const;

    /// Returns the file name
    /**
     *  Note to get the file name, line number, and description of the error,
     *  use GetFullError().
     *  \return File name where the error occurred
     */
    const std::string& GetFileName() const;

    /// Returns the line number
    /**
     *  Note to get the file name, line number, and description of the error,
     *  use GetFullError().
     *  \return Line number where the error occurred
     */
    const unsigned int& GetLineNumber() const;

    // "FILE (LINE): ERROR"
    /// Returns the file name, line number, and description of the error
    /**
     *  Returns the file name, line number, and description of the error in
     *  the following format: "FILE (LINE): ERROR".  For example, an error on
     *  line 10 of main.cpp might look like "main.cpp (10): An error has occurred".
     *  \return Full error string
     */
    const std::string& GetFullError() const;
  protected:
    std::string mError;
    std::string mFileName;
    unsigned int mLineNumber;
  private:
    mutable std::string mFullError;
    mutable bool mIsFullErrorSet;
  };

  /// This function throws an exception if the condition is false
  template< class T >
  void check(bool condition, const std::string& error, const std::string &fileName, const unsigned int lineNumber) {
    BOOST_STATIC_ASSERT(
      (boost::is_class< T >::value) && ((boost::is_same< pcce::Exception, T >::value) || (boost::is_base_of< pcce::Exception, T >::value)));
    if (!condition) {
      throw(T(error, fileName, lineNumber));
    }
  }
  
  /** \def PCCE_THROW(error)
   *  \brief Macro to throw an Exception
   * 
   *  This macro takes care of throwing an exception and setting the file name and
   *  line number.
   *  \param error Description of the error
   *  \sa pcce::Exception
   */
  #define PCCE_THROW(error) throw(pcce::Exception((error), __FILE__, __LINE__))
  
  /** \def PCCE_CHECK(cond, error)
   *  \brief Macro to check a condition and throw an exception if it is false
   * 
   *  This macro takes care of calling check() and setting the file name and
   *  line number.
   *  \param cond Boolean whether the condition was met or not
   *  \param error Description of the error to throw if cond is false
   *  \sa pcce::Exception pcce::check
   */
  #define PCCE_CHECK(cond, error) pcce::check< pcce::Exception >((cond), (error), __FILE__, __LINE__)

}

#endif
