/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Math functions

#ifndef _PCCE_MATHUTIL_H_
#define _PCCE_MATHUTIL_H_

#include <cmath>
#include "config.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace pcce {
  
  const tReal Pi = static_cast< tReal>(M_PI);
  const tReal TwoPi = static_cast< tReal>(2 * M_PI);
  const tReal PiDiv2 = static_cast< tReal>(M_PI / 2);

  typedef enum { crLess, crEqual, crGreater } tCompareResult;

  tCompareResult CompareReal(const tReal& x, const tReal& y);
  bool SameReal(const tReal& x, const tReal& y);
  
  tReal RoundTo(const tReal& x, unsigned short digits);

  tReal Truncate(const tReal& x);

  bool IsNaN(const tReal& x);
  bool IsInf(const tReal& x);
  
  template< typename T > tCompareResult Compare(const T& a, const T& b) {
    if (a > b) {
      return crGreater;
    } else if (a < b) {
      return crLess;
    } else {
      return crEqual;
    }
  }
  
  template< typename T > tCompareResult Sign(const T& a) {
    return Compare(a, static_cast< T >(0));
  }
}

#endif
