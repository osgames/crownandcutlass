/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Game Object Class

#ifndef _PCCE_GAMEOBJECT_H_
#define _PCCE_GAMEOBJECT_H_

#include <boost/shared_ptr.hpp>
#include <OgrePrerequisites.h>
#include "PropertyBag.h"

namespace pcce {

  typedef unsigned int tGameObjectID;

  const tGameObjectID InvalidGameObjectID = 0;

  /// GameObjects are used to hold any game related info the engine user wants.
  class GameObject: public PropertyBag {
  public:
    /// Do not construct a game object directly.  Use GameObjectDB.NewObject() instead.
    GameObject(tGameObjectID id);
    ~GameObject();
    
    bool IsRenderReady() const;
    bool IsPhysicsReady() const;

    std::string GetMaterialName() const;
    std::string GetRenderModelName() const;
    std::string GetPhysicsModelName() const;
    
    void SetMaterialName(const std::string& materialName);
    void SetRenderModelName(const std::string& renderModelName);
    void SetPhysicsModelName(const std::string& physicsModelName);

    tGameObjectID GetID();

    bool HasEntity();
    Ogre::Entity* GetEntity();
    void SetEntity(Ogre::Entity* entity);
    void ClearEntity();
    
  private:
    tGameObjectID mID;
    Ogre::Entity* mEntity;
  };
  
  typedef boost::shared_ptr< GameObject > tGameObjectPtr;

  const std::string kMaterialNameProperty = "MaterialName";
  const std::string kRenderModelNameProperty = "RenderModelName";
  const std::string kPhysicsModelNameProperty = "PhysicsModelName";

}

#endif
