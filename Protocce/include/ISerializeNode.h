/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/
 
// Serialize Node Interface

#ifndef _PCCE_ISERIALIZENODE_H_
#define _PCCE_ISERIALIZENODE_H_

#include <boost/smart_ptr.hpp>
#include <string>
//#include "ISerializer.h"

namespace pcce {
  
  class Variant;
  class ISerializeNode;  
  typedef boost::shared_ptr< ISerializeNode > tISerializeNodePtr;

  /// Base Serialize Node
  /**
   * The Serialize Node is a structure which can be passed to a class to be
   * serialized. It can be read from, or written to, but this is done by
   * the ISerializer. Each type of serializer should have two Serialize Node
   * types. One for input, and one for output. In this way the classes can 
   * have a single Serialize function for both input and output, in a similar
   * style to the Boost::Serialization library. But this way we don't need
   * to depend on streams.
   */ 
  class ISerializeNode {
  public:
    virtual ~ISerializeNode() {};
    
    /// Variant Serialization
    /**
     * Called by ISerializable, it is specific for each
     * ISerializer implementation and guarantees that it will be 
     * done the same across the serialization.
     */
    virtual tISerializeNodePtr SerializeVariant(const std::string name, Variant v) = 0;
    /// Type Serialization
    /**
     * This gives you an enclosure of the correct type to put
     * other items in if you need
     */
    virtual tISerializeNodePtr SerializeType(const std::string type, const std::string value) = 0;
    
    /// Node Value
    /**
     * Returns the string value of the node.
     */
    virtual Variant Value() = 0; 

  };

}

#endif /* _PCCE_ISERIALIZENODE_H_ */
