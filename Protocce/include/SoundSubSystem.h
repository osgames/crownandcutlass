/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#ifndef _PCCE_SOUNDSUBSYSTEM_H_
#define _PCCE_SOUNDSUBSYSTEM_H_

#include <deque>
#include <set>
#include <list>
#include <map>
#include <boost/shared_ptr.hpp>
#include "OpenALSource.h"
#include "ISound.h"
#include "OggStream.h"
#include "IThreadedSubSystem.h"

namespace pcce {
  
  class SoundSubSystem: public IThreadedSubSystem {
  public:
    virtual const std::string vGetSubSystemName() { return "Sound"; };
    
    void HandleBufferEvent(const tIEventPtr& e);
    void HandleStreamEvent(const tIEventPtr& e);

  protected:
    void vInitializeThread(tPropertyBagPtr configSection);
    void vRunThread();
    void vShutdownThread();

  private:
    class BufferHandle {
    public:
      BufferHandle(tSoundID id, tOpenALSourcePtr source):
        mID(id),
        mSource(source)
      {
      }
      tSoundID mID;
      tOpenALSourcePtr mSource;
      
      bool operator==(const BufferHandle& rhs) {
        return mID == rhs.mID;
      }
    };
    typedef std::set< tOpenALSourcePtr > tOpenALSourcePtrSet;
    typedef std::deque< tOpenALSourcePtr > tOpenALSourcePtrDeque;
    typedef std::list< BufferHandle > tBufferHandleList;
    typedef std::map< tSoundID, tOpenALSourcePtr > tSoundIDSourceMap;
    typedef std::map< tSoundID, tOggStreamPtr > tStreamMap;

    tOpenALSourcePtrSet mSources;
    tOpenALSourcePtrDeque mAvailSources;
    tBufferHandleList mPlayingBuffers;
    tSoundIDSourceMap mBufferSourceMap;
    
    tStreamMap mPlayingStreams;
    
    tOpenALSourcePtr GetBufferSource(const tSoundID id, bool& newSource);
    bool HasSource(const tSoundID id, tOpenALSourcePtr& source);
    tOpenALSourcePtr AcquireBufferSource(const tSoundID id);
    void ReleaseSource(const tSoundID id);
    
    tOpenALSourcePtr DoAcquireSource();
    
    bool IsStreamActive(const tSoundID id, tOggStreamPtr& stream);
    void ReleaseStream(const tSoundID id);
    void UpdateStreams();
    
    void SanityCheck();
  };
  
}

#endif
