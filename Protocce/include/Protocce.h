/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Main header

#ifndef _PCCE_PROTOCCE_H_
#define _PCCE_PROTOCCE_H_

#include <string>
#include <stack>
#include <boost/shared_ptr.hpp>
#include "Singleton.h"
#include "Configuration.h"
#include "Log.h"
#include "EventDispatcherFwd.h"
#include "EventSystem.h"
#include "ISubSystem.h"
#include "GameObjectDB.h"
#include "ResourceManager.h"
#include "IGameStateFwd.h"
#include "TimerFwd.h"

/// Highest-level namespace for the %Protocce engine
/**
 *  This is the namespace that all %Protocce code belongs to.  Certain
 *  sub-systems will have nested namespaces to help distinguish them,
 *  but everything will belong to the "pcce" namespace.
 */
namespace pcce {

  class Log;

  /// Main class to wrap up the engine
  /**
   *  This is the main class that is used for startup and shutdown of the
   *  engine components.  It is the engine user's primary contact point with
   *  the engine.
   */
  class Protocce {
  public:
    Protocce(tConfigurationPtr config);
    ~Protocce();
    
    void Initialize(const tISubSystemPtrList& subSystems);
    void Shutdown();

    void PushState(tIGameStatePtr state);
    void PopState();

    void RunGame();

    tConfigurationPtr GetConfiguration();
    tTimerPtr GetTimer();
  private:
    typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
    typedef ScopedSingleton< GameObjectDBSingleton > GameObjectDBManager;
    typedef ScopedSingleton< ResourceManagerSingleton > ResManager;
    typedef ScopedSingleton< LogSingleton > LogManager;
    typedef std::stack< tIGameStatePtr > tIGameStateStack;
    
    tIGameStateStack mStateStack;
    
    tConfigurationPtr mConfig;
    
    tTimerPtr mTimer;

    tEventConnection mThreadExceptionConnection;
    
    tISubSystemPtrList mSubSystems;
    
    bool mInitialized;
    
    EventSystemManager mEventSystemManager;
    GameObjectDBManager mGameObjectDBManager;
    ResManager mResManager;
    LogManager mLogManager;
    
    void DoClearStateStack();
  };
}

/**
 *  \mainpage Protocce Documentation
 *  \section intro_sec Introduction
 *  This documentation is a work in progress primarily intended to help
 *  the developers navigate through the code during discussions.  As we
 *  stabilize the engine, we will transition this document to assist
 *  users of the engine.
 */

#endif
