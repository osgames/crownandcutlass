/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#ifndef _PCCE_XMLNODE_H_
#define _PCCE_XMLNODE_H_

#include <string>
#include <list>
#include "PropertyBag.h"

class TiXmlElement;

namespace pcce {

  class Variant;
  class XmlDoc;

  class XmlNode {
  public:
    XmlNode(XmlDoc* doc, TiXmlElement* element);

    const std::string GetName() const;

    XmlNode* GetNextSibling() const;
    XmlNode* GetNextSibling(const std::string& nodeName) const;

    XmlNode* GetPrevSibling() const;
    XmlNode* GetPrevSibling(const std::string& nodeName) const;

    XmlNode* GetFirstChild() const;
    XmlNode* GetFirstChild(const std::string& nodeName) const;

    XmlNode* CreateChild(const std::string& nodeName);

    XmlNode* GetParent() const;

    const Variant GetAttribute(const std::string& name) const;
    void SetAttribute(const std::string& name, Variant value);
    const tPropertyBagPtr& GetAttributes() const;

    friend inline bool operator==(const XmlNode& a, const XmlNode& b);
    friend inline bool operator!=(const XmlNode& a, const XmlNode& b);
    friend inline bool operator==(const XmlNode& a, const void* b);
    friend inline bool operator!=(const XmlNode& a, const void* b);

  private:
    XmlDoc* mDoc;
    TiXmlElement* mElement;
    tPropertyBagPtr mAttributes;

    void BuildAttributes();
  };

  inline bool operator==(const XmlNode& a, const XmlNode& b) {
    return a.mElement == b.mElement;
  }

  inline bool operator!=(const XmlNode& a, const XmlNode& b) {
    return a.mElement != b.mElement;
  }

  inline bool operator==(const XmlNode& a, const void* b) {
    return a.mElement == b;
  }

  inline bool operator!=(const XmlNode& a, const void* b) {
    return a.mElement != b;
  }

}

#endif
