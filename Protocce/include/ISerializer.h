/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *                                      *
 ******************************************************************************/
 
 // Serializer Interface

#ifndef _PCCE_ISERIALIZER_H_
#define _PCCE_ISERIALIZER_H_

#include <map>
#include <string>
#include <list>
#include <boost/function.hpp>
#include <boost/smart_ptr.hpp>
#include "IResource.h"
#include "ISerializeNode.h"

namespace pcce {  
  typedef boost::function< void () > tStaticLoadFunction;
  typedef boost::function< void (tISerializeNodePtr Node) > tRegisterSaveFunction;   

  /// Base Serializer Interface
  /**
   * This is the core interface of the serialization system.
   * 
   */ 
  class ISerializer {
  public:
    virtual ~ISerializer() {};
    
    void RegisterStaticConstructor(const std::string Type, const tStaticLoadFunction StaticLoadFunction);
    void RegisterAutomaticSaveFunction(const tRegisterSaveFunction SaveFunction);

    /// Virtual Save function.
    /**
     * Implemented by the child class, it will start off the
     * save serialization process.
     */
    virtual bool Save(const std::string &Location) = 0;
    /// Virtual Save function.
    /**
     * Implemented by the child class, it will load
     * the resource and create all the objects necessary
     * for the state.
     */
    virtual bool Load(const std::string &Location) = 0;
    
  protected:
    std::map< std::string, tStaticLoadFunction > mLoadFunctionMap;
    std::list< tRegisterSaveFunction > mSaveFunctionList;
  };	
}

#endif /*_PCCE_ISERIALIZER_H_*/
