/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Timer class

#ifndef _PCCE_TIMER_H_
#define _PCCE_TIMER_H_

#include "config.h"
#include <map>
#include <boost/thread/recursive_mutex.hpp>
#include "util.h"
#include "TimerFwd.h"

namespace pcce {

  /// Unique ID for custom timers
  /**
   *  This type is used to uniquely identify custom timers.  It is used for all
   *  custom timer methods.
   *  \sa Timer::StartCustomTimer()
   *  \sa Timer::GetCustomTimerStartTime()
   *  \sa Timer::CheckCustomTimer()
   *  \sa Timer::EndCustomTimer()
   *  \sa Timer::ResetCustomTimer()
   *  \sa Timer::IsCustomTimerActive()
   */
  typedef unsigned int tTimerKey;
  
  /// %Timer class
  /**
   *  This class provides all of the engine's timing functionality.  It allows
   *  the user to see the number of milliseconds that have passed since the
   *  timer was started (which happens when the engine is initialized), measures
   *  frame rate-related timing info, and allows the user to set custom timers.
   *  A custom timer allows the user to have basically a stopwatch.  The user
   *  can start a custom timer, check how long it has been since a custom timer
   *  was started, reset the starting time for a custom timer, or stop a custom
   *  timer.  Note: Attempting to using an invalid custom timer key will raise
   *  an exception.
   *  \sa GetTimeSinceStart()
   *      for a note about maximum amount of time that can be measured using
   *      this class
   *  \sa StartCustomTimer()
   *  \sa GetCustomTimerStartTime()
   *  \sa CheckCustomTimer()
   *  \sa EndCustomTimer()
   *  \sa ResetCustomTimer()
   *  \sa IsCustomTimerActive()
   */
  class Timer {
  public:
    Timer();
    ~Timer();

    // Returns time since last frame
    tMillisecond MarkFrameStart();
    tMillisecond GetTimeSinceFrameStart();
    tMillisecond GetTimeBetweenFrameStarts();

    tTimerKey StartCustomTimer();
    // Returns time that a custom timer started
    tMillisecond GetCustomTimerStartTime(const tTimerKey key);
    // Returns time since timer started
    tMillisecond CheckCustomTimer(const tTimerKey key);
    // Returns time since timer started
    tMillisecond EndCustomTimer(const tTimerKey key);
    // Returns time since timer started and restarts timer
    tMillisecond ResetCustomTimer(const tTimerKey key);
    bool IsCustomTimerActive(const tTimerKey key);
  private:
    typedef std::map< tTimerKey, tMillisecond > tTimerMap;
    
    boost::recursive_mutex mMapMutex;

    tTimerMap mMap;
    tTimerKey mNextTimerKey;
    tTimerKey mFrameKey;

    tMillisecond mLastFrameStart;
    };

}

#endif

