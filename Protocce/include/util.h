/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Utility functions

#ifndef _PCCE_UTIL_H_
#define _PCCE_UTIL_H_

#include <cstddef>

namespace pcce {

  typedef std::size_t tHashValue;

  /// Unit of time in milliseconds
  typedef unsigned long tMillisecond;

  const tMillisecond msPerS = 1000;
  const tMillisecond usPerMs = 1000;

  /// Cross-platform sleep function
  /**
   *  This function is a cross-platform way to sleep for at least the specified
   *  number of milliseconds.  Note: It may sleep longer than the specified 
   *  amount of time.  The value is only a minimum.
   *  \param time Minimum number of milliseconds to sleep
   */
  void SleepMs(tMillisecond time);


  /// Returns the time in milliseconds since the application was started.
  /**
   *  This function returns the time in milliseconds since the application
   *  was started.
   *  Note: On most systems (32-bit at least), this value will wrap after
   *  approximately 49.7 days.  That is currently a limitation of our engine.
   */
  tMillisecond GetTimeSinceStart();
  
}

#endif
