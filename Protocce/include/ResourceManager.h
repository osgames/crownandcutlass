/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#ifndef _PCCE_RESOURCEMANAGER_H_
#define _PCCE_RESOURCEMANAGER_H_

#include <string>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include "Singleton.h"
#include "IResource.h"

namespace pcce {

  class IResourceFactory;
  typedef boost::shared_ptr< IResourceFactory > tIResourceFactoryPtr;

  class ResourceManager: public boost::noncopyable {
  public:
    ResourceManager();
    ~ResourceManager();

    tIResourcePtr GetResource(const std::string& name, bool shouldLoad = true);

    void LoadResource(const std::string& name);
    void LoadResourceGroup(const std::string& group);
    void UnloadResource(const std::string& name);
    void UnloadResourceGroup(const std::string& group);

    void CreateResourcesFromFile(const std::string& fileName);
    tIResourcePtr CreateResource(
      const std::string& resourceType, const std::string& name, const std::string& group);

    void DestroyResource(const std::string& name);
    void DestroyResourceGroup(const std::string& group);

    void RegisterResourceFactory(const std::string& resourceType, const tIResourceFactoryPtr factory);
    void UnregisterResourceFactory(const std::string& resourceType);

    void SanityCheck();

  private:
    typedef std::map< std::string, tIResourcePtr > tIResourceMap;
    typedef std::multimap< std::string, tIResourcePtr > tIResourceMultiMap;
    typedef std::pair< std::string, tIResourcePtr > tIResourceMapPair;

    typedef std::map< std::string, tIResourceFactoryPtr > tIResourceFactoryMap;
    typedef std::pair< std::string, tIResourceFactoryPtr > tIResourceFactoryMapPair;

    tIResourceMap mResourceNameMap;
    tIResourceMultiMap mResourceGroupMap;

    tIResourceFactoryMap mFactoryMap;

    boost::mutex mMutex;

    tIResourceFactoryPtr DoGetFactory(const std::string& resourceType);
    void DoInsertResource(const tIResourcePtr resource, const std::string& group);

    void DoSanityCheck();
  };

  typedef pcce::Singleton< ResourceManager > ResourceManagerSingleton;

}

#endif
