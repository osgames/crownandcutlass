/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Base Process class

#ifndef _PCCE_IPROCESS_H_
#define _PCCE_IPROCESS_H_

#include <list>
#include <boost/shared_ptr.hpp>
#include "util.h"

namespace pcce {
  
  class IProcess;
    
  typedef boost::shared_ptr< IProcess > tIProcessPtr;
  typedef std::list< pcce::tIProcessPtr > tIProcessPtrList;

  /// Base process class
  /**
   *  This class provides a base for all processes.  A process is a game-state
   *  specific bit of game logic.  It will get updated once every frame.
   */
  class IProcess {
  public:
    IProcess();
    
    virtual ~IProcess() { };
    /// Virtual update method
    /**
     *  This method gets called once a frame.  As it is a pure virtual method
     *  it must be implemented.
     */
    virtual void vUpdate(const tMillisecond timeDelta) = 0;
    
    void Kill();
    void UnKill();
    bool IsKilled();
    
    tIProcessPtr GetNext();
    void SetNext(tIProcessPtr next);
    void ResetNext();
    
  private:
    tIProcessPtr mNext;
    bool mKilled;
  };

}

#endif
