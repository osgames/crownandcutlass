/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#ifndef _PCCE_OGGSTREAM_H_
#define _PCCE_OGGSTREAM_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include <al.h>
#include "OpenALSource.h"
#include "OggFile.h"

namespace pcce {
  
  class OggStream {
  public:
    OggStream(const std::string fileName, tOpenALSourcePtr source);
    ~OggStream();
    
    bool IsPlaying();
    
    void Play();
    void Pause();
    void Stop();
    
    void Update();
    
    tOpenALSourcePtr GetSource();
    void SetSource(tOpenALSourcePtr source);
    
  private:
    static const size_t sBufferCount = 2;
    typedef enum { ssPlaying, ssPaused, ssStopped } tStreamState;
    
    tStreamState mState;
    tOpenALSourcePtr mSource;
    OggFile mFile;
    ALenum mFormat;
    ALsizei mFreq;
    ALuint mBuffers[sBufferCount];
    unsigned int mQueuedBuffers;
    unsigned int mProcessedBuffers;
    bool mStreamActive;

    unsigned int QueuedBufferCount();
    unsigned int ProcessedBufferCount();
    
    bool FillBuffer(ALuint& buffer);
    void QueueBuffer(const ALuint& buffer);
    
    unsigned int UnqueueProcessedBuffers(ALuint buffers[]);
  };
  
  typedef boost::shared_ptr< OggStream > tOggStreamPtr;
  
}

#endif
