/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Threaded sub-system base class

#ifndef _PCCE_ITHREADEDSUBSYSTEM_H_
#define _PCCE_ITHREADEDSUBSYSTEM_H_

#include <string>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include "IEventFwd.h"
#include "util.h"
#include "EventDispatcherFwd.h"
#include "ISubSystem.h"

namespace pcce {

  class EventConnectionManager;

  class IThreadedSubSystem: public ISubSystem {
  public:
    IThreadedSubSystem();
    virtual ~IThreadedSubSystem();
    
    void vInitialize(tPropertyBagPtr configSection);
    void vShutdown();
    
    /// Sends the event to the subsystem's thread
    void SendEventToThread(const tIEventPtr& event);

  protected:
    void DispatchThreadEventBlocking();
    void DispatchThreadEvents();

    void AddThreadHandler(tHashValue eventType, tEventHandler handler);
    void RemoveThreadHandler(tHashValue eventType);
    
    bool IsRunning();

    virtual void vInitializeThread(tPropertyBagPtr configSection) = 0;
    virtual void vRunThread() = 0;
    virtual void vShutdownThread() = 0;

  private:
    boost::scoped_ptr< boost::thread > mThread;
    
    // Connection manager for connections to event system
    boost::scoped_ptr< EventConnectionManager > mSystemConns;
    
    boost::scoped_ptr< EventDispatcher > mDispatcher;
    // Connection manager for connections made with mDispatcher
    boost::scoped_ptr< EventConnectionManager > mDispatchConns;
    
    boost::mutex mEventMutex;
    boost::condition mEventCondition;
    tIEventPtrList mEventList;

    boost::mutex mRunningMutex;
    bool mRunning;
    boost::condition mInitCondition;
    
    tPropertyBagPtr mConfigSection;

    void ThreadStart();
    
    // This doesn't lock.  All it does it return mRunning, so I can use it with boost::thread.
    bool DoIsRunning();
  };

  // The main protocce object should register this with the event system to
  // handle the thread exception events.
  void HandleThreadExceptionEvent(const tIEventPtr& event);

}

#endif
