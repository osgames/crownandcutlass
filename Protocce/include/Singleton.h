/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Singleton template

#ifndef _PCCE_SINGLETON_H_
#define _PCCE_SINGLETON_H_

#include "config.h"
#include <boost/shared_ptr.hpp>
#include "Exception.h"

namespace pcce {

  /// Singleton template class
  /**
   *  This is a template class to automatically create thread-safe singletons.
   *  T should be a class with a default constructor.  Note: While the Singleton
   *  class itself is thread-safe, it can make no guarantees about the class it
   *  manages.  That must be handled seperately.
   */
  template<class T> class Singleton {
  private:
    static boost::shared_ptr<T> sT;
  public:
    static void Initialize() {
      if (sT) {
        PCCE_THROW("Attempt to reinitialize singleton class");
      }
      sT.reset(new T());
    }
    static void Shutdown() {
      if (!sT) {
        PCCE_THROW("Attempt to shutdown unintialized singleton class");
      }
      if (sT.use_count() != 1) {
        PCCE_THROW("Attempt to shutdown singleton class while still in use");
      }
      sT.reset();
    }
    static boost::shared_ptr<T> Get() {
      if (!sT) {
        PCCE_THROW("Attempt to use unintialized singleton class");
      }
      return sT;
    }
    static bool IsInitialized() {
      return sT;
    }
  };
  template<class T> boost::shared_ptr<T> Singleton<T>::sT;
  
  /// Scoped singleton manager template class
  /**
   *  This class provides an easy way to manage Singleton initialization and 
   *  shutdown.  When it is constructed it calls T::Initialize(), and when it is
   *  deleted it calls T::Shutdown().  Code that uses a Singleton need not worry
   *  about this class, only code that is responsible for managing a Singleton
   *  should use this class.
   */
  template<class T> class ScopedSingleton {
  public:
    /// Initializes Singleton class T
    ScopedSingleton() {
      T::Initialize();
    };
    /// Shutsdown Singleton class T
    ~ScopedSingleton() {
      T::Shutdown();
    };
  };

}

#endif
