/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#ifndef _PCCE_IRESOURCE_H_
#define _PCCE_IRESOURCE_H_

#include <string>
#include <boost/intrusive_ptr.hpp>
#include <boost/thread/mutex.hpp>

namespace pcce {

  class IResource {
  public:
    IResource(const std::string& name);
    virtual ~IResource();

    void Load();
    void Unload();

    std::string GetName();

    std::string GetGroup();

    // This should only be called by the ResourceManager, change this to enforce that!
    void SetGroup(const std::string& group);
    
    unsigned int GetRefCount();

    friend void intrusive_ptr_add_ref(IResource* r);
    friend void intrusive_ptr_release(IResource* r);

  protected:
    virtual bool vDoLoad() = 0;
    virtual bool vDoUnload() = 0;

    virtual std::string vGetType() = 0;

  private:
    boost::mutex mMutex;
    unsigned int mRefCount;
    bool mLoaded;
    std::string mName;
    std::string mGroup;

    unsigned int AddRef();
    unsigned int Release();
  };

  typedef boost::intrusive_ptr< IResource > tIResourcePtr;
  void intrusive_ptr_add_ref(IResource* r);
  void intrusive_ptr_release(IResource* r);

}

#endif
