/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Variant class

#ifndef _PCCE_VARIANT_H_
#define _PCCE_VARIANT_H_

#include <string>
#include <boost/variant.hpp>
#include "config.h"
#include "Exception.h"
#include "PropertyBag.h"
#include "ISerializable.h"
#include "VariantFwd.h"

namespace pcce {

  /// Underlying type used by Variant class
  /**
   * This is the underlying boost::variant type that is used by the Variant
   * class.  Note that bool is intentionally not included here, even though
   * IsBool and AsBool exist.  That is because const char* will be implicitly
   * converted to a bool if it is included.  A bool can be converted to an
   * int automatically, so leaving it out allows us to still have AsBool/IsBool
   * but also keep the automatic conversion from a string literal to a Variant.
   */  
  typedef boost::variant< int, unsigned int, long int, unsigned long int, tReal, char, unsigned char, std::string, tPropertyBagPtr > tVariant;

  /// Variant class that can store a single value of many types
  /**
   *  This variant class allows the user to store a single value from the tVariant
   *  and convert freely between them using a string stream. A Variant may also
   *  be empty, which can be checked using Variant::HasValue().
   */
  class Variant {
  public:
    /// Default constructor, creates an empty Variant
    Variant();

    /// Constructs a new Variant with the given tVariant value
    Variant(const tVariant value);

    /// Constructs a new Variant with the given int value
    Variant(const int value);

    /// Constructs a new Variant with the given unsigned int value
    Variant(const unsigned int value);

    /// Constructs a new Variant with the given long int value
    Variant(const long int value);

    /// Constructs a new Variant with the given unsigned long int value
    Variant(const unsigned long int value);

    /// Constructs a new Variant with the given tReal value
    Variant(const tReal value);

    /// Constructs a new Variant with the given char value
    Variant(const char value);
    
    /// Constructs a new Variant with the given unsigned char value
    Variant(const unsigned char value);

    /// Constructs a new Variant with the given string value
    Variant(const std::string value);
    /// Constructs a new Variant with the given const char* value
    /**
     *  This constructor takes a const char*, but converts the value to
     *  a string.  It is provided for convenience.
     */
    Variant(const char* value);

    /// Constructs a new Variant with the given tPropertyBagPtr
    Variant(const tPropertyBagPtr value);
    
    /// Returns true if the Variant has a value (i.e. is not empty)
    bool HasValue() const;

    /// Clears the Variant's value
    /**
     *  This method clears the Variant's value.  That will cause all
     *  of the Get*() methods to throw a VariantEmptyException if
     *  called, until a value is set again using Set().
     */
    void Clear();
    
    /// Returns the Variant's value
    /**
     *  This returns the Variant's value as a tVariant.  It will throw a
     *  VariantEmptyException if the Variant does not have a value.
     */
    tVariant Get() const;

    /// Sets the Variant's value
    void Set(tVariant value);

    /// Returns the Variant's value as an int
    /**
     *  This method returns the Variant's value as an int.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to an int using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    int AsInt() const;

    /// Returns the Variant's value as a bool
    /**
     *  This method returns the Variant's value as a bool.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to a bool using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    bool AsBool() const;

    /// Returns the Variant's value as an unsigned int
    /**
     *  This method returns the Variant's value as an unsigned int.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to an unsigned int using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    unsigned int AsUnsignedInt() const;

    /// Returns the Variant's value as a long int
    /**
     *  This method returns the Variant's value as a long int.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to a long int using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    long int AsLongInt() const;

    /// Returns the Variant's value as an unsigned long int
    /**
     *  This method returns the Variant's value as an unsigned long int.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to an unsigned long int using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    unsigned long int AsUnsignedLongInt() const;

    /// Returns the Variant's value as a tReal
    /**
     *  This method returns the Variant's value as a tReal.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to a tReal using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    tReal AsReal() const;

    /// Returns the Variant's value as a char
    /**
     *  This method returns the Variant's value as a char.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to a char using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    char AsChar() const;

    /// Returns the Variant's value as an unsigned char
    /**
     *  This method returns the Variant's value as an unsigned char.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to an unsigned char using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    unsigned char AsUnsignedChar() const;

    /// Returns the Variant's value as a string
    /**
     *  This method returns the Variant's value as a string.  If the variant
     *  is empty, it will throw a VariantEmptyException.  If the value
     *  cannot be converted to a string using std::stringstream, it will
     *  throw a VariantConversionException.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     *  \sa Variant::Get()
     */
    std::string AsString() const;

    /// Returns the Variant's value as a tPropertyBagPtr
    /**
     *  This method returns the Variant's value as a tPropertyBagPtr.  If the variant
     *  is empty, it will throw a VariantEmptyException.  In this case, no conversion
     *  is attempted.  Only an actual tPropertyBagPtr can be retrieved by this method.
     *  \sa Variant::Get()
     */
    tPropertyBagPtr AsPropertyBagPtr() const;

    /// Returns true if tVariant value can be converted to an int
    /**
     *  This method returns true if the tVariant value can be converted to
     *  an int.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsInt() const;

    /// Returns true if tVariant value can be converted to a bool
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a bool.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsBool() const;

    /// Returns true if tVariant value can be converted to an unsigned int
    /**
     *  This method returns true if the tVariant value can be converted to
     *  an unsigned int.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsUnsignedInt() const;

    /// Returns true if tVariant value can be converted to a long int
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a long int.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsLongInt() const;

    /// Returns true if tVariant value can be converted to an unsigned long int
    /**
     *  This method returns true if the tVariant value can be converted to
     *  an unsigned long int.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsUnsignedLongInt() const;

    /// Returns true if tVariant value can be converted to a tReal
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a tReal.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsReal() const;

    /// Returns true if tVariant value can be converted to a char
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a char.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsChar() const;

    /// Returns true if tVariant value can be converted to an unsigned char
    /**
     *  This method returns true if the tVariant value can be converted to
     *  an unsigned char.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsUnsignedChar() const;

    /// Returns true if tVariant value can be converted to a string
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a string.
     *  Note: This method actually does the conversion and changes the tVariant value
     *  to the type specified.  That way, if you write something like
     *  "Variant v("123"); if (v.IsInt()) { cout << v.AsInt(); }", you will only do one
     *  conversion.  However, if you alternate between say "AsInt()" and "AsString()",
     *  the conversion will happen every time.
     */
    bool IsString() const;

    /// Returns true if tVariant value can be converted to a tPropertyBagPtr
    /**
     *  This method returns true if the tVariant value can be converted to
     *  a tPropertyBagPtr.  This method will only return true if the tVariant value
     *  actually is a tPropertyBagPtr, since conversion to/from othe types is not possible
     *  in this case.
     */
    bool IsPropertyBagPtr() const;
  private:
    bool mHasValue;
    // Note: This is mutable so that the underlying type can be converted on the fly.  Only the
    //   Set() and Is*() methods, and the boost::variant vistor should change this.
    mutable tVariant mValue;
  };

  /// Base class for Variant-related exceptions
  class VariantException: public Exception {
  public:
    VariantException(const std::string &error, const std::string &fileName, const unsigned int lineNumber):
      Exception( error, fileName, lineNumber)
    {};
  };

  /// Exception that is thrown when attempting to get a value from an empty Variant
  class VariantEmptyException: public VariantException {
  public:
    VariantEmptyException(const std::string &error, const std::string &fileName, const unsigned int lineNumber):
      VariantException( error, fileName, lineNumber)
    {};
  };

  /// Exception that is thrown when a type conversion fails.
  /**
   *  This is the class of the exception that is thrown when a boost::lexical_cast<>
   *  type conversion fails.
   */
  class VariantConversionException: public VariantException {
  public:
    VariantConversionException(const std::string &error, const std::string &fileName, const unsigned int lineNumber):
      VariantException( error, fileName, lineNumber)
    {};
  };

}

#endif
