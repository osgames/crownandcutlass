/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Base GameState class

#ifndef _PCCE_IGAMESTATE_H_
#define _PCCE_IGAMESTATE_H_

#include "util.h"
#include "IProcess.h"
#include "ProcessList.h"
#include "Scene.h"
#include "IGameStateFwd.h"

namespace pcce {

  //class Scene;

  /// Base game state class
  /**
   *  This class is the base game state class.  The game states are the primary
   *  location for game logic.
   */
  class IGameState {
  public:
    IGameState();
    virtual ~IGameState() {};
    
    virtual void vSwitchTo() {};
    virtual void vSwitchFrom() {};

    /// Update each of the processes currently live in this state
    /**
     *  This method runs through the list of active processes in this state
     *  and updates them.
     *  \param timeDelta Amount of time in milliseconds that has passed since the last update
     */
    void ExecProcesses(const tMillisecond timeDelta);
    
    bool HasActiveProcesses();
    
    tScenePtr GetScene();
    
  protected:
    // The current scene
    tScenePtr mScene;

    /// Add a process to the process list
    void AddProcess(tIProcessPtr process);
    
  private:
    tProcessListPtr mProcessList;
  };

}

#endif
