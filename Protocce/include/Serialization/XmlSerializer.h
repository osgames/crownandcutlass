/******************************************************************************
 * Protocce Engine                              *
 * Part of the Crown and Cutlass Project                    *
 * http://www.crownandcutlass.com                       *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford         *
 * All rights reserved.                             *
 *                                      *
 * Redistribution and use in source and binary forms, with or without     *
 * modification, are permitted provided that the following conditions     *
 * are met:                                   *
 * 1. Redistributions of source code must retain the above copyright      *
 *  notice, this list of conditions and the following disclaimer.       *
 * 2. Redistributions in binary form must reproduce the above copyright     *
 *  notice, this list of conditions and the following disclaimer in the   *
 *  documentation and/or other materials provided with the distribution.  *
 * 3. The names of the authors may not be used to endorse or promote products *
 *  derived from this software without specific prior written permission.   *
 *                                      *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR     *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,       *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      *
 *                                      *
 ******************************************************************************/
 
 // XML Serializer Class

#ifndef _PCCE_XMLSERIALIZER_H_
#define _PCCE_XMLSERIALIZER_H_

#include "ISerializer.h"
#include <boost/smart_ptr.hpp>

namespace pcce {
  class XmlDoc;
  
  class XmlSerializer: public ISerializer {
  public:
    XmlSerializer();
    virtual ~XmlSerializer();
    
    // Could do SetFileName
    /// Virtual Save function.
    /**
     * Implemented by the child class, it will start off the
     * save serialization process.
     */
    bool Save(const std::string &Location);
    /// Virtual Save function.
    /**
     * Implemented by the child class, it will load
     * the resource and create all the objects necessary
     * for the state.
     */
    bool Load(const std::string &Location);
  private:
    boost::scoped_ptr< XmlDoc > mSerializeDoc;
  };

}

#endif /*_PCCE_XMLSERIALIZER_H_*/
