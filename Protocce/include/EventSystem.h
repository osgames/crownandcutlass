/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Event system

#ifndef _PCCE_EVENTSYSTEM_H_
#define _PCCE_EVENTSYSTEM_H_

#include <map>
#include <set>
#include <boost/shared_ptr.hpp>
#include <boost/signals/signal1.hpp>
#include <boost/thread/mutex.hpp>
#include "Singleton.h"
#include "IEventFwd.h"
#include "EventDispatcher.h"

namespace pcce {

  /// Event system class
  /**
   *  The event system is the primary means of communication within the engine.
   *  Rather than communicating with other objects or sub-systems directly, we
   *  pass events around, which are classes derived from IEvent.  You can
   *  register a listener for a specific event type.
   */
  class EventSystem: public EventDispatcher {
  public:
    EventSystem();
    ~EventSystem();

    /// Queue an event for execution later
    /**
     *  Queues an event for execution when ExecuteQueue() is called.  This is
     *  the primary method of event passing for game logic.
     */
    void QueueEvent(tIEventPtr event);
    /// Dispatches the queue of events
    void DispatchQueue();
    // This is mostly for debugging
    unsigned int GetQueuedEventCount();

    /// Registers an event type
    bool RegisterEventType(const tHashValue EventType);
    /// Returns count of registered event types
    unsigned int GetRegisteredEventTypeCount();

  private:
    /// Set of tHashValue
    /**
     *  This set of hash values is used by the event system to keep track of which
     *  event types have already been registered.  That allows us to detect hash
     *  collisions.
     */
    typedef std::set< pcce::tHashValue > tHashValueSet;
    /// Pair used by the tHashValueSet
    /**
     *  This pair is used to do lookups into a tHashValueSet.  It is used by the
     *  event system to detect event type hash collisions.
     */
    typedef std::pair< tHashValueSet::const_iterator, bool > tHashValueSetPair;

    boost::mutex mTypeMutex;
    boost::mutex mEventQueueMutex;
     
    tHashValueSet mRegisteredTypes;
     
    tIEventPtrList mEventQueue;
    
    tIEventPtrList GetQueuedEvents();
  };
  
  /// Event system singleton
  typedef pcce::Singleton<EventSystem> EventSystemSingleton;

}

#endif
