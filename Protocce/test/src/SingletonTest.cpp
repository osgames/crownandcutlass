/* Protocce Test
 * Singleton Test class
 */

#include "config.h"
#include <boost/shared_ptr.hpp>
#include "Exception.h"
#include "Singleton.h"
#include "SingletonTest.h"

using namespace pcce;
using namespace pccetest;
using namespace boost;

class Foo {
private:
  static unsigned int sCount;
public:
  Foo() {
    sCount++;
  }
  int GetInitCount() {
    return sCount;
  }
};

unsigned int Foo::sCount = 0;

typedef Singleton< Foo > FooSingleton;
typedef ScopedSingleton< FooSingleton > FooSingletonManager;

void SingletonTest::vDoRunTest() {
  bool success;
  PCCE_CHECK(
    !FooSingleton::IsInitialized(),
    "Singleton should not be initialized yet");

  success = true;
  try {
    FooSingleton::Get();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "Get did not fail on an uninitialized singleton");

  {
    FooSingletonManager s;

    PCCE_CHECK(
      FooSingleton::IsInitialized(),
      "Singleton should be initialized now");
    success = true;
    try {
      FooSingleton::Initialize();
      success = false;
    } catch (Exception&) {
    }
    PCCE_CHECK(success, "Singleton reinitialized");

    PCCE_CHECK(
      FooSingleton::Get()->GetInitCount() == 1,
      "Singleton type created more than once"); 

    PCCE_CHECK(
      FooSingleton::Get().use_count() == 2, "Use count should be 2");
    {
      shared_ptr< Foo > f = FooSingleton::Get();
      PCCE_CHECK(
        FooSingleton::Get().use_count() == 3, "Use count should now be 3");

      PCCE_CHECK(
        FooSingleton::Get()->GetInitCount() == 1,
        "Singleton type created more than once"); 
      PCCE_CHECK(
        f->GetInitCount() == 1,
        "Singleton type created more than once"); 
    }
    PCCE_CHECK(
      FooSingleton::Get().use_count() == 2, "Use count should be 2 again");
  }
  PCCE_CHECK(
    !FooSingleton::IsInitialized(),
    "Singleton should no longer be initialized");

  success = true;
  try {
    FooSingleton::Shutdown();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "Singleton shutdown twice");

  success = true;
  try {
    FooSingleton::Get();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "Get did not fail on an uninitialized singleton");
}
