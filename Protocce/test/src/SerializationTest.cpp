/* Protocce Test
 * Serialization Test class
 */

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include "ISerializable.h"
#include "ISerializer.h"
#include "PropertyBag.h"
#include "Serialization/XmlSerializer.h"
#include "Serialization/XmlSerializeNode.h"
#include "SerializationTest.h"

using namespace pccetest;
using namespace pcce;
using namespace boost;

void SerializationTest::vDoRunTest() {
  TestSave();
  TestLoad();
}

void SerializationTest::TestSave() {
  shared_ptr<XmlSerializer> Serializer(new XmlSerializer());
  
  shared_ptr<PropertyBag> PB(new PropertyBag());
  
  PB->AddProperty("Test");
  PB->SetValue("Test", 123);
  
  Serializer->RegisterAutomaticSaveFunction(bind(&PropertyBag::Save, PB, _1));
  
  Serializer->Save("../data/SerializeSave.psf");
}

void SerializationTest::TestLoad() {
  
}
