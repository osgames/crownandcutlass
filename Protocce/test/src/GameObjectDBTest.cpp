/* Protocce Test
 * GameObjectDB Test class
 */

#include "GameObjectDB.h"
#include "Exception.h"
#include "GameObjectDBTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

typedef ScopedSingleton< GameObjectDBSingleton > GameObjectDBManager;

void GameObjectDBTest::vDoRunTest() {
  tGameObjectPtr obj;
  bool success;

  GameObjectDBManager s;
  PCCE_CHECK(
    GameObjectDBSingleton::IsInitialized(),
    "GameObjectDBManager failed to initialized system");

  PCCE_CHECK(
    !GameObjectDBSingleton::Get()->TryGetGameObject(100, obj),
    "TryGetObject should have failed");

  success = true;
  try {
    obj = GameObjectDBSingleton::Get()->GetGameObject(100);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "GetObject should have raised");

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 0, "Count should be 0");

  obj = GameObjectDBSingleton::Get()->NewGameObject();
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 1, "Expected ObjectID to be 1");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 1, "Count should be 1");

  obj = GameObjectDBSingleton::Get()->GetGameObject(1);
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 1, "Expected ObjectID to be 1");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->TryGetGameObject(1, obj),
    "TryGetObject should not have failed");
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 1, "Expected ObjectID to be 1");
  obj.reset();

  obj = GameObjectDBSingleton::Get()->NewGameObject();
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 2, "Expected ObjectID to be 2");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 2, "Count should be 2");

  obj = GameObjectDBSingleton::Get()->NewGameObject();
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 3, "Expected ObjectID to be 3");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 3, "Count should be 3");

  obj = GameObjectDBSingleton::Get()->GetGameObject(1);
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 1, "Expected ObjectID to be 1");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->TryGetGameObject(1, obj),
    "TryGetObject should not have failed");
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 1, "Expected ObjectID to be 1");
  obj.reset();

  obj = GameObjectDBSingleton::Get()->GetGameObject(2);
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 2, "Expected ObjectID to be 2");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->TryGetGameObject(2, obj),
    "TryGetObject should not have failed");
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 2, "Expected ObjectID to be 2");
  obj.reset();

  GameObjectDBSingleton::Get()->DeleteGameObject(1);
  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 2, "Count should be 2");

  GameObjectDBSingleton::Get()->DeleteGameObject(100);
  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 2, "Count should be 2");

  GameObjectDBSingleton::Get()->DeleteGameObject(3);
  PCCE_CHECK(
    GameObjectDBSingleton::Get()->GetCount() == 1, "Count should be 1");

  obj = GameObjectDBSingleton::Get()->GetGameObject(2);
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 2, "Expected ObjectID to be 2");
  obj.reset();

  PCCE_CHECK(
    GameObjectDBSingleton::Get()->TryGetGameObject(2, obj),
    "TryGetObject should not have failed");
  PCCE_CHECK(obj, "Obj should not be empty");
  PCCE_CHECK(obj->GetID() == 2, "Expected ObjectID to be 2");
  obj.reset();

  PCCE_CHECK(
    !GameObjectDBSingleton::Get()->TryGetGameObject(1, obj),
    "TryGetObject should have failed");

  success = true;
  try {
    obj = GameObjectDBSingleton::Get()->GetGameObject(1);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "GetObject should have raised");
};
