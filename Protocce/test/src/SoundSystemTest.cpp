/* Protocce Test
 * Sound System Test class
 */

#include <iostream>
#include "Exception.h"
#include "Singleton.h"
#include "EventSystem.h"
#include "ResourceManager.h"
#include "SoundResource.h"
#include "SoundResourceFactory.h"
#include "SoundBuffer.h"
#include "SoundStream.h"
#include "SoundEvents.h"
#include "ThreadExceptionEvent.h"
#include "IThreadedSubSystem.h"
#include "SoundSubSystem.h"
#include "EventConnectionManager.h"
#include "PropertyBag.h"
#include "Variant.h"
#include "util.h"
#include "soundutil.h"
#include "Log.h"
#include "SoundSystemTest.h"

using namespace std;
using namespace pcce;
using namespace pccetest;

namespace {
  const string OggResName = "ogg";
  //const string WaveResName = "wave";
  const string LongResName = "long";
  const string TestResGroup = "test";

  const string streamFileName = DataPath + "stream.ogg";
  
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  typedef ScopedSingleton< ResourceManagerSingleton > ResManager;
  typedef ScopedSingleton< LogSingleton > LogManager;
  
  class ALManager {
  public:
    ALManager() {
      InitializeAL();
    };
    ~ALManager() {
      ShutdownAL();
    }
  };
  
  class SubSystemManager {
  public:
    SubSystemManager(tISubSystemPtr system, tPropertyBagPtr section): mSystem(system) {
      system->vInitialize(section);
    }
    ~SubSystemManager() {
      mSystem->vShutdown();
    }
  private:
    tISubSystemPtr mSystem;
  };
  
  void SetSoundResourceFileName(tIResourcePtr resource, const string& fileName) {
    unsigned int refCount = resource->GetRefCount();
    {
      boost::intrusive_ptr< SoundResource > r = boost::dynamic_pointer_cast< SoundResource >(resource);
      PCCE_CHECK(r, "SetSoundResourceFileName: Resource is not sound resource");
      r->SetFileName(fileName);
    }
    PCCE_CHECK(resource->GetRefCount() == refCount, "Cast does not work");
  }
  
  tSoundAction lastAction = saReleaseSource;
  unsigned int bufferEventCount = 0;
  unsigned int streamEventCount = 0;
  
  void HandleBufferEvent(const tIEventPtr& e) {
    boost::shared_ptr< SoundBufferEvent > event = boost::dynamic_pointer_cast< SoundBufferEvent >(e);
    lastAction = event->GetAction();
    ++bufferEventCount;
  }
  
  void HandleStreamEvent(const tIEventPtr& e) {
    boost::shared_ptr< SoundStreamEvent > event = boost::dynamic_pointer_cast< SoundStreamEvent >(e);
    lastAction = event->GetAction();
    ++streamEventCount;
  }
}

void SoundSystemTest::vDoRunTest() {
  const string oggFileName = DataPath + "test.ogg";
  //const string wavFileName = DataPath + "test.wav";
  
  LogManager lm;
  EventSystemManager esm;
  ResManager rm;
  
  LogSingleton::Get()->SetCoutEnabled(true);
  LogSingleton::Get()->SetFileName("");
  
  tIResourceFactoryPtr factory(new SoundResourceFactory());
  ResourceManagerSingleton::Get()->RegisterResourceFactory(SoundResourceType, factory);
  
  {
    tIResourcePtr r1 = ResourceManagerSingleton::Get()->CreateResource(SoundResourceType, OggResName, TestResGroup);
    SetSoundResourceFileName(r1, oggFileName);
    //tIResourcePtr r2 = ResourceManagerSingleton::Get()->CreateResource(SoundResourceType, WaveResName, TestResGroup);
    //SetSoundResourceFileName(r2, wavFileName);
    tIResourcePtr r2 = ResourceManagerSingleton::Get()->CreateResource(SoundResourceType, LongResName, TestResGroup);
    SetSoundResourceFileName(r2, streamFileName);
  }
  

  tEventConnection threadExceptionConn = EventSystemSingleton::Get()->AddHandler(
    ThreadExceptionEvent::sGetEventType(), HandleThreadExceptionEvent);
  
  TestSoundBuffer();
  TestSoundStream();

  VerifyResult("Are your speakers plugged in and turned up", true);
  TestSoundResource();
  
  TestSoundSystemBuffer();
  TestSoundSystemStream();
  
  threadExceptionConn.disconnect();
}

void SoundSystemTest::TestSoundResource() {
  const string oggFileName = DataPath + "test.ogg";

  cout << "About to test SoundResource" << endl;
  {
    ALManager alm;
    PCCE_AL_CHECK();

    ResourceManagerSingleton::Get()->UnloadResourceGroup(TestResGroup);
    PCCE_AL_CHECK();
    
    tIResourcePtr r = ResourceManagerSingleton::Get()->GetResource(OggResName);
    PCCE_CHECK(r, "Could not get resource");
    PCCE_CHECK(r->GetRefCount() == 3, "RefCount should be 3");
    boost::intrusive_ptr< SoundResource > soundRes = boost::dynamic_pointer_cast< SoundResource >(r);
    PCCE_CHECK(soundRes, "Resource is not sound resource");
    PCCE_CHECK(soundRes.get() == r.get(), "Cast didn\'t work");
    PCCE_CHECK(r->GetRefCount() == 4, "RefCount should be 4");
    PCCE_CHECK(r->GetRefCount() == soundRes->GetRefCount(), "r->RefCount should equal soundRes->RefCount");
    
    tOpenALSourcePtr source(new OpenALSource());
    PCCE_AL_CHECK();
    alSourcei(source->GetSource(), AL_BUFFER, soundRes->GetBuffer());
    PCCE_AL_CHECK();
    alSourcePlay(source->GetSource());
    PCCE_AL_CHECK();
  
    VerifyResult("Did you hear \"ogg test\"", true);
    
    alSourceStop(source->GetSource());
    PCCE_AL_CHECK();
  }
  PCCE_AL_CHECK();
}

void SoundSystemTest::TestSoundBuffer() {
  cout << "About to test SoundBuffer" << endl;
  
  tEventConnection c = EventSystemSingleton::Get()->AddHandler(SoundBufferEvent::sGetEventType(), HandleBufferEvent);

  {
    ALManager alm;
    PCCE_AL_CHECK();

    ResourceManagerSingleton::Get()->UnloadResourceGroup(TestResGroup);
    
    PCCE_CHECK(bufferEventCount == 0, "Event count should be 0");
    {
      SoundBuffer sb1(OggResName);
      PCCE_CHECK(bufferEventCount == 0, "Event count should still be 0");
    
      sb1.vPlay();
      PCCE_CHECK(bufferEventCount == 1, "Event count should be 1");
      PCCE_CHECK(lastAction == saPlay, "Last action should be saPlay");
  
      sb1.vPause();
      PCCE_CHECK(bufferEventCount == 2, "Event count should be 2");
      PCCE_CHECK(lastAction == saPause, "Last action should be saPause");
      
      sb1.vStop();
      PCCE_CHECK(bufferEventCount == 3, "Event count should be 3");
      PCCE_CHECK(lastAction == saStop, "Last action should be saStop");
    }
    PCCE_CHECK(bufferEventCount == 4, "Event count should be 4");
    PCCE_CHECK(lastAction == saReleaseSource, "Last action should be saReleaseSource");
    
    c.disconnect();
  }
  PCCE_AL_CHECK();
}

void SoundSystemTest::TestSoundStream() {
  cout << "About to test SoundStream" << endl;
  
  tEventConnection c = EventSystemSingleton::Get()->AddHandler(SoundStreamEvent::sGetEventType(), HandleStreamEvent);

  {
    PCCE_CHECK(streamEventCount == 0, "Event count should be 0");
    {
      SoundStream ss(streamFileName);
      PCCE_CHECK(streamEventCount == 0, "Event count should still be 0");
    
      ss.vPlay();
      PCCE_CHECK(streamEventCount == 1, "Event count should be 1");
      PCCE_CHECK(lastAction == saPlay, "Last action should be saPlay");
  
      ss.vPause();
      PCCE_CHECK(streamEventCount == 2, "Event count should be 2");
      PCCE_CHECK(lastAction == saPause, "Last action should be saPause");
      
      ss.vStop();
      PCCE_CHECK(streamEventCount == 3, "Event count should be 3");
      PCCE_CHECK(lastAction == saStop, "Last action should be saStop");
    }
    PCCE_CHECK(streamEventCount == 4, "Event count should be 4");
    PCCE_CHECK(lastAction == saReleaseSource, "Last action should be saReleaseSource");
    
    c.disconnect();
  }
  PCCE_AL_CHECK();
}

void SoundSystemTest::TestSoundSystemBuffer() {
  cout << "About to test SoundSubSystem" << endl;
  
  tISubSystemPtr s(new SoundSubSystem());
  tPropertyBagPtr p(new PropertyBag());
  p->AddProperty("sources");
  p->SetValue("sources", 2);
  
  {
    SubSystemManager sm(s, p);
    p.reset();
    PCCE_AL_CHECK();
    
    EventSystemSingleton::Get()->DispatchQueue();
    
    ResourceManagerSingleton::Get()->UnloadResourceGroup(TestResGroup);
    PCCE_AL_CHECK();
    
    {
      SoundBuffer sb(OggResName);
      PCCE_AL_CHECK();
      
      sb.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did you hear \"ogg test\" again", true);
  
      sb.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did you hear \"ogg test\" a third time", true);
      
      cout << "Loading large ogg file..." << endl;
      SoundBuffer longSound(LongResName);
      PCCE_AL_CHECK();
      
      longSound.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Do you hear the music (note: it starts very softly)", true);
      
      longSound.vPause();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music stop", true);
  
      longSound.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music resume", true);
      
      longSound.vStop();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music stop again", true);
  
      // Note: This will cause an exception, since the sound system will still be
      //   trying to use the old buffer.  Fix it!
      //ResourceManagerSingleton::Get()->UnloadResourceGroup(TestResGroup);
  
      longSound.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music restart from the beginning", true);
      
      SoundBuffer sb2(OggResName);
      PCCE_AL_CHECK();
      
      sb.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did you hear \"ogg test\" while the music played", true);
      
      sb2.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did you hear \"ogg test\" and the music stopped", true);
      
      longSound.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Do you hear the music again", true);
    }
    
    SleepMs(100);
    EventSystemSingleton::Get()->DispatchQueue();
  }
  PCCE_AL_CHECK();
}

void SoundSystemTest::TestSoundSystemStream() {
  cout << "About to test SoundSubSystem streaming" << endl;
  
  tISubSystemPtr s(new SoundSubSystem());
  tPropertyBagPtr p(new PropertyBag());
  p->AddProperty("sources");
  p->SetValue("sources", 3);

  {
    SubSystemManager sm(s, p);
    p.reset();
    PCCE_AL_CHECK();
    
    EventSystemSingleton::Get()->DispatchQueue();
    
    ResourceManagerSingleton::Get()->UnloadResourceGroup(TestResGroup);
    PCCE_AL_CHECK();
    
    {
      SoundStream ss(streamFileName);
      
      ss.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Is the music playing", true);
      
      ss.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Is the music still playing", true);
      
      ss.vPause();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music stop", true);

      ss.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music resume", true);

      ss.vStop();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music stop again", true);

      ss.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did the music start over", true);

      SoundBuffer sb(OggResName);
      PCCE_AL_CHECK();
      
      sb.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did you hear \"ogg test\" while the music played", true);
      
      SoundStream ss2(streamFileName);
      ss2.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Are two copies of the music playing", true);

      cout << "Loading large ogg file..." << endl;
      SoundBuffer longSound(LongResName);
      PCCE_AL_CHECK();
      
      longSound.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Are three copies of the music playing", true);

      sb.vPlay();
      SleepMs(10);
      EventSystemSingleton::Get()->DispatchQueue();
      PCCE_AL_CHECK();
      VerifyResult("Did one copy of the music stop and did you hear \"ogg test\"", true);
    }
  }
  VerifyResult("Did the music stop", true);
}
