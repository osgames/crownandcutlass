/* Protocce Test
 * Process List Test class
 */

#include <memory>
#include "ProcessList.h"
#include "IProcess.h"
#include "Exception.h"
#include "ProcessListTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

class MyProcess: public IProcess {
public:
  MyProcess() {
    mCount = 0;
  }
  void vUpdate(const tMillisecond timeDelta) {
    sCount++;
    mCount++;
  }
  unsigned int mCount;
  static unsigned int sCount;
};

unsigned int MyProcess::sCount = 0;


class MyLoopProcess: public IProcess {
public:
  MyLoopProcess() {
    mCount = 0;
  }
  void vUpdate(const tMillisecond timeDelta) {
    tIProcessPtr next(new MyLoopProcess());
    sCount++;
    mCount++;
    SetNext(next);
    Kill();
  }
  unsigned int mCount;
  static unsigned int sCount;
};

unsigned int MyLoopProcess::sCount = 0;


void ProcessListTest::vDoRunTest() {
  RunBasicTest();
  RunNextTest();
  RunLoopTest();
}

void ProcessListTest::RunBasicTest() {
  tIProcessPtr p(new MyProcess());
  {
    auto_ptr< ProcessList > list(new ProcessList());
    PCCE_CHECK(list->GetProcessCount() == 0, "Count did not start off empty");
    
    list->ExecuteAllProcesses(5);
    PCCE_CHECK(list->GetProcessCount() == 0, "Count did not start off empty");

    list->AddProcess(p);
    PCCE_CHECK(list->GetProcessCount() == 1, "Process was not added");
    
    list->ExecuteAllProcesses(5);
    PCCE_CHECK(list->GetProcessCount() == 1, "Process count should be 1");
    PCCE_CHECK(MyProcess::sCount == 1, "Execute count should be 1");
    
    list->ExecuteOneProcess(5);
    PCCE_CHECK(list->GetProcessCount() == 1, "Process count should be 1");
    PCCE_CHECK(MyProcess::sCount == 2, "Execute count should be 2");
    
    tIProcessPtr p2(new MyProcess());
    list->AddProcess(p2);
    PCCE_CHECK(list->GetProcessCount() == 2, "Process was not added");
    
    list->ExecuteAllProcesses(5);
    PCCE_CHECK(MyProcess::sCount == 4, "Execute count should be 4");
    
    list->ExecuteOneProcess(5);
    PCCE_CHECK(MyProcess::sCount == 5, "Execute count should be 5");
    
    p2->Kill();
    PCCE_CHECK(list->GetProcessCount() == 2, "Process was removed early");
    
    list->ExecuteAllProcesses(5);
    PCCE_CHECK(MyProcess::sCount == 6, "Execute count should be 6");
    PCCE_CHECK(list->GetProcessCount() == 1, "Process was not removed");
    PCCE_CHECK(p2.unique(), "Reference to p2 was leaked when p2 was killed");
    
    p->Kill();
    list->ExecuteAllProcesses(5);
    PCCE_CHECK(MyProcess::sCount == 6, "Execute count should still be 6");
    PCCE_CHECK(list->GetProcessCount() == 0, "Process was not removed");
  }
  
  PCCE_CHECK(p.unique(), "Reference to p was leaked when the list was deleted");
}

void ProcessListTest::RunNextTest() {
  auto_ptr< ProcessList > list(new ProcessList());
  
  MyProcess::sCount = 0;
  
  tIProcessPtr p(new MyProcess());
  tIProcessPtr p2(new MyProcess());
  
  p->SetNext(p2);
  
  PCCE_CHECK(list->GetProcessCount() == 0, "Process list is not empty");
  list->AddProcess(p);
  PCCE_CHECK(list->GetProcessCount() == 1, "Process was not added");
  
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(MyProcess::sCount == 1, "Execute count should be 1");
  
  p->Kill();
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(list->GetProcessCount() == 1, "Process count should still be 1");
  PCCE_CHECK(MyProcess::sCount == 1, "Execute count should be 1");
  
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(MyProcess::sCount == 2, "Execute count should be 2");
  
  p2->Kill();
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(list->GetProcessCount() == 0, "Process count should still be 1");
  PCCE_CHECK(MyProcess::sCount == 2, "Execute count should still be 2");
}


void ProcessListTest::RunLoopTest() {
  auto_ptr< ProcessList > list(new ProcessList());
  
  tIProcessPtr p(new MyLoopProcess());
  
  PCCE_CHECK(list->GetProcessCount() == 0, "Process list is not empty");
  list->AddProcess(p);
  PCCE_CHECK(list->GetProcessCount() == 1, "Process was not added");
  
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(MyLoopProcess::sCount == 1, "Execute count should be 1");
  PCCE_CHECK(list->GetProcessCount() == 1, "Process was not replaced");
  
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(MyLoopProcess::sCount == 2, "Execute count should be 2");
  PCCE_CHECK(list->GetProcessCount() == 1, "Process was not replaced");
  
  list->ExecuteAllProcesses(5);
  PCCE_CHECK(MyLoopProcess::sCount == 3, "Execute count should be 3");
  PCCE_CHECK(list->GetProcessCount() == 1, "Process was not replaced");
  
  MyLoopProcess* lp = (MyLoopProcess*) p.get();
  PCCE_CHECK(lp->mCount == 1, "Instance executed more than once");
}
