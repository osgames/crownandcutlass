/* Protocce Test
 * Event Test class
 */

#include "Exception.h"
#include "IEvent.h"
#include "EventTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

namespace {
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  
  PCCE_EVENT_CLASS(MyEvent) {
  public:
    static const std::string sGetEventName() { return "test event"; };
  };
  
  PCCE_EVENT_CLASS(MyEvent2) {
  public:
    static const std::string sGetEventName() { return "TEST EVENT"; };
  };
  
  PCCE_EVENT_CLASS(TestEvent) {
  public:
    static const std::string sGetEventName() { return "event test event"; };
  };
}

void EventTest::vDoRunTest() {
  bool success;

  success = true;
  try {
    TestEvent::sGetEventType();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(
    success,
    "Got event type without event system initialized");

  EventSystemManager s;
  tIEventPtr ev(new TestEvent());

  PCCE_CHECK(
    ev->vGetEventName() == TestEvent::sGetEventName(),
    "TestEvent static name does not equal virtual name");
  PCCE_CHECK(
    ev->vGetEventType() == TestEvent::sGetEventType(),
    "TestEvent static type does not equal virtual type");

  PCCE_CHECK(
    !EventSystemSingleton::Get()->RegisterEventType(TestEvent::sGetEventType()),
    "Reregistered TestEvent type");

  MyEvent::sGetEventType();
  success = true;
  try {
    MyEvent2::sGetEventType();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(
    success,
    "Event type names duplicate check should be case insensitive");
}
