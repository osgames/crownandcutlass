/* Protocce Test
 * EventSystem Test class
 */

#include <boost/shared_ptr.hpp>
#include <iostream>
#include "Exception.h"
#include "IEvent.h"
#include "EventSystem.h"
#include "EventSystemTest.h"

using namespace pcce;
using namespace pccetest;
using namespace boost;
using namespace std;

namespace {
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  
  PCCE_EVENT_CLASS(TestEvent) {
  public:
    static const std::string sGetEventName() { return "test event"; };
  };
  
  PCCE_EVENT_CLASS(AnotherTestEvent) {
  public:
    static const std::string sGetEventName() { return "another test event"; };
  };
  
  unsigned int eventCount = 0;
  
  void HandleTestEvent(const tIEventPtr& event) {
    eventCount++;
  }
  
  void HandleAnotherTestEvent(const tIEventPtr& event) {
    tIEventPtr newEvent(new AnotherTestEvent());
    EventSystemSingleton::Get()->QueueEvent(newEvent);
  }
}

void EventSystemTest::vDoRunTest() {
  TestGeneral();
  TestEventQueue();
  TestAllEventListeners();
  TestEventCounts();
}

void EventSystemTest::TestGeneral() {
  bool success;
  tHashValue hash;

  PCCE_CHECK(
    !EventSystemSingleton::IsInitialized(),
    "EventSystem already initialized");
  success = true;
  try {
    EventSystemSingleton::Shutdown();
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "EventSystem shutdown when not initialized");

  // Now init event system singleton
  {
    EventSystemManager s;
    PCCE_CHECK(
      EventSystemSingleton::IsInitialized(),
      "EventSystemManager failed to initialized system");

    // "AllEventsType" gets registered automatically...
    PCCE_CHECK(EventSystemSingleton::Get()->GetRegisteredEventTypeCount() == 1, "Unexpected event type");

    hash = HashString("123");
    PCCE_CHECK(
      EventSystemSingleton::Get()->RegisterEventType(hash),
      "Could not register event");
    PCCE_CHECK(
      !EventSystemSingleton::Get()->RegisterEventType(hash),
      "Registered an event twice");
    PCCE_CHECK(
      !EventSystemSingleton::Get()->RegisterEventType(AllEventsType),
      "Registered AllEventsType");

    PCCE_CHECK(EventSystemSingleton::Get()->GetRegisteredEventTypeCount() == 2, "Registered event type count should be 2");
    
    tEventConnection connection;
    tEventConnection connection2;
    tEventConnection connection3;
    
    tIEventPtr event(new TestEvent());
    tIEventPtr anotherEvent(new AnotherTestEvent());
    
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(
      eventCount == 0,
      "Handler received event without registering for it");
      
    connection = EventSystemSingleton::Get()->AddHandler(
      event->vGetEventType(), HandleTestEvent);
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(
      eventCount == 1,
      "Handler did not receive event");
      
    connection2 = EventSystemSingleton::Get()->AddHandler(
      event->vGetEventType(), HandleTestEvent);
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(
      eventCount == 3,
      "Handlers did not receive event");
    
    EventSystemSingleton::Get()->DispatchEvent(anotherEvent);
    PCCE_CHECK(
      eventCount == 3,
      "Handlers received even they did not register for");
      
    connection3 = EventSystemSingleton::Get()->AddHandler(
      anotherEvent->vGetEventType(), HandleTestEvent);
    EventSystemSingleton::Get()->DispatchEvent(anotherEvent);
    PCCE_CHECK(
      eventCount == 4,
      "Handler did not receive another event");
      
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(
      eventCount == 6,
      "Handlers did not receive events");
      
    connection2.disconnect();
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(
      eventCount == 7,
      "Handlers did not receive events");
    
    // Try to remove a listener that is already gone, make sure nothing breaks
    connection2.disconnect();
    
    EventSystemSingleton::Get()->DispatchEvent(event);
    EventSystemSingleton::Get()->DispatchEvent(anotherEvent);
    PCCE_CHECK(
      eventCount == 9,
      "Handler did not receive its events");
    
    connection.disconnect();
    connection3.disconnect();  
    EventSystemSingleton::Get()->DispatchEvent(event);
    EventSystemSingleton::Get()->DispatchEvent(anotherEvent);
    PCCE_CHECK(
      eventCount == 9,
      "Handler receive events after being totally removed");
          
    success = true;
    try {
      tEventHandler h;
      EventSystemSingleton::Get()->AddHandler(event->vGetEventType(), h);
      success = false;
    } catch (Exception&) {
    }
    PCCE_CHECK(success, "EventSystem allowed add null handler");
    
    event.reset();
    success = true;
    try {
      EventSystemSingleton::Get()->DispatchEvent(event);
      success = false;
    } catch (Exception&) {
    }
    PCCE_CHECK(success, "EventSystem allowed execute null event");
    
    success = true;
    try {
      EventSystemSingleton::Get()->QueueEvent(event);
      success = false;
    } catch (Exception&) {
    }
    PCCE_CHECK(success, "EventSystem allowed queue null event");

    PCCE_CHECK(EventSystemSingleton::Get()->GetRegisteredEventTypeCount() == 4, "Registered event type count should be 4");
  }
  PCCE_CHECK(
    !EventSystemSingleton::IsInitialized(),
    "EventSystemManager failed to shutdown system");
}

void EventSystemTest::TestEventQueue() {
  eventCount = 0;
  
  EventSystemManager s;
  PCCE_CHECK(
    EventSystemSingleton::IsInitialized(),
    "EventSystemManager failed to initialized system");
  
  tIEventPtr event(new TestEvent());
  tIEventPtr anotherEvent(new AnotherTestEvent());
  
  tEventConnection connection =  EventSystemSingleton::Get()->AddHandler(
    event->vGetEventType(), HandleTestEvent);
  tEventConnection connection2 = EventSystemSingleton::Get()->AddHandler(
    anotherEvent->vGetEventType(), HandleTestEvent);
    
  EventSystemSingleton::Get()->QueueEvent(event);
  EventSystemSingleton::Get()->QueueEvent(anotherEvent);
  EventSystemSingleton::Get()->QueueEvent(event);
  
  PCCE_CHECK(
    eventCount == 0,
    "Event queue test is not set up correctly");
  EventSystemSingleton::Get()->DispatchQueue();
  PCCE_CHECK(
    eventCount == 3,
    "Event queue did not work");
  // Make sure the event queue is empty
  EventSystemSingleton::Get()->DispatchQueue();
  PCCE_CHECK(
    eventCount == 3,
    "Event queue was not empty");
    
  tEventConnection connection3 =  EventSystemSingleton::Get()->AddHandler(
    event->vGetEventType(), HandleAnotherTestEvent);
    
  EventSystemSingleton::Get()->QueueEvent(event);
  EventSystemSingleton::Get()->QueueEvent(anotherEvent);
  EventSystemSingleton::Get()->QueueEvent(event);
  
  EventSystemSingleton::Get()->DispatchQueue();
  PCCE_CHECK(
    eventCount == 6,
    "Event queue did not work");
  EventSystemSingleton::Get()->DispatchQueue();
  PCCE_CHECK(
    eventCount == 8,
    "Event queue did not work");
  
  connection.disconnect();
  connection2.disconnect();
  connection3.disconnect();
}

void EventSystemTest::TestAllEventListeners() {
  eventCount = 0;
  
  EventSystemManager s;
  PCCE_CHECK(
    EventSystemSingleton::IsInitialized(),
    "EventSystemManager failed to initialized system");
  
  tIEventPtr event(new TestEvent());
  tIEventPtr anotherEvent(new AnotherTestEvent());
  
  tEventConnection c = EventSystemSingleton::Get()->AddHandler(
    event->vGetEventType(), HandleTestEvent);
  tEventConnection c2 = EventSystemSingleton::Get()->AddHandler(
    anotherEvent->vGetEventType(), HandleTestEvent);
  tEventConnection c3 = EventSystemSingleton::Get()->AddHandler(
    AllEventsType, HandleTestEvent);
  
  PCCE_CHECK(
    eventCount == 0,
    "All event listener test is not set up correctly");
  EventSystemSingleton::Get()->DispatchEvent(event);
  PCCE_CHECK(
    eventCount == 2,
    "Both listeners did not get event");
  
  EventSystemSingleton::Get()->DispatchEvent(anotherEvent);
  PCCE_CHECK(
    eventCount == 4,
    "All event listener should not consume events");

  c.disconnect();
  c2.disconnect();
  c3.disconnect();
}

void EventSystemTest::TestEventCounts() {
  eventCount = 0;
  
  EventSystemManager s;
  PCCE_CHECK(
    EventSystemSingleton::IsInitialized(),
    "EventSystemManager failed to initialized system");

  // All event signal gets auto-created
  PCCE_CHECK(EventSystemSingleton::Get()->ActiveEventTypeCount() == 1, "Event type count should be 1");

  tIEventPtr event(new TestEvent());
  tEventConnection c = EventSystemSingleton::Get()->AddHandler(
    event->vGetEventType(), HandleTestEvent);
  PCCE_CHECK(EventSystemSingleton::Get()->ActiveEventTypeCount() == 2, "ActiveEventTypeCount should be 2");
  tEventConnection c2 = EventSystemSingleton::Get()->AddHandler(
    AnotherTestEvent::sGetEventType(), HandleTestEvent);
  PCCE_CHECK(EventSystemSingleton::Get()->ActiveEventTypeCount() == 3, "ActiveEventTypeCount should be 3");

  EventSystemSingleton::Get()->DispatchEvent(event);
  PCCE_CHECK(
    eventCount == 1,
    "Event count should be 1");

  PCCE_CHECK(EventSystemSingleton::Get()->EventTypeCount() == 3, "ActiveEventTypeCount should still be 3");
  EventSystemSingleton::Get()->RemoveInactiveEventTypes();
  PCCE_CHECK(EventSystemSingleton::Get()->EventTypeCount() == 3, "EventTypeCount should still be 3 after clean");

  c2.disconnect();
  EventSystemSingleton::Get()->RemoveInactiveEventTypes();
  PCCE_CHECK(EventSystemSingleton::Get()->EventTypeCount() == 2, "EventTypeCount should now be 2");

  c.disconnect();

  PCCE_CHECK(EventSystemSingleton::Get()->EventTypeCount() == 2, "EventTypeCount should now be 2");
  PCCE_CHECK(EventSystemSingleton::Get()->ActiveEventTypeCount() == 1, "EventTypeCount should now be 1");
}

