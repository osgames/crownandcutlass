/* Protocce Test
 * OggStream Test class
 */

#include "Exception.h"
#include "util.h"
#include "soundutil.h"
#include "OpenALSource.h"
#include "OggStream.h"
#include "OggStreamTest.h"

using namespace std;
using namespace pcce;
using namespace pccetest;

namespace {
  class ALManager {
  public:
    ALManager() {
      InitializeAL();
    };
    ~ALManager() {
      ShutdownAL();
    }
  };
}

void OggStreamTest::vDoRunTest() {
  const string streamFileName = DataPath + "stream.ogg";

  ALManager alm;
  tOpenALSourcePtr source(new OpenALSource());
  {
    OggStream s(streamFileName, source);
  }

  VerifyResult("Are your speakers plugged in and turned up", true);
  
  // This causes issues in OS X, put this here to make testing easier
  {
    OggStream s(streamFileName, source);
    s.Play();
    s.Stop();
  }
   
  {
    OggStream s(streamFileName, source);
    s.Play();
    for (unsigned int i = 0; i < 100; ++i) {
      s.Update();
      SleepMs(100);
    }
    s.Play();
    VerifyResult("Did you hear the music (note: it starts very softly)", true);

    for (unsigned int i = 0; i < 10; ++i) {
      s.Update();
      SleepMs(100);
    }
    
    s.Play();
    for (unsigned int i = 0; i < 30; ++i) {
      s.Update();
      SleepMs(100);
    }
    VerifyResult("Did the music continue", true);
    
    s.Pause();
    s.Play();
    for (unsigned int i = 0; i < 50; ++i) {
      s.Update();
      SleepMs(100);
    }
    VerifyResult("Did the music continue again", true);

    s.Stop();
    s.Play();
    for (unsigned int i = 0; i < 50; ++i) {
      s.Update();
      SleepMs(100);
    }
    VerifyResult("Did the music restart", true);
  }
}
