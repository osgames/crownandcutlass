/* Protocce Test
 * PropertyBag Test class
 */

#include <iostream>
#include "PropertyBag.h"
#include "Variant.h"
#include "Exception.h"
#include "PropertyBagTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void PropertyBagTest::vDoRunTest() {
  bool success;

  auto_ptr< PropertyBag > bag(new PropertyBag());
  PCCE_CHECK(!bag->HasProperty("Foo"), "PropertyBag shouldn't have property");
  bag->RemoveProperty("Foo");

  PCCE_CHECK(bag->GetPropertyCount() == 0, "PropertyBag shouldn't have any properties");

  bag->AddProperty("a");
  PCCE_CHECK(bag->GetPropertyCount() == 1, "PropertyBag should have 1 property");
  bag->SetValue("a", 1);
  PCCE_CHECK(bag->GetPropertyCount() == 1, "PropertyBag should still have 1 property");
  bag->AddProperty("z");
  PCCE_CHECK(bag->GetPropertyCount() == 2, "PropertyBag should have 2 properties");
  bag->SetValue("z", 2);
  PCCE_CHECK(bag->GetPropertyCount() == 2, "PropertyBag should still have 2 properties");

  bag->AddProperty("Foo");
  PCCE_CHECK(bag->HasProperty("Foo"), "PropertyBag should have property");

  PCCE_CHECK(!bag->GetValue("Foo").HasValue(), "Value should be empty");
  bag->SetValue("Foo", 5);
  PCCE_CHECK(bag->GetValue("Foo").HasValue(), "Value shouldn't be empty");
  bag->AddProperty("Foo");
  PCCE_CHECK(bag->GetValue("Foo").HasValue(), "Value still shouldn't be empty");
  bag->ClearValue("Foo");
  PCCE_CHECK(!bag->GetValue("Foo").HasValue(), "Value should be empty again");

  bag->RemoveProperty("Foo");
  PCCE_CHECK(!bag->HasProperty("Foo"), "PropertyBag shouldn't have property again");

  bag->AddProperty("d");
  bag->SetValue("d", tReal(1.0));
  PCCE_CHECK(bag->GetValueAsReal("d") == tReal(1.0), "'d' value not a real");

  bag->AddProperty("s");
  bag->SetValue("s", string("Hello"));
  PCCE_CHECK(bag->GetValueAsString("s") == "Hello", "'s' value does not match");

  success = true;
  try {
    bag->GetValueAsInt("s");
    success = false;
  } catch (Exception&) {
  }
  
  tPropertyBagPtr second(new PropertyBag());
  second->AddProperty("Test");
  second->SetValue("Test", 1);
  bag->AddProperty("Second");
  bag->SetValue("Second", second);
  PCCE_CHECK(bag->GetValueAsPropertyBagPtr("Second")->GetValueAsInt("Test") == 1, "Nested bag value not correct");

  PCCE_CHECK(bag->GetPropertyCount() == 5, "PropertyBag should have 5 properties");

  PCCE_CHECK(bag->HasProperty("a"), "'a' property got lost");
  PCCE_CHECK(bag->GetValueAsInt("a") == 1, "'a' value got changed");
  PCCE_CHECK(bag->HasProperty("z"), "'z' property got lost");
  PCCE_CHECK(bag->GetValueAsInt("z") == 2, "'a' value got changed");
  bag->RemoveProperty("a");
  bag->RemoveProperty("z");

  PCCE_CHECK(bag->GetPropertyCount() == 3, "PropertyBag should have 3 properties");

  bag->AddProperty("z");
  
  for (PropertyBag::const_iterator i = bag->begin(); i != bag->end(); ++i) {
  	PCCE_CHECK(bag->HasProperty(i.first()), "PropertyBag does not have Iterator property.");
    cout << "i.first() == " << i.first() << " | i.second() ";
    Variant v =  i.second();
    if (v.HasValue()) {
      cout << "== ";
      if (v.IsPropertyBagPtr()) {
        cout << "a property bag";
      } else {
        cout << v.AsString();
      }
    } else {
      cout << "has no value.";
    }
    cout << endl;
  }
};
