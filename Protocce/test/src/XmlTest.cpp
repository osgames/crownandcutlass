/* Protocce Test
 * Xml Test class
 */

#include <memory>
#include <iostream>
#include <fstream>
#include "XmlDoc.h"
#include "XmlNode.h"
#include "Exception.h"
#include "Variant.h"
#include "stringutil.h"
#include "XmlTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void XmlTest::vDoRunTest() {
  TestReadXml();
  TestWriteXml();
  TestReadAndWriteXml();
}

void XmlTest::TestReadXml() {
  const string InvalidFile = DataPath + "Invalid.xml";
  const string EmptyFile = DataPath + "Empty.xml";
  const string MultipleRootsFile = DataPath + "MultipleRoots.xml";
  const string ValidFile = DataPath + "Valid.xml";

  auto_ptr< XmlDoc > doc;
  const XmlNode* rootNode = NULL;
  const XmlNode* nodeA = NULL;
  const XmlNode* nodeB = NULL;
  bool success;
  
  success = false;
  try {
    doc.reset(new XmlDoc("invalidfilename"));
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Created XML file with invalid filename");

  doc.reset(new XmlDoc());
  PCCE_CHECK(doc->RootNode() == NULL, "Empty constructor should return an empty XML doc");

  doc->LoadFile(ValidFile);
  PCCE_CHECK(doc->RootNode() != NULL, "LoadFile failed when called explicitly");

  doc.reset(new XmlDoc(ValidFile));
  PCCE_CHECK(doc->RootNode() != NULL, "LoadFile failed when called from constructor");

  success = false;
  try {
    doc->LoadFile(EmptyFile);
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Loaded empty file");
  PCCE_CHECK(doc->RootNode() == NULL, "Failed LoadFile due to empty did not reset the document");

  success = false;
  try {
    doc->LoadFile(MultipleRootsFile);
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Loaded file with multiple roots");
  PCCE_CHECK(doc->RootNode() == NULL, "Failed LoadFile due to multiple roots did not reset the document");

  success = false;
  try {
    doc->LoadFile(InvalidFile);
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Loaded file with invalid XML");
  PCCE_CHECK(doc->RootNode() == NULL, "Failed LoadFile due to invalid XML did not reset the document");

  doc.reset(new XmlDoc(ValidFile));
  PCCE_CHECK(doc->RootNode("Root") != NULL, "GetRootNode with name failed");

  rootNode = doc->RootNode();
  PCCE_CHECK(rootNode != NULL, "Unexpected load failure!");
  PCCE_CHECK(rootNode->GetName() == "Root", "Expected node to be named \"Root\", found \"" + rootNode->GetName() + "\"");

  nodeA = doc->RootNode();
  PCCE_CHECK(rootNode == nodeA, "Calling RootNode twice results in different node addresses");

  success = false;
  try {
    doc->RootNode("Invalid");
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "GetRootNode with invalid name did not fail");

  nodeA = rootNode->GetFirstChild();
  PCCE_CHECK(nodeA != NULL, "GetFirstChild() failed");
  PCCE_CHECK(rootNode->GetFirstChild("First") == nodeA, "GetFirstChild returned unexpected result");
  PCCE_CHECK(nodeA->GetParent() == rootNode, "Parent of node is not expected value");

  PCCE_CHECK(nodeA->GetAttributes()->GetPropertyCount() == 0, "Attribute list should be empty");
  success = false;
  try {
    nodeA->GetAttribute("invalid");
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Invalid attribute did not throw");

  nodeB = nodeA->GetFirstChild("SubValue");
  PCCE_CHECK(nodeB != NULL, "'First' element child not found");
  PCCE_CHECK(nodeA->GetFirstChild("subvalue") == NULL, "GetFirstChild() is not case sensitive");
  PCCE_CHECK(nodeB == nodeA->GetFirstChild(), "'First' element child error");
  PCCE_CHECK(nodeB->GetName() == "SubValue", "'First' element child name incorrect");
  PCCE_CHECK(nodeB->GetAttributes()->GetPropertyCount() == 1, "'First' element's child does not have correct attributes");
  PCCE_CHECK(nodeB->GetAttribute("value").AsInt() == 123, "'First' element attribute is incorrect");
  PCCE_CHECK(nodeB->GetNextSibling() == NULL, "'First' element's child has a next sibling");
  PCCE_CHECK(nodeB->GetPrevSibling() == NULL, "'First' element's child has a next sibling");
  PCCE_CHECK(nodeB->GetFirstChild() == NULL, "'First' element's child has a child");

  success = false;
  try {
    nodeB->GetAttribute("Value");
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Attribute names are not case sensitive");

  nodeA = nodeA->GetNextSibling();
  PCCE_CHECK(nodeA->GetName() == "Second", "'First' element sibling's name incorrect");
  PCCE_CHECK(nodeA->GetAttribute("name").AsString() == "Foo", "'First' element attribute is incorrect");

  nodeB = nodeA->GetFirstChild("SubValue");
  PCCE_CHECK(nodeB != NULL, "'Second' element child not found");
  PCCE_CHECK(nodeB->GetAttributes()->GetPropertyCount() == 2, "'Second' element's child does not have correct attributes");
  PCCE_CHECK(nodeB->GetNextSibling()->GetPrevSibling() == nodeB, "Next sibling's prev sibling is not node");

  nodeB = nodeB->GetNextSibling("SubValue");
  PCCE_CHECK(nodeB != NULL, "'Second' element's next child not found");
  PCCE_CHECK(nodeB->GetAttributes()->GetPropertyCount() == 2, "'Second' element's child does not have correct attributes");
  PCCE_CHECK(nodeB->GetAttributes()->GetValueAsInt("value") == 789, "'Second' element's child attribute 'value' should be 789");
  PCCE_CHECK(nodeB->GetNextSibling() == NULL, "Unexpected next sibling");
}

void XmlTest::TestWriteXml() {
  const string WriteFile = DataPath + "Write.xml";
  
  {
    auto_ptr< XmlDoc > doc(new XmlDoc());
    PCCE_CHECK(doc->RootNode() == NULL, "RootNode is not NULL");
    doc->CreateRootNode("Test");
    PCCE_CHECK(doc->RootNode() != NULL, "RootNode should not be NULL");
    PCCE_CHECK(doc->RootNode() == doc->RootNode(), "RootNode address changed between calls to RootNode()");

    XmlNode *nodeA = doc->RootNode();
    PCCE_CHECK(nodeA != NULL, "Root node is NULL");
    PCCE_CHECK(nodeA->GetName() == "Test", "Unexpected root node name");
    XmlNode *nodeB = nodeA->CreateChild("Foo");
    PCCE_CHECK(nodeB != NULL, "CreateChild returned NULL");
    PCCE_CHECK(nodeB->GetName() == "Foo", "Child's name does not match");

    nodeB->SetAttribute("name", "a");
    nodeB->SetAttribute("value", 123);

    nodeB = nodeA->CreateChild("Foo");
    PCCE_CHECK(nodeB != NULL, "CreateChild returned NULL second time");
    nodeB = nodeB->CreateChild("SubFoo");
    PCCE_CHECK(nodeB != NULL, "CreateChild returned NULL for sub object");

    nodeB->SetAttribute("test", true);

    doc->SaveFile(WriteFile);
  }

  ifstream s(WriteFile.c_str(), ios::in);
  PCCE_CHECK(s, "Could not open XML file to read it back in");
  string contents;
  string nl = GetNL();
  while (!s.eof()) {
    string line;
    getline(s, line);
    if (!contents.empty()) {
      contents.append(nl);
    }
    contents.append(line);
  }
  const string expectedContents = string("")
    + "<Test>" + nl
    + "    <Foo name=\"a\" value=\"123\" />" + nl
    + "    <Foo>" + nl
    + "        <SubFoo test=\"1\" />" + nl
    + "    </Foo>" + nl
    + "</Test>" + nl;
  if (expectedContents != contents) {
    cout << "Expected:" << endl << expectedContents << "!" << endl << endl;
    cout << "Got:" << endl << contents << "!" << endl << endl;
    PCCE_THROW("Content of file does not match expected value");
  }
}

void XmlTest::TestReadAndWriteXml() {
  // Implement this once the Xml wrapper supports writing
}
