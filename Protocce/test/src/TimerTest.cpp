/* Protocce Test
 * Timer Test class
 */

#include <memory>
#include <iostream>
#include "TimerTest.h"
#include "Exception.h"
#include "Timer.h"
#include "util.h"

using namespace pccetest;
using namespace std;
using namespace pcce;

void TimerTest::vDoRunTest() {
  RunSimpleDumpTest();
  RunInvalidTest();
}

void TimerTest::RunSimpleDumpTest() {
  auto_ptr< Timer > t(new Timer());
  
  cout << "Time since start: " << GetTimeSinceStart() << endl;
  SleepMs(1);
  cout << "Time since start (after SleepMs(1)): " << GetTimeSinceStart() << endl;

  cout << "FRAME START: " << t->MarkFrameStart() << endl;

  tTimerKey key = t->StartCustomTimer();
  cout << "Custom timer start: " << t->GetCustomTimerStartTime(key) << endl;
  cout << "Time since custom start: " << t->CheckCustomTimer(key) << endl;

  cout << "Time since frame start: " << t->GetTimeSinceFrameStart() << endl;

  cout << "Time since start: " << GetTimeSinceStart() << endl;

  cout << "Time between frame starts: " << t->GetTimeBetweenFrameStarts() << endl;

  cout << "FRAME START: " << t->MarkFrameStart() << endl;

  cout << "Time since custom start: " << t->CheckCustomTimer(key) << endl;

  cout << "Time since start: " << GetTimeSinceStart() << endl;
  SleepMs(1000);
  cout << "Time since start (after SleepMs(1000)): " << GetTimeSinceStart() << endl;

  cout << "Custom timer reset: " << t->ResetCustomTimer(key) << endl;
  cout << "New custom timer start: " << t->GetCustomTimerStartTime(key) << endl;
  cout << "New time since custom start: " << t->CheckCustomTimer(key) << endl;

  cout << "Time since start: " << GetTimeSinceStart() << endl;
  SleepMs(10);
  cout << "Time since start (after SleepMs(10)): " << GetTimeSinceStart() << endl;
}

void TimerTest::RunInvalidTest() {
  bool success;
  tMillisecond start;
  tTimerKey key, invalidKey;
  auto_ptr< Timer > t(new Timer());
  
  // Just to be sure some measurable time has passed
  SleepMs(1);
  
  key = t->StartCustomTimer();
  invalidKey = key + 2;
  
  start = t->GetCustomTimerStartTime(key);
  PCCE_CHECK(start > 0, "start = 0");
  SleepMs(10);
  PCCE_CHECK(t->CheckCustomTimer(key) > 0, "No time has passed since timer start");
  
  PCCE_CHECK(t->IsCustomTimerActive(key), "Key is not active");
  PCCE_CHECK(!t->IsCustomTimerActive(invalidKey), "Invalid key is active");
  
  success = true;
  try {
    t->GetCustomTimerStartTime(invalidKey);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "GetCustomTimerStartTime worked on invalid key");
  
  success = true;
  try {
    t->CheckCustomTimer(invalidKey);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "CheckCustomTimer worked on invalid key");
  
  success = true;
  try {
    t->ResetCustomTimer(invalidKey);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "ResetCustomTimer worked on invalid key");
  
  success = true;
  try {
    t->EndCustomTimer(invalidKey);
    success = false;
  } catch (Exception&) {
  }
  PCCE_CHECK(success, "EndCustomTimer worked on invalid key");
  
  PCCE_CHECK(!t->IsCustomTimerActive(invalidKey), "Invalid key was activated by failed tests");
  PCCE_CHECK(t->IsCustomTimerActive(key), "Key is was deactivated by failed tests");
  
  t->EndCustomTimer(key);
  PCCE_CHECK(!t->IsCustomTimerActive(key), "Key is was not deactivated");
}
