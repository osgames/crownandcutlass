/* Protocce Test
 * Log Test class
 */

#include <limits>
#include "mathutil.h"
#include "Exception.h"
#include "MathUtilTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void MathUtilTest::vDoRunTest() {
  const tReal negInf = -numeric_limits< tReal >::infinity();
  const tReal inf = numeric_limits< tReal >::infinity();
  PCCE_CHECK(Sign(negInf) == crLess, "-Inf is not negative");
  PCCE_CHECK(IsInf(negInf), "-Inf is not infinite");
  PCCE_CHECK(Sign(inf) == crGreater, "Inf is not positive");
  PCCE_CHECK(IsInf(inf), "Inf is not infinite");

  tReal x, y, z;
  x = 0;
  y = 0;
  PCCE_CHECK(SameReal(x, y), "0 and 0 should be the same real");

  x = 1;
  PCCE_CHECK(!SameReal(x, y), "1 and 0 should not be the same real");

  PCCE_CHECK(CompareReal(1.0, 1.0) == crEqual, "1 should equal 1");
  PCCE_CHECK(CompareReal(1.0, 1.1) == crLess, "1 should be less than 1.1");
  PCCE_CHECK(CompareReal(1.1, 1.0) == crGreater, "1.1 should be greater than 1");

  x = 100.0;
  x = x / 3.0;
  y = 1.0;
  y = y / 3.0;
  z = 33.0;

  PCCE_CHECK(RoundTo(x - y - z, 0) == 0.0, "100/3 - 1/3 - 33 should round to 0");
  PCCE_CHECK(RoundTo(y, 0) == 0.0, "1/3 rounded to 0 digits should be 0");
  PCCE_CHECK(SameReal(RoundTo(y, 1), 0.3), "1/3 rounded to 1 digit should be .3");
  // This check works on every machine I've tried except my MacBook Pro.  It
  //   would be nice to very that the floating point comparison breaks, but for
  //   now I'm just disabling it.
  //PCCE_CHECK(x - y - z != 0.0, "Floating point math should not give an exact result!.");
  PCCE_CHECK(SameReal(x - y - z, 0.0), "100/3 - 1/3 - 33 should equal 0");
  
  PCCE_CHECK(SameReal(Truncate(x), 33.0), "100/3 should truncate to 33.0");
  PCCE_CHECK(SameReal(Truncate(-x), -33.0), "-100/3 should truncate to -33.0");
};
