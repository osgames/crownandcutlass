/* Protocce Test
 * Protocce Main Test
 */

#include "config.h"
#ifdef PCCE_OSX
#include <Carbon/Carbon.h>
#endif
#include <Ogre.h>
#include <CEGUI.h>

#include "ProtocceMainTest.h"
#include "Configuration.h"
#include "ISubSystem.h"
#include "RenderSubSystem.h"
#include "Protocce.h"

using namespace pccetest;
using namespace pcce;

void ProtocceTest::vDoRunTest() {
  tISubSystemPtrList list;
  tISubSystemPtr sys;
  tConfigurationPtr config(new Configuration());
  //config.LoadINIFile(???);
    
  sys.reset(new RenderSubSystem());
  list.push_back(sys);
  
  Protocce p(config);
  p.Initialize(list);
  Ogre::Root::getSingleton().addResourceLocation(DataPath + "Taharez", "FileSystem", "GUI", true);
  CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLook.scheme", (CEGUI::utf8*)"GUI");
  CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook", "MouseArrow");
  p.RunGame();
  
//  CEGUI::SchemeManager::getSingleton().loadScheme("../data/Taharez/TaharezLook.scheme");
}
