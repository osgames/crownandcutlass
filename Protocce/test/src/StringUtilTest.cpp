/* Protocce Test
 * StringUtil Test class
 */

#include <iostream>
#include "stringutil.h"
#include "Variant.h"
#include "Exception.h"
#include "StringUtilTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void StringUtilTest::vDoRunTest() {
  TestHashString();
  TestFormat();
}

void StringUtilTest::TestHashString() {
  tHashValue v1, v2;

  v1 = 0;
  v1 = HashString("This is a test");
  PCCE_CHECK(v1 != 0, "HashString returned unexpected 0");

  v2 = HashString("this is a test");
  PCCE_CHECK(v1 != v2, "HashString should be case-sensitive");
  v2 = HashString("THIS IS A TEST");
  PCCE_CHECK(v1 != v2, "HashString should be case-sensitive");

  v1 = HashString("123");
  v2 = HashString("124");
  PCCE_CHECK(v1 != v2, "HashString is broken");
}

void StringUtilTest::TestFormat() {
  bool success;

  string formatStr, s1, s2;

  tVariantList v;
  v.push_back(string("ABC"));
  v.push_back(98);
  v.push_back(0);

  formatStr = "% Test|| %|%";
  s1 = "ABC Test| 98%";
  s2 = Format(formatStr, v);
  cout << "Format str: " << formatStr << endl;
  cout << "Expected: " << s1 << endl;
  cout << "Got: " << s2 << endl;
  PCCE_CHECK(s1 == s2, string("Expected \"") + s1 + string("\" got \"") + s2 + string("\""));

  formatStr = "% Test| %";
  success = false;
  try {
    s1 = Format(formatStr, v);
  } catch (Exception& e) {
    success = true;
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
  }
  PCCE_CHECK(success, "Format should fail on unexpected escape");

  formatStr = "% Test % % %";
  success = false;
  try {
    s1 = Format(formatStr, v);
  } catch (Exception& e) {
    success = true;
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
  }
  PCCE_CHECK(success, "Format should fail on too few values");
}
