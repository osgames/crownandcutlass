#include "config.h"
#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#ifdef PCCE_OSX
#include <Carbon/Carbon.h>
#endif
#include <Ogre.h>
#include <CEGUI.h>
#include <OIS/OIS.h>
#include "Exception.h"
#include "ITest.h"
#include "VariantTest.h"
#include "StringUtilTest.h"
#include "LogTest.h"
#include "SingletonTest.h"
#include "EventTest.h"
#include "EventSystemTest.h"
#include "EventConnectionManagerTest.h"
#include "ProcessListTest.h"
#include "TimerTest.h"
#include "ThreadedSubSystemTest.h"
#include "PropertyBagTest.h"
#include "GameObjectTest.h"
#include "GameObjectDBTest.h"
#include "XmlTest.h"
#include "SoundUtilTest.h"
#include "ResourceManagerTest.h"
#include "MathUtilTest.h"
#include "ProtocceMainTest.h"
#include "ConfigurationTest.h"
#include "SoundSystemTest.h"
#include "InputSubSystemTest.h"
#include "OggStreamTest.h"
//#include "SerializationTest.h"

using namespace pccetest;
using namespace pcce;
using namespace boost;
using namespace std;

typedef shared_ptr< ITest > tTestPtr;
typedef list< tTestPtr > tTestPtrList;

typedef set< string > tStringSet;
typedef map< string, string > tStringMap;

tInteractiveAction StringToInteractiveAction(const string& str) {
  if (str == "ask") {
    return INTERACTIVEACTION_ASK;
  } else if (str == "assume") {
    return INTERACTIVEACTION_ASSUMEPASSED;
  } else if (str == "skip") {
    return INTERACTIVEACTION_SKIP;
  } else {
    PCCE_THROW("Unrecognized interactive action (" + str + ")... Should be 'ask', 'assume', or 'skip'");
  }
}

void HandleSkip(const string& type, const string& testName, bool& lastSkipped, unsigned int& skippedCount) {
  if (!lastSkipped) {
    cout << endl;
  }
  cout << "Skipping " << type << " test " << testName << endl;
  ++skippedCount;
  lastSkipped = true;
}

void HandleFail(const string& testName, tStringMap& failedMap, const string& classDescr, const string& message) {
  stringstream s;
  
  s << classDescr;
  if (!(classDescr.empty() || message.empty())) {
    s << ": ";
  }
  s << message;
  
  failedMap[testName] = s.str();
  cout << "!!! " << s.str() << endl;
}

int main(int argc, char* argv[]) {
  tTestPtrList tests;
  tInteractiveAction interactiveAction;
  bool skipInteractive, enabledOnly(false), disabledOnly(false);
  tStringMap failedMap;
  unsigned int failedCount = 0;
  unsigned int skippedCount = 0;
  bool lastSkipped = true;
  tStringSet disabledSet;
  tStringSet enabledSet;
  
  cout << "Version Info:" << endl << GetPcceFullBuildInfoStr() << endl << endl;

  // Add program options
  {
    program_options::options_description desc("Allowed options");
    desc.add_options()
      ("help", "  Display this help message")
      ("interactive,i", program_options::value< string >()->default_value("ask"), "  How to handle interactive tests ('ask', 'assume', or 'skip')")
      ("disable,d", program_options::value< vector< string > >(), "  List of tests to disable")
      ("enable,e", program_options::value< vector< string > >(), "  List of test to enable")
    ;
    program_options::variables_map vm;
    try {
      program_options::store(program_options::parse_command_line(argc, argv, desc), vm);
    } catch (boost::program_options::error& e) {
      cout << e.what() << endl;
      return 1;
    } catch (std::exception& e) {
      cout << e.what() << endl;
      return 1;
    } catch (...) {
      cout << "Unknown error parsing commandline" << endl;
      return 1;
    }
    program_options::notify(vm);
    
    if (vm.count("help")) {
      cout << desc << "\n";
      return 1;
    }
    if (vm.count("disable")) {
      vector< string > v = vm["disable"].as< vector<string> >();
      for (vector< string >::const_iterator i = v.begin(); i != v.end(); ++i) {
        disabledSet.insert(*i);
      }
      disabledOnly = true;
    }
    if (vm.count("enable")) {
      vector< string > v = vm["enable"].as< vector<string> >();
      for (vector< string >::const_iterator i = v.begin(); i != v.end(); ++i) {
        enabledSet.insert(*i);
      }
      enabledOnly = true;
    }
    try {
      string s = vm["interactive"].as< string >();
      interactiveAction = StringToInteractiveAction(s);
    } catch (Exception& e) {
      cout << e.GetFullError() << endl;
      return 1;
    } catch (...) {
      cout << "Unknown exception" << endl;
      return 1;
    }
    skipInteractive = interactiveAction == INTERACTIVEACTION_SKIP;
    if (enabledOnly && disabledOnly) {
      cout << "Error: You cannot use both the enable and disable options." << endl << desc << endl;
      return 1;
    }
  }
  switch(interactiveAction) {
    case INTERACTIVEACTION_ASK:
      cout << "Asking user during interactive tests";
      break;
    case INTERACTIVEACTION_ASSUMEPASSED:
      cout << "Assuming interactive tests pass";
      break;
    case INTERACTIVEACTION_SKIP:
      cout << "Skipping interactive tests";
      break;
    default:
      PCCE_THROW("Unexpected interactive action");
  }
  cout << endl << endl;

  // Initialize the test list
  tests.push_back(tTestPtr(new VariantTest()));
  tests.push_back(tTestPtr(new StringUtilTest()));
  tests.push_back(tTestPtr(new LogTest()));
  tests.push_back(tTestPtr(new SingletonTest()));
  tests.push_back(tTestPtr(new EventTest()));
  tests.push_back(tTestPtr(new EventSystemTest()));
  tests.push_back(tTestPtr(new EventConnectionManagerTest()));
  tests.push_back(tTestPtr(new ProcessListTest()));
  tests.push_back(tTestPtr(new TimerTest()));
  tests.push_back(tTestPtr(new ThreadedSubSystemTest()));
  tests.push_back(tTestPtr(new PropertyBagTest()));
  tests.push_back(tTestPtr(new GameObjectTest()));
  tests.push_back(tTestPtr(new GameObjectDBTest()));
  tests.push_back(tTestPtr(new XmlTest()));
  tests.push_back(tTestPtr(new ResourceManagerTest()));
  tests.push_back(tTestPtr(new SoundUtilTest()));
  tests.push_back(tTestPtr(new OggStreamTest()));
  tests.push_back(tTestPtr(new SoundSystemTest()));
  tests.push_back(tTestPtr(new InputSubSystemTest()));
  tests.push_back(tTestPtr(new MathUtilTest()));
  tests.push_back(tTestPtr(new ConfigurationTest()));
  tests.push_back(tTestPtr(new ProtocceTest()));
  
  // Make sure enabled/disabled tests are valid
  {
    tStringSet bothSet = disabledSet;
    bothSet.insert(enabledSet.begin(), enabledSet.end());
    for (tTestPtrList::const_iterator i = tests.begin(); i != tests.end(); ++i) {
      tTestPtr test = *i;
      bothSet.erase(test->vTestName());
    }
    size_t count = bothSet.size();
    if (count > 0) {
      cout << "Error: Unrecognized test";
      if (count > 1) {
        cout << "s";
      }
      cout << " (";
      for (tStringSet::const_iterator i = bothSet.begin(); i != bothSet.end(); ++i) {
        if (i != bothSet.begin()) {
          cout << ", ";
        }
        cout << *i;
      }
      cout << ")" << endl;
      return 1;
    }
  }

  // Run the tests
  for (tTestPtrList::const_iterator i = tests.begin(); i != tests.end(); ++i) {
    tTestPtr test = *i;
    if (skipInteractive && test->vIsInteractiveTest()) {
      HandleSkip("interactive", test->vTestName(), lastSkipped, skippedCount);
      continue;
    }
    if (disabledOnly && (disabledSet.find(test->vTestName()) != disabledSet.end())) {
      HandleSkip("disabled", test->vTestName(), lastSkipped, skippedCount);
      continue;
    }
    if (enabledOnly && (enabledSet.find(test->vTestName()) == enabledSet.end())) {
      HandleSkip("disabled", test->vTestName(), lastSkipped, skippedCount);
      continue;
    }
    if (i != tests.begin()) {
      cout << endl;
    }
    cout << "Running test " << test->vTestName() << endl;
    try {
      test->RunTest(interactiveAction);
    } catch (Exception& e) {
      HandleFail(test->vTestName(), failedMap, "", e.GetFullError());
    } catch (Ogre::Exception& e) {
      HandleFail(test->vTestName(), failedMap, "Ogre::Exception", e.getFullDescription());
    } catch (CEGUI::Exception& e) {
      HandleFail(test->vTestName(), failedMap, "CEGUI::Exception", e.getMessage().c_str());
    } catch (OIS::Exception& e) {
      HandleFail(test->vTestName(), failedMap, "OIS::Exception", e.eText);
    } catch (std::exception& e) {
      HandleFail(test->vTestName(), failedMap, "std::exception", e.what());
    } catch (...) {
      HandleFail(test->vTestName(), failedMap, "Unknown exception", "");
    }
    cout << "Completed test " << test->vTestName() << endl;
    lastSkipped = false;
  }

  // Display the results
  failedCount = failedMap.size();
  
  cout << endl << "Result:" << endl;
  cout << skippedCount << " tests skipped" << endl;
  cout << tests.size() - skippedCount << " tests run" << endl;
  cout << tests.size() - skippedCount - failedCount << " successful" << endl;
  
  cout << failedCount << " failed";
  if (failedCount > 0) {
    cout << ":" << endl;
    for (tStringMap::const_iterator i = failedMap.begin(); i != failedMap.end(); ++i) {
      if (i != failedMap.begin()) {
        cout << endl;
      }
      cout << "  " << i->first << ":" << endl;
      cout << "    " << i->second << endl;
    }
  } else {
    cout << endl;
  }

  if (failedCount == 0) {
    return 0;
  } else {
    return 1;
  }
}
