/* Protocce Test
 * Variant Test class
 */

#include <iostream>
#include "Variant.h"
#include "PropertyBag.h"
#include "Exception.h"
#include "VariantTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void VariantTest::vDoRunTest() {
  bool success;

  Variant v;
  PCCE_CHECK(!v.HasValue(), "Variant should not have value");

  v.Clear();
  PCCE_CHECK(!v.HasValue(), "Variant still should not have value");

  success = false;
  try {
    v.Get();
  } catch (VariantEmptyException& e) {
    cout << "GetValue failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "GetValue should fail when there is no value");

  success = false;
  try {
    v.AsInt();
  } catch (VariantEmptyException& e) {
    cout << "GetValueAsInt failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "GetValueAsInt should fail when there is no value");

  v.Set(5);
  PCCE_CHECK(v.HasValue(), "Variant should have value");
  PCCE_CHECK(v.IsInt(), "Variant should have int value");

  PCCE_CHECK(v.AsInt() == 5, "GetValueAsInt should = 5");
  PCCE_CHECK(v.AsUnsignedInt() == 5, "GetValueAsUnsignedInt should = 5");
  PCCE_CHECK(v.AsString() == "5", "GetValueAsStr should = \"5\"");

  v.Set('6');
  PCCE_CHECK(v.IsChar(), "Variant should now have char");
  PCCE_CHECK(v.AsInt() == 6, "GetValueAsInt should now = 6");
  PCCE_CHECK(v.AsString() == "6", "GetValueAsStr should now = \"6\"");
  PCCE_CHECK(v.AsChar() == '6', "GetValueAsChar should now = '6'");
  
  v.Set(true);
  PCCE_CHECK(v.IsBool(), "Variant should now have bool");
  PCCE_CHECK(v.AsBool(), "AsBool should be true");
  PCCE_CHECK(v.AsString() == "1", "AsString should now = \"1\"");
  PCCE_CHECK(v.AsInt() == 1, "AsString should now = 1");
  PCCE_CHECK(v.AsBool(), "AsBool should still be true");
  
  v = false;
  PCCE_CHECK(v.IsBool(), "Variant should now have bool");
  PCCE_CHECK(!v.AsBool(), "AsBool should be false");
  PCCE_CHECK(v.AsString() == "0", "AsString should now = \"0\"");
  PCCE_CHECK(v.AsInt() == 0, "AsString should now = 0");
  PCCE_CHECK(!v.AsBool(), "AsBool should still be false");

  v.Set("This is a test");
  PCCE_CHECK(v.IsString(), "Variant should now have string");
  PCCE_CHECK(v.AsString() == "This is a test", "GetValueAsStr should = \"This is a test\"");
  success = false;
  try {
    v.AsInt();
  } catch (VariantConversionException& e) {
    cout << "Conversion failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "GetValueAsInt should fail on garbage string");

  unsigned long int i = 123;
  v = i;
  PCCE_CHECK(v.AsInt() == 123, "Variant should now have 123");
  PCCE_CHECK(v.AsUnsignedLongInt() == i, "Variant should = i");
  PCCE_CHECK(v.IsUnsignedLongInt(), "Variant should be unsigned long int");
  PCCE_CHECK(v.IsInt(), "Variant should support regular int");

  v.Set("This is a test");
  PCCE_CHECK(!v.IsUnsignedLongInt(), "Variant should not be unsigned long int");

  v.Set(i);
  PCCE_CHECK(v.AsInt() == 123, "Variant should now have 123");
  PCCE_CHECK(v.AsUnsignedLongInt() == i, "Variant should = i");
  PCCE_CHECK(v.IsUnsignedLongInt(), "Variant should be unsigned long int");
  
  PCCE_CHECK(!v.IsPropertyBagPtr(), "IsPropertyBagPtr should return false for Variant that is not tPropertyBagPtr");
  success = false;
  try {
    v.AsPropertyBagPtr();
  } catch (VariantConversionException& e) {
    cout << "Conversion failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "AsPropertyBagPtr should fail on unsigned long int");
  
  tPropertyBagPtr pb(new PropertyBag());
  pb->AddProperty("Foo");
  pb->SetValue("Foo", 123);
  PCCE_CHECK(pb->GetValueAsInt("Foo") == 123, "Property bag is not correct");
  v.Set(pb);
  PCCE_CHECK(v.IsPropertyBagPtr(), "IsPropertyBagPtr should return true");
  success = false;
  try {
    v.AsInt();
  } catch (VariantConversionException& e) {
    cout << "Conversion failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "AsInt should fail on tPropertyBagPtr");
};
