/* Protocce Test
 * Configuration Test class
 */

#include <iostream>
#include "Configuration.h"
#include "Exception.h"
#include "Variant.h"
#include "stringutil.h"
#include "ConfigurationTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

const string ConfigFile = DataPath + "TestConfig.ini";
const string InvalidFile = DataPath + "InvalidConfig.ini";
const string MissingFile = DataPath + "MissingConfig.ini";

void ConfigurationTest::vDoRunTest() {
  const int Test1Result = 123;
  const string Test2Result = "This is my Test";
  const tReal Test3Result = 14.6;
  
  tConfigurationPtr c(new Configuration());

  bool success = true;
  try {
    c->LoadINIFile(MissingFile);
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Missing config file was loaded successfully");

  success = false;
  try {
    c->LoadINIFile(InvalidFile);
  } catch (Exception& e) {
    cout << "Caught exception as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "Invalid config file was loaded successfully");
  
  c->LoadINIFile(ConfigFile);
  
  PCCE_CHECK(c->HasProperty("Foo"), "Foo section is missing");
  PCCE_CHECK(c->HasProperty("Bar"), "Bar section is missing");
  
  tPropertyBagPtr bag = c->GetConfigSection("Foo");
  int Test1;
  string Test2;
  tReal Test3;
  tVariantList v;
  
  Test1 = bag->GetValueAsInt("Test1");
  v.push_back(Test1);
  PCCE_CHECK(Test1 == Test1Result, Format("Test1 has the wrong value (%).", v));
  Test2 = bag->GetValueAsString("Test2");
  v.clear();
  v.push_back(Test2);
  PCCE_CHECK(Test2.compare(Test2Result) == 0, Format("Test2 has the wrong value (%).", v));
  Test3 = bag->GetValueAsReal("Test3");
  v.clear();
  v.push_back(Test3);
  PCCE_CHECK(Test3 == Test3Result, Format("Test3 has the wrong value, (%).", v));
  
  bag = c->GetConfigSection("Bar");
  
  Test2 = bag->GetValueAsString("abc");
  v.clear();
  v.push_back(Test2);
  PCCE_CHECK(Test2 == "def", Format("Bar.abc has the wrong value (%).", v));

  Test2 = bag->GetValueAsString("key");
  v.clear();
  v.push_back(Test2);
  PCCE_CHECK(Test2 == "value", Format("Bar.abc has the wrong value (%).", v));
  
  c->SaveINIFile("config.ini");
}
