/* Protocce Test
 * Event Connection Manager Test class
 */

#include <iostream>
#include "Exception.h"
#include "IEvent.h"
#include "EventSystem.h"
#include "EventConnectionManager.h"
#include "EventConnectionManagerTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

namespace {
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  
  PCCE_EVENT_CLASS(MyEvent) {
  public:
    static const std::string sGetEventName() { return "my event"; };
  };
  
  PCCE_EVENT_CLASS(MyOtherEvent) {
  public:
    static const std::string sGetEventName() { return "my other event"; };
  };
  
  unsigned int EventCount = 0;
  
  void HandleEvent(const tIEventPtr& event) {
    ++EventCount;
  }
}

void EventConnectionManagerTest::vDoRunTest() {
  PCCE_CHECK(EventCount == 0, "EventCount should be 0");

  EventSystemManager s;
  tIEventPtr event(new MyEvent());
  tIEventPtr otherEvent(new MyOtherEvent());

  EventSystemSingleton::Get()->DispatchEvent(event);
  PCCE_CHECK(EventCount == 0, "EventCount should still be 0");

  {
    boost::scoped_ptr< EventConnectionManager > cm(new EventConnectionManager());

    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(EventCount == 0, "EventCount should still be 0");

    // Make sure this doesn't raise
    cm->RemoveHandler(MyEvent::sGetEventType());

    cm->AddHandler(MyEvent::sGetEventType(), HandleEvent);

    bool success = false;
    try {
      cm->AddHandler(MyEvent::sGetEventType(), HandleEvent);
    } catch (Exception &e) {
      cout << "Adding second handler failed as expected ("
           << e.GetError() << ")" << endl;
      success = true;
    }
    PCCE_CHECK(success, "Adding duplicate handler did not fail as expected");

    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(EventCount == 1, "EventCount should be 1");

    EventSystemSingleton::Get()->DispatchEvent(otherEvent);
    PCCE_CHECK(EventCount == 1, "EventCount should still be 1");

    cm->AddHandler(MyOtherEvent::sGetEventType(), HandleEvent);

    EventSystemSingleton::Get()->DispatchEvent(otherEvent);
    PCCE_CHECK(EventCount == 2, "EventCount should be 2");

    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(EventCount == 3, "EventCount should be 3");

    cm->RemoveHandler(MyOtherEvent::sGetEventType());

    EventSystemSingleton::Get()->DispatchEvent(otherEvent);
    PCCE_CHECK(EventCount == 3, "EventCount should still be 3");
    EventSystemSingleton::Get()->DispatchEvent(event);
    PCCE_CHECK(EventCount == 4, "EventCount should be 4");
  }

  EventSystemSingleton::Get()->DispatchEvent(event);
  EventSystemSingleton::Get()->DispatchEvent(otherEvent);
  PCCE_CHECK(EventCount == 4, "EventCount should still be 4");
}

