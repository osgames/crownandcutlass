/* Protocce Test
 * Log Test class
 */

#include <string>
#include <iostream>
#include "Exception.h"
#include "IResource.h"
#include "IResourceFactory.h"
#include "XmlNode.h"
#include "Variant.h"
#include "ResourceManager.h"
#include "ResourceManagerTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;
using namespace boost;

class TestResource: public IResource {
public:
  TestResource(const std::string name): IResource(name), mInfo("") {
  }

  void SetInfo(const string& info) {
    mInfo = info;
  }
  const string& GetInfo() {
    return mInfo;
  }

  static bool IsLoaded() {
    return sTestLoaded;
  }

protected:
  bool vDoLoad() {
    sTestLoaded = true;
    return true;
  }

  bool vDoUnload() {
    sTestLoaded = false;
    return true;
  }

  std::string vGetType() { return "TestResource"; };

private:
  static bool sTestLoaded;
  string mInfo;
};
bool TestResource::sTestLoaded = false;

class TestResourceFactory: public IResourceFactory {
public:
  tIResourcePtr vCreateResource(const string name, const string resourceType) {
    PCCE_CHECK(resourceType == "TestResource", "Incorrect resource type");
    tIResourcePtr result(new TestResource(name));
    return result;
  }

  tIResourcePtr vCreateResource(const XmlNode* node) {
    tIResourcePtr result(new TestResource(node->GetAttribute("name").AsString()));
    intrusive_ptr< TestResource > p =dynamic_pointer_cast< TestResource >(result);
    p->SetInfo(node->GetAttribute("info").AsString());
    return result;
  }
};

typedef ScopedSingleton< ResourceManagerSingleton > RM;

void ResourceManagerTest::vDoRunTest() {
  bool success;
  {
    RM rm;
    ResourceManagerSingleton::Get()->SanityCheck();
  }
  RM rm;
  ResourceManagerSingleton::Get()->SanityCheck();

  tIResourceFactoryPtr factory(new TestResourceFactory());
  ResourceManagerSingleton::Get()->RegisterResourceFactory("TestResource", factory);
  ResourceManagerSingleton::Get()->SanityCheck();

  success = false;
  try {
    ResourceManagerSingleton::Get()->CreateResource("", "", "");
  } catch (Exception& e) {    
    cout << "CreateResource failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "CreateResource succeeded with empty strings");

  success = false;
  try {
    ResourceManagerSingleton::Get()->CreateResource("", "a", "");
  } catch (Exception& e) {    
    cout << "CreateResource failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "CreateResource succeeded with blank type");

  success = false;
  try {
    ResourceManagerSingleton::Get()->CreateResource("a", "a", "");
  } catch (Exception& e) {    
    cout << "CreateResource failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "CreateResource succeeded with invalid type");

  success = false;
  try {
    ResourceManagerSingleton::Get()->CreateResource("TestResource", "", "");
  } catch (Exception& e) {    
    cout << "CreateResource failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "CreateResource succeeded with valid type, but empty strings");

  tIResourcePtr r;
  intrusive_ptr< TestResource > p;
  r = ResourceManagerSingleton::Get()->CreateResource("TestResource", "a", "");
  p = dynamic_pointer_cast< TestResource >(r);
  p->SetInfo("This is a test");
  p = NULL;
  r = NULL;

  r = ResourceManagerSingleton::Get()->GetResource("a");
  PCCE_CHECK(r->GetName() == "a", "Resource name is not correct");
  p = dynamic_pointer_cast< TestResource >(r);
  PCCE_CHECK(p->IsLoaded(), "Resource should be loaded");
  PCCE_CHECK(p->GetInfo() == "This is a test", "Resource data is incorrect");
  r->Load();
  r->Load();
  PCCE_CHECK(p->IsLoaded(), "Resource should still be loaded");
  p = NULL;
  r = NULL;

  PCCE_CHECK(!TestResource::IsLoaded(), "Resource should have been unloaded");
  ResourceManagerSingleton::Get()->LoadResourceGroup("");
  PCCE_CHECK(TestResource::IsLoaded(), "Resource should have been loaded again");
  ResourceManagerSingleton::Get()->UnloadResourceGroup("");
  PCCE_CHECK(!TestResource::IsLoaded(), "Resource should have been unloaded again");

  r = ResourceManagerSingleton::Get()->GetResource("a");
  PCCE_CHECK(TestResource::IsLoaded(), "GetResource should have loaded it");
};
