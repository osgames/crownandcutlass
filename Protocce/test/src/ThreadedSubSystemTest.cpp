/* Protocce Test
 * ThreadedSubSystem Test class
 */

#include <iostream>
#include <boost/bind.hpp>
#include "IThreadedSubSystem.h"
#include "Exception.h"
#include "Singleton.h"
#include "EventSystem.h"
#include "EventConnectionManager.h"
#include "ThreadExceptionEvent.h"
#include "Timer.h"
#include "PropertyBag.h"
#include "ThreadedSubSystemTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

namespace {
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  
  PCCE_EVENT_CLASS(TestEvent) {
  public:
    TestEvent(unsigned int value) { mValue = value; };
    
    unsigned int GetValue() { return mValue; };
  
    static const std::string sGetEventName() { return "my test event"; };
  private:
    unsigned int mValue;
  };
  
  PCCE_EVENT_CLASS(ReturnEvent) {
  public:
    static const std::string sGetEventName() { return "return event"; };
  };
  
  PCCE_EVENT_CLASS(ErrorEvent) {
  public:
    static const std::string sGetEventName() { return "error event"; };
  };
  
  unsigned int ReturnCount = 0;
  
  void HandleReturnEvent(const tIEventPtr& event) {
    if (ReturnEvent::sGetEventType() == event->vGetEventType()) {
      ReturnCount++;
    }
  }
  
  
  class TestBlockingSubSystem: public IThreadedSubSystem {
  private:
    boost::mutex mLoopMutex;
    boost::mutex mEventMutex;
    static unsigned int sTotalValue;
    unsigned int mLoopCount;
    
  public:
    TestBlockingSubSystem(): IThreadedSubSystem(), mLoopCount(0) { }
    
    const string vGetSubSystemName() { return "TestGet"; };
    static unsigned int GetTotalValue() { return sTotalValue; };
    
    unsigned int GetLoopCount() {
      boost::mutex::scoped_lock lock(mLoopMutex);
      return mLoopCount;
    }
    
  private:
    void HandleErrorEvent(const tIEventPtr& event) {
      PCCE_THROW("Thread exception");
    }
    void HandleTestEvent(const tIEventPtr& event) {
      boost::shared_ptr<TestEvent> e = boost::dynamic_pointer_cast<TestEvent>(event);
      sTotalValue += e->GetValue();
      tIEventPtr newEvent(new ReturnEvent());
      EventSystemSingleton::Get()->QueueEvent(newEvent);
    }
    
  protected:
    void vInitializeThread(tPropertyBagPtr configSettings) {
      cout << "Thread started" << endl;
      
      AddThreadHandler(TestEvent::sGetEventType(), bind(&TestBlockingSubSystem::HandleTestEvent, this, _1));
      AddThreadHandler(ErrorEvent::sGetEventType(), bind(&TestBlockingSubSystem::HandleErrorEvent, this, _1));
    }
    
    void vRunThread() {
      while (IsRunning()) {
        {
          boost::mutex::scoped_lock lock(mLoopMutex);
          mLoopCount++;
        }
        DispatchThreadEventBlocking();
      }
    }
  
    void vShutdownThread() {
      RemoveThreadHandler(ErrorEvent::sGetEventType());
      RemoveThreadHandler(TestEvent::sGetEventType());
      
      cout << "Thread stopping" << endl;
    }
  };
  
  unsigned int TestBlockingSubSystem::sTotalValue = 0;
  
  class TestListSubSystem: public IThreadedSubSystem {
  private:
    boost::mutex mLoopMutex;
    boost::mutex mEventCountMutex;
    static unsigned int sTotalValue;
    unsigned int mLoopCount;
    unsigned int mEventCount;
    
  public:
    TestListSubSystem(): IThreadedSubSystem(), mLoopCount(0), mEventCount(0) { }
    
    const string vGetSubSystemName() { return "TestList"; };
    static unsigned int GetTotalValue() { return sTotalValue; };
  
    unsigned int GetLoopCount() {
      boost::mutex::scoped_lock lock(mLoopMutex);
      return mLoopCount;
    }
  
    unsigned int GetEventCount() {
      boost::mutex::scoped_lock lock(mEventCountMutex);
      return mEventCount;
    }
    
  private:
    void HandleTestEvent(const tIEventPtr& event) {
      {
        boost::mutex::scoped_lock lock(mEventCountMutex);
        mEventCount++;
      }
      boost::shared_ptr<TestEvent> e = boost::dynamic_pointer_cast<TestEvent>(event);
      sTotalValue += e->GetValue();
      tIEventPtr newEvent(new ReturnEvent());
      EventSystemSingleton::Get()->QueueEvent(newEvent);
    }
    
  protected:
    void vInitializeThread(tPropertyBagPtr configSettings) {
      cout << "Thread started" << endl;
      
      AddThreadHandler(TestEvent::sGetEventType(), bind(&TestListSubSystem::HandleTestEvent, this, _1));
    }
    
    void vRunThread() {
      while (IsRunning()) {
        {
          boost::mutex::scoped_lock lock(mLoopMutex);
          mLoopCount++;
        }
        DispatchThreadEvents();
      }
    }
    
    void vShutdownThread() {
      RemoveThreadHandler(TestEvent::sGetEventType());
  
      cout << "Thread stopping" << endl;
    }
  };
  
  unsigned int TestListSubSystem::sTotalValue = 0;
}


void ThreadedSubSystemTest::vDoRunTest() {
  bool success;
  tPropertyBagPtr configSection(new PropertyBag());
  tIEventPtr event;
  EventSystemManager m;
  tEventConnection ExceptionConnection = EventSystemSingleton::Get()->AddHandler(
    ThreadExceptionEvent::sGetEventType(), HandleThreadExceptionEvent);
  
  // Make sure have everything set up correctly
  tIEventPtr e(new ThreadExceptionEvent("My error message", "MyFile.cpp", 10));
  success = true;
  try {
    EventSystemSingleton::Get()->DispatchEvent(e);
    success = false;
  } catch (Exception& e) {
    PCCE_CHECK(
      e.GetFullError() == "MyFile.cpp (10): My error message",
      "ThreadExceptionEvent did not result in expected exception");
  }
  PCCE_CHECK(success, "ThreadExceptionEvent did not result in an exception");
  
  tEventConnection ReturnConnection = EventSystemSingleton::Get()->AddHandler(
    ReturnEvent::sGetEventType(), HandleReturnEvent);
  
  // The environment seems to be set up correctly, now move on to actual tests
  {
    boost::shared_ptr< TestBlockingSubSystem > system(new TestBlockingSubSystem());

    // Startup / shutdown test
    system->vInitialize(configSection);
    SleepMs(500);
    system->vShutdown();

    // Make sure we can pass data back and forth
    system->vInitialize(configSection);
    SleepMs(500);
    event.reset(new TestEvent(10));
    EventSystemSingleton::Get()->QueueEvent(event);
    event.reset(new TestEvent(77));
    EventSystemSingleton::Get()->QueueEvent(event);
    EventSystemSingleton::Get()->DispatchQueue();
    SleepMs(500);
    PCCE_CHECK(system->GetTotalValue() == 87, "Value not added");
    PCCE_CHECK(EventSystemSingleton::Get()->GetQueuedEventCount() == 2, "Return event not queued");
    EventSystemSingleton::Get()->DispatchQueue();
    PCCE_CHECK(EventSystemSingleton::Get()->GetQueuedEventCount() == 0, "Queue not empty");
    PCCE_CHECK(system->GetTotalValue() == 87, "Value added unexpectedly");
    PCCE_CHECK(ReturnCount == 2, "Return listener receive count should be 2");
    SleepMs(500);
    event.reset(new TestEvent(10));
    EventSystemSingleton::Get()->QueueEvent(event);
    event.reset(new TestEvent(77));
    EventSystemSingleton::Get()->QueueEvent(event);
    EventSystemSingleton::Get()->DispatchQueue();
    SleepMs(500);
    cout << "Loop count: " << system->GetLoopCount() << endl;
    PCCE_CHECK(system->GetLoopCount() == 6, "TestGetSubSystem looped more than expected");
    system->vShutdown();

    // Test throwing exception
    system->vInitialize(configSection);
    SleepMs(500);
    event.reset(new ErrorEvent());
    EventSystemSingleton::Get()->QueueEvent(event);
    EventSystemSingleton::Get()->DispatchQueue();
    SleepMs(500);
    PCCE_CHECK(EventSystemSingleton::Get()->GetQueuedEventCount() == 1, "Thread exception event not queued");
    success = true;
    try {
      EventSystemSingleton::Get()->DispatchQueue();
      success = false;
    } catch (Exception& e) {
      cout << e.GetError() << " from line " << e.GetLineNumber() << endl;
      PCCE_CHECK(e.GetError() == "TestGet: Thread exception", "Unexpected exception");
    }
    PCCE_CHECK(success, "Thread exception was lost");
    system->vShutdown();
  }
  
  {
    cout << endl << "List test:" << endl;
    boost::shared_ptr< TestListSubSystem > system(new TestListSubSystem());
    system->vInitialize(configSection);
    SleepMs(100);
    event.reset(new TestEvent(10));
    EventSystemSingleton::Get()->QueueEvent(event);
    event.reset(new TestEvent(77));
    EventSystemSingleton::Get()->QueueEvent(event);
    EventSystemSingleton::Get()->DispatchQueue();
    SleepMs(100);
    PCCE_CHECK(system->GetTotalValue() == 87, "Value not added");
    PCCE_CHECK(EventSystemSingleton::Get()->GetQueuedEventCount() == 2, "Return event not queued");
    EventSystemSingleton::Get()->DispatchQueue();
    PCCE_CHECK(EventSystemSingleton::Get()->GetQueuedEventCount() == 0, "Queue not empty");
    PCCE_CHECK(system->GetTotalValue() == 87, "Value added unexpectedly");
    PCCE_CHECK(ReturnCount == 6, "Return listener receive count should be 6");
    SleepMs(500);
    event.reset(new TestEvent(10));
    EventSystemSingleton::Get()->QueueEvent(event);
    event.reset(new TestEvent(77));
    EventSystemSingleton::Get()->QueueEvent(event);
    EventSystemSingleton::Get()->DispatchQueue();
    SleepMs(100);
    system->vShutdown();
    
    unsigned int value;
    value = system->GetLoopCount();
    cout << "Loop count: " << value << endl;
    PCCE_CHECK(value > 6, "TestListSubSystem looped less than expected");
    
    value = system->GetEventCount();
    cout << "Event count: " << value << endl;
    PCCE_CHECK(value == 4, "TestListSubSystem did not receive right number of events");
  }
  
  ReturnConnection.disconnect();
}
