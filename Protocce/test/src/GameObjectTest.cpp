/* Protocce Test
 * GameObject Test class
 */

#include "GameObject.h"
#include "Exception.h"
#include "Variant.h"
#include "GameObjectTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

void GameObjectTest::vDoRunTest() {  
  auto_ptr< GameObject > go(new GameObject(1));
  PCCE_CHECK(go->GetID() == 1, "GameObject has the wrong ID");
  
  go->AddProperty("test");
  PCCE_CHECK(go->HasProperty("test"), "GameObject doesn't have Property 'Test'");
  go->SetValue("test", 14);
  PCCE_CHECK(go->HasValue("test"), "'Test' value should not be empty");
  PCCE_CHECK(go->GetValueAsInt("test") == 14, "'Test' has the wrong value");
  
  go->SetMaterialName("Brick");
  go->SetRenderModelName("brick.mesh");
  go->SetPhysicsModelName("brick.phys");
  
  PCCE_CHECK(go->GetValueAsString("MaterialName") == "Brick", "Material name did not set correctly");
  PCCE_CHECK(go->GetValueAsString("RenderModelName") == "brick.mesh", "Render Model name did not set correctly");
  PCCE_CHECK(go->GetValueAsString("PhysicsModelName") == "brick.phys", "Physics Model name did not set correctly");
  
  PCCE_CHECK(go->IsRenderReady(), "Game object should be render ready");
  PCCE_CHECK(go->IsPhysicsReady(), "Game object should be physics ready");
};
