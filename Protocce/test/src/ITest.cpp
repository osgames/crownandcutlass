/* Protocce Test
 * Base Test class
 */

#include <iostream>
#include "Exception.h"
#include "ITest.h"

using namespace std;
using namespace pcce;
using namespace pccetest;

ITest::ITest(): mVerifyResultCalled(false) {
};

void ITest::RunTest(const tInteractiveAction interactiveAction) {
  if (vIsPostInitTest() != false/*ProtocceSingleton::IsInitialized()*/) {
    PCCE_THROW("Error: Test / Engine init mismatch");
  }
  PCCE_CHECK(
    (not vIsInteractiveTest()) or (interactiveAction != INTERACTIVEACTION_SKIP),
    "Interactive tests should be skipped if action = INTERACTIVE_SKIP!");
  mInteractiveAction = interactiveAction;
  
  vDoRunTest();
  PCCE_CHECK(vIsInteractiveTest() == mVerifyResultCalled, "Interactive test did not call VerifyResult()");
}

void ITest::VerifyResult(const std::string& question, const bool expectedAnswer) {
  PCCE_CHECK(vIsInteractiveTest(), "Non-interactive Test calling VerifyResult");
  mVerifyResultCalled = true;
  
  if (mInteractiveAction != INTERACTIVEACTION_ASK) {
    cout << question << "? (assumed correct)" << endl;
  } else {
    char c;
    cout << question << "? (y/N) ";
    cin >> c;
    bool answer = (c == 'y') || (c == 'Y');
    PCCE_CHECK(answer == expectedAnswer, question);
  }
}
