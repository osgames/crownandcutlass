/* Protocce Test
 * Sound Util Test class
 */

#include <iostream>
#include <memory>
#include <alc.h>
#include "soundutil.h"
#include "Exception.h"
#include "util.h"
#include "SoundUtilTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

class ALWrapper {
public:
  ALWrapper() {
    InitializeAL();
  }
  ~ALWrapper() {
    ShutdownAL();
  }
};
typedef auto_ptr< ALWrapper > tALWrapperPtr;

class BufferWrapper {
public:
  BufferWrapper(ALuint buffer) {
    mBuffer = buffer;
  }
  ~BufferWrapper() {
    alDeleteBuffers(1, &mBuffer);
  }
  ALuint GetBuffer() {
    return mBuffer;
  }
private:
  ALuint mBuffer;
};
typedef auto_ptr< BufferWrapper > tBufferWrapperPtr;

class SourceWrapper {
public:
  SourceWrapper(ALuint source) {
    mSource = source;
  }
  ~SourceWrapper() {
    alSourceStop(mSource);
    alDeleteSources(1, &mSource);
  }
  ALuint GetSource() {
    return mSource;
  }
private:
  ALuint mSource;
};
typedef auto_ptr< SourceWrapper > tSourceWrapperPtr;

void SoundUtilTest::vDoRunTest() {
  const string oggFileName = DataPath + "test.ogg";
  const string wavFileName = DataPath + "test.wav";
  ALuint buffer;
  bool success;

  VerifyResult("Are your speakers plugged in and turned up", true);

  success = false;
  try {
    buffer = LoadOgg("invalid.name");
  } catch (Exception& e) {
    cout << "LoadOgg failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "LoadOgg successful on invalid filename");

  success = false;
  try {
    buffer = LoadWav("invalid.name");
  } catch (Exception& e) {
    cout << "LoadWav failed as expected (" << e.GetError() << ")" << endl;
    success = true;
  }
  PCCE_CHECK(success, "LoadWav successful on invalid filename");
  
  InitializeAL();
  CheckALError("after initialization");
  ShutdownAL();

  tALWrapperPtr w(new ALWrapper());
  CheckALError("after wrapper initialization");
  
  /*
  ALCcontext* context = alcGetCurrentContext();
  PCCE_CHECK(context != NULL, "alcGetCurrentContext() returned NULL");

  ALCdevice* device = alcGetContextsDevice(context);
  PCCE_CHECK(device != NULL, "alcGetContextsDevice() returned NULL");
  PCCE_CHECK(alcGetError(device) == ALC_NO_ERROR, "AL device has error");
  */
  {
    CheckALError("before loading files");

    success = false;
    try {
      buffer = LoadOgg(wavFileName);
    } catch (Exception& e) {
      cout << "LoadOgg failed as expected (" << e.GetError() << ")" << endl;
      success = true;
    }
    PCCE_CHECK(success, "LoadOgg successful on wav file");
    CheckALError("after failed ogg load");

    success = false;
    try {
      buffer = LoadWav(oggFileName);
    } catch (Exception& e) {
      cout << "LoadWav failed as expected (" << e.GetError() << ")" << endl;
      success = true;
    }
    PCCE_CHECK(success, "LoadWav successful on ogg file");
    CheckALError("after failed wav load");

    buffer = AL_NONE;
    buffer = LoadOgg(oggFileName);
    CheckALError("after first ogg load");
    PCCE_CHECK(buffer != AL_NONE, "LoadOgg return AL_NONE");
    tBufferWrapperPtr b(new BufferWrapper(buffer));
    CheckALError("after creating buffer");

    tSourceWrapperPtr s(new SourceWrapper(GenSource(b->GetBuffer())));
    CheckALError("after creating source");
    PCCE_CHECK(s->GetSource() != AL_NONE, "GenSource return AL_NONE");

    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"ogg test\"", true);
    CheckALError("after first play");
    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"ogg test\" again", true);
    CheckALError("after second play");

    s.reset();
    CheckALError("after deleting source");
    b.reset();
    CheckALError("after deleting buffer");
    
    b.reset(new BufferWrapper(LoadOgg(oggFileName)));
    CheckALError("after reloading ogg");
    s.reset(new SourceWrapper(GenSource(b->GetBuffer())));
    CheckALError("after creating next source");

    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"ogg test\" a third time", true);
    CheckALError("after third play");

    buffer = AL_NONE;
    buffer = LoadWav(wavFileName);
    CheckALError("after loading wav");
    b.reset(new BufferWrapper(buffer));
    PCCE_CHECK(b->GetBuffer() != AL_NONE, "LoadWav return AL_NONE");
  
    s.reset(new SourceWrapper(GenSource(b->GetBuffer())));
    PCCE_CHECK(s->GetSource() != AL_NONE, "GenSource return AL_NONE");

    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"wave test\"", true);
    CheckALError("after playing wav");
    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"wave test\" again", true);
    CheckALError("after playing wav twice");

    s.reset();
    CheckALError("after deleting wav source");
    b.reset(new BufferWrapper(LoadWav(wavFileName)));
    CheckALError("after reloading wav");
    s.reset(new SourceWrapper(GenSource(b->GetBuffer())));
    CheckALError("after recreating wav source");

    alSourcePlay(s->GetSource());
    VerifyResult("Did you hear \"wave test\" a third time", true);
    CheckALError("after playing wav third time");

    s.reset();
    b.reset();
    CheckALError("after deleting source and buffer");
  }
};

void SoundUtilTest::CheckALError(const std::string& s) {
  switch(alGetError()) {
    case AL_NO_ERROR:
      return;
    case AL_INVALID_NAME:
      PCCE_THROW("AL error " + s + ": a bad name (ID) was passed to an OpenAL function");
    case AL_INVALID_ENUM:
      PCCE_THROW("AL error " + s + ": an invalid enum value was passed to an OpenAL function");
    case AL_INVALID_VALUE:
      PCCE_THROW("AL error " + s + ": an invalid value was passed to an OpenAL function");
    case AL_INVALID_OPERATION:
      PCCE_THROW("AL error " + s + ": the requested operation is not valid");
    case AL_OUT_OF_MEMORY:
      PCCE_THROW("AL error " + s + ": the requested operation resulted in OpenAL running out of memory");
    default:
      PCCE_THROW("AL error " + s + ": unknown error");
  }
}
