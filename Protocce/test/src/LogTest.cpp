/* Protocce Test
 * Log Test class
 */

#include <iostream>
#include <fstream>
#include <string>
#include "Log.h"
#include "Variant.h"
#include "Exception.h"
#include "LogTest.h"

using namespace pcce;
using namespace pccetest;
using namespace std;

typedef ScopedSingleton< LogSingleton > LogManager;

void LogTest::vDoRunTest() {
  const string fileName("Test.log");
  {
    LogManager m;
    
    PCCE_CHECK(LogSingleton::IsInitialized(), "LogSingleton should be initialized");
    
    LogSingleton::Get()->SetLogLevel(llDebug);
  
    LogSingleton::Get()->SetCoutEnabled(true);
    LogSingleton::Get()->LogMessage("This is a debug test", mlDebug);
    LogSingleton::Get()->LogMessage("This is a quiet test", mlQuiet);    
  
    LogSingleton::Get()->SetFileName(fileName);
    LogSingleton::Get()->LogMessage("This is another test", mlDebug);
    LogSingleton::Get()->LogMessage("This is a loud test", mlLoud);
  
    tVariantList v;
    v.push_back(1);
    LogSingleton::Get()->LogMessageFmt("This is % more test", v, mlDebug);
    
    LogSingleton::Get()->SetLogLevel(llQuiet);
    
    LogSingleton::Get()->LogMessage("This is a debug test", mlDebug);
    LogSingleton::Get()->LogMessage("This is a loud test", mlLoud);
    LogSingleton::Get()->LogMessage("This is a quiet test", mlQuiet);
  }

  ifstream s(fileName.c_str(), ios::in);
  PCCE_CHECK(s, "Could not open log to read it back in");

  string line;
  getline(s, line);
  PCCE_CHECK(line == "This is another test", "Line 1 of log is incorrect");

  getline(s, line);
  PCCE_CHECK(line == "This is a loud test", "Line 2 of log is incorrect");

  getline(s, line);
  PCCE_CHECK(line == "This is 1 more test", "Line 3 of log is incorrect");
  
  getline(s, line);
  PCCE_CHECK(line == "This is a quiet test", "Line 4 of log is incorrect");

  if (!s.eof()) {
    getline(s, line);
    PCCE_CHECK(line == "", "Line is not empty");
    PCCE_CHECK(s.eof(), "Log is not at end of file");
  }
};
