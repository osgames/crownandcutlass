/* Protocce Test
 * ThreadedSubSystem Test class
 */

#include <string>
#include <sstream>
#include <iostream>

#include <OIS/OIS.h>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

#include "PropertyBag.h"
#include "InputSubSystem.h"
#include "RequestWindowHandleEvent.h"
#include "ISubSystem.h"
#include "EventSystem.h"
#include "Log.h"
#include "InputTranslator.h"
#include "UpdateSubSystemEvent.h"
#include "InputSubSystemTest.h"

#if defined OIS_WIN32_PLATFORM
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#elif defined OIS_APPLE_PLATFORM
#include <Carbon/Carbon.h>
#else
#include <X11/Xlib.h>
#endif

using namespace std;
using namespace OIS;
using namespace boost;
using namespace pcce;
using namespace pccetest;

namespace {
  PCCE_EVENT_CLASS(KeyboardInputEvent) {
  private:
    OIS::KeyCode mKey;
    pcce::tKeyState mKeyState;
  public:
    KeyboardInputEvent(const OIS::KeyCode key_code, const pcce::tKeyState state): mKey(key_code), mKeyState(state) {};
    static const std::string sGetEventName() { return "keyboard input"; };
    
    const OIS::KeyCode GetKey() { return mKey; };
    const pcce::tKeyState GetState() { return mKeyState; }; 
  };

  PCCE_EVENT_CLASS(MouseClickEvent) {
  private:
    OIS::MouseButtonID mButtonId;
    const OIS::MouseState &mMouseState;
    pcce::tKeyState mKeyState;
  public:
    MouseClickEvent(const OIS::MouseState &mouse_state, const OIS::MouseButtonID mouse_button_id, const pcce::tKeyState state): 
      mButtonId(mouse_button_id), mMouseState(mouse_state), mKeyState(state) {};
    static const std::string sGetEventName() { return "mouse click"; };

    const OIS::MouseState& GetMouseState() { return mMouseState; };
    const OIS::MouseButtonID& GetMouseButtonId() { return mButtonId; };
    const pcce::tKeyState& GetButtonState() { return mKeyState; };
  };

  PCCE_EVENT_CLASS(MouseMoveEvent) {
  private:
    const OIS::MouseState &mMouseState;
  public:
    MouseMoveEvent(const OIS::MouseState &mouse_state): mMouseState(mouse_state) {};
    static const std::string sGetEventName() { return "mouse move"; };

    const OIS::MouseState& GetMouseState() { return mMouseState; };
  };

  class WindowHandler {
  public:
    WindowHandler() {
      const unsigned int WindowSize = 100;

      #if defined OIS_WIN32_PLATFORM
      //Create a capture window for Input Grabbing
      mHWnd = CreateWindowEx(
        WS_EX_APPWINDOW,
        "EDIT",
        "",
        WS_VISIBLE,
        0,
        0,
        WindowSize,
        WindowSize,
        NULL,
        NULL,
        NULL,
        NULL);
      if (mHWnd == NULL) {
        DWORD error = GetLastError();
        PCCE_THROW(Variant(error).AsString());
      }
      SendMessage(mHWnd, WM_INITDIALOG, NULL, NULL);

      Update();

      ShowWindow(mHWnd, SW_SHOW);
      
      #elif defined OIS_APPLE_PLATFORM
      // Create OSX carbon windows
      WindowAttributes attr = kWindowStandardDocumentAttributes
        | kWindowStandardHandlerAttribute
        | kWindowInWindowMenuAttribute;
      Rect r;
      SetRect(&r, 0, 0, WindowSize, WindowSize);
      CreateNewWindow(kDocumentWindowClass, attr, &r, &mWin);
      RepositionWindow(mWin, NULL, kWindowCenterOnMainScreen);
      InstallStandardEventHandler(GetWindowEventTarget(mWin));
      
      ProcessSerialNumber process = { 0, kCurrentProcess };
      TransformProcessType(&process, kProcessTransformToForegroundApplication);
      SetFrontProcess(&process);
      
      ShowWindow(mWin);
      SelectWindow(mWin);
      
      #else
      //Connects to default X window
      PCCE_CHECK(xDisp = XOpenDisplay(0), "Could not open X display");
      //Create a window
      xWin = XCreateSimpleWindow(xDisp,DefaultRootWindow(xDisp), 0,0, WindowSize, WindowSize, 0, 0, 0);
      //bind our connection to that window
      XMapWindow(xDisp, xWin);
      //Select what events we want to listen to locally
      XSelectInput(xDisp, xWin, StructureNotifyMask);
      
      XEvent evtent;
      do {
        XNextEvent(xDisp, &evtent);
      } while(evtent.type != MapNotify);
      #endif
      
      mConn = EventSystemSingleton::Get()->AddHandler(
        RequestWindowHandleEvent::sGetEventType(),
        bind(&WindowHandler::HandleWindowRequest, this, _1));
    }
    ~WindowHandler() {
      mConn.disconnect();

      #if defined OIS_WIN32_PLATFORM
      ShowWindow(mHWnd, SW_HIDE);
      DestroyWindow(mHWnd);
      #elif defined OIS_APPLE_PLATFORM
      DisposeWindow(mWin);
      #else
      // Be nice to X and clean up the x window
      XDestroyWindow(xDisp, xWin);
      XCloseDisplay(xDisp);
      #endif
    }

    void Update() {
      #if defined OIS_WIN32_PLATFORM
      MSG msg;
      while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
      #elif defined OIS_APPLE_PLATFORM
      EventRef osxEvent = NULL;
      EventTargetRef target = GetEventDispatcherTarget();
      if (ReceiveNextEvent(0, NULL, kEventDurationNoWait, true, &osxEvent) == noErr) {
        SendEventToEventTarget(osxEvent, target);
        ReleaseEvent(osxEvent);
      }
      #else
      #endif
    }

    Variant Handle() {
      #if defined OIS_WIN32_PLATFORM
      return reinterpret_cast< size_t >(mHWnd);
      #elif defined OIS_APPLE_PLATFORM
      return reinterpret_cast< size_t >(mWin);
      #else
      return xWin;
      #endif
    }

    void HandleWindowRequest(const tIEventPtr& e) {
      shared_ptr< RequestWindowHandleEvent > event = dynamic_pointer_cast< RequestWindowHandleEvent >(e);
      event->WindowHandle = Handle();
    }

  private:
    tEventConnection mConn;

    #if defined OIS_WIN32_PLATFORM
    HWND mHWnd;
    #elif defined OIS_APPLE_PLATFORM
    WindowRef mWin;
    #else
    Display *xDisp;
    Window xWin;
    #endif
  };

}

void InputSubSystemTest::vDoRunTest() {
  typedef ScopedSingleton< EventSystemSingleton > EventSystemManager;
  typedef ScopedSingleton< LogSingleton > LogManager;

  EventSystemManager esm;
  LogManager l;
  
  LogSingleton::Get()->SetCoutEnabled(true);
  
  boost::shared_ptr< UpdateSubSystemEvent > updateEvent(new UpdateSubSystemEvent(0));
  
  mKeysPressed = 0;
  mMouseClickEvents = 0;
  mMouseMoveEvents = 0;
  
  EventSystemSingleton::Get()->AddHandler(
    KeyboardInputEvent::sGetEventType(),
    bind(&InputSubSystemTest::HandleKeyboardEvent, this, _1));
  EventSystemSingleton::Get()->AddHandler(
    MouseMoveEvent::sGetEventType(),
    bind(&InputSubSystemTest::HandleMouseMoveEvent, this, _1));
  EventSystemSingleton::Get()->AddHandler(
    MouseClickEvent::sGetEventType(),
    bind(&InputSubSystemTest::HandleMouseClickEvent, this, _1));

  {
    WindowHandler window;
    tISubSystemPtr inputSubSystem(new InputSubSystem());
    tPropertyBagPtr pBag(new PropertyBag());
  
    inputSubSystem->vInitialize(pBag);
  
    cout << "InputSubSystem Initialized.\n";
  
    tInputTranslatorPtr translator(new InputTranslator());
    for (OIS::KeyCode i = KC_UNASSIGNED; i <= KC_MEDIASELECT; i = static_cast< OIS::KeyCode >(i + 1)) {
      translator->AddKeyHandler(i, ksUp, bind(&InputSubSystemTest::CreateKeyEvent, this, _1, _2));
      translator->AddKeyHandler(i, ksDown, bind(&InputSubSystemTest::CreateKeyEvent, this, _1, _2));
    }
    SetActiveInputTranslator(translator);

    cout << "Keyboard Test: Please press and release 5 keys on the keyboard.\n";
    while (mKeysPressed < 10) {
      window.Update();
      EventSystemSingleton::Get()->DispatchEvent(updateEvent);
      EventSystemSingleton::Get()->DispatchQueue();
    }

    for (OIS::KeyCode i = KC_UNASSIGNED; i <= KC_MEDIASELECT; i = static_cast< OIS::KeyCode >(i + 1)) {
      translator->RemoveKeyHandler(i, ksUp);
      translator->RemoveKeyHandler(i, ksDown);
    }

    translator->AddMouseMoveHandler(bind(&InputSubSystemTest::CreateMouseMoveEvent, this, _1));

    cout << "Mouse Move Test: Please move your mouse around.\n";
    while (mMouseMoveEvents < 100) {
      window.Update();
      EventSystemSingleton::Get()->DispatchEvent(updateEvent);
      EventSystemSingleton::Get()->DispatchQueue();
    }

    translator->RemoveMouseMoveHandler();

    for (OIS::MouseButtonID i = MB_Left; i <= MB_Button7; i = static_cast< OIS::MouseButtonID >(i + 1)) {
      translator->AddMouseClickHandler(i, bind(&InputSubSystemTest::CreateMouseClickEvent, this, _1, _2, _3));
    }
       
    cout << "Mouse Click Test: Please Click mouse buttons 10 or so time.\n";
    while (mMouseClickEvents < 10) {
      window.Update();
      EventSystemSingleton::Get()->DispatchEvent(updateEvent);
      EventSystemSingleton::Get()->DispatchQueue();
    }
    
    cout << "Good Job! Thanks!\n";

    inputSubSystem->vShutdown();
  }
  
  VerifyResult("Do those key values look correct", true);
}

tIEventPtr InputSubSystemTest::CreateKeyEvent(const OIS::KeyCode kc, const tKeyState KeyState) {
  tVariantList v;
  v.push_back(kc);
  LogSingleton::Get()->LogMessageFmt("Sending event for key %...", v, mlQuiet);
  tIEventPtr result(new KeyboardInputEvent(kc, KeyState));
  return result;
}

tIEventPtr InputSubSystemTest::CreateMouseMoveEvent(const OIS::MouseState &ms) {
  tIEventPtr result(new MouseMoveEvent(ms));
  return result;
}

tIEventPtr InputSubSystemTest::CreateMouseClickEvent(const OIS::MouseState &ms, 
  const OIS::MouseButtonID mbid, const tKeyState state) {
  tIEventPtr result(new MouseClickEvent(ms, mbid, state));
  return result;
}

void InputSubSystemTest::HandleKeyboardEvent(const tIEventPtr& e) {
  shared_ptr< KeyboardInputEvent > evt = dynamic_pointer_cast< KeyboardInputEvent >(e);
  
  if (evt->GetState() == ksDown) {
    cout << "You pressed ";
  } else {
    cout << "You released ";
  }
  cout << evt->GetKey() << endl;
  
  ++mKeysPressed;
}

void InputSubSystemTest::HandleMouseClickEvent(const pcce::tIEventPtr& e) {
  shared_ptr< MouseClickEvent > evt = dynamic_pointer_cast< MouseClickEvent >(e);

  if (evt->GetButtonState() == ksDown) {
    cout << "You pressed ";
  } else {
    cout << "You released ";
  }
  
  std::string buttonName = "";
  switch (evt->GetMouseButtonId()) {
    case MB_Left: buttonName = "Left"; break;
    case MB_Right: buttonName = "Right"; break;
    case MB_Middle: buttonName = "Middle"; break;
    default: buttonName = "You have too many mouse buttons"; break;
  }
  cout << buttonName << " button" << endl;

  ++mMouseClickEvents;
}
void InputSubSystemTest::HandleMouseMoveEvent(const pcce::tIEventPtr& e) {
  shared_ptr< MouseMoveEvent > evt = dynamic_pointer_cast< MouseMoveEvent >(e);
  
  cout << "You moved the mouse, the current state is X: " << evt->GetMouseState().X.abs 
       << "(" << evt->GetMouseState().X.rel << ")"
       << " Y: " << evt->GetMouseState().Y.abs 
       << "(" << evt->GetMouseState().Y.rel << ")"
       << " Z: " << evt->GetMouseState().Z.abs
       << "(" << evt->GetMouseState().Z.rel << ")"
       << endl;
  
  ++mMouseMoveEvents;
}
