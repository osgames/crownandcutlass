/* Protocce Test
 * PropertyBag Test class
 */

#ifndef _PCCETEST_VARIANTTEST_H_
#define _PCCETEST_VARIANTTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class VariantTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Variant"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
