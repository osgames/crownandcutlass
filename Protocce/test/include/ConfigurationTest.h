/* Protocce Test
 * Configuration Test class
 */

#ifndef _PCCETEST_CONFIGURATION_H_
#define _PCCETEST_CONFIGURATION_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class ConfigurationTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Configuration"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
