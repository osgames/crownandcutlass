/* Protocce Test
 * OggStream Test class
 */

#ifndef _PCCETEST_OGGSTREAMTEST_H_
#define _PCCETEST_OGGSTREAMTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class OggStreamTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return true; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "OggStream"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_SOUND; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
