/* Protocce Test
 * Protocce Main Test class
 */

#ifndef _PCCETEST_PROTOCCEMAINTEST_H_
#define _PCCETEST_PROTOCCEMAINTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class ProtocceTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Protocce"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
