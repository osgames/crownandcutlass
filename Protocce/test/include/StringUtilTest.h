/* Protocce Test
 * StringUtil Test class
 */

#ifndef _PCCETEST_STRINGUTILTEST_H_
#define _PCCETEST_STRINGUTILTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class StringUtilTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "StringUtil"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();

    void TestHashString();
    void TestFormat();
  };

}

#endif
