/* Protocce Test
 * Resource Manager Test class
 */

#ifndef _PCCETEST_RESOURCEMANAGERTEST_H_
#define _PCCETEST_RESOURCEMANAGERTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class ResourceManagerTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "ResourceManager"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
