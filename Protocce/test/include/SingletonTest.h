/* Protocce Test
 * Singleton Test class
 */

#ifndef _PCCETEST_SINGLETONTEST_H_
#define _PCCETEST_SINGLETONTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class SingletonTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Singleton"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
