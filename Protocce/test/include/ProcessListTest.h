/* Protocce Test
 * Process List Test class
 */

#ifndef _PCCETEST_PROCESSLISTTEST_H_
#define _PCCETEST_PROCESSLISTTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class ProcessListTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "ProcessList"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_PROCESS; };

  private:
    // Actual test method
    void vDoRunTest();
    
    void RunBasicTest();
    void RunNextTest();
    void RunLoopTest();
  };

}

#endif
