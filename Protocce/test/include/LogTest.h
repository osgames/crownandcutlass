/* Protocce Test
 * Log Test class
 */

#ifndef _PCCETEST_LOGTEST_H_
#define _PCCETEST_LOGTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class LogTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Log"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
