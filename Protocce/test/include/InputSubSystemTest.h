/* Protocce Test
 * InputSubSystem Test class
 */

#ifndef _PCCETEST_INPUTSUBSYSTEMTEST_H_
#define _PCCETEST_INPUTSUBSYSTEMTEST_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include <OIS/OIS.h>
#include "IEventFwd.h"
#include "InputTranslator.h"
#include "ITest.h"

namespace pccetest {

  class InputSubSystemTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return true; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "InputSubSystem"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };
    
    pcce::tIEventPtr CreateKeyEvent(const OIS::KeyCode KeyCode, const pcce::tKeyState KeyState);
    pcce::tIEventPtr CreateMouseMoveEvent(const OIS::MouseState &ms);
    pcce::tIEventPtr CreateMouseClickEvent(const OIS::MouseState &ms, 
      const OIS::MouseButtonID mbid, const pcce::tKeyState state);
    
    void HandleKeyboardEvent(const pcce::tIEventPtr& e);
    void HandleMouseClickEvent(const pcce::tIEventPtr& e);
    void HandleMouseMoveEvent(const pcce::tIEventPtr& e);

  private:
    // Actual test method
    void vDoRunTest();
    
    int mKeysPressed;
    int mMouseClickEvents;
    int mMouseMoveEvents;
  };

}

#endif 
