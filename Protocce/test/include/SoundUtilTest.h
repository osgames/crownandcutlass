/* Protocce Test
 * Sound Util Test class
 */

#ifndef _PCCETEST_SOUNDUTILTEST_H_
#define _PCCETEST_SOUNDUTILTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class SoundUtilTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return true; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "SoundUtil"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_SOUND; };

  private:
    // Actual test method
    void vDoRunTest();

    // Check for an AL error, throw if found
    void CheckALError(const std::string& s);
  };

}

#endif
