/* Protocce Test
 * Math Util Test class
 */

#ifndef _PCCETEST_MATHUTILTEST_H_
#define _PCCETEST_MATHUTILTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class MathUtilTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "MathUtil"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
