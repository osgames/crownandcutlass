/* Protocce Test
 * EventSystem Test class
 */

#ifndef _PCCETEST_EVENTSYSTEMTEST_H_
#define _PCCETEST_EVENTSYSTEMTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class EventSystemTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "EventSystem"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_EVENT; };

  private:
    // Actual test method
    void vDoRunTest();
    void TestGeneral();
    void TestEventQueue();
    void TestAllEventListeners();
    void TestEventCounts();
  };

}

#endif
