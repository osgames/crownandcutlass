/* Protocce Test
 * Serialization Test
 */

#ifndef _PCCETEST_SERIALIZATIONTEST_H_
#define _PCCETEST_SERIALIZATIONTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class SerializationTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Serialization"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
    
    void TestSave();
    void TestLoad();
  };

}

#endif 
