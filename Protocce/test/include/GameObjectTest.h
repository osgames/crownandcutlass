/* Protocce Test
 * GameObject Test class
 */

#ifndef _PCCETEST_GAMEOBJECTTEST_H_
#define _PCCETEST_GAMEOBJECTTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class GameObjectTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "GameObject"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
