/* Protocce Test
 * Timer Test class
 */

#ifndef _PCCETEST_TIMERTEST_H_
#define _PCCETEST_TIMERTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class TimerTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Timer"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
    void RunSimpleDumpTest();
    void RunInvalidTest();
  };

}

#endif
