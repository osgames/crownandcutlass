/* Protocce Test
 * PropertyBag Test class
 */

#ifndef _PCCETEST_PROPERTYBAGTEST_H_
#define _PCCETEST_PROPERTYBAGTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class PropertyBagTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "PropertyBag"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
