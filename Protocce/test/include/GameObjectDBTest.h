/* Protocce Test
 * GameObjectDB Test class
 */

#ifndef _PCCETEST_GAMEOBJECTDBTEST_H_
#define _PCCETEST_GAMEOBJECTDBTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class GameObjectDBTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "GameObjectDB"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
