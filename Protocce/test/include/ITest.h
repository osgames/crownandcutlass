/* Protocce Test
 * Base Test class
 */

#ifndef _PCCETEST_ITEST_H_
#define _PCCETEST_ITEST_H_

#include <string>
#include "config.h"

namespace pccetest {

  enum tTestType {
    TESTTYPE_GENERAL,
    TESTTYPE_EVENT,
    TESTTYPE_PROCESS,
    TESTTYPE_SOUND,
    TESTTYPE_GRAPHICS,
    TESTTYPE_PHYSICS,
    TESTTYPE_INPUT,
    TESTTYPE_NETWORK
    };
  
  enum tInteractiveAction {
    INTERACTIVEACTION_ASK,
    INTERACTIVEACTION_ASSUMEPASSED,
    INTERACTIVEACTION_SKIP
  };

#ifdef WIN32
  // In windows there are "debug" and "release" subdirs
  const std::string DataPath = "../../data/";
#else
  const std::string DataPath = "../data/";
#endif

  class ITest {
  public:
    ITest();
    virtual ~ITest() {};

    void RunTest(const tInteractiveAction interactiveAction);

    virtual bool vIsPostInitTest() = 0;
    virtual bool vIsInteractiveTest() = 0;
    virtual const std::string vTestName() = 0;
    virtual pccetest::tTestType vTestType() = 0;

    virtual void vDoRunTest() = 0;

  protected:
    void VerifyResult(const std::string& question, const bool expectedAnswer);

  private:
    bool mVerifyResultCalled;
    tInteractiveAction mInteractiveAction;
  };

}

#endif
