/* Protocce Test
 * Event Test class
 */

#ifndef _PCCETEST_EVENTTEST_H_
#define _PCCETEST_EVENTTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class EventTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Event"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_EVENT; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
