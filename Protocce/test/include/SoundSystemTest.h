/* Protocce Test
 * Sound System Test class
 */

#ifndef _PCCETEST_SOUNDSYSTEMTEST_H_
#define _PCCETEST_SOUNDSYSTEMTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class SoundSystemTest: public ITest {
  public:
    bool vIsInteractiveTest() { return true; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "SoundSystem"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_SOUND; };

  private:
    void vDoRunTest();

    void TestSoundResource();
    void TestSoundBuffer();
    void TestSoundStream();
    void TestSoundSystemBuffer();
    void TestSoundSystemStream();
  };

}

#endif
