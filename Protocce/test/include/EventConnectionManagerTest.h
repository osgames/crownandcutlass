/* Protocce Test
 * Event Connection Manager Test class
 */

#ifndef _PCCETEST_EVENT_CONNECTION_MANAGER_TEST_H_
#define _PCCETEST_EVENT_CONNECTION_MANAGER_TEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class EventConnectionManagerTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "EventConnectionManager"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_EVENT; };

  private:
    // Actual test method
    void vDoRunTest();
  };

}

#endif
