/* Protocce Test
 * Xml Test class
 */

#ifndef _PCCETEST_XMLTEST_H_
#define _PCCETEST_XMLTEST_H_

#include <string>
#include "ITest.h"

namespace pccetest {

  class XmlTest: public ITest {
  public:
    // Some info about the test
    bool vIsInteractiveTest() { return false; };
    bool vIsPostInitTest() { return false; };
    const std::string vTestName() { return "Xml"; };
    pccetest::tTestType vTestType() { return pccetest::TESTTYPE_GENERAL; };

  private:
    // Actual test method
    void vDoRunTest();

    void TestReadXml();
    void TestWriteXml();
    void TestReadAndWriteXml();
  };

}

#endif
