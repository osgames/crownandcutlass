/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Protocce Main Engine File

#include <string>
#include "Protocce.h"
#include "Variant.h"
#include "Timer.h"
#include "SoundResourceFactory.h"
#include "SoundResource.h"
#include "IGameState.h"
#include "IThreadedSubSystem.h"
#include "ThreadExceptionEvent.h"
#include "UpdateSubSystemEvent.h"
#include "SceneChangeEvent.h"
#include "Exception.h"

using namespace pcce;
using namespace std;

Protocce::Protocce(tConfigurationPtr config):
  mStateStack(),
  mConfig(config),
  mTimer(),
  mThreadExceptionConnection(),
  mSubSystems(),
  mInitialized(false),
  mEventSystemManager(),
  mGameObjectDBManager(),
  mResManager(),
  mLogManager()
{
}

Protocce::~Protocce() {
  DoClearStateStack();
  if (mInitialized) {
    Shutdown();
  }
}

void Protocce::Initialize(const tISubSystemPtrList& subSystems) {
  const string LogSection = "Log";
  const string LogCout = "cout";
  const string LogFileName = "filename";
  
  PCCE_CHECK(!mInitialized, "Protocce::Initialize: Already initialized");
  mInitialized = true;

  tPropertyBagPtr section;
  
  boost::shared_ptr< Log > log = LogSingleton::Get();
  if (!mConfig->TryGetConfigSection(LogSection, section)) {
    section.reset(new PropertyBag());
  }
  log->SetCoutEnabled(section->GetValueDef(LogCout, true).AsBool());
  log->SetFileName(section->GetValueDef(LogFileName, "Protocce.log").AsString());

  LogSingleton::Get()->LogMessage("Version Info:" + GetNL() + GetPcceFullBuildInfoStr() + GetNL(), mlQuiet);
  
  log->LogMessage("Started Protocce initialization", mlLoud);
  
  mTimer.reset(new Timer());

  boost::shared_ptr< EventSystem > eventSystem = EventSystemSingleton::Get();
  mThreadExceptionConnection = eventSystem->AddHandler(
    ThreadExceptionEvent::sGetEventType(), HandleThreadExceptionEvent);
  
  tIResourceFactoryPtr factory(new SoundResourceFactory());
  ResourceManagerSingleton::Get()->RegisterResourceFactory(SoundResourceType, factory);

  for (tISubSystemPtrList::const_iterator i = subSystems.begin(); i != subSystems.end(); ++i) {
    string name = (*i)->vGetSubSystemName();
    
    mSubSystems.push_back(*i);
    if (!mConfig->TryGetConfigSection(name, section)) {
      section.reset(new PropertyBag());
    }
    log->LogMessage(GetNL() + "Initializing " + name + " subsystem", mlLoud);
    (*i)->vInitialize(section);
    // Make sure there are no queued thread exceptions
    eventSystem->DispatchQueue();
  }
  
  log->LogMessage("Protocce initialized", mlQuiet);
}

void Protocce::Shutdown() {
  if (mInitialized) {
    boost::shared_ptr< Log > log = LogSingleton::Get();
    log->LogMessage(GetNL() + "Started Protocce shutdown", mlLoud);

    DoClearStateStack();
    
    // This should be a const_reverse_iterator, but != is broken in Apple's gcc
    for (tISubSystemPtrList::reverse_iterator i = mSubSystems.rbegin(); i != mSubSystems.rend(); ++i) {
      log->LogMessage(GetNL() + "Shutting down " + (*i)->vGetSubSystemName() + " subsystem", mlLoud);
      try {
        (*i)->vShutdown();
      } catch (Exception&) {
        log->LogMessage("Error while shutting down " + (*i)->vGetSubSystemName() + " subsystem", mlQuiet);
      }
    }
    mTimer.reset();

    mThreadExceptionConnection.disconnect();
    
    mInitialized = false;
    log->LogMessage("Protocce shutdown", mlQuiet);
  }
}

tConfigurationPtr Protocce::GetConfiguration() {
  return mConfig;
}

tTimerPtr Protocce::GetTimer() {
  return mTimer;
}

void Protocce::PushState(tIGameStatePtr state) {
  mStateStack.push(state);
}

void Protocce::PopState() {
  PCCE_CHECK(!mStateStack.empty(), "Cannot pop state, state stack is empty");
  do {
    mStateStack.pop();
  } while((!mStateStack.empty()) && (mStateStack.top()->HasActiveProcesses()));
}

void Protocce::RunGame() {
  boost::shared_ptr< EventSystem > eventSystem = EventSystemSingleton::Get();
  tIGameStatePtr lastState;
  // This isn't a tIEventPtr just so we can call updateEvent->SetMilliseconds(...) directly
  boost::shared_ptr< UpdateSubSystemEvent > updateEvent(new UpdateSubSystemEvent(0));
  
  mTimer->MarkFrameStart();
  
  while (!mStateStack.empty()) {
    tMillisecond ms = mTimer->MarkFrameStart();
    
    tIGameStatePtr currentState = mStateStack.top();
    if (currentState != lastState) {
      if (lastState) {
        lastState->vSwitchFrom();
      }
      currentState->vSwitchTo();
      lastState = currentState;
      
      tScenePtr scene = currentState->GetScene();
      PCCE_CHECK(scene, "Protocce::RunGame: Current state does not have a scene");
      tIEventPtr sceneEvent(new SceneChangeEvent(scene));
      
      eventSystem->DispatchEvent(sceneEvent);
    }
    
    eventSystem->DispatchQueue();
    
    currentState->ExecProcesses(ms);
    
    updateEvent->SetMilliseconds(ms);
    eventSystem->DispatchEvent(updateEvent);

    if ((currentState == mStateStack.top()) && (!currentState->HasActiveProcesses())) {
      PopState();
    }
  }
}

void Protocce::DoClearStateStack() {
  while (!mStateStack.empty()) {
    mStateStack.pop();
  }
}
