/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "Exception.h"
#include "IResource.h"
#include "IResourceFactory.h"
//#include "util.h"
#include "Variant.h"
#include "XmlDoc.h"
#include "XmlNode.h"
#include "ResourceManager.h"

using namespace std;
using namespace boost;
using namespace pcce;

ResourceManager::ResourceManager() {
}

ResourceManager::~ResourceManager() {
}

tIResourcePtr ResourceManager::GetResource(const string& name, bool shouldLoad) {
  PCCE_CHECK(!name.empty(), "GetResource: Name is blank");

  mutex::scoped_lock lock(mMutex);
  tIResourceMap::const_iterator i = mResourceNameMap.find(name);  
  PCCE_CHECK(
    i != mResourceNameMap.end(),
    "Resource \"" + name + "\" not found");

  tIResourcePtr result = i->second;
  if (shouldLoad) {
    result->Load();
  }
  return result;
}

void ResourceManager::LoadResource(const string& name) {
  GetResource(name, true);
}

void ResourceManager::LoadResourceGroup(const string& group) {
  mutex::scoped_lock lock(mMutex);

  tIResourceMultiMap::iterator start, i;
  tIResourceMultiMap::const_iterator end;
  start = mResourceGroupMap.lower_bound(group);
  end = mResourceGroupMap.upper_bound(group);
  for(i = start; i != end; i++) {
    i->second->Load();
  }
}

void ResourceManager::UnloadResource(const string& name) {
  tIResourcePtr resource = GetResource(name, false);
  resource->Unload();
}

void ResourceManager::UnloadResourceGroup(const string& group) {
  mutex::scoped_lock lock(mMutex);

  tIResourceMultiMap::iterator start, i;
  tIResourceMultiMap::const_iterator end;
  start = mResourceGroupMap.lower_bound(group);
  end = mResourceGroupMap.upper_bound(group);
  for(i = start; i != end; i++) {
    i->second->Unload();
  }
}

void ResourceManager::CreateResourcesFromFile(const string& fileName) {
  //unsigned int count = 0;
  XmlDoc doc(fileName);

  // Find the resource nodes
  const XmlNode* node = doc.RootNode("Resources")->GetFirstChild("Resource");
  PCCE_CHECK(node != NULL, "Resource file " + fileName + " does not contain any <Resource> nodes");

  // elem is now the first <Resource> node, loop through and load the resources
  for (; node != NULL; node = node->GetNextSibling("Resource")) {
    string resourceType = node->GetAttribute("type").AsString();
    PCCE_CHECK(!resourceType.empty(), "Resource type value is blank");

    tIResourcePtr resource = DoGetFactory(resourceType)->vCreateResource(node);
    PCCE_CHECK(resource != NULL, "Factory for type \"" + resourceType + "\" returned empty resource");

    string group = node->GetAttribute("group").AsString();

    DoInsertResource(resource, group);
    //++count;
  }

  DoSanityCheck();

  //Log::s_log->Message("Resource file %s loaded %d resources", filename.c_str(), count);
}

tIResourcePtr ResourceManager::CreateResource(
  const string& resourceType, const string& name, const string& group)
{
  PCCE_CHECK(!name.empty(), "CreateResource: Resource name may not be blank");
  tIResourcePtr result = DoGetFactory(resourceType)->vCreateResource(name, resourceType);
  DoInsertResource(result, group);
  return result;
}

void ResourceManager::DestroyResource(const string& name) {
  mutex::scoped_lock lock(mMutex);

  string group = GetResource(name, false)->GetGroup();

  tIResourceMultiMap::iterator start, i;
  tIResourceMultiMap::const_iterator end;
  start = mResourceGroupMap.lower_bound(group);
  end = mResourceGroupMap.upper_bound(group);
  for(i = start; i != end; i++) {
    if (i->second->GetName() == name) {
      mResourceGroupMap.erase(i);
      break;
    }
  }

  // Erase from name map
  mResourceNameMap.erase(name);

  DoSanityCheck();
}

void ResourceManager::DestroyResourceGroup(const string& group) {
  mutex::scoped_lock lock(mMutex);

  tIResourceMultiMap::iterator i, temp;
  tIResourceMultiMap::const_iterator end;

  i = mResourceGroupMap.lower_bound(group);
  end = mResourceGroupMap.upper_bound(group);
  while (i != end) { 
    temp = i;
    i++;
    // Erase from both the name and group maps
    mResourceNameMap.erase(temp->second->GetName());
    mResourceGroupMap.erase(temp);
  }
  DoSanityCheck();
}

void ResourceManager::RegisterResourceFactory(const string& resourceType, const tIResourceFactoryPtr factory) {
  mutex::scoped_lock lock(mMutex);

  PCCE_CHECK(!resourceType.empty(), "Cannot register factory for blank resource type");
  tIResourceFactoryMap::iterator i = mFactoryMap.find(resourceType);
  PCCE_CHECK(i == mFactoryMap.end(), "Factory already registered for type \"" + resourceType + "\"");
  mFactoryMap.insert(i, tIResourceFactoryMapPair(resourceType, factory));
}

void ResourceManager::UnregisterResourceFactory(const string& resourceType) {
  mutex::scoped_lock lock(mMutex);

  mFactoryMap.erase(resourceType);
}

void ResourceManager::SanityCheck() {
  mutex::scoped_lock lock(mMutex);

  DoSanityCheck();
}

tIResourceFactoryPtr ResourceManager::DoGetFactory(const std::string& resourceType) {
  mutex::scoped_lock lock(mMutex);

  PCCE_CHECK(!resourceType.empty(), "Resource type cannot be blank");
  tIResourceFactoryMap::iterator i = mFactoryMap.find(resourceType);
  PCCE_CHECK(i != mFactoryMap.end(), "Could not find factory for type \"" + resourceType + "\"");
  return i->second;
}

void ResourceManager::DoInsertResource(const tIResourcePtr resource, const string& group) {
  mutex::scoped_lock lock(mMutex);

  resource->SetGroup(group);

  // Insert into name map
  string name = resource->GetName();
  tIResourceMap::iterator i = mResourceNameMap.find(name);
  PCCE_CHECK(i == mResourceNameMap.end(), "Resource already exists named \"" + name + "\"");
  mResourceNameMap.insert(i, tIResourceMapPair(name, resource));
  
  // Insert into group map
  mResourceGroupMap.insert(tIResourceMapPair(group, resource));

  DoSanityCheck();
}

void ResourceManager::DoSanityCheck() {
  PCCE_CHECK(mResourceNameMap.size() == mResourceGroupMap.size(), "Name and group map out of sync");
}
