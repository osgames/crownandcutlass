/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Log class

#include <iostream>
#include "stringutil.h"
#include "Variant.h"
#include "Exception.h"
#include "Log.h"

using namespace std;
using namespace boost;
using namespace pcce;

namespace {
  
  inline tLogLevel MessageLevelToLogLevel(const tMessageLevel& level) {
    return static_cast< tLogLevel >(level);
  }
  
}

Log::Log(): mWriteToCout(false), mStream(), mLogLevel(llDebug) {
}

Log::Log(tLogLevel logLevel): mWriteToCout(false), mStream(), mLogLevel(logLevel) {
}
    
void Log::SetCoutEnabled(bool value) {
  mutex::scoped_lock lock(mMutex);
  mWriteToCout = value;
}

void Log::SetFileName(const string& fileName) {
  mutex::scoped_lock lock(mMutex);
  if (mStream.is_open()) {
    mStream.close();
  }
  if (fileName != "") {
    mStream.clear();
    mStream.open(fileName.c_str(), ios::out);
    PCCE_CHECK(mStream, string("Could not open log ") + fileName);
  }
}
    
void Log::LogMessage(const string& message, tMessageLevel level) {
  mutex::scoped_lock lock(mMutex);
  if (MessageLevelToLogLevel(level) <= mLogLevel) {
    if (mWriteToCout) {
      cout << message << endl;
    }
    if (mStream) {
      mStream << message << endl;
    }
  }
}

void Log::LogMessageFmt(const string& formatStr, const tVariantList values, tMessageLevel level) {
  LogMessage(Format(formatStr, values), level);
}

void Log::SetLogLevel(tLogLevel logLevel) {
  mutex::scoped_lock lock(mMutex);
  mLogLevel = logLevel;
}
