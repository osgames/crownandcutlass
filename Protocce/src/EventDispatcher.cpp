/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Event Dispatcher class

#include "Exception.h"
#include "IEvent.h"
#include "EventDispatcher.h"

using namespace std;
using namespace boost;
using namespace pcce;

EventDispatcher::EventDispatcher() {
  // Make sure AllEventsType signal is created...
  Signal(AllEventsType);
}

EventDispatcher::~EventDispatcher() {
  DoCleanSignals(false);
  if (!mMap.empty()) {
    // LOG WARNING MESSAGE!
    mMap.clear();
  }
}

tEventConnection EventDispatcher::AddHandler(const tHashValue eventType, tEventHandler handler) {
  PCCE_CHECK(handler, "EventDispatcher::AddHandler: Cannot add null handler");
  
  tEventSignalPtr signal = Signal(eventType);
  return signal->connect(handler);
}

void EventDispatcher::DispatchEvent(tIEventPtr event) {
  PCCE_CHECK(event, "EventDispatcher::DispatchEvent: Cannot dispatch a null event");
  
  tEventSignalPtr signal = Signal(event->vGetEventType());
  tEventSignalPtr allEventSignal = Signal(AllEventsType);
  
  // Pass the event to the allEventListeners
  (*allEventSignal)(event);
  
  // Now pass it to the listeners who registered for this event
  (*signal)(event);
}

void EventDispatcher::DispatchEvents(const tIEventPtrList& events) {
  for (tIEventPtrList::const_iterator i = events.begin(); i != events.end(); ++i) {
    DispatchEvent(*i);
  }
}

void EventDispatcher::RemoveInactiveEventTypes() {
  mutex::scoped_lock lock(mMutex);
  DoCleanSignals(true);
}

unsigned int EventDispatcher::ActiveEventTypeCount() {
  mutex::scoped_lock lock(mMutex);
  DoCleanSignals(true);
  return mMap.size();
}

unsigned int EventDispatcher::EventTypeCount() {
  mutex::scoped_lock lock(mMutex);
  return mMap.size();
}

EventDispatcher::tEventSignalPtr EventDispatcher::Signal(const tHashValue eventType) {
  mutex::scoped_lock lock(mMutex);
  
  tEventSignalPtrMap::iterator i = mMap.find(eventType);
  if (i == mMap.end()) {
    tEventSignalPtr result(new tEventSignal());
    mMap.insert(i, tEventSignalPtrMapPair(eventType, result));
    return result;
  } else {
    return i->second;
  }
}

void EventDispatcher::DoCleanSignals(bool skipAllEventsType) {
  tEventSignalPtrMap::iterator i = mMap.begin();
  while (i != mMap.end()) {
    // Don't remove all event type listener, since it'll just get recreated
    // by the next event...
    if (skipAllEventsType && (i->first == AllEventsType)) {
      ++i;
      continue;
    }

    if (i->second->empty()) {
      mMap.erase(i++);
    } else {
      ++i;
    }
  }
}
