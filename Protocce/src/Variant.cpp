/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Variant Class

#include <sstream>
#include "Exception.h"
#include "Variant.h"

using namespace std;
using namespace boost;
using namespace pcce;

template<typename T>
class ConvertResult {
public:
  bool success;
  T value;
};

class VariantPropBagConverter: public static_visitor< ConvertResult< tPropertyBagPtr > > {
public:
  ConvertResult< tPropertyBagPtr > operator()( const tPropertyBagPtr & value ) const {
    ConvertResult< tPropertyBagPtr > result;
    result.success = true;
    result.value = value;
    return result;
  }
  template <typename T>
  ConvertResult< tPropertyBagPtr > operator()( const T & value ) const {
    ConvertResult< tPropertyBagPtr > result;
    result.success = false;
    return result;
  }
};

template <typename T>
class VariantConverter: public static_visitor< ConvertResult< T > > {
private:
  tVariant& mRef;
public:
  VariantConverter(tVariant& ref): mRef(ref) {};
  
  ConvertResult< T > operator()( const T & value ) const {
    ConvertResult< T > result;
    result.success = true;
    result.value = value;
    return result;
  }
  ConvertResult< T > operator()( const tPropertyBagPtr & value ) const {
    ConvertResult< T > result;
    result.success = false;
    result.value = T();
    return result;
  }
  template <typename U>
  ConvertResult< T > operator()( const U & value ) const {
    stringstream s;
    ConvertResult< T > result;
    
    s.clear();
    s << value;
    PCCE_CHECK(s.good(), "Streaming value to stringstream failed");
    s >> result.value;
    result.success = !s.fail();
    // In order to avoid having to constantly convert using streams, we store
    //   the value in the new type.
    if (result.success) {
      mRef = result.value;
    }
    return result;
  }
};

Variant::Variant(): mHasValue(false), mValue(0) {
}

Variant::Variant(const tVariant value): mHasValue(true), mValue(value) {
}

Variant::Variant(const int value): mHasValue(true), mValue(value) {
}

Variant::Variant(const unsigned int value): mHasValue(true), mValue(value) {
}

Variant::Variant(const long int value): mHasValue(true), mValue(value) {
}

Variant::Variant(const unsigned long int value): mHasValue(true), mValue(value) {
}

Variant::Variant(const tReal value): mHasValue(true), mValue(value) {
}

Variant::Variant(const char value): mHasValue(true), mValue(value) {
}

Variant::Variant(const std::string value): mHasValue(true), mValue(value) {
}

Variant::Variant(const char* value): mHasValue(true), mValue(string(value)) {
}

Variant::Variant(const tPropertyBagPtr value): mHasValue(true), mValue(value) {
}

bool Variant::HasValue() const {
  return mHasValue;
}

void Variant::Clear() {
  mHasValue = false;
  mValue = 0;
}

tVariant Variant::Get() const {
  check< VariantEmptyException >(mHasValue, "Variant does not have a value", __FILE__, __LINE__);
  return mValue;
}

void Variant::Set(tVariant value) {
  mHasValue = true;
  mValue = value;
}

int Variant::AsInt() const {
  tVariant v = Get();
  ConvertResult< int > result = apply_visitor(VariantConverter< int >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to int", __FILE__, __LINE__);
  return result.value;
}

bool Variant::AsBool() const {
  tVariant v = Get();
  // See the doc string for tVariant for explanation of why bool is excluded
  //   from the boost::variant template.
  ConvertResult< bool > result = apply_visitor(VariantConverter< bool >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to bool", __FILE__, __LINE__);
  return result.value;
}

unsigned int Variant::AsUnsignedInt() const {
  tVariant v = Get();
  ConvertResult< unsigned int > result = apply_visitor(VariantConverter< unsigned int >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to unsigned int", __FILE__, __LINE__);
  return result.value;
}

long int Variant::AsLongInt() const {
  tVariant v = Get();
  ConvertResult< long int > result = apply_visitor(VariantConverter< long int >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to long int", __FILE__, __LINE__);
  return result.value;
}

unsigned long int Variant::AsUnsignedLongInt() const {
  tVariant v = Get();
  ConvertResult< unsigned long int > result = apply_visitor(VariantConverter< unsigned long int >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to unsigned long int", __FILE__, __LINE__);
  return result.value;
}

tReal Variant::AsReal() const {
  tVariant v = Get();
  ConvertResult< tReal > result = apply_visitor(VariantConverter< tReal >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to real", __FILE__, __LINE__);
  return result.value;
}

char Variant::AsChar() const {
  tVariant v = Get();
  ConvertResult< char > result = apply_visitor(VariantConverter< char >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to char", __FILE__, __LINE__);
  return result.value;
}

unsigned char Variant::AsUnsignedChar() const {
  tVariant v = Get();
  ConvertResult< unsigned char > result = apply_visitor(VariantConverter< unsigned char >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to unsigned char", __FILE__, __LINE__);
  return result.value;
}

string Variant::AsString() const {
  tVariant v = Get();
  ConvertResult< string > result = apply_visitor(VariantConverter< string >(mValue), v);
  check< VariantConversionException >(result.success, "Could not convert variant to string", __FILE__, __LINE__);
  return result.value;
}

tPropertyBagPtr Variant::AsPropertyBagPtr() const {
  tVariant v = Get();
  ConvertResult< tPropertyBagPtr > result = apply_visitor(VariantPropBagConverter(), v);
  check< VariantConversionException >(result.success, "Variant is not a PropertyBagPtr", __FILE__, __LINE__);
  return result.value;
}

bool Variant::IsInt() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< int > result = apply_visitor(VariantConverter< int >(mValue), v);
    return result.success;
  }
}

bool Variant::IsBool() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    // See the doc string for tVariant for explanation of why bool is excluded
    //   from the boost::variant template.
    ConvertResult< bool > result = apply_visitor(VariantConverter< bool >(mValue), v);
    return result.success;
  }
}

bool Variant::IsUnsignedInt() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< unsigned int > result = apply_visitor(VariantConverter< unsigned int >(mValue), v);
    return result.success;
  }
}

bool Variant::IsLongInt() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< long int > result = apply_visitor(VariantConverter< long int >(mValue), v);
    return result.success;
  }
}

bool Variant::IsUnsignedLongInt() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< unsigned long int > result = apply_visitor(VariantConverter< unsigned long int >(mValue), v);
    return result.success;
  }
}

bool Variant::IsReal() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< tReal > result = apply_visitor(VariantConverter< tReal >(mValue), v);
    return result.success;
  }
}

bool Variant::IsChar() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< char > result = apply_visitor(VariantConverter< char >(mValue), v);
    return result.success;
  }
}

bool Variant::IsUnsignedChar() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< unsigned char > result = apply_visitor(VariantConverter< unsigned char >(mValue), v);
    return result.success;
  }
}

bool Variant::IsString() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< string > result = apply_visitor(VariantConverter< string >(mValue), v);
    return result.success;
  }
}

bool Variant::IsPropertyBagPtr() const {
  if (!mHasValue) {
    return false;
  } else {
    tVariant v = Get();
    ConvertResult< tPropertyBagPtr > result = apply_visitor(VariantPropBagConverter(), v);
    return result.success;
  }
}
