/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "config.h"
#include <boost/bind.hpp>
#include "ThreadExceptionEvent.h"
#include "EventConnectionManager.h"
#include "Log.h"
#include "IThreadedSubSystem.h"

using namespace pcce;
using namespace boost;

IThreadedSubSystem::IThreadedSubSystem(): mRunning(false) {
}

IThreadedSubSystem::~IThreadedSubSystem() {
  try {
    if (IsRunning()) {
      LogSingleton::Get()->LogMessage("TheadedSubSystem destroyed without calling vShutdown", mlQuiet);
    }
  } catch (...) {
    // Just eat any exceptions since shared_ptr's destructors can't throw
  }
}

void IThreadedSubSystem::vInitialize(tPropertyBagPtr configSection) {
  boost::mutex::scoped_lock lock(mRunningMutex);
  PCCE_CHECK(!mRunning, "Cannot init running threaded subsystem");

  mConfigSection = configSection;
  mSystemConns.reset(new EventConnectionManager());
  mDispatchConns.reset(new EventConnectionManager());
  mDispatcher.reset(new EventDispatcher());

  mThread.reset(new thread(bind(&IThreadedSubSystem::ThreadStart, this)));

  mInitCondition.wait(lock, bind(&IThreadedSubSystem::DoIsRunning, this));
}

void IThreadedSubSystem::vShutdown() {
  boost::mutex::scoped_lock lock(mRunningMutex);
  if (DoIsRunning()) {
    mRunning = false;
    lock.unlock();
    mEventCondition.notify_one();
    mThread->join();
  }
}

void IThreadedSubSystem::AddThreadHandler(tHashValue eventType, tEventHandler handler) {
  PCCE_CHECK(
    mSystemConns && mDispatchConns && mDispatcher,
    "IThreadedSubSystem::AddThreadHandler: Internal error");
  
  // Register the handler from the caller with the dispatcher on this thread
  mDispatchConns->AddConnection(eventType, mDispatcher->AddHandler(eventType, handler));
  
  // Now register the method to push the event to the thread with the actual event system
  mSystemConns->AddHandler(eventType, bind(&IThreadedSubSystem::SendEventToThread, this, _1));
}

void IThreadedSubSystem::RemoveThreadHandler(tHashValue eventType) {
  PCCE_CHECK(
    mSystemConns && mDispatchConns && mDispatcher,
    "IThreadedSubSystem::RemoveThreadHandler: Internal error");

  mSystemConns->RemoveHandler(eventType);
  mDispatchConns->RemoveHandler(eventType);
}

bool IThreadedSubSystem::IsRunning() {
  boost::mutex::scoped_lock lock(mRunningMutex);
  return mRunning;
}

void IThreadedSubSystem::DispatchThreadEventBlocking() {
  PCCE_CHECK(mDispatcher, "IThreadedSubSystem::DispatchThreadEventBlocking: mDispatcher is null");

  tIEventPtr e;
  {
    boost::mutex::scoped_lock lock(mEventMutex);
  
    while (mEventList.empty()) {
      mEventCondition.wait(lock);
      if (!IsRunning()) {
        return;
      }
    }
    e = mEventList.front();
    mEventList.pop_front();
  }
  
  mDispatcher->DispatchEvent(e);
}

void IThreadedSubSystem::DispatchThreadEvents() {
  PCCE_CHECK(mDispatcher, "IThreadedSubSystem::DispatchThreadEvents: mDispatcher is null");
  tIEventPtrList l;
  {
    boost::mutex::scoped_lock lock(mEventMutex);
    l.swap(mEventList);
  }
  mDispatcher->DispatchEvents(l);
}

void IThreadedSubSystem::ThreadStart() {
  try {
    vInitializeThread(mConfigSection);
    mConfigSection.reset();
    {
      boost::mutex::scoped_lock lock(mRunningMutex);
      mRunning = true;
      mInitCondition.notify_one();
    }
    
    vRunThread();
    vShutdownThread();
  } catch (Exception& e) {
    tIEventPtr event(new ThreadExceptionEvent(
      vGetSubSystemName() + ": " + e.GetError(),
      e.GetFileName(),
      e.GetLineNumber()));
    EventSystemSingleton::Get()->QueueEvent(event);

    mInitCondition.notify_one();
  }
}

void IThreadedSubSystem::SendEventToThread(const tIEventPtr& event) {
  {
    boost::mutex::scoped_lock lock(mEventMutex);
    mEventList.push_back(event);
  }
  mEventCondition.notify_one();
}

bool IThreadedSubSystem::DoIsRunning() {
  return mRunning;
}

void pcce::HandleThreadExceptionEvent(const tIEventPtr& event) {
  PCCE_CHECK(event->vGetEventType() == ThreadExceptionEvent::sGetEventType(), "HandleThreadExceptionEvent: Unexpected event type");

  boost::shared_ptr<ThreadExceptionEvent> e = boost::dynamic_pointer_cast<ThreadExceptionEvent>(event);
  throw(pcce::Exception(e->GetError(), e->GetFileName(), e->GetLineNumber()));
}
