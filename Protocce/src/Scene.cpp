/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Scene class

#include "config.h"
#ifdef PCCE_OSX
#include <Carbon/Carbon.h>
#endif
#include <Ogre.h>
#include "Exception.h"
#include "GameObjectDB.h"
#include "DeleteSceneEvent.h"
#include "Scene.h"

using namespace pcce;

Scene::Scene(): mEntityGameObjectMap(), mOgreScene(NULL), mOgreCamera(NULL) {
  mOgreScene = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC);
}

Scene::Scene(Ogre::SceneType sceneType): mOgreScene(NULL), mOgreCamera(NULL) {
  mOgreScene = Ogre::Root::getSingleton().createSceneManager(sceneType);
}

Scene::Scene(Ogre::SceneManager* sceneManager): mOgreScene(sceneManager), mOgreCamera(NULL) {
  Ogre::SceneManager::CameraIterator c = mOgreScene->getCameraIterator();
  if (c.hasMoreElements()) { 
    mOgreCamera = c.getNext();
    PCCE_CHECK(mOgreCamera != NULL, "Scene Ogre::CameraIterator returned null");
  }
  PCCE_CHECK(!c.hasMoreElements(), "Scene cannot have multiple cameras");
}

Scene::~Scene() {
  tIEventPtr sceneEvent(new DeleteSceneEvent(mOgreScene));      
  EventSystemSingleton::Get()->DispatchEvent(sceneEvent);
}

Ogre::SceneManager* Scene::GetOgreSceneManager() {
  return mOgreScene;
}

Ogre::Entity* Scene::CreateOgreEntity(tGameObjectPtr gameObject) {
  PCCE_CHECK(gameObject, "Scene::CreateOgreEntity: Cannot add empty game object to scene");
  PCCE_CHECK(!gameObject->HasEntity(), "Scene::CreateOgreEntity: Game object already has entity");
  
  Ogre::Entity* result = mOgreScene->createEntity("", "");
  mEntityGameObjectMap.insert(tEntityGameObjectMap::value_type(result, gameObject->GetID()));
  gameObject->SetEntity(result);
  
  return result;
}

void Scene::RemoveEntity(Ogre::Entity* entity) {
  tEntityGameObjectMap::iterator i = mEntityGameObjectMap.find(entity);
  if (i != mEntityGameObjectMap.end()) {
    tGameObjectPtr gameObject = GameObjectDBSingleton::Get()->GetGameObject(i->second);
    mEntityGameObjectMap.erase(i);
    gameObject->SetEntity(NULL);
  }
}

void Scene::RemoveEntity(tGameObjectPtr gameObject) {
  tEntityGameObjectMap::iterator i = mEntityGameObjectMap.find(gameObject->GetEntity());
  if (i != mEntityGameObjectMap.end()) {
    mEntityGameObjectMap.erase(i);
    gameObject->SetEntity(NULL);
  }
}

tGameObjectID Scene::GetGameObjectID(Ogre::Entity* entity) {
  tEntityGameObjectMap::const_iterator i = mEntityGameObjectMap.find(entity);
  PCCE_CHECK(i != mEntityGameObjectMap.end(), "Scene::GetGameObjectID: Unknown entity");
  return i->second;
}

tGameObjectPtr Scene::GetGameObject(Ogre::Entity* entity) {
  tGameObjectID id = GetGameObjectID(entity);
  return GameObjectDBSingleton::Get()->GetGameObject(id);
}

Ogre::Camera* Scene::CreateOgreCamera() {
  PCCE_CHECK(mOgreCamera == NULL, "Scene already has a camera created");
  
  mOgreCamera = mOgreScene->createCamera("Camera");
  mOgreCamera->setFixedYawAxis(false);
  mOgreCamera->setPosition(0,50,0);
  mOgreCamera->lookAt(0,0,0);
  mOgreCamera->setNearClipDistance(10);
  
  return mOgreCamera;
}

Ogre::Camera* Scene::GetOgreCamera() {
  return mOgreCamera;
}
