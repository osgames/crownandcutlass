/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/
 
// Xml Serializer Class

#include "Serialization/XmlSerializer.h"
#include "Serialization/XmlSerializeNode.h"
#include "ISerializer.h"
#include "XmlNode.h"
#include "XmlDoc.h"
#include <list>
#include <string>

using namespace std;
using namespace pcce;

XmlSerializer::XmlSerializer(): mSerializeDoc(new XmlDoc()) {
}

XmlSerializer::~XmlSerializer() {}

bool XmlSerializer::Load(const string &Location) {
//  /*
//  SerializeDoc->LoadFile(Location);
//  
//  for (node in LoadLocation) {
//    SerializeList[node.type].ExecuteLoad(node); 
//  }
//  */
  return true;
}

bool XmlSerializer::Save(const string &Location) {
  XmlNode *RootNode;
  XmlNode *Node;
  tISerializeNodePtr SerializeNode;
  
  RootNode = mSerializeDoc->CreateRootNode("ProtocceSerialization");
  
  for (list< tRegisterSaveFunction >::const_iterator i = mSaveFunctionList.begin(); 
       i != mSaveFunctionList.end(); 
       i++) {
    Node = RootNode->CreateChild("Node");
    
    SerializeNode.reset( new XmlSerializeNode(Node) );
    
    tRegisterSaveFunction SaveFunc(*i);
    SaveFunc(SerializeNode);
  }
  
  mSerializeDoc->SaveFile(Location);
  return true;   
}
