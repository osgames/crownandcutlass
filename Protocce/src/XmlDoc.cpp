/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "tinyxml/tinyxml.h"
#include "Exception.h"
#include "Variant.h"
#include "stringutil.h"
#include "XmlNode.h"
#include "XmlDoc.h"

using namespace std;
using namespace pcce;

XmlDoc::XmlDoc(): mDoc(new TiXmlDocument()), mMap() {
}

XmlDoc::XmlDoc(const string& fileName): mDoc(new TiXmlDocument()), mMap() {
  LoadFile(fileName);
}

XmlDoc::~XmlDoc() {
  delete mDoc;
}

void XmlDoc::LoadFile(const string& fileName) {
  if (!mDoc->LoadFile(fileName.c_str())) {
    delete mDoc;
    mDoc = new TiXmlDocument();
    tVariantList args;
    args.push_back(fileName);
    args.push_back(mDoc->ErrorDesc());
    args.push_back(mDoc->ErrorRow());
    args.push_back(mDoc->ErrorCol());
    PCCE_THROW(Format("Could not parse XML document %. % (line %, column %)", args));
  }
  const TiXmlElement* root = mDoc->RootElement();
  if (root == NULL) {
    delete mDoc;
    mDoc = new TiXmlDocument();
    PCCE_THROW("Malformed XML (missing root node) in " + fileName);
  }
  if (root->NextSiblingElement() != NULL) {
    delete mDoc;
    mDoc = new TiXmlDocument();
    PCCE_THROW("Malformed XML (multiple root nodes) in " + fileName);
  }
}

void XmlDoc::SaveFile(const string& fileName) {
  if (!mDoc->SaveFile(fileName.c_str())) {
    tVariantList args;
    args.push_back(fileName);
    args.push_back(mDoc->ErrorDesc());
    args.push_back(mDoc->ErrorRow());
    args.push_back(mDoc->ErrorCol());
    PCCE_THROW(Format("Could not save XML document %. % (line %, column %)", args));
  }
}

XmlNode* XmlDoc::RootNode() {
  TiXmlElement* elem = mDoc->RootElement();
  if (elem == NULL) {
    return NULL;
  } else {
    return NodeFromElement(mDoc->RootElement());
  }
}

XmlNode* XmlDoc::RootNode(const string& nodeName) {
  XmlNode* root = RootNode();
  string name = root->GetName();
  PCCE_CHECK(name == nodeName, "XML root name (\"" + name + "\") is not \"" + nodeName + "\"");
  return root;
}

XmlNode* XmlDoc::CreateRootNode(const std::string& nodeName) {
  // This isn't thread safe...
  XmlNode* root = RootNode();
  PCCE_CHECK(root == NULL, "XmlDoc already has root node");

  auto_ptr< TiXmlElement > element(new TiXmlElement(nodeName));
  TiXmlElement *p = element.get();
  mDoc->LinkEndChild(p);
  element.release();
  return NodeFromElement(p);
}

XmlNode* XmlDoc::NodeFromElement(TiXmlElement* element) {
  PCCE_CHECK(element != NULL, "XmlDoc::NodeFromElement: Element is NULL");
  tXmlWrapperMap::iterator i = mMap.find(element);
  if (i == mMap.end()) {
    XmlNode value(this, element);
    tXmlWrapperPair pair(element, value);
    mMap.insert(i, pair);
    i = mMap.find(element);
    PCCE_CHECK(i != mMap.end(), "XmlDoc: Internal error creating XmlNode");
  }
  return &(i->second);
}
