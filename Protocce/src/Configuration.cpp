/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <fstream>
#include <string>
#include <boost/algorithm/string/trim.hpp>
#include "Variant.h"
#include "PropertyBag.h"
#include "Configuration.h"

using namespace std;
using namespace pcce;

tPropertyBagPtr Configuration::GetConfigSection(const string& name) const {
  Variant v = GetValue(name);
  PCCE_CHECK(v.IsPropertyBagPtr(), "Configuration::GetConfigSection: \"" + name + "\" is not a property bag");
  return v.AsPropertyBagPtr();
}

bool Configuration::TryGetConfigSection(const std::string& name, tPropertyBagPtr& result) const {
  bool success = HasProperty(name);
  if (success) {
    Variant v = GetValue(name);
    success = v.IsPropertyBagPtr();
    if (success) {
      result = v.AsPropertyBagPtr();
    }
  }
  return success;
}

void Configuration::LoadINIFile(const string& filename) {
  string line, key, value;
  string::size_type loc, len;
  tPropertyBagPtr section;
  ifstream f(filename.c_str() , ios::in);
  PCCE_CHECK(f, "Could not open config file " + filename);
  
  while (!f.eof()) {
    getline(f, line);
    boost::trim(line);
    if (line.empty()) {
      continue;
    }
    
    len = line.length();
    if ((line[0] == '[') && (line[len-1] == ']')) {
      key = boost::trim_copy(line.substr(1, len-2));
      PCCE_CHECK(!key.empty(), "Invalid config file (section name missing)");
      section.reset(new PropertyBag());
      AddProperty(key);
      SetValue(key, section);
    } else {
      PCCE_CHECK(section, "Invalid config file (missing section name)");
      
      loc = line.find("=", 0);
      if (loc == string::npos) {
        break;
      }
      key = boost::trim_copy(line.substr(0, loc));
      value = boost::trim_copy(line.substr(loc + 1, len));
      section->AddProperty(key);
      section->SetValue(key, value);
    }
  }
  
  f.close();
}

void Configuration::SaveINIFile(const string& filename) const {
  ofstream f(filename.c_str() , ios::out);
  
  for (PropertyBag::const_iterator i = this->begin(); i != this->end(); ++i) {
    f << "[ " << i.first() << " ]" << endl << endl;
    
    tPropertyBagPtr bag = i.second().AsPropertyBagPtr();
    for (PropertyBag::const_iterator j = bag->begin(); j != bag->end(); ++j) {
      f << j.first() << "=" << j.second().AsString() << endl; 
    }
    f << endl;
  }
  
  f.close();
}
