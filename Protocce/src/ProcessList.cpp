/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Process List

#include "config.h"
#include "IProcess.h"
#include "Exception.h"
#include "ProcessList.h"

using namespace pcce;

class ProcessList::ProcessListItem {
public:
  ProcessListItem() { mNext = NULL; };

  ProcessListItem* mNext;
  pcce::tIProcessPtr mData;
};

ProcessList::ProcessList(): mCurrent(NULL), mPrevious(NULL), mLast(NULL), mCount(0) {
}

ProcessList::~ProcessList() {
  if (mCurrent != NULL) {
    ProcessListItem* item;
    
    mPrevious->mNext = NULL;
    mPrevious = NULL;
    
    while (mCurrent != NULL) {
      item = mCurrent;
      mCurrent = mCurrent->mNext;
      delete item;
    }
  }
}

void ProcessList::AddProcess(pcce::tIProcessPtr process) {
  process->UnKill();
  
  ProcessListItem* item = new ProcessListItem();
  item->mData = process;
  if (mCurrent == NULL) {
    PCCE_CHECK(mCount == 0, "Count is not 0");
    PCCE_CHECK(mPrevious == NULL, "Previous is not NULL");
    mCurrent = item;
    mPrevious = item;
  } else {
    PCCE_CHECK(mPrevious->mNext == mCurrent, "Previous's next is not current");
  }
  
  mPrevious->mNext = item;
  item->mNext = mCurrent;
  mPrevious = item;
  
  mCount++;
}
    
void ProcessList::ExecuteProcesses(tMillisecond timeDelta, tMillisecond maxTime) {
  tMillisecond currentTime, endTime;
  bool done = false;
  
  PCCE_CHECK(maxTime > 0, "maxTime must be greater than 0");
  
  if (mCurrent == NULL) {
    return;
  }
  
  PCCE_CHECK(mLast == NULL, "mLast is not NULL");
  mLast = mPrevious;
  
  currentTime = GetTimeSinceStart();
  endTime = currentTime + maxTime;
  
  while ((!done) && (mCurrent != NULL) && (currentTime < maxTime)) {
    if (mCurrent == mLast) {
      done = true;
    }
    DoHandleCurrentItem(timeDelta);
    currentTime = GetTimeSinceStart();
  }
  
  mLast = NULL;
}

void ProcessList::ExecuteAllProcesses(tMillisecond timeDelta) {
  bool done = false;
  
  if (mCurrent == NULL) {
    return;
  }
  
  PCCE_CHECK(mLast == NULL, "mLast is not NULL");
  mLast = mPrevious;
  
  while ((!done) && (mCurrent != NULL)) {
    if (mCurrent == mLast) {
      done = true;
    }
    DoHandleCurrentItem(timeDelta);
  }
  
  mLast = NULL;
}

void ProcessList::ExecuteOneProcess(tMillisecond timeDelta) {
  if (mCurrent == NULL) {
    return;
  }
  DoHandleCurrentItem(timeDelta);
}

unsigned int ProcessList::GetProcessCount() {
  return mCount;
}

void ProcessList::DoHandleCurrentItem(tMillisecond timeDelta) {
  PCCE_CHECK(mCurrent != NULL, "Current is NULL");
  tIProcessPtr data = mCurrent->mData;
  if (data->IsKilled()) {
    DoDeleteCurrentItem();
  } else {
    data->vUpdate(timeDelta);
    if (data->IsKilled()) {
      DoDeleteCurrentItem();
    } else {
      mPrevious = mCurrent;
      mCurrent = mCurrent->mNext;
    }
  }
}

void ProcessList::DoDeleteCurrentItem() {
  PCCE_CHECK(mCurrent != NULL, "Current is NULL");
  
  ProcessListItem* item = mCurrent;
  tIProcessPtr next = item->mData->GetNext();
  
  if (mCount == 1) {
    mCurrent = NULL;
    mPrevious = NULL;
  } else {
    mCurrent = mCurrent->mNext;
    mPrevious->mNext = mCurrent;
  }
  delete item;
  mCount--;
  
  if (next) {
    AddProcess(next);
  }
}
