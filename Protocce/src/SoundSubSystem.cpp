/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <boost/bind.hpp>
#include "soundutil.h"
#include "Exception.h"
#include "Variant.h"
#include "IEvent.h"
#include "EventDispatcher.h"
#include "EventConnectionManager.h"
#include "SoundEvents.h"
#include "SoundResource.h"
#include "SoundSubSystem.h"

using namespace std;
using namespace pcce;

namespace {
  ALuint DoGetBuffer(tIResourcePtr soundResource) {
    boost::intrusive_ptr< SoundResource > r = boost::dynamic_pointer_cast< SoundResource >(soundResource);
    PCCE_CHECK(r, "Resource is not sound resource");
    return r->GetBuffer();
  }
}

void SoundSubSystem::vInitializeThread(tPropertyBagPtr configSection) {
  // Config setting name
  const string SoundSourceCount = "sources";
  // This seems like an ok default...
  unsigned int sourceCount = 16;
  // Just a sanity check, so we don't end up trying to create a billion sources or something
  const unsigned int MaxSourceCount = 256;

  ClearALError();
  InitializeAL();
  PCCE_AL_CHECK();

  if (configSection) {
    sourceCount = configSection->GetValueDef(SoundSourceCount, sourceCount).AsUnsignedInt();
    if ((sourceCount == 0) || (sourceCount > MaxSourceCount)) {
      tVariantList v;
      v.push_back(SoundSourceCount);
      v.push_back(MaxSourceCount);
      PCCE_THROW(Format("\"%\" config setting must be between 1 and %", v));
    }
  }

  for (unsigned int i = 0; i < sourceCount; ++i) {
    tOpenALSourcePtr s(new OpenALSource());
    mSources.insert(s);
    mAvailSources.push_back(s);
  }

  AddThreadHandler(SoundBufferEvent::sGetEventType(), bind(&SoundSubSystem::HandleBufferEvent, this, _1));
  AddThreadHandler(SoundStreamEvent::sGetEventType(), bind(&SoundSubSystem::HandleStreamEvent, this, _1));
  
  SanityCheck();
}

void SoundSubSystem::vRunThread() {
  while (IsRunning()) {
    if (mPlayingStreams.empty()) {
      DispatchThreadEventBlocking();
    } else {
      UpdateStreams();
      DispatchThreadEvents();
      SleepMs(0);
    }
  }
}

void SoundSubSystem::vShutdownThread() {
  RemoveThreadHandler(SoundStreamEvent::sGetEventType());
  RemoveThreadHandler(SoundBufferEvent::sGetEventType());
  
  SanityCheck();

  mAvailSources.clear();
  mSources.clear();

  ClearALError();

  ShutdownAL();
  PCCE_AL_CHECK();
}

void SoundSubSystem::HandleBufferEvent(const tIEventPtr& e) {
  boost::shared_ptr< SoundBufferEvent > event = boost::dynamic_pointer_cast< SoundBufferEvent >(e);

  SanityCheck();
  
  tOpenALSourcePtr source;
  tSoundID id = event->GetID();
  
  switch (event->GetAction()) {
    case saPlay: {
      bool newSource;
      source = GetBufferSource(id, newSource);
      BufferHandle newHandle(id, source);
      if (newSource) {
        alSourcei(source->GetSource(), AL_BUFFER, DoGetBuffer(event->GetResource()));
        PCCE_AL_CHECK();
      } else {
        mPlayingBuffers.remove(newHandle);
      }
      mPlayingBuffers.push_back(newHandle);
      alSourcePlay(source->GetSource());
      PCCE_AL_CHECK();
      break;
    }
    case saPause:
      if (HasSource(id, source)) {
        alSourcePause(source->GetSource());
        PCCE_AL_CHECK();
      }
      break;
    case saStop:
      if (HasSource(id, source)) {
        alSourceStop(source->GetSource());
        PCCE_AL_CHECK();
      }
      break;
    case saReleaseSource:
      ReleaseSource(id);
      break;
    default:
      PCCE_THROW("Unrecognized sound action");
  }

  SanityCheck();
}

void SoundSubSystem::HandleStreamEvent(const tIEventPtr& e) {
  boost::shared_ptr< SoundStreamEvent > event = boost::dynamic_pointer_cast< SoundStreamEvent >(e);
  
  SanityCheck();
  
  tOggStreamPtr stream;
  tSoundID id = event->GetID();
  
  switch (event->GetAction()) {
    case saPlay:
      if (!IsStreamActive(id, stream)) {
        tOpenALSourcePtr source = DoAcquireSource();
        stream.reset(new OggStream(event->GetFileName(), source));
        mPlayingStreams[id] = stream;
      }
      stream->Play();
      break;
    case saPause:
      if (IsStreamActive(id, stream)) {
        stream->Pause();
      }
      break;
    case saStop:
      if (IsStreamActive(id, stream)) {
        stream->Stop();
      }
      break;
    case saReleaseSource:
      ReleaseStream(id);
      break;
    default:
      PCCE_THROW("Unrecognized sound action");
  }
  
  SanityCheck();
}

tOpenALSourcePtr SoundSubSystem::GetBufferSource(const tSoundID id, bool& newSource) {
  tOpenALSourcePtr result;
  
  newSource = !HasSource(id, result);
  if (newSource) {
    result = AcquireBufferSource(id);
  }
  
  return result;
}

bool SoundSubSystem::HasSource(const tSoundID id, tOpenALSourcePtr& source) {
  bool result;
  tSoundIDSourceMap::const_iterator i = mBufferSourceMap.find(id);
  
  result = (i != mBufferSourceMap.end()); 
  if (result) {
    source = i->second;
  }
  
  return result;
}

tOpenALSourcePtr SoundSubSystem::AcquireBufferSource(const tSoundID id) {
  tOpenALSourcePtr result = DoAcquireSource();

  mBufferSourceMap[id] = result;
  
  return result;
}

tOpenALSourcePtr SoundSubSystem::DoAcquireSource() {
  tOpenALSourcePtr result;
  
  if (!mAvailSources.empty()) {
    result = mAvailSources.front();
    mAvailSources.pop_front();
  } else {
    PCCE_CHECK(!mPlayingBuffers.empty(), "Out of sources (too many streams active?)");
    
    BufferHandle handle = mPlayingBuffers.front();
    mPlayingBuffers.pop_front();
    mBufferSourceMap.erase(handle.mID);

    result = handle.mSource;
    alSourceStop(result->GetSource());
    PCCE_AL_CHECK();
    // This probably isn't necessary since we are about to assign this source
    //   to another buffer, but I'll do it to be safe
    alSourcei(result->GetSource(), AL_BUFFER, AL_NONE);
    PCCE_AL_CHECK();
  }
  
  return result;
}

void SoundSubSystem::ReleaseSource(const tSoundID id) {
  tOpenALSourcePtr source;
  if (HasSource(id, source)) {
    BufferHandle h(id, source);
    mPlayingBuffers.remove(h);
    mBufferSourceMap.erase(id);

    alSourceStop(source->GetSource());
    PCCE_AL_CHECK();
    alSourcei(source->GetSource(), AL_BUFFER, AL_NONE);
    PCCE_AL_CHECK();
    
    mAvailSources.push_back(source);
  }
}

bool SoundSubSystem::IsStreamActive(const tSoundID id, tOggStreamPtr& stream) {
  tStreamMap::const_iterator i = mPlayingStreams.find(id);
  bool result = (i != mPlayingStreams.end());
  if (result) {
    stream = i->second;
  }
  return result;
}

void SoundSubSystem::ReleaseStream(const tSoundID id) {
  tStreamMap::iterator i = mPlayingStreams.find(id);
  if (i != mPlayingStreams.end()) {
    tOggStreamPtr stream = i->second;
    tOpenALSourcePtr source = stream->GetSource();
    
    mPlayingStreams.erase(i);
    
    stream->Stop();
    stream.reset();

    alSourceStop(source->GetSource());
    PCCE_AL_CHECK();
    alSourcei(source->GetSource(), AL_BUFFER, AL_NONE);
    PCCE_AL_CHECK();
    
    mAvailSources.push_back(source);
  }
}

void SoundSubSystem::UpdateStreams() {
  for (tStreamMap::const_iterator i = mPlayingStreams.begin(); i != mPlayingStreams.end(); ++i) {
    (i->second)->Update();
  }
}

void SoundSubSystem::SanityCheck() {
  if (mAvailSources.size() + mPlayingBuffers.size() + mPlayingStreams.size() != mSources.size()) {
    tVariantList list;
    list.push_back(mAvailSources.size());
    list.push_back(mPlayingBuffers.size());
    list.push_back(mPlayingStreams.size());
    list.push_back(mSources.size());
    PCCE_THROW(Format("Avail (%) + Playing (%) + Streaming (%) != Total (%)", list));
  }
  if (mPlayingBuffers.size() != mBufferSourceMap.size()) {
    tVariantList list;
    list.push_back(mPlayingBuffers.size());
    list.push_back(mBufferSourceMap.size());
    PCCE_THROW(Format("Playing count (%) != map count (%)", list));
  }
}
