/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <algorithm>
#include <cstdio>
#include <vorbis/vorbisfile.h>
#include "config.h"
#include "Exception.h"
#include "OggFile.h"

namespace {
  int fseek_wrap(FILE *f,ogg_int64_t off,int whence){
    if(f==NULL)return(-1);
    return fseek(f,off,whence);
  }
      
  ov_callbacks OggCallbacks = {
    (size_t (*)(void *, size_t, size_t, void *))  fread,
    (int (*)(void *, ogg_int64_t, int))           fseek_wrap,
    (int (*)(void *))                             fclose,
    (long (*)(void *))                            ftell
  };
  
  const size_t BUFFER_SIZE = 32768;
}

using namespace pcce;

OggFile::OggFile(const std::string& fileName): mFileName(fileName), mVorbisFile(NULL), mChannels(0), mFreq(0) {
}

OggFile::~OggFile() {
  Close();
}

void OggFile::SetFileName(const std::string& fileName) {
  PCCE_CHECK(!IsOpen(), "OggFile::SetFileName: Cannot set filename of open OggFile");
  mFileName = fileName;
}

bool OggFile::IsOpen() {
  return NULL != mVorbisFile;
}

void OggFile::Open() {
  PCCE_CHECK(!IsOpen(), "OggFile::Open: OggFile already open");

  FILE* f = fopen(mFileName.c_str(), "rb");
  if (!f) {
    PCCE_THROW("LoadOgg: Could not load ogg from " + mFileName);
  }

  mVorbisFile = new OggVorbis_File();
  if (ov_open_callbacks(f, mVorbisFile, NULL, 0, OggCallbacks) != 0) {
    delete mVorbisFile;
    mVorbisFile = NULL;
    fclose(f);
    PCCE_THROW("LoadOgg: ov_open failed on " + mFileName);
  }
}

void OggFile::Close() {
  if (IsOpen()) {
    ov_clear(mVorbisFile);
    delete mVorbisFile;
    mVorbisFile = NULL;
  }
  ResetInfo();
}

void OggFile::Rewind() {
  PCCE_CHECK(IsOpen(), "OggFile::Rewind: OggFile must be open to rewind");
  
  PCCE_CHECK(ov_raw_seek(mVorbisFile, 0) == 0, "OggFile::Rewind: Could not rewind");
}

size_t OggFile::Read(size_t size, tCharVector& result) {
  // 0 for Little-Endian, 1 for Big-Endian
  #if PCCE_BIG_ENDIAN
  int endian = 1;
  #else
  int endian = 0;
  #endif
  char array[BUFFER_SIZE];
  long bytes;
  int bitStream;
  size_t totalRead = 0;
  
  PCCE_CHECK(size > 0, "OggFile::Read: size must be > 0");
  
  do {
    PCCE_CHECK(totalRead < size, "OggFile::Read: Internal error");
    
    // Read up to a buffer's worth of decoded sound data
    bytes = ov_read(mVorbisFile, array, std::min(size - totalRead, BUFFER_SIZE), endian, 2, 1, &bitStream);
    // Append to end of buffer
    result.insert(result.end(), array, array + bytes);
    
    totalRead += bytes;
  } while ((bytes > 0) && (totalRead < size));
  
  return totalRead;
}

size_t OggFile::Read(tCharVector& result) {
  size_t totalCount = 0;
  size_t readCount;
  
  do {
    readCount = Read(BUFFER_SIZE, result);
    totalCount += readCount;
  } while (readCount == BUFFER_SIZE);
  
  return totalCount;
}

unsigned short OggFile::GetChannelCount() {
  PCCE_CHECK(IsOpen(), "OggFile::GetFormat: You must open a file first");
  if (mChannels == 0) {
    GetInfo();
  }
  return mChannels;
}

unsigned long OggFile::GetFrequency() {
  PCCE_CHECK(IsOpen(), "OggFile::GetFrequency: You must open a file first");
  if (mFreq == 0) {
    GetInfo();
  }
  return mFreq;
}

void OggFile::GetInfo() {
  PCCE_CHECK(IsOpen(), "OggFile::LoadInfo: You must open a file first");
  
  vorbis_info* info = ov_info(mVorbisFile, -1);
  PCCE_CHECK(info != NULL, "OggFile::LoadInfo: ov_info failed on " + mFileName);

  mChannels = info->channels;
  mFreq = info->rate;
}

void OggFile::ResetInfo() {
  mChannels = 0;
  mFreq = 0;
}
