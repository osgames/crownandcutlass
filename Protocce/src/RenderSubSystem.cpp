/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Render scene class

#include "config.h"
#include <boost/bind.hpp>
#ifdef PCCE_OSX
#include <Carbon/Carbon.h>
#endif
#include <Ogre.h>
#include <OgreCEGUIRenderer.h>
#include <CEGUI.h>
#include "Exception.h"
#include "Log.h"
#include "UpdateSubSystemEvent.h"
#include "SceneChangeEvent.h"
#include "DeleteSceneEvent.h"
#include "RequestWindowHandleEvent.h"
#include "RenderSubSystem.h"

using namespace std;
using namespace boost;
using namespace pcce;

namespace pcce {
  
  class OgreLog: public Ogre::LogListener {
  public:
    OgreLog(): mLog(LogSingleton::Get()) {};
    
    void messageLogged(const Ogre::String& message, Ogre::LogMessageLevel lml, bool maskDebug, const Ogre::String &logName) {
      mLog->LogMessage(message, mlQuiet);
    }
    
  private:
    boost::shared_ptr< Log > mLog;
  };
  
}

class RenderSubSystem::Impl {
public:
  Impl(): mRoot(), mLog(), mLogManager(), mCeguiRenderer(NULL), mCeguiSystem(NULL), mWindow(NULL), mViewport(NULL)  { };
  ~Impl() {
    mCeguiSystem.reset();
    mCeguiRenderer.reset();
    mRoot.reset();
    mLogManager.reset();
    mLog.reset();
  }
  
  void Initialize(unsigned int width, unsigned int height, bool fullscreen, const std::string& title) {
    mLogManager.reset(new Ogre::LogManager());
    mLog.reset(new OgreLog());
    Ogre::Log* log = mLogManager->createLog("protocce", true, false, true);
    log->addListener(mLog.get());
    
    mRoot.reset(new Ogre::Root("", ""));
    
    mRoot->loadPlugin("RenderSystem_GL");
    
    Ogre::RenderSystemList *renderSystems = mRoot->getAvailableRenderers();
    PCCE_CHECK(renderSystems->size() >= 0, "No ogre render systems available");

    mRoot->setRenderSystem(renderSystems->front());
    PCCE_CHECK(mRoot->getRenderSystem() != NULL, "Could not set Ogre render system");
    
    // false because we are not using an autocreated window
    mRoot->initialise(false);
    mWindow = mRoot->createRenderWindow(title, width, height, fullscreen);
    PCCE_CHECK(mWindow != NULL, "Could not create Ogre render window");
    
    mViewport = mWindow->addViewport(NULL);
    PCCE_CHECK(mViewport != NULL, "Could not create viewport");
    mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
    
    mCeguiRenderer.reset(new CEGUI::OgreCEGUIRenderer(mWindow));
    
    mCeguiSystem.reset(new CEGUI::System(mCeguiRenderer.get()));
  }
  void Update() {
    Ogre::WindowEventUtilities::messagePump();
    mRoot->renderOneFrame();
    mWindow->update();
  }
  void SetCamera(Ogre::Camera* camera) {
    mViewport->setCamera(camera);
  }
  void SetActiveSceneManager(Ogre::SceneManager* scene) {
    mActiveScene = scene;
    mCeguiRenderer->setTargetSceneManager(mActiveScene);
  }
  Ogre::SceneManager* GetActiveSceneManager() {
    return mActiveScene;
  }
  Variant GetWindowHandle() {
    size_t windowHnd = 0;
    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    PCCE_CHECK(windowHnd != 0, "RenderSubSystem::Impl: Could not get window handle");
    return windowHnd;
  }
private:
  scoped_ptr< Ogre::Root > mRoot;
  scoped_ptr< OgreLog > mLog;
  scoped_ptr< Ogre::LogManager > mLogManager;
  scoped_ptr< CEGUI::OgreCEGUIRenderer > mCeguiRenderer;
  scoped_ptr< CEGUI::System > mCeguiSystem;
  Ogre::RenderWindow* mWindow;
  Ogre::Viewport* mViewport;
  Ogre::SceneManager* mActiveScene;
};

RenderSubSystem::RenderSubSystem(): ISubSystem(), mImpl(), mConns() {
}

RenderSubSystem::~RenderSubSystem() {
}

void RenderSubSystem::vInitialize(tPropertyBagPtr configSection) {
  const string RenderWidth = "width";
  const string RenderHeight = "height";
  const string RenderFullscreen = "fullscreen";
  const string RenderTitle = "title";
  
  unsigned int width = 800;
  unsigned int height = 600;
  bool fullscreen = false;
  string title = "Protocce render window";
  
  PCCE_CHECK(!mImpl, "Render subsystem already initialized");
  
  if (configSection) {
    if (configSection->HasProperty(RenderWidth)) {
      width = configSection->GetValueAsUnsignedInt(RenderWidth);
    }
    if (configSection->HasProperty(RenderHeight)) {
      height = configSection->GetValueAsUnsignedInt(RenderHeight);
    }
    if (configSection->HasProperty(RenderFullscreen)) {
      fullscreen = configSection->GetValueAsBool(RenderFullscreen);
    }
    if (configSection->HasProperty(RenderTitle)) {
      title = configSection->GetValueAsString(RenderTitle);
    }
  }
  
  mImpl.reset(new RenderSubSystem::Impl());
  // This should use the configuration settings
  mImpl->Initialize(width, height, fullscreen, title);
  
  mConns.AddHandler(
    UpdateSubSystemEvent::sGetEventType(),
    bind(&RenderSubSystem::HandleUpdateEvent, this, _1));
  mConns.AddHandler(
    SceneChangeEvent::sGetEventType(),
    bind(&RenderSubSystem::HandleSceneChangeEvent, this, _1));
  mConns.AddHandler(
    RequestWindowHandleEvent::sGetEventType(),
    bind(&RenderSubSystem::HandleRequestWindowEvent, this, _1));
}

void RenderSubSystem::vShutdown() {
  mConns.RemoveHandler(RequestWindowHandleEvent::sGetEventType());
  mConns.RemoveHandler(SceneChangeEvent::sGetEventType());
  mConns.RemoveHandler(UpdateSubSystemEvent::sGetEventType());
  
  // Shut down graphics system now
  mImpl.reset();
}

void RenderSubSystem::HandleUpdateEvent(const tIEventPtr& e) {
  mImpl->Update();
}

void RenderSubSystem::HandleSceneChangeEvent(const tIEventPtr& e) {
  shared_ptr< SceneChangeEvent > event = dynamic_pointer_cast< SceneChangeEvent >(e);
  tScenePtr scene = event->GetScene();
  PCCE_CHECK(scene, "Scene change event without scene");
  mImpl->SetCamera(scene->GetOgreCamera());
  mImpl->SetActiveSceneManager(scene->GetOgreSceneManager());
}

void RenderSubSystem::HandleDeleteSceneEvent(const tIEventPtr& e) {
  shared_ptr< DeleteSceneEvent > event = dynamic_pointer_cast< DeleteSceneEvent >(e);
  Ogre::SceneManager* scene = event->GetScene();
  if (mImpl->GetActiveSceneManager() == scene) {
    mImpl->SetCamera(NULL);
  }
  Ogre::Root::getSingleton().destroySceneManager(scene);
}

void RenderSubSystem::HandleRequestWindowEvent(const tIEventPtr& e) {
  shared_ptr< RequestWindowHandleEvent > event = dynamic_pointer_cast< RequestWindowHandleEvent >(e);
  event->WindowHandle = mImpl->GetWindowHandle();
}
