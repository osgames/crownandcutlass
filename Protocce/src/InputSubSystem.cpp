/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Input class

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/bind.hpp>

#include <OIS.h>
#include <CEGUI.h>

#include "UpdateSubSystemEvent.h"
#include "IEventFwd.h"
#include "RequestWindowHandleEvent.h"
#include "InputTranslator.h"
#include "SetInputTranslatorEvent.h"
#include "EventSystem.h"
#include "Log.h"
#include "Variant.h"
#include "InputSubSystem.h"

using namespace std;
using namespace boost;
using namespace OIS;
using namespace pcce;

class InputSubSystem::Impl : public KeyListener, public MouseListener {
public:
  Impl(): mInputManager(NULL), mKeyboard(NULL), mMouse(NULL), mActiveInputTrans() {};
  ~Impl() {
    if (mMouse) {
      mInputManager->destroyInputObject(mMouse);
      mMouse = 0;
      LogSingleton::Get()->LogMessage("InputSubSystem: Mouse Input not properly shutdown.", mlLoud);
    }
    if (mKeyboard) {
      mInputManager->destroyInputObject(mKeyboard);
      mKeyboard = 0;
      LogSingleton::Get()->LogMessage("InputSubSystem: Keyboard Input not properly shutdown.", mlLoud);
    }
    if (mInputManager) {
      mInputManager->destroyInputSystem(mInputManager);
      mInputManager = 0;
      LogSingleton::Get()->LogMessage("InputSubSystem: Input SubSystem not properly shutdown.", mlLoud);
    }    
  };
  
  bool keyPressed(const KeyEvent &arg) {
    CEGUI::System* sys = CEGUI::System::getSingletonPtr();
    if (sys) {
      bool injectCharSuccess = sys->injectChar(arg.text);
      bool keyDownSuccess = sys->injectKeyDown(arg.key);

      if (keyDownSuccess || injectCharSuccess) {
        return true;
      }
    }
    
    if (mActiveInputTrans) {
      mActiveInputTrans->ReceiveKeyboardInput(arg.key, ksDown);
    }
    return true;
  }

  bool keyReleased(const KeyEvent &arg) {
    CEGUI::System* sys = CEGUI::System::getSingletonPtr();
    if (sys && sys->injectKeyUp(arg.key)) {
      return true;
    }
    
    if (mActiveInputTrans) {
      mActiveInputTrans->ReceiveKeyboardInput(arg.key, ksUp);
    }
    return true;
  }

  bool mouseMoved(const MouseEvent &arg) {
    CEGUI::System* sys = CEGUI::System::getSingletonPtr();
    if (sys && sys->injectMouseMove((float)arg.state.X.rel, float(arg.state.Y.rel))) {
      return true;
    }

    if (mActiveInputTrans) {
      mActiveInputTrans->ReceiveMouseMoveInput(arg.state);
    }
    return true;
  }

  bool mousePressed(const MouseEvent &arg, MouseButtonID id) {
    CEGUI::System* sys = CEGUI::System::getSingletonPtr();
    if (sys && sys->injectMouseButtonDown(ConvertOISButtonToCEGUI(id))) {
      return true;
    }

    if (mActiveInputTrans) {
      mActiveInputTrans->ReceiveMouseButtonInput(arg.state, id, ksDown);
    }
    return true;
  }

  bool mouseReleased(const MouseEvent &arg, MouseButtonID id) {
    CEGUI::System* sys = CEGUI::System::getSingletonPtr();
    if (sys && sys->injectMouseButtonUp(ConvertOISButtonToCEGUI(id))) {
      return true;
    }

    if (mActiveInputTrans) {
      mActiveInputTrans->ReceiveMouseButtonInput(arg.state, id, ksUp);
    }
    return true;
  }
  
  void Initialize(Variant windowHandle) {
    PCCE_CHECK(windowHandle.IsInt(), "The window handle passed to the Input Subsystem was not an integer.");
    mInputManager = OIS::InputManager::createInputSystem(windowHandle.AsInt());
    mKeyboard = dynamic_cast<Keyboard*>(mInputManager->createInputObject(OISKeyboard, true));
    mKeyboard->setTextTranslation(OIS::Keyboard::Unicode);
    mKeyboard->setEventCallback( this );
    mMouse = dynamic_cast<Mouse*>(mInputManager->createInputObject(OISMouse, true));
    mMouse->setEventCallback( this );
  }
  
  void ShutDown() {
    if (mMouse) {
      mInputManager->destroyInputObject(mMouse);
      mMouse = NULL;
    }
    if (mKeyboard) {
      mInputManager->destroyInputObject(mKeyboard);
      mKeyboard = NULL;
    }
    if (mInputManager) {
      mInputManager->destroyInputSystem(mInputManager);
      mInputManager = NULL;
    }
  }
  
  void SetInputTranslator(tInputTranslatorPtr inputTrans) {
    mActiveInputTrans = inputTrans;
  }
  
  void Update() {
    mKeyboard->capture();
    mMouse->capture();
  }
  
private:
  CEGUI::MouseButton ConvertOISButtonToCEGUI(const OIS::MouseButtonID buttonId) {
    CEGUI::MouseButton result = CEGUI::LeftButton;
    switch (buttonId) {
      case OIS::MB_Left:
        result = CEGUI::LeftButton;
        break;
      case OIS::MB_Right:
        result = CEGUI::RightButton;
        break;
      case OIS::MB_Middle:
        result = CEGUI::MiddleButton;
        break;
      default:
        break;
    }
    return result;
  }

  InputManager* mInputManager;
  Keyboard* mKeyboard;
  Mouse* mMouse;
  tInputTranslatorPtr mActiveInputTrans;
};

InputSubSystem::InputSubSystem(): ISubSystem(), mImpl(new Impl()), mConns() {
}

InputSubSystem::~InputSubSystem() {
}

void InputSubSystem::vInitialize(tPropertyBagPtr configSection) {
  shared_ptr< RequestWindowHandleEvent > windowHandleEvent(new RequestWindowHandleEvent);
  
  mConns.AddHandler(
    UpdateSubSystemEvent::sGetEventType(),
    bind(&InputSubSystem::HandleUpdateEvent, this, _1));
  mConns.AddHandler(
    SetInputTranslatorEvent::sGetEventType(),
    bind(&InputSubSystem::HandleSetInputTranslatorEvent, this, _1));

  windowHandleEvent->WindowHandle = "NaN";
  EventSystemSingleton::Get()->DispatchEvent(windowHandleEvent);
  
  mImpl->Initialize(windowHandleEvent->WindowHandle);
  
}

void InputSubSystem::vShutdown() {
  mImpl->ShutDown();
}

void InputSubSystem::HandleUpdateEvent(const tIEventPtr& e) {
  mImpl->Update();
}

void InputSubSystem::HandleSetInputTranslatorEvent(const tIEventPtr& e) {
  shared_ptr< SetInputTranslatorEvent > inputTrans = dynamic_pointer_cast< SetInputTranslatorEvent >(e);
  mImpl->SetInputTranslator(inputTrans->GetInputTranslator());
}
