/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <vector>
#include <alc.h>
#include "Exception.h"
#include "config.h"
#include "OggFile.h"
#include "soundutil.h"

using namespace std;

namespace {
  const string ALNoErrorStr = "No AL error occurred";
  const string ALInvalidNameStr = "AL error: a bad name (ID) was passed to an OpenAL function";
  const string ALInvalidEnumStr = "AL error: an invalid enum value was passed to an OpenAL function";
  const string ALInvalidValueStr = "AL error: an invalid value was passed to an OpenAL function";
  const string ALInvalidOpStr = "AL error: the requested operation is not valid";
  const string ALOutOfMemoryStr = "AL error: the requested operation resulted in OpenAL running out of memory";
  const string ALOtherErrorStr = "AL error: unknown error";
}

void pcce::InitializeAL() {
  ClearALError();
  
  PCCE_CHECK(alcGetCurrentContext() == NULL, "AL already initialized");

  ALCdevice* device = alcOpenDevice(NULL); // select the "preferred device"
  PCCE_CHECK(device != NULL, "InitializeAL: Cannot open preferred device");
  if (alcGetError(device) != ALC_NO_ERROR) {
    alcCloseDevice(device);
    PCCE_THROW("InitializeAL: Could not open device (alc error)");
  }

  // TODO: check the context attributes, maybe something is useful:
  // http://www.openal.org/openal_webstf/specs/oal11spec_html/oal11spec6.html
  // 6.2.1. Context Attributes
  // my bet is on ALC_STEREO_SOURCES ;-)
  ALCcontext* context = alcCreateContext(device, NULL);
  if (context == NULL) {
    alcCloseDevice(device);
    PCCE_THROW("InitializeAL: Could not create context");
  }
  if (alcGetError(device) != ALC_NO_ERROR) {
    alcDestroyContext(context);
    alcCloseDevice(device);
    PCCE_THROW("InitializeAL: Could not open device (alc error)");
  }

  if (alcMakeContextCurrent(context) != ALC_TRUE) {
    alcDestroyContext(context);
    alcCloseDevice(device);
    PCCE_THROW("InitializeAL: Could not make context current");
  }
  if (alcGetError(device) != ALC_NO_ERROR) {
    alcMakeContextCurrent(NULL);
    alcDestroyContext(context);
    alcCloseDevice(device);
    PCCE_THROW("InitializeAL: Could not make context current (alc error)");
  }

  ClearALError();

  // Should the listener be set here?  It seems to be set to a default, and
  //   eventually I'd like to have a SetALListener method that takes pcce
  //   vectors (once we get vectors done).

  //--- listener config -------------------------------------
  ALfloat listenerPos[] = { 0.0, 0.0, 0.0 };
  ALfloat listenerVel[] = { 0.0, 0.0, 0.0 };

  // Orientation of the listener. (first 3 elements are "at",
  // second 3 are "up")
  ALfloat listenerOri[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };

  alListenerfv(AL_POSITION, listenerPos);
  alListenerfv(AL_VELOCITY, listenerVel);
  alListenerfv(AL_ORIENTATION, listenerOri);
  if (alGetError() != AL_NO_ERROR) {
    ShutdownAL();
    PCCE_THROW("InitializeAL: Could not set listener position");
  }
  //---------------------------------------------------------
}

void pcce::ShutdownAL() {
  ClearALError();
  
  ALCcontext* context = alcGetCurrentContext();
  if (context == NULL) {
    PCCE_AL_CHECK();
    return;
  }

  ALCdevice* device = alcGetContextsDevice(context);
  PCCE_AL_CHECK();

  alcSuspendContext(context);
  PCCE_AL_CHECK();
  alcDestroyContext(context);
  PCCE_AL_CHECK();
  if (device != NULL) {
    alcCloseDevice(device);
    PCCE_AL_CHECK();
  }
}

ALuint pcce::LoadOgg(const std::string& fileName) {
  ALuint buffer = AL_NONE;
  ALenum format;
  ALsizei freq;
  tCharVector data;
  OggFile f(fileName);
  f.Open();

  // Check the number of channels... always use 16-bit samples
  switch(f.GetChannelCount()) {
    case 1:
      format = AL_FORMAT_MONO16;
      break;
    case 2:
      format = AL_FORMAT_STEREO16;
      break;
    default:
      PCCE_THROW("Ogg file must have 1 or 2 channels");
  }
  freq = f.GetFrequency();
  
  f.Read(data);
  f.Close();

  ClearALError();
  try {    
    alGenBuffers(1, &buffer);
    PCCE_CHECK(alGetError() == AL_NO_ERROR, "LoadOgg: Could not generate buffer");
    PCCE_CHECK(AL_NONE != buffer, "LoadOgg: Could not generate buffer");

    alBufferData(buffer, format, &data[0], data.size(), freq);
    PCCE_CHECK(alGetError() == AL_NO_ERROR, "LoadOgg: Could not load buffer data");

    return buffer;
  } catch (Exception& e) {
    if ((buffer != AL_NONE) && (alIsBuffer(buffer) == AL_TRUE)) {
      alDeleteBuffers(1, &buffer);
    }
    ClearALError();
    throw (e);
  }
}

//---inner functions (compiled static)-----
namespace pcce {
  // buffer is always in big endian!
  static unsigned short readByte16(const unsigned char buffer[2]) {
    #if PCCE_BIG_ENDIAN
    return (buffer[0] << 8) + buffer[1];
    #else
    return (buffer[1] << 8) + buffer[0];
    #endif
  }
  static unsigned long readByte32(const unsigned char buffer[4]) {
    #if PCCE_BIG_ENDIAN
    return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
    #else
    return (buffer[3] << 24) + (buffer[2] << 16) + (buffer[1] << 8) + buffer[0];
    #endif
  }
}
//-----------------------------------------

//  References:
//  -  http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
//  -  http://www.borg.com/~jglatt/tech/wave.htm
//  -  Alut source code: static BufferData *loadWavFile (InputStream *stream)
//     http://www.openal.org/repos/openal/tags/freealut_1_1_0/alut/alut/src/alutLoader.c
ALuint pcce::LoadWav(const std::string& fileName) {
  const unsigned int BUFFER_SIZE = 32768;     // 32 KB buffers
  long bytes;
  vector <char> data;
  ALenum format;
  ALsizei freq;

  // Local resources
  FILE *f = NULL;
  char *array = NULL;
  ALuint buffer = AL_NONE;

  ClearALError();

  // Main process
  try {

    // Open for binary reading
    f = fopen(fileName.c_str(), "rb");
    if (!f)
      PCCE_THROW("LoadWav: Could not load wav from " + fileName);

    // buffers
    char magic[5];
    magic[4] = '\0';
    unsigned char buffer32[4];
    unsigned char buffer16[2];

    // check magic
    PCCE_CHECK(fread(magic,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    PCCE_CHECK(std::string(magic) == "RIFF", "LoadWav: Wrong wav file format. This file is not a .wav file (no RIFF magic): "+ fileName );

    // skip 4 bytes (file size)
    fseek(f,4,SEEK_CUR);

    // check file format
    PCCE_CHECK(fread(magic,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    PCCE_CHECK(std::string(magic) == "WAVE", "LoadWav: Wrong wav file format. This file is not a .wav file (no WAVE format): "+ fileName );

    // check 'fmt ' sub chunk (1)
    PCCE_CHECK(fread(magic,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    PCCE_CHECK(std::string(magic) == "fmt ", "LoadWav: Wrong wav file format. This file is not a .wav file (no 'fmt ' subchunk): "+ fileName );

    // read (1)'s size
    PCCE_CHECK(fread(buffer32,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned long subChunk1Size = readByte32(buffer32);
    PCCE_CHECK(subChunk1Size >= 16, "Wrong wav file format. This file is not a .wav file ('fmt ' chunk too small, truncated file?): "+ fileName );

    // check PCM audio format
    PCCE_CHECK(fread(buffer16,2,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned short audioFormat = readByte16(buffer16);
    PCCE_CHECK(audioFormat == 1, "LoadWav: Wrong wav file format. This file is not a .wav file (audio format is not PCM): "+ fileName );

    // read number of channels
    PCCE_CHECK(fread(buffer16,2,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned short channels = readByte16(buffer16);

    // read frequency (sample rate)
    PCCE_CHECK(fread(buffer32,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned long frequency = readByte32(buffer32);

    // skip 6 bytes (Byte rate (4), Block align (2))
    fseek(f,6,SEEK_CUR);

    // read bits per sample
    PCCE_CHECK(fread(buffer16,2,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned short bps = readByte16(buffer16);

    if (channels == 1)
      format = (bps == 8) ? AL_FORMAT_MONO8 : AL_FORMAT_MONO16;
    else
      format = (bps == 8) ? AL_FORMAT_STEREO8 : AL_FORMAT_STEREO16;

    // check 'data' sub chunk (2)
    PCCE_CHECK(fread(magic,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    PCCE_CHECK(std::string(magic) == "data", "LoadWav: Wrong wav file format. This file is not a .wav file (no data subchunk): "+ fileName );

    PCCE_CHECK(fread(buffer32,4,1,f) == 1, "LoadWav: Cannot read wav file "+ fileName );
    unsigned long subChunk2Size = readByte32(buffer32);

    // The frequency of the sampling rate
    freq = frequency;
    PCCE_CHECK(sizeof(freq) == sizeof(frequency), "LoadWav: freq and frequency different sizes");

    array = new char[BUFFER_SIZE];

    while (data.size() != subChunk2Size) {
      // Read up to a buffer's worth of decoded sound data
      bytes = fread(array, 1, BUFFER_SIZE, f);

      if (bytes <= 0)
        break;

      if (data.size() + bytes > subChunk2Size)
       bytes = subChunk2Size - data.size();

      // Append to end of buffer
      data.insert(data.end(), array, array + bytes);
    };

    delete []array;
    array = NULL;

    fclose(f);
    f = NULL;

    alGenBuffers(1, &buffer);
    PCCE_CHECK(alGetError() == AL_NO_ERROR, "LoadWav: Could not generate buffer");
    PCCE_CHECK(AL_NONE != buffer, "LoadWav: Could not generate buffer");

    alBufferData(buffer, format, &data[0], data.size(), freq);
    PCCE_CHECK(alGetError() == AL_NO_ERROR, "LoadWav: Could not load buffer data");

    return buffer;
  } catch (Exception e) {
    if (buffer)
      if (alIsBuffer(buffer) == AL_TRUE)
        alDeleteBuffers(1, &buffer);

    if (array)
      delete []array;

    if (f)
      fclose(f);

    throw (e);
  }
}

ALuint pcce::GenSource() {
  ALuint source;
  bool looping = false;
  ALfloat sourcePos[] = {0.0, 0.0, 0.0};
  ALfloat sourceVel[] = {0.0, 0.0, 0.0};

  ClearALError();

  // Bind buffer with a source.
  alGenSources(1, &source);
  PCCE_CHECK(alGetError() == AL_NO_ERROR, "GenSource: Could not generate source");

  alSourcef(source, AL_PITCH, 1.0);
  alSourcef(source, AL_GAIN, 1.0);
  alSourcefv(source, AL_POSITION, sourcePos);
  alSourcefv(source, AL_VELOCITY, sourceVel);
  alSourcei(source, AL_LOOPING, looping);

  PCCE_CHECK(alGetError() == AL_NO_ERROR, "GenSource: Could not set source attributes");

  return source;
}

ALuint pcce::GenSource(ALuint buffer) {
  ClearALError();
  ALuint source = GenSource();
  alSourcei(source, AL_BUFFER, buffer);

  PCCE_CHECK(alGetError() == AL_NO_ERROR, "GenSource: Could not set source buffer");

  return source;
}

void pcce::ClearALError() {
  alGetError();
}

const std::string& pcce::GetALErrorStr(const ALenum error) {
  switch(error) {
    case AL_NO_ERROR:
      return ALNoErrorStr;
      break;
    case AL_INVALID_NAME:
      return ALInvalidNameStr;
      break;
    case AL_INVALID_ENUM:
      return ALInvalidEnumStr;
      break;
    case AL_INVALID_VALUE:
      return ALInvalidValueStr;
      break;
    case AL_INVALID_OPERATION:
      return ALInvalidOpStr;
      break;
    case AL_OUT_OF_MEMORY:
      return ALOutOfMemoryStr;
      break;
    default:
      return ALOtherErrorStr;
      break;
  }
  // Avoid warning in OSX
  return ALOtherErrorStr;
}
