/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "tinyxml/tinyxml.h"
#include "Exception.h"
#include "Variant.h"
#include "XmlDoc.h"
#include "XmlNode.h"

using namespace std;
using namespace pcce;

XmlNode::XmlNode(XmlDoc* doc, TiXmlElement* element): mDoc(doc), mElement(element), mAttributes(new PropertyBag()) {
  PCCE_CHECK(mDoc != NULL, "XmlNode: Doc is NULL");
  PCCE_CHECK(mElement != NULL, "XmlNode: Element is NULL");
  BuildAttributes();
}

const std::string XmlNode::GetName() const {
  return mElement->ValueStr();
}

XmlNode* XmlNode::GetNextSibling() const {
  TiXmlElement* element = mElement->NextSiblingElement();
  if (element == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(element);
  }
}

XmlNode* XmlNode::GetNextSibling(const string& nodeName) const {
  TiXmlElement* element = mElement->NextSiblingElement(nodeName);
  if (element == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(element);
  }
}

XmlNode* XmlNode::GetPrevSibling() const {
  TiXmlNode* node = mElement->PreviousSibling();
  if (node == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(node->ToElement());
  }
}

XmlNode* XmlNode::GetPrevSibling(const string& nodeName) const {
  TiXmlNode* node = mElement->PreviousSibling(nodeName);
  if (node == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(node->ToElement());
  }
}

XmlNode* XmlNode::GetFirstChild() const {
  TiXmlElement* element = mElement->FirstChildElement();
  if (element == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(element);
  }
}

XmlNode* XmlNode::GetFirstChild(const string& nodeName) const {
  TiXmlElement* element = mElement->FirstChildElement(nodeName);
  if (element == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(element);
  }
}

XmlNode* XmlNode::CreateChild(const string& nodeName) {
  auto_ptr< TiXmlElement > element(new TiXmlElement(nodeName));
  TiXmlElement *p = element.get();
  mElement->LinkEndChild(p);
  element.release();
  return mDoc->NodeFromElement(p);
}

XmlNode* XmlNode::GetParent() const {
  TiXmlNode* node = mElement->Parent();
  if (node == NULL) {
    return NULL;
  } else {
    return mDoc->NodeFromElement(node->ToElement());
  }
}

const Variant XmlNode::GetAttribute(const string& name) const {
  Variant result = mAttributes->GetValueDef(name, Variant());
  PCCE_CHECK(result.HasValue(), "XmlNode does not have attribute named \"" + name + "\"");
  return result;
}

void XmlNode::SetAttribute(const std::string& name, Variant value) {
  mAttributes->AddProperty(name);
  mAttributes->SetValue(name, value);
  mElement->SetAttribute(name, value.AsString());
}

const tPropertyBagPtr& XmlNode::GetAttributes() const {
  return mAttributes;
}

void XmlNode::BuildAttributes() {
  for (const TiXmlAttribute* a = mElement->FirstAttribute(); a != NULL; a = a->Next()) {
    string name = a->NameTStr();
    PCCE_CHECK(!mAttributes->HasProperty(name), "XmlNode has duplicate attribute names");
    mAttributes->AddProperty(name);
    mAttributes->SetValue(name, a->ValueStr());
  }
}
