/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Utility functions

#include "config.h"
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#pragma comment ( lib, "winmm" )
#include <windows.h>
#include <mmsystem.h>
#else
#include <sys/time.h>
#endif
#include <boost/thread/mutex.hpp>
#include "Exception.h"
#include "config.h"
#include "mathutil.h"
#include "util.h"

using namespace std;
using namespace pcce;

#ifndef WIN32
// Just a helper function to make initializing the global possible
timeval GetTimeOfDayWrapper();
#endif

// Globals for GetTimeSinceStart()
static boost::mutex gCurrentMutex;
// This is just so we can avoid possible issues with time going
// backwards like SDL seems to do
static tMillisecond gLastTime = 0;
#ifdef WIN32
static DWORD gStartTime = timeGetTime();
static DWORD gIncrement = 0;
#else
static timeval gStartTime = GetTimeOfDayWrapper();
#endif

void pcce::SleepMs(tMillisecond time) {
#ifdef WIN32
  Sleep(time);
#else
  // This should really check the return value
  usleep(time * usPerMs);
#endif
}

tMillisecond pcce::GetTimeSinceStart() {
  boost::mutex::scoped_lock lock(gCurrentMutex);
  tMillisecond current;
#ifdef WIN32
  DWORD now = timeGetTime();
  if (now < gStartTime) {
    PCCE_CHECK(gIncrement == 0, "gIncrement is not zero (multiple timer wraps?)");
    const DWORD maxTime = -1;
    gIncrement = maxTime - gStartTime;
    gStartTime = 0;
  }
  current = now - gStartTime + gIncrement;
#else
  timeval t;
  PCCE_CHECK(gettimeofday(&t, NULL) == 0, "gettimeofday failed");
  
  current = 0;
  switch (Compare(t.tv_sec, gStartTime.tv_sec)) {
    case crLess:
      break;
    case crEqual:
      if (t.tv_usec > gStartTime.tv_usec) {
        current = (t.tv_usec - gStartTime.tv_usec) / usPerMs;
      }
      break;
    case crGreater:
      current = ((t.tv_sec - 1 - gStartTime.tv_sec) * msPerS)
        + ((t.tv_usec + ((1 * msPerS * usPerMs) - gStartTime.tv_usec)) / usPerMs);
      break;
    default:
      PCCE_THROW("Unexpected Compare result");
      break;
  }
#endif
  if (current < gLastTime) {
    PCCE_THROW("Timer running backwards");
    //current = gLastTime;
  }
  gLastTime = current;
  return gLastTime;
}

#ifndef WIN32
timeval GetTimeOfDayWrapper() {
  timeval time;
  PCCE_CHECK(gettimeofday(&time, NULL) == 0, "gettimeofday failed");
  return time;
}
#endif
