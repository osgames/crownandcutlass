/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Input Translator Class

#include "Exception.h"
#include "IEvent.h"
#include "EventSystem.h"
#include "Log.h"
#include "SetInputTranslatorEvent.h"
#include "InputTranslator.h"

using namespace std;
using namespace boost;
using namespace pcce;
using namespace OIS;

void InputTranslator::AddKeyHandler(const OIS::KeyCode kc, const tKeyState state, const tKeyInputToEvent InputFunction) {
	pair<KeyCode, tKeyState> keyPair(kc, state);
  tKeyMap::iterator i = mKeyMap.find(keyPair);
  if (i != mKeyMap.end()) { 
    PCCE_THROW("This key already has a keyboard event handler.");
  }
  mKeyMap.insert(i, make_pair(keyPair, InputFunction));
}

void InputTranslator::RemoveKeyHandler(const OIS::KeyCode kc, const tKeyState state) {
  mKeyMap.erase(make_pair(kc, state));
}

bool InputTranslator::ReceiveKeyboardInput(const OIS::KeyCode kc, const tKeyState state) {
	pair<const KeyCode, const tKeyState> keyPair(kc, state);
  tKeyMap::const_iterator i = mKeyMap.find(keyPair);
  
  if (i != mKeyMap.end()) { 
    tKeyInputToEvent inputToEvent = i->second;
    tIEventPtr event = inputToEvent(kc, state);

    if (event) {
      EventSystemSingleton::Get()->QueueEvent(event);
      return true;
    }
  }
  return false;
}

void InputTranslator::AddMouseMoveHandler(tMouseMoveToEvent InputFunction) {
  mMouseMoveEvent.swap(InputFunction);
}

void InputTranslator::RemoveMouseMoveHandler() {
  mMouseMoveEvent.clear();
}

void InputTranslator::AddMouseClickHandler(const OIS::MouseButtonID mbid, const tMouseClickToEvent InputFunction) {
  tMouseClickMap::iterator i = mMouseClickMap.find(mbid);
  if (i != mMouseClickMap.end()) { 
    PCCE_THROW("This button already has a mouse click event handler.");
  }
  mMouseClickMap.insert(i, make_pair(mbid, InputFunction));
}

void InputTranslator::RemoveMouseClickHandler(const OIS::MouseButtonID mbid) {
  mMouseClickMap.erase(mbid);
}

bool InputTranslator::ReceiveMouseMoveInput(const OIS::MouseState &ms) {
  if (!mMouseMoveEvent.empty()) {
    tIEventPtr event = mMouseMoveEvent(ms);

    if (event) {
      EventSystemSingleton::Get()->QueueEvent(event);
      return true;
    }
  }
  return false;
}
bool InputTranslator::ReceiveMouseButtonInput(const OIS::MouseState &ms, 
                                              const OIS::MouseButtonID mbid, 
                                              const tKeyState state) {
  tMouseClickMap::const_iterator i;
  i = mMouseClickMap.find(mbid);

  if (i != mMouseClickMap.end()) {
    tMouseClickToEvent inputToEvent = i->second;
    tIEventPtr event = inputToEvent(ms, mbid, state);

    if (event) {
      EventSystemSingleton::Get()->QueueEvent(event);
      return true;
    }
  }
  return false;
}

void pcce::SetActiveInputTranslator(const tInputTranslatorPtr& translator) {
  tIEventPtr event(new SetInputTranslatorEvent(translator));
  EventSystemSingleton::Get()->DispatchEvent(event);
}
