/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "Log.h"
#include "IResource.h"

using namespace pcce;
using namespace std;
using namespace boost;

IResource::IResource(const string& name): mMutex(), mRefCount(0), mLoaded(false), mName(name), mGroup("") {
}

IResource::~IResource() {
  if (mLoaded) {
    LogSingleton::Get()->LogMessage("Resource " + mName + " deleted while still loaded", mlDebug);
  }
}

void IResource::Load() {
  if (!mLoaded) {
    mLoaded = vDoLoad();
  }
}

void IResource::Unload() {
  if (mLoaded) {
    mLoaded = !vDoUnload();
  }
}

string IResource::GetName() {
  return mName;
}

string IResource::GetGroup() {
  return mGroup;
}

void IResource::SetGroup(const std::string& group) {
  mGroup = group;
}

unsigned int IResource::GetRefCount() {
  mutex::scoped_lock lock(mMutex);
  return mRefCount;
}

unsigned int IResource::AddRef() {
  mutex::scoped_lock lock(mMutex);
  return ++mRefCount;
}

unsigned int IResource::Release() {
  mutex::scoped_lock lock(mMutex);
  PCCE_CHECK(mRefCount != 0, "IResource::Release(): mRefCount = 0");
  unsigned int result = --mRefCount;
  // This is <= 2 because the resource manager always keeps 2 copies of any
  //   created resources, but we don't want that to keep the data loaded.
  if (mRefCount <= 2) {
    Unload();
  }
  return result;
}

void pcce::intrusive_ptr_add_ref(IResource* r) {
  PCCE_CHECK(r != NULL, "IResource intrusive_ptr_add_ref: r is NULL");
  r->AddRef();
}

void pcce::intrusive_ptr_release(IResource* r) {
  PCCE_CHECK(r != NULL, "IResource intrusive_ptr_release: r is NULL");
  if (r->Release() == 0) {
    delete r;
  }
}
