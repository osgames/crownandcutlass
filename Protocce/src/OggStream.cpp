/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "soundutil.h"
#include "OggStream.h"

using namespace pcce;

OggStream::OggStream(const std::string fileName, tOpenALSourcePtr source):
  mState(ssStopped),
  mSource(source),
  mFile(fileName),
  mQueuedBuffers(0),
  mProcessedBuffers(0),
  mStreamActive(false)
{
  mFile.Open();
  switch (mFile.GetChannelCount()) {
    case 1:
      mFormat = AL_FORMAT_MONO16;
      break;
    case 2:
      mFormat = AL_FORMAT_STEREO16;
      break;
    default:
      PCCE_THROW("Ogg file must have 1 or 2 channels");
  }
  mFreq = mFile.GetFrequency();

  ClearALError();
  alGenBuffers(sBufferCount, mBuffers);
  PCCE_AL_CHECK();
}

OggStream::~OggStream() {
  try {
    if (mSource) {
      Stop();
    }
  } catch (Exception&) {
  }
  
  ClearALError();
  alDeleteBuffers(sBufferCount, mBuffers);
  ClearALError();
}

bool OggStream::IsPlaying() {
  return mProcessedBuffers == mQueuedBuffers;
}

void OggStream::Play() {
  PCCE_CHECK(mSource, "Cannot play OggStream without source");
  
  if (mState != ssPlaying) {
    if (!mStreamActive) {
      for (size_t i = 0; i < sBufferCount; ++i) {
        mStreamActive = FillBuffer(mBuffers[i]);
        if (!mStreamActive) {
          break;
        }
        QueueBuffer(mBuffers[i]);
      }
    }

    mState = ssPlaying;
  }
  
  Update();
  
  ClearALError();
  alSourcePlay(mSource->GetSource());
  PCCE_AL_CHECK();
}

void OggStream::Pause() {
  PCCE_CHECK(mSource, "Cannot pause OggStream without source");
  
  PCCE_CHECK(mState != ssStopped, "Cannot pause stopped OggStream");
  
  mState = ssPaused;

  ClearALError();
  alSourcePause(mSource->GetSource());
  PCCE_AL_CHECK();
}

void OggStream::Stop() {
  PCCE_CHECK(mSource, "Cannot stop OggStream without source");
  
  if (mState != ssStopped) {
    mState = ssStopped;

    ClearALError();
    alSourceStop(mSource->GetSource());
    PCCE_AL_CHECK();

    // According to the OpenAL 1.1 spec when a source is stopped, all of its
    //   queued buffers become processed.
    ALuint buffers[sBufferCount];
    UnqueueProcessedBuffers(buffers);

    mFile.Rewind();
    mQueuedBuffers = 0;
    mProcessedBuffers = 0;

    mStreamActive = false;
  }
}

void OggStream::Update() {
  PCCE_CHECK(mSource, "Cannot update OggStream without source");
  
  unsigned int processed;
  ALuint buffers[sBufferCount];
  
  processed = UnqueueProcessedBuffers(buffers);
  mProcessedBuffers += processed;
  
  if (!mStreamActive) {
    return;
  }
  
  for (size_t i = 0; i < processed; ++i) {
    mStreamActive = FillBuffer(buffers[i]);
    if (!mStreamActive) {
      break;
    }
    QueueBuffer(buffers[i]);
  }
}

tOpenALSourcePtr OggStream::GetSource() {
  return mSource;
}

void OggStream::SetSource(tOpenALSourcePtr source) {
  if (mSource) {
    Stop();
  }
  mSource = source;
  // Assign it to the right buffers!
}

unsigned int OggStream::QueuedBufferCount() {
  PCCE_CHECK(mSource, "OggStream::QueuedBufferCount: mSource is not set");
  
  ALint queued;
  ClearALError();
  alGetSourcei(mSource->GetSource(), AL_BUFFERS_QUEUED, &queued);
  PCCE_AL_CHECK();
  
  if (queued < 0) {
    queued = 0;
  }
  return queued;
}

unsigned int OggStream::ProcessedBufferCount() {
  PCCE_CHECK(mSource, "OggStream::ProcessedBuffers: mSource is not set");
  
  ALint processed;
  
  ClearALError();
  alGetSourcei(mSource->GetSource(), AL_BUFFERS_PROCESSED, &processed);
  PCCE_AL_CHECK();
  
  if (processed < 0) {
    processed = 0;
  }
  return processed;
}

bool OggStream::FillBuffer(ALuint& buffer) {
  const size_t BUFFER_SIZE = 65536;
  size_t readCount;
  tCharVector data;
  
  readCount = mFile.Read(BUFFER_SIZE, data);
  // If readCount is 0, we hit the end of the file
  if (readCount == 0) {
    return false;
  }
  
  ClearALError();
  alBufferData(buffer, mFormat, &data[0], data.size(), mFreq);
  PCCE_AL_CHECK();
  
  return true;
}

void OggStream::QueueBuffer(const ALuint& buffer) {
  PCCE_CHECK(mSource, "Cannot queue buffer without a source");
  
  ClearALError();
  alSourceQueueBuffers(mSource->GetSource(), 1, &buffer);
  PCCE_AL_CHECK();
  
  ++mQueuedBuffers;
}

unsigned int OggStream::UnqueueProcessedBuffers(ALuint buffers[]) {
  unsigned int processed = ProcessedBufferCount();
  
  if (processed >= 0) {
    PCCE_CHECK(
      processed <= sBufferCount,
      "OggStream::Stop: More buffers processed than allocated");
    
    alSourceUnqueueBuffers(mSource->GetSource(), processed, buffers);
    PCCE_AL_CHECK();
    
    // For some reason this fails on OSX
    //PCCE_CHECK(ProcessedBufferCount() == 0, "didnt unqueue");
  }
  
  return processed;
}
