/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// General exception class

#include "config.h"
#include <string>
#include <sstream>
#include "Exception.h"

using namespace pcce;
using namespace std;

Exception::Exception(const std::string &error, const std::string &fileName, const unsigned int lineNumber):
  mError(error), mFileName(fileName), mLineNumber(lineNumber), mFullError(""), mIsFullErrorSet(false)
{
}

Exception::Exception(const Exception& copy):
  mError(copy.mError),
  mFileName(copy.mFileName),
  mLineNumber(copy.mLineNumber),
  mFullError(copy.mFullError),
  mIsFullErrorSet(copy.mIsFullErrorSet)
{
}

Exception& Exception::operator=(const Exception& rhs) {
  if (this != &rhs) {
    mError = rhs.mError;
    mFileName = rhs.mFileName;
    mLineNumber = rhs.mLineNumber;
    mFullError = rhs.mFullError;
    mIsFullErrorSet = rhs.mIsFullErrorSet;
  }
  return *this;
}

const string& Exception::GetError() const {
  return mError;
}

const string& Exception::GetFileName() const {
  return mFileName;
}

const unsigned int& Exception::GetLineNumber() const {
  return mLineNumber;
}

const string& Exception::GetFullError() const {
  if (!mIsFullErrorSet) {
    std::ostringstream stream;
    stream << mFileName << " (" << mLineNumber << "): " << mError;
    mFullError = stream.str();
    mIsFullErrorSet = true;
  }
  return mFullError;
}

/*
void pcce::check(bool condition, const std::string& error, const std::string &fileName, const unsigned int lineNumber) {
  if (!condition) {
    throw(pcce::Exception(error, fileName, lineNumber));
  }
}
*/
