/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// String utility functions

#include <sstream>
#include <boost/functional/hash/hash.hpp>
#include "Exception.h"
#include "Variant.h"
#include "stringutil.h"

using namespace std;
using namespace pcce;

tHashValue pcce::HashString(const string& s) {
  boost::hash<string> string_hash;
  return string_hash(s);
}

string pcce::Format(const string& formatStr, const tVariantList values) {
  const char EscapeChar = '|';
  const char FormatChar = '%';

  ostringstream s;
  bool prevEscape = false;
  tVariantList::const_iterator valueIter = values.begin();
  
  for(string::const_iterator i = formatStr.begin(); i != formatStr.end(); ++i) {
    char c = *i;

    if (prevEscape) {
      switch (c) {
        case EscapeChar:
        case FormatChar:
          s << c;
          break;
        default:
          PCCE_THROW(string("Invalid escape character \"") + c + string("\""));
          break;
      }
    } else if (c == FormatChar) {
      PCCE_CHECK(valueIter != values.end(), "Too few values passed for format string");
      s << (*valueIter).AsString();
      ++valueIter;
    } else if (c != EscapeChar) {
      s << c;
    }

    prevEscape = (!prevEscape) && (c == EscapeChar);
  }
  return s.str();
}

string pcce::GetNL() {
  ostringstream s;
  s << endl;
  return s.str();
}
