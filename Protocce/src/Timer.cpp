/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Timer class

#include "Exception.h"
#include "Timer.h"

using namespace pcce;

Timer::Timer(): mNextTimerKey(0), mFrameKey(StartCustomTimer()), mLastFrameStart(GetTimeSinceStart()) {
}

Timer::~Timer() {
  // Anything need to happen here?
}

tMillisecond Timer::MarkFrameStart() {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  mLastFrameStart = GetCustomTimerStartTime(mFrameKey);
  ResetCustomTimer(mFrameKey);
  return GetCustomTimerStartTime(mFrameKey) - mLastFrameStart;
}

tMillisecond Timer::GetTimeBetweenFrameStarts() {
  tMillisecond frameStart = GetCustomTimerStartTime(mFrameKey);
  //PCCE_CHECK(frameStart >= mLastFrameStart, "Current frame started before previous frame");
  return frameStart - mLastFrameStart;
}

tMillisecond Timer::GetTimeSinceFrameStart() {
  return CheckCustomTimer(mFrameKey);
}

tTimerKey Timer::StartCustomTimer() {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  tTimerKey key = mNextTimerKey++;
  mMap[key] = GetTimeSinceStart();
  return key;
}

tMillisecond Timer::GetCustomTimerStartTime(const tTimerKey key) {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  tTimerMap::const_iterator i = mMap.find(key);
  PCCE_CHECK(i != mMap.end(), "Timer.GetCustomTimerStartTime: Key is not active");
  return i->second;
}

tMillisecond Timer::CheckCustomTimer(const tTimerKey key) {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  tTimerMap::const_iterator i = mMap.find(key);
  PCCE_CHECK(i != mMap.end(), "Timer.CheckCustomTimer: Key is not active");
  return GetTimeSinceStart() - i->second;
}

tMillisecond Timer::EndCustomTimer(const tTimerKey key) {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  tMillisecond time = CheckCustomTimer(key);
  mMap.erase(key);
  return time;
}

tMillisecond Timer::ResetCustomTimer(const tTimerKey key) {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  tTimerMap::const_iterator i = mMap.find(key);
  PCCE_CHECK(i != mMap.end(), "Timer.ResetCustomTimer: Key is not active");
  tMillisecond now = GetTimeSinceStart();
  tMillisecond time = now - i->second;
  mMap[key] = now;
  return time;
}

bool Timer::IsCustomTimerActive(const tTimerKey key) {
  boost::recursive_mutex::scoped_lock lock(mMapMutex);
  return mMap.find(key) != mMap.end();
}
