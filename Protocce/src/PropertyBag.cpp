/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Property Bag Class

#include "Exception.h"
#include "Variant.h"
#include "PropertyBag.h"

using namespace pcce;
using namespace std;
using namespace boost;

PropertyBag::PropertyBag(): mPropMap() {
}

PropertyBag::~PropertyBag() {
}

void PropertyBag::AddProperty(const string& name) {
  tPropertyMap::iterator i = mPropMap.find(name);
  if (i == mPropMap.end()) {
    mPropMap.insert(i, tPropertyMapPair(name, Variant()));
  }
}

void PropertyBag::RemoveProperty(const string& name) {
  mPropMap.erase(name);
}

bool PropertyBag::HasProperty(const string& name) const {
  tPropertyMap::const_iterator i = mPropMap.find(name);
  return i != mPropMap.end();
}

unsigned int PropertyBag::GetPropertyCount() const {
  return mPropMap.size();
}

Variant PropertyBag::GetValue(const string& name) const {
  tPropertyMap::const_iterator i = mPropMap.find(name);
  PCCE_CHECK(i != mPropMap.end(), "PropertyBag does not have property \"" + name + "\"");
  return i->second;
}

Variant PropertyBag::GetValueDef(const string& name, Variant defaultValue) const {
  tPropertyMap::const_iterator i = mPropMap.find(name);
  if (i == mPropMap.end()) {
    return defaultValue;
  } else {
    return i->second;
  }
}

bool PropertyBag::HasValue(const string& name) const {
  tPropertyMap::const_iterator i = mPropMap.find(name);
  PCCE_CHECK(i != mPropMap.end(), "PropertyBag does not have property \"" + name + "\"");
  return i->second.HasValue();
}

void PropertyBag::SetValue(const string& name, Variant value) {
  tPropertyMap::iterator i = mPropMap.find(name);
  PCCE_CHECK(i != mPropMap.end(), "PropertyBag does not have property \"" + name + "\"");
  i->second = value;
}

void PropertyBag::ClearValue(const string& name) {
  tPropertyMap::iterator i = mPropMap.find(name);
  PCCE_CHECK(i != mPropMap.end(), "PropertyBag does not have property \"" + name + "\"");
  i->second.Clear();
}

int PropertyBag::GetValueAsInt(const string& name) const {
  Variant v = GetValue(name);
  return v.AsInt();
}

bool PropertyBag::GetValueAsBool(const string& name) const {
  Variant v = GetValue(name);
  return v.AsBool();
}

unsigned int PropertyBag::GetValueAsUnsignedInt(const string& name) const {
  Variant v = GetValue(name);
  return v.AsUnsignedInt();
}

long int PropertyBag::GetValueAsLongInt(const string& name) const {
  Variant v = GetValue(name);
  return v.AsLongInt();
}

unsigned long int PropertyBag::GetValueAsUnsignedLongInt(const string& name) const {
  Variant v = GetValue(name);
  return v.AsUnsignedLongInt();
}

tReal PropertyBag::GetValueAsReal(const string& name) const {
  Variant v = GetValue(name);
  return v.AsReal();
}

char PropertyBag::GetValueAsChar(const string& name) const {
  Variant v = GetValue(name);
  return v.AsChar();
}

unsigned char PropertyBag::GetValueAsUnsignedChar(const string& name) const {
  Variant v = GetValue(name);
  return v.AsUnsignedChar();
}

std::string PropertyBag::GetValueAsString(const string& name) const {
  Variant v = GetValue(name);
  return v.AsString();
}

tPropertyBagPtr PropertyBag::GetValueAsPropertyBagPtr(const string& name) const {
  Variant v = GetValue(name);
  return v.AsPropertyBagPtr();
}

PropertyBag::const_iterator PropertyBag::begin() const {
  return const_iterator(mPropMap.begin());
}

PropertyBag::const_iterator PropertyBag::end() const {
  return const_iterator(mPropMap.end());
}

PropertyBag::const_iterator::const_iterator(tPropertyMap::const_iterator iter) {
  mMapIter = iter;
}
PropertyBag::const_iterator::~const_iterator() {}

// Assignment and relational operators
PropertyBag::const_iterator& PropertyBag::const_iterator::operator=(const const_iterator& other) {
  mMapIter = other.mMapIter;
  return(*this);
}

bool PropertyBag::const_iterator::operator==(const const_iterator& other) {
  return (mMapIter == other.mMapIter);
}

bool PropertyBag::const_iterator::operator!=(const const_iterator& other) {
  return (mMapIter != other.mMapIter);
}

// Update state so we go to the next element
PropertyBag::const_iterator& PropertyBag::const_iterator::operator++() {
  mMapIter++;
  return (*this);
}

const std::string& PropertyBag::const_iterator::first() {
  return (*mMapIter).first;
}

const Variant& PropertyBag::const_iterator::second() {
  return (*mMapIter).second;
}
