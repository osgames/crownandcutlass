/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Math functions

#include <limits>
#include "Exception.h"
#include "mathutil.h"

using namespace pcce;

tCompareResult pcce::CompareReal(const tReal& x, const tReal& y) {
  PCCE_CHECK(!IsNaN(x) && !IsNaN(y), "CompareReal: Cannot compare NaN");

  if (x == y) {
    return crEqual;
  } else if (IsInf(x)) {
    if (IsInf(y)) {
      // We already know that x != y.  Since they are both infinity, one
      //   must be -inf, while the other is +inf.  Just return the sign of
      //   x.  If x = inf, then y = -inf and x is greater.  If x = -inf,
      //  then y = inf, so x is less than y.
      return Sign(x);
    } else {
      return (x > y) ? crGreater : crLess;
    }
  } else if (IsInf(y)) {
    return (x > y) ? crGreater : crLess;
  } else {
    // This is * 20 because the epsilon itself is too small.  In my quick test,
    //   * 20.0 seemed to give decent results.
    const tReal e = std::numeric_limits< tReal >::epsilon() * static_cast< tReal >(20.0);
    tReal result = x - y;
    if (result < e) {
      if (result > -e) {
        return crEqual;
      } else {
        return crLess;
      }
    } else {
      return crGreater;
    }
  }
}

bool pcce::SameReal(const tReal& x, const tReal& y) {
  return CompareReal(x, y) == crEqual;
}

tReal pcce::RoundTo(const tReal& x, unsigned short digits) {
  PCCE_CHECK(!IsNaN(x), "RoundTo: Cannot round NaN");
  PCCE_CHECK(!IsInf(x), "RoundTo: Cannot round Infinity");

  tReal div = std::pow(static_cast< tReal >(10.0), digits);
  return Truncate(x * div) / div;
}

tReal pcce::Truncate(const tReal& x) {
  PCCE_CHECK(!IsNaN(x), "Truncate: Cannot truncate NaN");
  PCCE_CHECK(!IsInf(x), "Truncate: Cannot truncate Infinity");
  if (x < 0) {
    return std::ceil(x);
  } else {
    return std::floor(x);
  }
}

bool pcce::IsNaN(const tReal& x) {
  return std::numeric_limits< tReal >::quiet_NaN() == x;
}

bool pcce::IsInf(const tReal& x) {
  return (std::numeric_limits< tReal >::infinity() == x)
    || (-std::numeric_limits< tReal >::infinity() == x);
}
