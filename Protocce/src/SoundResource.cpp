/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include "Log.h"
#include "Exception.h"
#include "soundutil.h"
#include "SoundResource.h"

using namespace pcce;
using namespace std;

SoundResource::SoundResource(const std::string& name): IResource(name), mFileName(""), mBuffer(AL_NONE) {
}

SoundResource::~SoundResource() {
  Unload();
}

void SoundResource::SetFileName(const std::string& fileName) {
  PCCE_CHECK(!fileName.empty(), "Cannot set SoundResource\'s filename to empty string");

  Unload();
  mFileName = fileName;
}

ALuint SoundResource::GetBuffer() {
  Load();
  PCCE_CHECK(AL_NONE != mBuffer, "SoundResource\'s buffer is unset");

  PCCE_AL_CHECK();
  
  return mBuffer;
}

bool SoundResource::vDoLoad() {
  PCCE_CHECK(!mFileName.empty(), "SoundResource\'s file name is not set");
  PCCE_CHECK(AL_NONE == mBuffer, "SoundResource\'s buffer is already set");

  mBuffer = LoadOgg(mFileName);
  PCCE_CHECK(AL_NONE != mBuffer, "LoadOgg failed");

  PCCE_AL_CHECK();
  
  return true;
}

bool SoundResource::vDoUnload() {
  PCCE_CHECK(AL_NONE != mBuffer, "SoundResource\'s buffer is not set");
  ClearALError();
  if (alIsBuffer(mBuffer)) {
    ClearALError();
    alDeleteBuffers(1, &mBuffer);
  }
  ClearALError();
  mBuffer = AL_NONE;

  return true;
}
