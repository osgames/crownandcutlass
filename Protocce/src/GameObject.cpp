/******************************************************************************
 * Protocce Engine                                                            *
 * Part of the Crown and Cutlass Project                                      *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

// Game Object Class

#include <OgreEntity.h>
#include "Variant.h"
#include "GameObject.h"

using namespace pcce;

GameObject::GameObject(tGameObjectID id): mID(id), mEntity(NULL) {
  AddProperty(kMaterialNameProperty);
  SetValue(kMaterialNameProperty, "");
  
  AddProperty(kRenderModelNameProperty);
  SetValue(kRenderModelNameProperty, "");
  
  AddProperty(kPhysicsModelNameProperty);
  SetValue(kPhysicsModelNameProperty, "");
}

GameObject::~GameObject() {
}

bool GameObject::IsRenderReady() const {
  return HasValue(kMaterialNameProperty) && HasValue(kRenderModelNameProperty);
}

bool GameObject::IsPhysicsReady() const {
  return HasValue(kPhysicsModelNameProperty);
}

std::string GameObject::GetMaterialName() const {
  Variant v = GetValueDef(kMaterialNameProperty, "");
  if (!v.HasValue()) {
    v.Set("");
  }
  return v.AsString();
}

std::string GameObject::GetRenderModelName() const {
  Variant v = GetValueDef(kRenderModelNameProperty, "");
  if (!v.HasValue()) {
    v.Set("");
  }
  return v.AsString();
}

std::string GameObject::GetPhysicsModelName() const {
  Variant v = GetValueDef(kPhysicsModelNameProperty, "");
  if (!v.HasValue()) {
    v.Set("");
  }
  return v.AsString();
}
    
void GameObject::SetMaterialName(const std::string& materialName) {
  SetValue(kMaterialNameProperty, materialName);
  if (mEntity != NULL) {
    mEntity->setMaterialName(materialName);
  }
}

void GameObject::SetRenderModelName(const std::string& renderModelName) {
  PCCE_CHECK(mEntity == NULL, "GameObject::SetRenderModelName: Cannot change render model after game object has entity");
  SetValue(kRenderModelNameProperty, renderModelName);
}

void GameObject::SetPhysicsModelName(const std::string& physicsModelName) {
  SetValue(kPhysicsModelNameProperty, physicsModelName);
}

tGameObjectID GameObject::GetID() {
  return mID;
}

bool GameObject::HasEntity() {
  return mEntity == NULL;
}

Ogre::Entity* GameObject::GetEntity() {
  return mEntity;
}

void GameObject::SetEntity(Ogre::Entity* entity) {
  PCCE_CHECK(
    (mEntity == NULL) || (entity == NULL), "GameObject::SetEntity: Game object already has an entity");
  mEntity = entity;
}

void GameObject::ClearEntity() {
  mEntity = NULL;
}
