#!/usr/bin/perl

# Perl script to read a cvs log output and output just the messages
# Feed this script something like this:
#   cvs log -N -d"2004/12/09<now" .

$file = '';
$descr = -1;
$changes = '';
$author = '';

while (<>) {
  chomp;

  $descr = $descr - 1;
  
  #print "$descr $_";
  
  if (substr($_, 0, 14) eq 'Working file: ') {
    if ($file ne '') {
      print "Already in file\n";
      exit 1;
    }
    $file = substr($_, 14);
    $changes = '';
  }
  
  if (substr($_, 0, 12) eq 'description:') {
    if ($descr >= 0) {
      print "Already in description\n";
      exit 1;
    }
    $descr = 4;
  }
  
  if (substr($_, 0, 5) eq '=====') {
    if ($changes ne '') {
      print "$file\n$changes\n";
    }
    $file = '';
    $descr = -1;
  }
  
  if ($descr == 1) {
    $authorPos = index($_, 'author: ') + length('author: ');
    $author = substr($_, $authorPos, index($_, ';', $authorPos) - $authorPos);
  }
  
  if ($descr == 0) {
    $changes .= "- " . $_ . " ($author)\n";
    $descr = 4;
    $author = '';
  }
}