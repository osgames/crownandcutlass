/* Crown and Cutlass
 * Data Updater
 * File system functions code
 */

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <string>
#include <cerrno>
#include <iostream>
#include <sys/stat.h>
#include "ccfilesystem.h"

using namespace std;

string ccTrimPath(string file) {
  string result;

  if (file.substr(0, 2) == ("." + slashChar)) {
    result = file.substr(2);
  } else {
    result = file;
  }

  if ((result.length() > 0) && (result[result.length() - 1] == slashChar[0])) {
    result = result.substr(0, result.length() - 1);
  }

  return result;
}

void ccMkDir(string name) {
  if (
  #ifndef WIN32
  mkdir(name.c_str(), 0775) != 0
  #else
  CreateDirectory(name.c_str(), NULL) == 0
  #endif
  ) {
    throw string("Could not create dir ") + name;
  }
}

bool ccGetFileInfo(ccFileInfo *info) {
  string file;
  struct stat buf;

  if (info == NULL) {
    throw string("info null in ccGetFileInfo");
  }

  if (info->name == currentDirStr) {
    // No sense in trying to create ./
    return true;
  }

  #ifdef WIN32
  file = ccTrimPath(info->name);
  #else
  file = info->name;
  #endif

  if (stat(file.c_str(), &buf) != 0) {
    if (errno != ENOENT) {
      // The error was one that wasn't expect, so throw an exception
      throw string("Unknown file info error");
    }
    return false;
  }

  info->size = buf.st_size;

  if (S_ISDIR(buf.st_mode)) {
    info->fileType = ftDir;
  } else {
    info->fileType = ftReg;
  }

  return true;
}

string ccAppendPaths(string root, string path, string seperator) {
  string result = ccTrimPath(root);

  if (result.length() > 0) {
    return result + seperator + ccTrimPath(path);
  } else {
    return ccTrimPath(path);
  }
}
