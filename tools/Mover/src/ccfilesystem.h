/* Crown and Cutlass
 * Data Updater
 * File system functions header file
 */

#if !defined( _CCFILESYS_H_ )

#define _CCFILESYS_H_

#include <string>

#ifdef WIN32
const std::string slashChar = "\\";
const std::string endOfLine = "\r\n";
#else
const std::string slashChar = "/";
const std::string endOfLine = "\n";
#endif

const std::string currentDirStr = std::string("." + slashChar);

enum ccFileType { ftReg, ftDir };

struct ccFileInfo {
  std::string name;
  unsigned int size;
  ccFileType fileType;
};

void ccMkDir(std::string name);

// Uses info.name to lookup file size and type
// Note: The caller is responsible for creating and destroying info
bool ccGetFileInfo(ccFileInfo *info);

std::string ccAppendPaths(std::string root, std::string path, std::string seperator);

std::string ccTrimPath(std::string file);

#endif
