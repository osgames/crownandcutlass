/* Crown and Cutlass
 * Updater mover utility
 * main source file
 */
 
/* This program moves all the contents of one dir into another
 *   and then executes a program.  This allows the updater to
 *   download a copy of itself into a subdirectory, fork this
 *   program to update itself, and then restart the updater.
 */

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#endif
 
#include <cstdlib>
#include <cerrno>
#include <iostream>
#include <string>
#ifndef WIN32
#include <sys/wait.h>
#endif
#include "ccfilesystem.h"

using namespace std;

string mvLinux(string src, string dest) {
  string result;
  
  result = string("cp -r ") + src;
  if (result[result.length()-1] != '/') result += '/';
  result += string("* ") + dest;
  if (result[result.length()-1] != '/') result += '/';
  
  return result;
}

string mvWin32(string src, string dest) {
  string result;
  
  result = string("xcopy ") + src;
  if (result[result.length()-1] == '\\') result = result.substr(0, result.length()-1);
  result += string(" ") + dest;
  if (result[result.length()-1] == '\\') result = result.substr(0, result.length()-1);
  result = result + " /E /Y";
  
  return result;
}

string rmdirLinux(string dir) {
  return string("rm -rf ") + dir;
}

string rmdirWin32(string dir) {
  return string("rmdir /S /Q ") + dir;
}

bool checkDir(string dir) {
  ccFileInfo info;

  info.name = dir;
  try {
    if (!ccGetFileInfo(&info)) {
      cerr << "Error: could not open " << dir << endl;
      return false;
    }
  } catch (...) {
    return false;
  }
  if (info.fileType != ftDir) {
    cerr << "Error: " << dir << " is not a directory" << endl;
    return false;
  }

  return true;
}

int main(int argc, char **argv) {
  string command;
  ccFileInfo info;

  if ((argc < 3) || (argc > 4)) {
    cerr << "Usage: Mover src/ dest/ [newprog]" << endl;
    return 1;
  }

  // If this is the windows version, we must wait for process parent to exit
  // Note: The linux version waits before calling execv
  #ifdef WIN32
  int tempExitCode;
  HANDLE parentHandle;
  unsigned int parentPID;
  char buffer[10];

  if (GetEnvironmentVariable("CCUpdaterPID", buffer, 10) != 0) {
    parentPID = atoi(buffer);
    parentHandle = OpenProcess(SYNCHRONIZE, false, parentPID);
    if (WaitForSingleObject(parentHandle, 4000) == WAIT_TIMEOUT) {
      // Timeout waiting
      cerr << "Error: timeout waiting for parent to exit" << endl;
      return 8;
    }
  } else {
    cerr << "Warning: CCUpdaterPID not in environment" << endl;
  }
  #endif
  
  // Check src
  if (!checkDir(argv[1])) {
    return 2;
  }
  
  // Check dest
  if (!checkDir(argv[2])) {
    return 3;
  }
  
  // Get the move command 
  #ifdef WIN32
  command = mvWin32(argv[1], argv[2]);
  #else
  command = mvLinux(argv[1], argv[2]);
  #endif
  
  // Actually execute the move
  if (system(command.c_str()) != 0) {
    cerr << "Error: \"" << command << "\" failed" << endl;
    return 4;
  }
  
  // Get the command to remove the temp directory
  #ifdef WIN32
  command = rmdirWin32(argv[1]);
  #else
  command = rmdirLinux(argv[1]);
  #endif
  
  // Actually remove the temp directory
  if (system(command.c_str()) != 0) {
    cerr << "Error: \"" << command << "\" failed" << endl;
    return 4;
  }
  
  if (argc < 4) {
    return 0;
  }
  
  // Check prog to run after done
  info.name = argv[3];
  try {
    if (!ccGetFileInfo(&info)) {
      cerr << "Error: could not open " << info.name << endl;
      return 5;
    }
  } catch (...) {
    cerr << "Error: could not open " << info.name << endl;
    return 5;
  }
  if (info.fileType != ftReg) {
    cerr << "Error: " << info.name << " is not a file" << endl;
    return 5;
  }

  // Start the new program
  #ifndef WIN32
  // Linux version
  pid_t parentPid;
  pid_t pid;

  parentPid = getpid();
  
  // Now we need to fork and execute the new program
  pid = fork();
  if (pid == 0) {
    // This is the child process
    execv(argv[3], NULL);
  } else if (pid > 0) {
    // This is the parent
    // Nothing needs to happen, the parent just quits
  } else {
    // Fork must have returned -1
    cerr << "Error: Could not fork" << endl;
    return 7;
  }
  #else
  // Win32 version
  if (_spawnl(_P_NOWAIT, argv[3], argv[3], NULL) == -1) {
    cerr << "Error: Could not spawn new process" << endl;
    return 7;
  }
  #endif
  
  return 0;
}
