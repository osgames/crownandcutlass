/* Crown and Cutlass
 * Data Updater
 * File check functions header file
 */

#if !defined( _CCCHECKFILES_H_ )

#define _CCCHECKFILES_H_

#include <string>
#include <boost/filesystem/path.hpp>

struct ccUpdaterFile {
  boost::filesystem::path name;
  unsigned int size;
  std::string checksum;
  std::string url;
};

// Checks to see if the file matches the FileInfo passed in
// Returns true if it does, false otherwise
bool checkFile(const ccUpdaterFile info);

#endif
