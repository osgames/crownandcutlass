/* Crown and Cutlass
 * Data Updater
 * HTTP functions source file
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "SDL_net.h"
#include "cchttp.h"

using namespace std;

const string httpLineBreak = "\r\n";

string getResponseCode(const string headers) {
  bool badResponse = false;
  unsigned int eolLoc;

  if (headers.substr(0, 7) != "HTTP/1.") {
    badResponse = true;
  }
  if ((headers[7] != '0') && (headers[7] != '1')) {
    badResponse = true;
  }
  eolLoc = headers.find(httpLineBreak, 0);
  if (eolLoc == string::npos) {
    badResponse = true;
  }
  if (badResponse) {
    throw string("Invalid response from server");
  }

  return headers.substr(9, eolLoc - 9);
}

string readHttpFile(string remoteFile) {
  string hostName;
  string file;
  unsigned int firstSlash;
  IPaddress hostIP;
  TCPsocket socket;
  string getString;
  string headers;
  string data;
  string httpStatus;
  int transmitCount;

  // Split the remote file name into hostName and file
  // This should be case insensitive
  if (remoteFile.substr(0, 7) == "http://") {
    remoteFile = remoteFile.substr(7, INT_MAX);
  }

  firstSlash = remoteFile.find('/', 0);
  if (firstSlash != string::npos) {
    hostName = remoteFile.substr(0, firstSlash);
    file = remoteFile.substr(firstSlash);
  }

  // Connect to host
  if (SDLNet_ResolveHost(&hostIP, hostName.c_str(), 80) == -1) {
    throw string("Could not resolve host");
  }

  socket = SDLNet_TCP_Open(&hostIP);
  if (!socket) {
    throw string("Could not open TCP socket");
  }

  getString = "GET " + file + " HTTP/1.1" + httpLineBreak + "Host: " + hostName + httpLineBreak + "Connection: Close" + httpLineBreak + httpLineBreak;
  transmitCount = SDLNet_TCP_Send(socket, (void *) getString.c_str(), getString.length());
  if (transmitCount != (int) getString.length()) {
    SDLNet_TCP_Close(socket);
    throw string("Request problem");
  }

  headers = readHttpHeaders(socket);

  httpStatus = getResponseCode(headers);
  if (httpStatus.substr(0, 3) != "200") {
    throw string("Request failed on server (" + httpStatus + ")");
  }

  data = readHttpBody(socket);

  SDLNet_TCP_Close(socket);

  return data;
}

string readHttpHeaders(TCPsocket socket) {
  string line;
  string data;

  while (line.compare(httpLineBreak) != 0) {
    line = readHttpLine(socket, httpLineBreak);

    data += line;
  }

  return data;
}

string readHttpBody(TCPsocket socket) {
  string line;
  string data;

  // This is just to get into the while loop, it shouldn't ever get into the return value
  line = "dummy";

  while (line.length() != 0) {
    line = readHttpLine(socket, "\n");

    data += line;
  }

  return data;
}

string readHttpLine(TCPsocket socket, string lineBreak) {
  char recv;
  string line;
  int transmitCount;

  while ((line.length() < lineBreak.length()) || (lineBreak.compare(line.substr((int)line.length()-(int)lineBreak.length())) != 0)) {
    transmitCount = SDLNet_TCP_Recv(socket, &recv, 1);
    if (transmitCount != 1) {
      break;
    }
    line += recv;
  }

  return line;
}

void saveHttpFile(std::string remoteFile, std::string localDest) {
  ofstream output;
  string data;

  output.open(localDest.c_str(), ios::out | ios::trunc | ios::binary);

  if (!output) {
    throw string("Could not open") + localDest;
  }

  cout << "Downloading: " << remoteFile << endl;

  data = readHttpFile(remoteFile);

  output << data;
}

string ccTrimPath(string file) {
  string result = file;

  if ((result.length() > 0) && (result[result.length() - 1] == '/')) {
    result = result.substr(0, result.length() - 1);
  }

  return result;
}

string ccAppendPaths(string root, string path) {
  string result = ccTrimPath(root);

  if (result.length() > 0) {
    return result + '/' + ccTrimPath(path);
  } else {
    return ccTrimPath(path);
  }
}
