/* Crown and Cutlass
 * Data Updater
 * HTTP functions header file
 */

#if !defined( _CCHTTP_H_ )

#define _CCHTTP_H_

#include <string>
#include "SDL_net.h"

std::string readHttpFile(std::string remoteFile);

std::string readHttpHeaders(TCPsocket socket);

std::string readHttpBody(TCPsocket socket);

std::string readHttpLine(TCPsocket socket, std::string lineBreak);

void saveHttpFile(std::string remoteFile, std::string localDest);

std::string ccAppendPaths(std::string root, std::string path);
std::string ccTrimPath(std::string path);

#endif
