/* Crown and Cutlass
 * Data Updater
 * File check functions source code
 */

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include "md5/md5.h"
#include "cccheckfiles.h"

using namespace std;
using namespace boost::filesystem;

bool checkFile(const ccUpdaterFile file) {
  const unsigned int bufferSize = 4096;
  const unsigned int digestSize = 16;

  boost::filesystem::ifstream fileStream;
  char *buffer;
  unsigned int bytesRead;
  md5_state_t state;
  md5_byte_t digest[digestSize];
  string localCheckSum;
  unsigned int i;
  char tempOut[3];

  cout << file.name.string() << " ";

  if (!exists(file.name)) {
    // The file doesn't exist, so it needs to be updated
    cout << "does not exist" << endl;

    return false;
  }

  if (is_directory(file.name) || symbolic_link_exists(file.name)) {
    // It's not a regular file
    throw file.name.string() + string(" is not a regular file");
  }

  if (file.size != file_size(file.name)) {
    // File sizes don't match, needs update
    cout << "file size does not match" << endl;

    return false;
  }

  // Initialize the md5 code
  md5_init(&state);

  // Read the file in
  fileStream.open(file.name, ios::in | ios::binary);
  if (!fileStream) {
    throw string("Could not open ") + file.name.string();
  }
  buffer = new char[bufferSize];
  // Initialize bytesRead just so it gets into the loop
  bytesRead = 1;
  while (fileStream.good() && bytesRead > 0) {
    bytesRead = fileStream.readsome(buffer, bufferSize);
    md5_append(&state, (const md5_byte_t *)buffer, bytesRead);
  }
  delete buffer;
  fileStream.close();

  // Finished reading file, get the final md5 checksum
  md5_finish(&state, digest);
  for (i = 0; i < digestSize; i++) {
    snprintf(tempOut, 3, "%02x", digest[i]);
    localCheckSum += tempOut;
  }

  if (localCheckSum != file.checksum) {
    cout << "checksums do not match" << endl;

    return false;
  }

  // It all matches
  cout << "ok" << endl;
  return true;
}
