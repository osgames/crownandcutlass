/* Crown and Cutlass
 * Data Updater
 * Main source file
 */

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#endif

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/exception.hpp>
#include "SDL.h"
#include "SDL_net.h"
#include "cchttp.h"
#include "cccheckfiles.h"
#include "TinyXML/tinyxml.h"

using namespace std;
using namespace boost::filesystem;

#define VERSION "0.01"

#ifdef WIN32
#define PLATFORM "win32"
#else
#define PLATFORM "linux"
#endif

const string tempDir = "updaterTemp";
const string moverProg = "Mover";
const string ignoreFile = "Updater.ignore";

string host;

vector<path> tempDirs;
vector<path> ignoreList;

void prepExit() {
  cout << "Shutting down SDL" << endl;
  SDLNet_Quit();
  SDL_Quit();
}

void handleError(const string msg) {
  cout << "Error: " << msg << " (" << SDLNet_GetError() << ")" << endl;
}

bool shouldIgnoreFile(const path &fileName) {
  unsigned int size;
  vector<path>::iterator iter;

  size = ignoreList.size();
  if (size == 0) {
    return false;
  }

  iter = find(ignoreList.begin(), ignoreList.end(), fileName);

  return iter != ignoreList.end();
}

ccUpdaterFile parseFile(TiXmlElement* file, const path &dir) {
  ccUpdaterFile info;

  if (file == NULL) {
    throw string("Invalid file");
  }

  if (file->Attribute("name") == NULL) {
    throw string("Name attribute not found");
  }
  info.name = dir / path(file->Attribute("name"));

  if (file->Attribute("size") == NULL) {
    throw string("Size attribute not found");
  }
  // Could this be done better?
  info.size = atoi(file->Attribute("size"));

  if (file->Attribute("checksum") == NULL) {
    throw string("Checksum attribute not found");
  }
  info.checksum = file->Attribute("checksum");

  return info;
}

vector<ccUpdaterFile> parseDirectory(TiXmlElement* dir, const path &parentDir) {
  TiXmlElement* node;
  path currentDir;
  path tempDirCopy;
  vector<ccUpdaterFile> updates;
  unsigned int i;

  if (dir == NULL) {
    throw string("Invalid directory");
  }

  if (dir->Attribute("name") == NULL) {
    throw string("Name attribute not found");
  }

  currentDir = parentDir / dir->Attribute("name");

  tempDirCopy = tempDir / currentDir;

  for (node = dir->FirstChildElement("Directory"); node; node = node->NextSiblingElement("Directory")) {
    vector<ccUpdaterFile> subUpdates = parseDirectory(node, currentDir);

    for (i = 0; i < subUpdates.size(); i++) {
      updates.push_back(subUpdates[i]);
    }
  }

  for (node = dir->FirstChildElement("File"); node; node = node->NextSiblingElement("File")) {
    ccUpdaterFile info;
    info = parseFile(node, currentDir);

    if (shouldIgnoreFile(info.name)) {
      cout << info.name.string() << " IGNORED" << endl;
      continue;
    }

    if (!checkFile(info)) {
      updates.push_back(info);
    }
  }

  if (updates.size() > 0) {
    tempDirs.push_back(tempDirCopy);
  }

  return updates;
}

vector<ccUpdaterFile> parseXML(const string file) {
  vector<ccUpdaterFile> files;
  ccUpdaterFile info;
  TiXmlDocument doc;
  TiXmlElement* root;
  TiXmlElement* updaterInfo;
  TiXmlElement* filesNode;

  doc.Parse(file.c_str());
  if (doc.Error()) {
    throw string(doc.ErrorDesc());
  }

  root = doc.FirstChildElement("CrownAndCutlass");
  if (root == NULL) {
    throw string(doc.ErrorDesc());
  }

  updaterInfo = root->FirstChildElement("Updater");
  if (updaterInfo == NULL) {
    throw string("Could not find Updater section");
  }
  if (strcmp(updaterInfo->Attribute("version"), VERSION)) {
    throw string("Updater versions do not match: local v") + string(VERSION) + string(", server v") + string(updaterInfo->Attribute("version"));
  }

  filesNode = root->FirstChildElement("Files");
  if (filesNode == NULL) {
    throw string("Could not find Files section");
  }

  files = parseDirectory(filesNode->FirstChildElement("Directory"), "");

  return files;
}

string getRemoteAddress(const string filename) {
  string remoteLoc = ccAppendPaths(host, PLATFORM);

  return ccAppendPaths(remoteLoc, filename);
}

void updateFile(ccUpdaterFile file) {
  int tries = 3;
  string remoteFile;

  // Get the remote file before we start messing with file.name
  remoteFile = getRemoteAddress(file.name.string());

  // Change file.name to be in the temp directory
  file.name = path(tempDir / file.name);

  while (tries-- > 0) {
    saveHttpFile(remoteFile, file.name.string());

    if (checkFile(file)) {
      // File downloaded ok
      return;
    } else {
      cout << "Error downloading";
      if (tries > 0) {
        cout << ", " << tries << " tries remaining...";
      }
      cout << endl;
    }
  }
}

void moveFiles() {
  // Shared in both linux and win32
  int strSize;
  char *args[4];

  // Parent's done so get ready to exec the mover
  strSize = moverProg.length() + 1;
  args[0] = new char[strSize];
  strncpy(args[0], moverProg.c_str(), strSize);

  strSize = tempDir.length() + 1;
  args[1] = new char[strSize];
  strncpy(args[1], tempDir.c_str(), strSize);

  strSize = strlen(".") + 1;
  args[2] = new char[strSize];
  strncpy(args[2], ".", strSize);

  args[3] = NULL;

#ifndef WIN32
  int numTries = 4;
  pid_t pid;
  pid_t parentPid = getpid();

  // Now we need to fork and execute the mover
  pid = fork();
  if (pid == 0) {
    // This is the child process
    while (numTries > 0) {
      sleep(1);
      if (getppid() != parentPid) break;
      numTries--;
    }
    if (numTries == 0) {
      throw string("Timed out waiting for parent to exit");
    }

    // Parent exited, execute the mover
    execv(moverProg.c_str(), args);
    throw string(strerror(errno));
  } else if (pid > 0) {
    // This is the parent
    // Nothing needs to happen, the parent just quits
  }
#else
  // Win32 verion
  stringstream environmentStr;

  environmentStr.str("");
  environmentStr << "CCUpdaterPID=" << _getpid();
  if (_putenv(environmentStr.str().c_str()) != 0) {
    throw string("Could not set pid environment variable");
  }

  if (_spawnv(_P_NOWAIT, moverProg.c_str(), args) == -1) {
    throw string("Could not spawn mover process");
  }
#endif
}

void loadIgnoreList() {
  ifstream file(ignoreFile.c_str(), ios::in);

  string line;

  if (!file) {
    // The ignore file wasn't found, don't alert the user, though
    return;
  }

  while (file >> line) {
    if ((line[0] == '#') || (line.length() < 1)) continue;
    ignoreList.push_back(path(line));
  }
}

int main(int argc, char **argv) {
  string file;
  string remoteFile;
  string userInput;
  vector<ccUpdaterFile> files;
  unsigned int i;

  try {
    if (argc != 2) {
      cout << "Usage: Updater <location>" << endl;
      return 1;
    }

    if (SDL_Init(0) == -1) {
      // Can't use handle error here, since it's an SDL error, not an SDL_net error
      cout << "Error: Could not initialize SDL (" << SDL_GetError() << ")" << endl;
      return 1;
    }

    #ifdef WIN32
    // Set stdout and stderr back to the console window
    freopen("CON", "w", stdout);
    freopen("CON", "w", stderr);
    #endif

    if (SDLNet_Init() == -1) {
      handleError("Error: Could not initialize SDL_net");
      SDL_Quit();
      return 1;
    }

    host = argv[1];
    if (host[host.length() - 1] == '/') {
      host = host.substr(0, host.length() - 1);
    }

    remoteFile = host + '/' + PLATFORM + ".xml";

    cout << "Updater v" << VERSION << endl;

    // List of files that the user wants the updater to ignore
    loadIgnoreList();

    cout << "Downloading: " << remoteFile << endl;

    // Read file
    try {
      file = readHttpFile(remoteFile);

      if (file.length() <= 0) {
        throw string("Invalid file");
      }

      files = parseXML(file);
      if (files.size() != 0) {
        cout << endl << "Need to update:" << endl;
        for (i = 0; i < files.size(); i++) {
          cout << " " << files[i].name.string() << endl;
        }
        cout << "Update these files? [y/N] ";
        cin >> userInput;
        if (toupper(userInput[0]) == 'Y') {
          cout << endl;

          // Now make all the temp dirs
          // We put them in backwards, so walk through list backwards
          for (i = tempDirs.size(); i > 0; i--) {
            create_directory(tempDirs[i-1]);
          }

          // Download the actual files
          for (i = 0; i < files.size(); i++) {
            updateFile(files[i]);
          }

          // Move the files out of the temp dir using the Mover
          moveFiles();

          cout << endl << "Update successful" << endl;
        } else {
          cout << "Update aborted" << endl;
        }
      } else {
        cout << "No files to update" << endl;
      }
    } catch (string e) {
      cout << "Error: " << e << endl;
    }
  } catch (...) {
    cout << "Error: Unknown exception" << endl;
  }

  prepExit();

  return 0;
}

