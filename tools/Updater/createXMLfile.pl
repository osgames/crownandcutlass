#!/usr/bin/perl

# Crown and Cutlass
# Updater XML Generator

$version = '0.01';
$updater = './Updater';

$argc = @ARGV;
if ($argc == 1) {
  chdir(@ARGV[0]) || die "could not cd to " . @ARGV[0];
}

print "<CrownAndCutlass>\n";
print "  <Updater version=\"$version\" />\n";
print "  <Files>\n";

$indent = 4;
$lastindent = 0;
$indentstr = '';

handledir('.');

print "  </Files>\n";
print "</CrownAndCutlass>\n";

sub handledir {
  outputtext("<Directory name=\"" . extractfilename($_[0]) . "\">\n");
  opendir(DIR, $_[0]) || die "could not open $_[0]";
  
  $indent += 2;
  
  foreach $name (sort readdir(DIR)) {
    if (($name eq '.') or 
        ($name eq '..') or 
        (substr($name, length($name) - 1, 1) eq '~') or
        ("$_[0]/$name" eq $updater)) {
      next;
    }
    if (-d "$_[0]/$name") {
      handledir("$_[0]/$name");
    } else {
      handlefile("$_[0]/$name");
    }
  }
  closedir(DIR);
  
  $indent -= 2;
  
  outputtext("</Directory>\n");
}

# Handles outputing an entry for a file
# Args: String of full path to file
sub handlefile {
  my($size);
  my($checksum);
  $size = (stat($_[0]))[7];
  $checksum = getchecksum($_[0]);
  outputtext("<File name=\"" . extractfilename($_[0]) . "\" size=\"$size\" checksum=\"$checksum\" />\n");
}

# Properly indents a string
# Args: the text string you want to indent
sub outputtext {
  if ($indent > $lastindent) {
    for ($i = $lastindent; $i < $indent; $i++) {
      $indentstr .= " ";
    }
  } elsif ($indent < $lastindent) {
    $indentstr = substr($indentstr, 0, $indent);
  }
  $lastindent = $indent;

  print $indentstr . $_[0];
}

# Returns just the filename section of a path
# Args: String of full path to file (eg './dir/file.txt')
# Returns: Just the filename (eg 'file.txt');
sub extractfilename {
  return substr($_[0], rindex($_[0], '/') + 1);
}

# Calculates md5 hash of a file
# Args: String of full path to file
# Returns: Checksum string
sub getchecksum {
  my($output);
  my($result);
  
  $output = `md5sum $_[0]`;
  if (length($output) == 0) {
    return 'ERROR';
  }
  
  $result = substr($output, 0, index($output, ' '));
  
  if (length($result) != 32) {
    return 'ERROR';
  }
  return $result;
}
