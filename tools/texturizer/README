David Thulson
Crown and Cutlass
Terrain Texture Generator Initial Release

Notes:
I wanted to allow the user to give any number of textures and border levels, 
but I ended up just defining some stuff in the code.  If you want to change 
border levels or the blend amount, you'll need to recompile.  Eventually, I 
would like to add a better interface.

The border heights are based on a scale from -128 to 127.  Those heights are 
the value where the pixel is 50% of each texture that it is blending between.  
The blend amount is how far from the border to begin blending.  For instance, 
let's say you have a border at 30 and a blend value of 10.  A pixel at height 
25 would be 100% texture 1 and 0% texture 2.  A pixel at height 30 would be 
50% each texture, and a pixel at height 35 would be 0% texture 1 and 100% 
texture 2.

This makes use of some of the code from the actual game and a simplified 
version of the terrain class.  Much of this code is from old versions and may 
be out of date.  However, it still works.  Using the ray-triangle code used 
for collisions in the game, it casts a ray from above the terrain to determine
the height for each pixel of the final texture.  Reusing that code was very 
helpful and gave me a great and easy way to determine the height at any point 
on the terrain.

Requirements:
SDL
SDL_image
