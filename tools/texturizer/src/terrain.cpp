#if defined (WIN32)
#include <windows.h>
#endif

#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL_image.h>
#include "normals.h"
#include "collisions.h"
#include "terrain.h"

// Used to get index in terrain array based on (x, z) coords
#define TERRAIN(X, Z) terrain[((Z) * w) + (X)]

#define NORMALS(X, Y, Z) normals[(((Y)*w + (X)) * 3) + (Z)]

#define TNORMALS_INDEX(X, Y, Z) ((((Y)*w + (X)) * 3) + (Z))

Terrain::Terrain(char *file, GLfloat vScaleIn) {
  SDL_Surface *Image;
  //QuadBox *box;
  Uint8 *p;

  // Load the image, check for errors, if image is not found quit
  Image = IMG_Load(file);
  if (!Image) {
    printf("Could not load \"%s\"\n", file);
  }

  w = Image->w;
  h = Image->h;

  terrain = new GLfloat[w*h];
  normals = new GLfloat[w*h*3];

  p = (Uint8 *) Image->pixels;

  for (int y = 0; y < h; y++){
    for (int x = 0; x < w; x++){
      TERRAIN(x, y) = (p[y*Image->pitch + x*Image->format->BytesPerPixel] - 127.9) * vScaleIn;
    }
  }

  SDL_FreeSurface(Image);

  //calcNormals();

  //BuildTexture("land.png", &texture);

  /*
  // Set up the root quadtree box
  box = new QuadBox();

  box->b1 = new Point(0.0 - (int) w/2, 0.0, 0.0 - (int) h/2);
  box->b2 = new Point((int) w/2, 0.0, 0.0 - (int) h/2);
  box->b3 = new Point(0.0 - (int) w/2, 0.0, (int) h/2);
  box->b4 = new Point((int) w/2, 0.0, (int) h/2);

  // Set up the frustum
  frustum = new Frustum();

  // Initialize the quadtree root
  root = new QuadNode(this, box);

  // Don't need to worry about deleting the box, the quadtree will do it
  */

  // No longer need the normals, since the display lists are created
  // For this we need the normals to compute the slope of the point
  delete []normals;
  normals = NULL;
}

Terrain::~Terrain() {
  // Delete the quadtree
  //delete root;

  // Delete the terrain data & normals
  delete []terrain;
  
  //delete []normals;

  // Again, still don't need to delete the constructor's box, the quadtree
  //   handles it
}

bool Terrain::checkCollision_Line(GLfloat xIn, GLfloat yIn, int size, 
				 double v[3], float mag) {
  int xMin, xMax;
  int yMin, yMax;

  xIn = (w/2) - xIn;
  yIn = (h/2) - yIn;

  xMin = (int) xIn - (size/2);
  xMax = (int) xIn + (size/2);
  if (xMin < 0) xMin = 0;
  if (xMax > (w-2)) xMax = (w-2);

  yMin = (int) yIn - (size/2);
  yMax = (int) yIn + (size/2);
  if (yMin < 0) yMin = 0;
  if (yMax > (h-2)) yMax = (h-2);
  
  for (int y = yMin; y < yMax; y++){
    for(int x = xMin; x < xMax; x++) {
      //      if (needCollision(x, y)) {
      if (true) {
	double orig[3];
	double vert0[3];
	double vert1[3];
	double vert2[3];
	double vert3[3];
	double r1, r2, r3;

	orig[0] = xIn;
	orig[1] = 0;
	orig[2] = yIn;

	vert0[0] = x;
	vert0[1] = TERRAIN(x, y);
	vert0[2] = y;

	vert1[0] = x+1;
	vert1[1] = TERRAIN(x+1, y);
	vert1[2] = y;

	vert2[0] = x;
	vert2[1] = TERRAIN(x, y+1);
	vert2[2] = y+1;

	vert3[0] = x+1;
	vert3[1] = TERRAIN(x+1, y+1);
	vert3[2] = y+1;

	if (intersect_triangle(orig, v, vert0, vert2, vert1, &r1, &r2, &r3)) {
	  if (r1 < mag) return true;
	}
	if (intersect_triangle(orig, v, vert2, vert3, vert1, &r1, &r2, &r3)) {
	  if (r1 < mag) return true;
	}
      }
    }
  }

  return false;
}

void Terrain::calcNormals() {
  GLfloat point1[3], point2[3], point3[3];
  GLfloat *tnormals1;
  GLfloat *tnormals2;

  if (normals == NULL) return;

  tnormals1 = new GLfloat[w*h*3];
  tnormals2 = new GLfloat[w*h*3];

  for (short int z = 0; z < h-1; z++) {
    for (short int x = 0; x < w-1; x++) {
      point1[0] = x;
      point1[1] = TERRAIN(x, z);
      point1[2] = z;

      point2[0] = x;
      point2[1] = TERRAIN(x, z+1);
      point2[2] = z+1;

      point3[0] = x+1;
      point3[1] = TERRAIN(x+1, z);
      point3[2] = z;

      crossProduct(point1, point2, point3, &tnormals1[TNORMALS_INDEX(x, z, 0)]);

      point1[0] = x;
      point1[1] = TERRAIN(x, z+1);
      point1[2] = z+1;

      point2[0] = x+1;
      point2[1] = TERRAIN(x+1, z+1);
      point2[2] = z+1;

      point3[0] = x+1;
      point3[1] = TERRAIN(x+1, z);
      point3[2] = z;

      crossProduct(point1, point2, point3, &tnormals2[TNORMALS_INDEX(x, z, 0)]);
    }
  }

  for (short int z = 1; z < h-1; z++) {
    for (short int x = 1; x < w-1; x++) {
      NORMALS(x, z, 0) = (tnormals2[TNORMALS_INDEX(x-1, z-1, 0)] + tnormals1[TNORMALS_INDEX(x-1, z, 0)] +
			  tnormals2[TNORMALS_INDEX(x-1, z, 0)] + tnormals1[TNORMALS_INDEX(x, z-1, 0)] +
			  tnormals2[TNORMALS_INDEX(x, z-1, 0)] + tnormals1[TNORMALS_INDEX(x, z, 0)]);
      NORMALS(x, z, 1) = (tnormals2[TNORMALS_INDEX(x-1, z-1, 1)] + tnormals1[TNORMALS_INDEX(x-1, z, 1)] + 
			  tnormals2[TNORMALS_INDEX(x-1, z, 1)] + tnormals1[TNORMALS_INDEX(x, z-1, 1)] +
			  tnormals2[TNORMALS_INDEX(x, z-1, 1)] + tnormals1[TNORMALS_INDEX(x, z, 1)]);
      NORMALS(x, z, 2) = (tnormals2[TNORMALS_INDEX(x-1, z-1, 2)] + tnormals1[TNORMALS_INDEX(x-1, z, 2)] +
			  tnormals2[TNORMALS_INDEX(x-1, z, 2)] + tnormals1[TNORMALS_INDEX(x, z-1, 2)] +
			  tnormals2[TNORMALS_INDEX(x, z-1, 2)] + tnormals1[TNORMALS_INDEX(x, z, 2)]);

      // Send normalize pointer to first element for that normal
      normalize(&NORMALS(x, z, 0));
    }
  }

  // Get rid of temp normal arrays
  delete []tnormals1;
  delete []tnormals2;
}

GLfloat Terrain::getHeight(int x, int y) {
  if ((x < w) && (y < h)) {
    return TERRAIN(x, y);
  } else return 0;
}

GLfloat Terrain::calcHeight(GLfloat x, GLfloat z) {
  // The idea here is that I cast a ray down from above the terrain using the
  //   collision checking code to figure out the exact height at any point
  int tempXCoord, tempZCoord;
  
  int xCoord = (int) x;
  int zCoord = (int) z;

  if ((x >= w) || (z >= h)) {
    printf("Error: calcHeight - x or z too large\n");
    return 0;
  }
  if ((x < 0) || (z < 0)) {
    printf("Error: calcHeight - x or z less than 0\n");
    return 0;
  }
  
  if (xCoord + 1 < w) {
    tempXCoord = xCoord + 1;
  } else {
    tempXCoord = xCoord;
  }
  
  if (zCoord + 1 < h) {
    tempZCoord = zCoord + 1;
  } else {
    tempZCoord = zCoord;
  }

  double orig[3] = {x, 128, z};  // Point we're casting from
  double ray[3] = {0, -1, 0};  // Direction of ray
  double vert0[3] = {xCoord, TERRAIN(xCoord, zCoord), zCoord};
  double vert1[3] = {xCoord+1, TERRAIN(tempXCoord, zCoord), zCoord};
  double vert2[3] = {xCoord, TERRAIN(xCoord, tempZCoord), zCoord+1};
  double vert3[3] = {xCoord+1, TERRAIN(tempXCoord, tempZCoord), zCoord+1};
  double r1, r2, r3;

  if (intersect_triangle(orig, ray, vert0, vert2, vert1, &r1, &r2, &r3)) {
    return 128 - r1;
  }
  if (intersect_triangle(orig, ray, vert2, vert3, vert1, &r1, &r2, &r3)) {
    return 128 - r1;
  }

  // Shouldn't ever get here because the ray should hit at least one of the triangles
  printf("Problem!\n");
  return 0;
}

int Terrain::getWidth() {
  return w;
}

int Terrain::getHeight() {
  return h;
}

float Terrain::getSlope(int x, int z) {
  float temp[3] = {0, 1, 0};

  return angleBetweenVectors(temp, &NORMALS(x, z, 0));
}
