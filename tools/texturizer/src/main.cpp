#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <GL/gl.h>
#include <SDL/SDL_image.h>
#include "image.h"
#include "terrain.h"

#define NTEX 3
#define BORDER0 22
#define BORDER1 105
#define BLEND 40
#define FILE0 "./beach.png"
#define FILE1 "./grass.png"
#define FILE2 "./snow.png"

// These are used for creating the map image
#define DEFAULT_RED 210
#define DEFAULT_GREEN 229
#define DEFAULT_BLUE 165

#define OCEAN_RED 11
#define OCEAN_GREEN 72
#define OCEAN_BLUE 124
/*
#define SHORE_RED 216
#define SHORE_GREEN 130
#define SHORE_BLUE 130
*/
/**/
#define SHORE_RED 158
#define SHORE_GREEN 200
#define SHORE_BLUE 102
/**/

#define RES 1024

float getPercent(float height, int t, int nTex, int *borders) {
  if (t > nTex) {
    printf("Invalid texture number\n");
    return 0.0;
  }

  int bottom;
  int top;
  float bDist;
  float tDist;

  if (t == 0) {
    bottom = -128;
    bottom -= BLEND;
  } else {
    bottom = borders[t-1];
    bottom -= BLEND/2;
  }

  if (t == nTex-1) {
    top = 127;
    top += BLEND;
  } else {
    top = borders[t];
    top += BLEND/2;
  }

  bDist = height - bottom;
  tDist = top - height;

  if ((bDist < 0) || (tDist < 0)) return 0;

  if ((bDist >= BLEND) && (tDist >= BLEND)) return 1;

  if (bDist < BLEND) {
    // On bottom
    return bDist / BLEND;
  }

  if (tDist < BLEND) {
    // On top
    return tDist / BLEND;
  }

  printf("Problem!\n");
  return 0.0;
}

int main(int argc, char** argv) {
  Terrain *terrain;

  // Image data
  unsigned char *texture;
  unsigned char *mapTexture;

  // Array of textures to use
  SDL_Surface *images[NTEX];

  // Number of textures
  int nTex = NTEX;

  // Size of texture
  int tWidth;
  int tHeight;

  // Size of heightmap
  int hWidth;
  int hHeight;

  // Ratios for texture to heightmap conversions
  float xRatio;
  float zRatio;

  // For use in loops
  float height;
  float percent;
  int index;
  int pixel;

  // For use in slope computation
  //float slope;

  // Texture borders
  int borders[NTEX-1];

  borders[0] = BORDER0;
  borders[1] = BORDER1;
  
  images[0] = IMG_Load(FILE0);
  if (!images[0]) {
    // Error loading texture
    printf("Error: could not load texture 0\n");
    exit(1);
  }
  images[1] = IMG_Load(FILE1);
  if (!images[1]) {
    // Error loading texture
    printf("Error: could not load texture 1\n");
    SDL_FreeSurface(images[0]);
    exit(1);
  }
  images[2] = IMG_Load(FILE2);
  if (!images[2]) {
    // Error loading texture
    printf("Error: could not load texture 2\n");
    SDL_FreeSurface(images[0]);
    SDL_FreeSurface(images[1]);
    exit(1);
  }

  terrain = new Terrain("./map.png", 1);

  // Size of texture
  tWidth = RES;
  tHeight = RES;

  // Size of heightmap
  hWidth = terrain->getWidth();
  hHeight = terrain->getHeight();

  // Ratios for texture to heightmap conversions
  xRatio = ((float) hWidth) / ((float) tWidth);
  zRatio = ((float) hHeight) / ((float) tHeight);

  texture = new unsigned char[tWidth * tHeight * 3];
  mapTexture = new unsigned char[tWidth * tHeight * 3];

  // Loop to actually make the image
  for (int z=0; z < tHeight; z++) {
    for (int x=0; x < tWidth; x++) {
      height = terrain->calcHeight(x*xRatio, z*zRatio);

      unsigned char *p;
      
      index = ((tHeight-z-1)*tWidth+x)*3;
      
      // Needs to work between points in heightmap
      //slope = terrain->getSlope(x, z);
      //printf("%d %d %f\n", x, z, slope);

      for (int i=0; i<nTex; i++) {
	percent = getPercent(height, i, nTex, borders);
	if (percent == 0) continue;  // No sense in proceeding

	// Gets rid of some pointer arithmatic warnings/errors
	p = (unsigned char *) images[i]->pixels;

	pixel = ((z%images[i]->h)*images[i]->w + (x%images[i]->w))*images[i]->format->BytesPerPixel;

	texture[index] += (unsigned char) (percent * p[pixel]);
	texture[index+1] += (unsigned char) (percent * p[pixel+1]);
	texture[index+2] += (unsigned char) (percent * p[pixel+2]);

	if (percent == 1) break;  // Might as well stop, already done
      }

      // Add depth fog
      if (height < 0) {
	percent = (-128 - height) / (-128);
	percent = percent * percent * percent;

	texture[index] = (unsigned char) (percent * texture[index]);
	texture[index+1] = (unsigned char) (percent * texture[index+1]);
	texture[index+2] = (unsigned char) (percent * texture[index+2]);
      }

      // This if and the previous if (the depth fog one) could be combined
      //   but it seems more straight-forward this way.
      //if (height >= 0 && height < 60) {
      if (height >= 0) {
	/*
	mapTexture[index] = SHORE_RED;
	mapTexture[index+1] = SHORE_GREEN;
	mapTexture[index+2] = SHORE_BLUE;
	*/
	/**/
	// On land, so copy the pixel to the map
	mapTexture[index] = texture[index];
	mapTexture[index+1] = texture[index+1];
	mapTexture[index+2] = texture[index+2];
	/**/
      //} else if (height < 0 && height > -25) {
      } else {
	mapTexture[index] = OCEAN_RED;
	mapTexture[index+1] = OCEAN_GREEN;
	mapTexture[index+2] = OCEAN_BLUE;
      }  /*else {
	// In the water, just color the pixel for the ocean
	mapTexture[index] = DEFAULT_RED;
	mapTexture[index+1] = DEFAULT_GREEN;
	mapTexture[index+2] = DEFAULT_BLUE;
	 */
	/*
	// Gets rid of some pointer arithmatic warnings/errors
	p = (unsigned char *) waterSurface->pixels;

	pixel = ((z%waterSurface->h)*waterSurface->w + (x%waterSurface->w))*waterSurface->format->BytesPerPixel;

	mapTexture[index] += (unsigned char) p[pixel];
	mapTexture[index+1] += (unsigned char) p[pixel+1];
	mapTexture[index+2] += (unsigned char) p[pixel+2];
	*/
      //}
    }
  }

  // Save the texture
  WriteTGAFile("./land.tga", tWidth, tHeight, texture);
  WriteTGAFile("./mapscreen.tga", tWidth, tHeight, mapTexture);

  // Cleanup
  for (int i=0; i<nTex; i++) {
    SDL_FreeSurface(images[i]);
  }
  //SDL_FreeSurface(texture);

  delete terrain;
}
