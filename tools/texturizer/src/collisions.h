#ifndef _COLLISIONS_H_
#define _COLLISIONS_H_

/* This is from "Fast, Minimum Storage Ray-Triangle Intersection"
 * by Ben Trumbore and Tomas M�ller.
 * http://www.acm.org/jgt/papers/MollerTrumbore97/
 *
 * It acutally checks for the ray-triangle collision
 */

#define TEST_CULL

int
intersect_triangle(double orig[3], double dir[3],
                   double vert0[3], double vert1[3], double vert2[3],
                   double *t, double *u, double *v);

#endif /* _COLLISIONS_H */
