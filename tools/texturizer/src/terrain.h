#if !defined( _TERRAIN_H_ )

#define _TERRAIN_H_

#include <GL/gl.h>

#define TERRAIN_TEX_SCALE .2

// So I don't have to include the headers
class QuadNode;
class QuadBox;
class Frustum;

class Terrain {
public:
  Terrain(char *file, GLfloat vScale);
  ~Terrain();

  // Check a line to see if it collided with the terrain around it
  bool checkCollision_Line(GLfloat xIn, GLfloat yIn, int size, double v[3], float mag);

  // Get the height of the terrain at a specific location
  GLfloat getHeight(int x, int y);

  // Calc height at any point (doesn't need to be exactly on a vertex)
  GLfloat calcHeight(GLfloat x, GLfloat z);

  // Return w or h
  int getWidth();
  int getHeight();
  
  float getSlope(int x, int z);

private:
  GLfloat *terrain;  // Should be MAX_X*MAX_Y
  GLfloat *normals;  // Should be MAX_X*MAX_Y*3

  // Width (# of pixels) of heightmap
  int w;
  // Height (# of pixels) of heightmap
  int h;

  //GLuint texture;

  // Root of quadtree
  QuadNode *root;

  void calcNormals();
};

#endif
