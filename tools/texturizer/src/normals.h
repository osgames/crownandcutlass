#if !defined ( _NORMALS_H_ )

#define _NORMALS_H_

double vectorLength(float vector[3]);

void normalize(float normalVector[3]);

void crossProduct(float point1[3], float point2[3], float point3[3], float normal[3]);

void dotProduct(float vector1[3], float vector2[3], float result[3]);

float angleBetweenVectors(float vector1[3], float vector2[3]);

#endif
