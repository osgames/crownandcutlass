/* Image Manipulating Functions
 * Copied from Chapter 7 of OpenGL Game Programming
 *   and NeHe lesson 6
 * Modified by David Thulson
 */

typedef struct {
  unsigned char imageTypeCode;
  short int     imageWidth;
  short int     imageHeight;
  unsigned char bitCount;
  unsigned char *imageData;
} TGAFILE;

// Uses SDL_image library
int BuildTexture(char *filename, GLuint *texId);

// Uses the TGAFILE struct, so use BuildTexture instead
int LoadTGAFile(char *filename, TGAFILE *tgaFile);

// Uses the TGAFILE struct, should rewrite to use SDL
int WriteTGAFile(char *filename, short int width, short int height, unsigned char* imageData);

void SaveScreenshot(char *filename, int x, int y);
