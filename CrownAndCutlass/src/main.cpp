/******************************************************************************
 * Crown and Cutlass                                                          *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <iostream>
#include "config.h"
#ifdef PCCE_OSX
#include <Carbon/Carbon.h>
#endif
#include <Ogre.h>
#include <CEGUI.h>
#include <Protocce.h>
#include <ISubSystem.h>
#include <RenderSubSystem.h>
#include <SoundSubSystem.h>
#include <InputSubSystem.h>
#include <Variant.h>
#include <PropertyBag.h>
#include <Configuration.h>
#include <Log.h>
#include "MenuState.h"
#include "IntroState.h"

using namespace std;
using namespace pcce;
using namespace crowncutlass;

void HandleSubSystem(const tConfigurationPtr& config, tISubSystemPtr& system, tISubSystemPtrList& list) {
  const string enabledStr = "enabled";

  bool wantSystem;
  tPropertyBagPtr section;

  if (config->TryGetConfigSection(system->vGetSubSystemName(), section)) {
    wantSystem = section->GetValueDef(enabledStr, true).AsBool();
  } else {
    wantSystem = true;
  }
  if (wantSystem) {
    list.push_back(system);
  }
}

int main(int argc, char* argv[]) {
  try {    
    tConfigurationPtr config(new Configuration());
    tISubSystemPtrList systems;

    config->LoadINIFile("CrownAndCutlass.ini");

    {
      tISubSystemPtr system;

      //system.reset(new PhysicsSubSystem());
      //HandleSubSystem(config, system, systems);

      system.reset(new RenderSubSystem());
      HandleSubSystem(config, system, systems);
      
      system.reset(new InputSubSystem());
      HandleSubSystem(config, system, systems);

      system.reset(new SoundSubSystem());
      HandleSubSystem(config, system, systems);
    }

    Protocce p(config);

    p.Initialize(systems);

    // No need to keep these around
    systems.clear();
    config.reset();

    {
      tIGameStatePtr state(new MenuState());
      p.PushState(state);
      
      state.reset(new IntroState());
      p.PushState(state);   
    }

    Ogre::Root::getSingleton().addResourceLocation(
      p.GetConfiguration()->GetConfigSection("Game")->GetValueAsString("TexturePath"), "FileSystem", "Texture", true);
    
    // Load CEGUI LookAndFeel
    Ogre::Root::getSingleton().addResourceLocation(
      p.GetConfiguration()->GetConfigSection("Game")->GetValueAsString("GUIPath"), "FileSystem", "GUI", true);

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    
    CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLook.scheme", (CEGUI::utf8*)"GUI");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook", "MouseArrow");

    try {
      p.RunGame();
    } catch (Exception& e) {
      LogSingleton::Get()->LogMessage(e.GetFullError(), mlQuiet);
    } catch (Ogre::Exception& e) {
      LogSingleton::Get()->LogMessage("Ogre error: " + e.getFullDescription(), mlQuiet);
    } catch (CEGUI::Exception& e) {
      LogSingleton::Get()->LogMessage(("Cegui error: " + e.getMessage()).c_str(), mlQuiet); 
    } catch (...) {
      LogSingleton::Get()->LogMessage("Unknown error", mlQuiet);
    }

    p.Shutdown();
  } catch (Exception& e) {
    cerr << e.GetFullError() << endl;
  } catch (Ogre::Exception& e) {
    cerr << "Ogre error: " << e.getFullDescription() << endl;
  } catch (...) {
    cerr << "Unknown error" << endl;
  }

  return 0;
}

