/******************************************************************************
 * Crown and Cutlass                                                          *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <boost/bind.hpp>
#include <OIS/OIS.h>
#include <CEGUI.h>

#include <Exception.h>
#include <NoOpProcess.h>
#include <InputTranslator.h>
#include <SetInputTranslatorEvent.h>
#include <GameStateEvents.h>
#include <IProcess.h>
#include <Log.h>
#include "MenuState.h"

using namespace boost;
using namespace pcce;
using namespace crowncutlass;

void MenuState::vSwitchTo() {
  LogSingleton::Get()->LogMessage("Switching to menu state", mlQuiet);

  PCCE_CHECK(!mScene, "Cannot switch to already started menu state");
  mScene.reset(new Scene());
  mScene->CreateOgreCamera();
  
  // Create background rectangle
  Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
  // Cover the whole screen
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  // Hacky, but we need to set the bounding box to something big
  rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
  //rect->setMaterial("Background");
  // Render the background before everything else
  rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

  // Attach background to the scene
  Ogre::SceneNode* node = mScene->GetOgreSceneManager()->getRootSceneNode()->createChildSceneNode("Background");
  node->attachObject(rect);
  
  CEGUI::System::getSingleton().setGUISheet(CEGUI::WindowManager::getSingleton().loadWindowLayout("MenuState.layout"));
  CEGUI::FontManager::getSingleton().createFont("bluehighway-12.font", (CEGUI::utf8*)"GUI");
  CEGUI::System::getSingleton().setDefaultFont((CEGUI::utf8*)"BlueHighway-12");
  
  mNoOpProc = tIProcessPtr(new NoOpProcess());
  
  AddProcess(mNoOpProc);
  
  mConns.AddHandler(
    EndStateEvent::sGetEventType(), bind(&MenuState::EndState, this, _1));
  
  tInputTranslatorPtr translator(new InputTranslator());
  translator->AddKeyHandler(OIS::KC_ESCAPE, ksUp, bind(CreateEvent< EndStateEvent >));
  
  SetActiveInputTranslator(translator);
}

void MenuState::vSwitchFrom() {
  LogSingleton::Get()->LogMessage("Switching from menu state", mlQuiet);
  mConns.RemoveHandler(EndStateEvent::sGetEventType());

  PCCE_CHECK(mScene, "Cannot switch from non-started menu state");
  mScene.reset();
}

void MenuState::EndState(const tIEventPtr& e) {
  mNoOpProc->Kill();
}
