/******************************************************************************
 * Crown and Cutlass                                                          *
 * http://www.crownandcutlass.com                                             *
 * Copyright (c) 2004 - 2008 David Thulson and Collin Sanford                 *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 * 1. Redistributions of source code must retain the above copyright          *
 *    notice, this list of conditions and the following disclaimer.           *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 *    notice, this list of conditions and the following disclaimer in the     *
 *    documentation and/or other materials provided with the distribution.    *
 * 3. The names of the authors may not be used to endorse or promote products *
 *    derived from this software without specific prior written permission.   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR       *
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES  *
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.    *
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,           *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT   *
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  *
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY      *
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT        *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF   *
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 *                                                                            *
 ******************************************************************************/

#include <boost/bind.hpp>
#include <OIS/OIS.h>

#include <Exception.h>
#include <WaitProcess.h>
#include <InputTranslator.h>
#include <SetInputTranslatorEvent.h>
#include <GameStateEvents.h>
#include <IProcess.h>
#include <Log.h>
#include "IntroState.h"

using namespace boost;
using namespace pcce;
using namespace crowncutlass;

void IntroState::vSwitchTo() {
  LogSingleton::Get()->LogMessage("Switching to intro state", mlQuiet);

  PCCE_CHECK(!mScene, "Cannot switch to already started intro state");
  mScene.reset(new Scene());
  mScene->CreateOgreCamera();
  
  // Create background rectangle
  Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
  // Cover the whole screen
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  // Hacky, but we need to set the bounding box to something big
  rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
  //rect->setMaterial("Background");
  // Render the background before everything else
  rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

  rect->setMaterial("texture/LoadBackground");

  // Attach background to the scene
  Ogre::SceneNode* node = mScene->GetOgreSceneManager()->getRootSceneNode()->createChildSceneNode("Background");
  node->attachObject(rect);
  
  mWaitProc = tIProcessPtr(new WaitProcess(10 * msPerS));
  
  AddProcess(mWaitProc);
  
  mConn = EventSystemSingleton::Get()->AddHandler(
    EndStateEvent::sGetEventType(),
    bind(&IntroState::EndState, this, _1));
  
  tInputTranslatorPtr translator(new InputTranslator());
  translator->AddKeyHandler(OIS::KC_ESCAPE, ksUp, bind(CreateEvent< EndStateEvent >));
  
  SetActiveInputTranslator(translator);
}

void IntroState::vSwitchFrom() {
  LogSingleton::Get()->LogMessage("Switching from intro state", mlQuiet);
  mConn.disconnect();

  PCCE_CHECK(mScene, "Cannot switch from non-started intro state");
  mScene.reset();
}

void IntroState::EndState(const tIEventPtr& e) {
  mWaitProc->Kill();
}
