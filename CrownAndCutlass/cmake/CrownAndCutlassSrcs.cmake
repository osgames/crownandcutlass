# This includes a list of common source files for Crown and Cutlass itself and
#   the Crown and Cutlass GameState test application.

SET(CROWNCUTLASS_SRC_FILE_NAMES
  IntroState.cpp
  MenuState.cpp
)

SET(CROWNCUTLASS_HEADER_FILE_NAMES
  IntroState.h
  MenuState.h
)
