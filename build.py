#!/usr/bin/python

import platform
import os
import sys
import shutil
from optparse import OptionParser

build_dir = None
cmake_generator = None
root_dir = None
cmake_exec = None
make_func = None
default_svn_client = None
svn_clients = { 'svn': 'svn update'
              , 'TortoiseSVN': '???'
              , 'ZigVersion': '???'
              }

def rmtree_preserve_svn(path):
    # Get the last working directory
    pwd = os.path.abspath(os.path.curdir)
    os.chdir(path)
    try:
        delete_list = [i for i in os.listdir(os.path.curdir) if i[0] <> "."]
        for file in delete_list:
            if os.path.isdir(file):
                rmtree_preserve_svn(file)
                if len(os.listdir(file)) == 0:
                    os.rmdir(file)
            else:
                os.remove(file)
    finally:
        # make sure to revert to the last working directory
        os.chdir(pwd)

def exec_command(command):
    print "> " + command
    return os.system(command)

def make_osx(project, target):
    # Something like this
    status = exec_command("xcodebuild " + project + " " + target)
    if status != 0:
        return 1
    else:
        return 0

def make_win32(project, target):
    # Something like this
    status = exec_command("msbuild " + project + " " + target)
    if status != 0:
        return 1
    else:
        return 0

def make_linux(project, target):
    # Something like this
    status = exec_command("make")
    if status != 0:
        return 1
    else:
        return 0

def get_platform():
    global build_dir, cmake_generator, cmake_exec, make_func, default_svn_client
    sys_val = platform.system().lower()
    if sys_val == "darwin":
        build_dir = "osx"
        cmake_generator = "Xcode"
        cmake_exec = "cmake"
        make_func = make_osx
        default_svn_client = "ZigVersion"
    elif sys_val == "windows":
        build_dir = "win32"
        cmake_generator = "Visual Studio 8 2005"
        cmake_exec = "C:/Progra~1/CMake2~1.4/bin/cmake.exe"
        make_func = make_win32
        default_svn_client = "TortoiseSVN"
    else:
        build_dir = "linux"
        cmake_generator = "Unix Makefiles"
        cmake_exec = "cmake"
        make_func = make_linux
        default_svn_client = "svn"

def update_svn(client):
    command = svn_clients[client]
    if (command == None) or (len(command) == 0):
        return 1
    
    status = exec_command(command)
    if status != 0:
        return 1
    else:
        return 0

def do_build(src_path, project, clean_dir):
    global cmake_generator, cmake_exec, make_func
    if clean_dir:
        rmtree_preserve_svn(".")
    status = exec_command(cmake_exec + " -G \"" + cmake_generator + "\" " + src_path)
    if status != 0:
        return 1
    return make_func(project, "release")

def build_protocce(clean_dir):
    global build_dir, root_dir
    os.chdir(root_dir + "/Protocce/lib/" + build_dir)
    return do_build("../../src", "Protocce", clean_dir)

def build_protocce_test(clean_dir):
    global build_dir, root_dir
    os.chdir(root_dir + "/Protocce/test/bin/" + build_dir)
    return do_build("../../src", "Test", clean_dir)

def build_crownandcutlass(clean_dir):
    global build_dir, root_dir
    os.chdir(root_dir + "/CrownAndCutlass/bin/" + build_dir)
    return do_build("../../src", "CrownAndCutlass", clean_dir)

def build_gamestate_test(clean_dir):
    global build_dir, root_dir
    os.chdir(root_dir + "/CrownAndCutlass/test/bin/" + build_dir)
    return do_build("../../src", "GameStateTest", clean_dir)

def run_protocce_test():
    global build_dir, root_dir
    os.chdir(root_dir + "/Protocce/test/bin/" + build_dir)
    status = exec_command("./Test -iassume")
    if status != 0:
        return 1
    else:
        return 0

def build():
    global svn_clients
    svn_choices = [ x for x in svn_clients.iterkeys() ]
    
    print "Building Crown and Cutlass..."
    print "Getting platform info..."
    global build_dir, cmake_generator, root_dir, default_svn_client
    get_platform()
    
    print "Parsing command-line arguments..."
    parser = OptionParser()
    parser.add_option(
        "-c", "--clean",
        action="store_true",
        dest="clean",
        default=False,
        help="clean build dir prior to calling cmake")
    parser.add_option(
        "-u", "--update",
        action="store_true",
        dest="update",
        default=False,
        help="update working dir from SVN prior to starting build")
    parser.add_option(
        "-s", "--svn-client",
        type="choice",
        choices=svn_choices,
        dest="client",
        default=default_svn_client,
        help="select SVN client (" + ", ".join(svn_choices) + ")")
    parser.add_option(
        "-r", "--run-test",
        action="store_true",
        dest="run_test",
        default=False,
        help="run the Protocce test after building")
    (options, args) = parser.parse_args()
    
    if options.update:
        print "Updating from SVN..."
        status = update_svn(options.client)
        if status != 0:
            print "ERROR: Could not update from SVN"
            return status
    
    print "Building Protocce..."
    status = build_protocce(options.clean)
    if status != 0:
        print "ERROR: build failed (Protocce)"
        return status
    
    print "Building Protocce Test..."
    status = build_protocce_test(options.clean)
    if status != 0:
        print "ERROR: build failed (Protocce Test)"
        return status
    
    if options.run_test:
        print "Running Protocce Test..."
        status = run_protocce_test()
        if status != 0:
            print "ERROR: Protocce Test failed"
            return status
    
    print "Building Crown and Cutlass..."
    status = build_crownandcutlass(options.clean)
    if status != 0:
        print "ERROR: build failed (Crown and Cutlass)"
        return status
    
    print "Building GameState Test..."
    status = build_gamestate_test(options.clean)
    if status != 0:
        print "ERROR: build failed (GameState Test)"
        return status
    
    return 0

def main():
    global root_dir 
    root_dir = os.getcwd()
    print root_dir
    try:
        status = build()
    finally:
        os.chdir(root_dir)
    sys.exit(status)
    
if __name__ == "__main__":
    main()
