#include "Person.h"

using namespace dct;

Person::Person(
    const std::string& firstName,
    const std::string& lastName,
    const unsigned int age):
    mFirstName(firstName), mLastName(lastName), mAge(age)
{
}

const std::string Person::FullName() const {
    return mFirstName + std::string(" ") + mLastName;
}

const unsigned int& Person::Age() const {
    return mAge;
}

