#include <iostream>
#include "Person.h"

using namespace std;
using namespace dct;

int main(int argc, const char* argv[]) {
    Person p("John", "Doe", 35);

    cout << p.FullName() << " is " << p.Age() << endl;

    return 0;
}
