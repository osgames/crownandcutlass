import os
from pyplusplus import module_builder

#Creating an instance of class that will help you to expose your declarations
mb = module_builder.module_builder_t( [r"../include/Person.h"]
                                      , gccxml_path=r"/usr/local/bin/gccxml" 
                                      , working_directory=r"../include"
                                      , include_paths=[r"../include"]
                                      , define_symbols=[] )


#Well, don't you want to see what is going on?
mb.print_declarations()

#Creating code creator. After this step you should not modify/customize declarations.
mb.build_code_creator( module_name='dct' )

#Writing code to file.
mb.write_module( '../src/pybind.cpp' )

