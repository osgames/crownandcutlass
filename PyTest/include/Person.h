#ifndef _DCT_PERSON_H_
#define _DCT_PERSON_H_

#include <string>

namespace dct {

class Person {
public:
    Person(
        const std::string& firstName,
        const std::string& lastName,
        const unsigned int age);

    const std::string FullName() const;

    const unsigned int& Age() const;

private:
    std::string mFirstName;
    std::string mLastName;

    unsigned int mAge;

    Person() {};
};

}

#endif

