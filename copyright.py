#!/usr/bin/python

import os
import datetime
import re
import shutil

copyright_text = '^(.*Copyright \(c\) 2004 - )(20[0-9]{2})( David Thulson and Collin Sanford.*)$'
copyright_re = re.compile(copyright_text)
files_to_check = frozenset([ '.*\.h'
                           , '.*\.hpp'
                           , '.*\.c'
                           , '.*\.cpp'
                           , '.*\.h\.in'
                           , '.*\.hpp\.in'
                           , '.*\.c\.in'
                           , '.*\.cpp\.in'
                           , 'LICENSE'
                           ])

def get_current_year():
    t = datetime.date.today()
    return t.year
    
def fix_file(name, current_year_as_str):
    print 'Fixing year in ' + name
    backup_name = name + '.copyright_bak'
    shutil.copyfile(name, backup_name)
    in_file = open(backup_name, 'r')
    out_file = open(name, 'w')
    matched = False
    for line in in_file:
        m = copyright_re.match(line)
        if m:
            matched = True
            out_file.write(m.group(1) + current_year_as_str + m.group(3) + os.linesep)
        else:
        	out_file.write(line)
    os.remove(backup_name)
    if not matched:
        raise Exception, "Copyright line not found in " + name

def check_file(name, current_year_as_str):
    f = open(name, 'r')
    for line in f:
        m = copyright_re.match(line)
        if m and (m.group(2) != current_year_as_str):
            return True
    return False

def check_files(root_dir, current_year_as_str):
    to_fix = []
    name_re = re.compile('^(' + '|'.join(files_to_check) + ')$')
    
    # Loop through file names and fine candidates for copyright info
    for root, dirs, files in os.walk(root_dir):
        for name in files:
            fullname = os.path.join(root, name)
            if (not name.startswith('.')) and name_re.match(name) and check_file(fullname, current_year_as_str):
                to_fix.append(fullname)
        for name in dirs:
            if name.startswith('.'):
                dirs.remove(name)
    
    # Now we have the list to fix, so loop through it and fix them
    for name in to_fix:
        fix_file(name, current_year_as_str)

def main():
    root_dir = os.getcwd()
    current_year = get_current_year()
    current_year_as_str = str(current_year)
    if current_year < 2008:
        print "ERROR: " + current_year_as_str + " is too old to be correct."
        print "  Make sure your computer date is right."
    else:
        check_files(root_dir, current_year_as_str)
    
if __name__ == "__main__":
    main()
