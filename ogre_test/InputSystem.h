#ifndef _INPUT_SYSTEM_H_
#define _INPUT_SYSTEM_H_

#include "Ogre.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "OISEvents.h"
#include "EventHandler.h"

class InputSystem {
public:
  InputSystem(Ogre::RenderWindow* win);
  ~InputSystem();

  void Capture();

  void AddListener(tEventHandlerPtr handler);
  void RemoveListener();

private:
  OIS::InputManager *mInputManager;
  OIS::Keyboard *mKeyboard;
  OIS::Mouse *mMouse;
  tEventHandlerPtr mHandler;
};

#endif
