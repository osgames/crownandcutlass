#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <string>
#include <list>
#include <boost/shared_ptr.hpp>

class Block {
public:
  Block(float x, float z, std::string material);

  float GetX();
  float GetZ();
  std::string GetMaterial();

private:
  float mX;
  float mZ;
  std::string mMaterial;
};

typedef boost::shared_ptr< Block > tBlockPtr;
typedef std::list< tBlockPtr > tBlockPtrList;

#endif
