#include <sstream>
#include <map>
#include "tinyxml/tinyxml.h"
#include "GraphicsSystem.h"
#include "Level.h"

using namespace std;
using namespace Ogre;

typedef map< string, string > tMaterialNameMap;

Level::Level(const string filename) {
  ostringstream outstream;

  TiXmlDocument doc(filename);
  TiXmlElement* element;
  TiXmlElement* row;
  TiXmlElement* block;

  int currentRow;
  int currentColumn;

  MaterialPtr mat;
  string color;
  string matName;
  tMaterialNameMap matMap;
  tBlockPtr tempBlock;

  doc.LoadFile();
  if (doc.Error()) {
    outstream.str("");
    outstream << "Could not load " << filename << "(" << doc.ErrorRow() << ": " << doc.ErrorDesc() << ")";
    throw outstream.str();
  }
  element = doc.FirstChildElement("breakoutlevel");
  if (element == NULL) {
    throw string("Could not find \"breakoutlevel\" element");
  }
  if (element->QueryIntAttribute("level", &mLevelNumber) != TIXML_SUCCESS) {
    throw string("Could not parse level number");
  }
  mTitle = element->Attribute("title");
  if (element->QueryIntAttribute("width", &mWidth) != TIXML_SUCCESS) {
    throw string("Could not parse width");
  }
  if (element->QueryIntAttribute("height", &mHeight) != TIXML_SUCCESS) {
    throw string("Could not parse height");
  }
  element = element->FirstChildElement("blocks");
  if (element == NULL) {
    throw string("Could not find \"blocks\" element");
  }

  mat = MaterialManager::getSingleton().getByName("BlockFFFFFF.material");
  matMap["FFFFFF"] = "BlockFFFFFF.material";

  for(row = element->FirstChildElement("row"); row; row = row->NextSiblingElement("row")) {
    // We know row is valid, since it's a condition of the loop
    if (row->QueryIntAttribute("number", &currentRow) != TIXML_SUCCESS) {
      throw string("Could not parse row number");
    }
    // >= because we are 0 indexed
    if ((currentRow >= mHeight) || (currentRow < 0)) {
      outstream.str("");
      outstream << "Row index (" << currentRow << ") out of bounds (" << mHeight << ")";
      throw outstream.str();
    }

    for(block = row->FirstChildElement("block"); block; block = block->NextSiblingElement("block")) {
      if (block->QueryIntAttribute("column", &currentColumn) != TIXML_SUCCESS) {
        throw string("Could not parse block column");
      }
      // >= because we are 0 indexed
      if ((currentColumn >= mWidth) || (currentColumn < 0)) {
        outstream.str("");
        outstream << "Column index (" << currentColumn << ") out of bounds (" << mWidth << ") in row " << currentRow;
        throw outstream.str();
      }
      color = block->Attribute("color");
      matName = "Block" + color + ".material";
      if (matMap.find(color) == matMap.end()) {
        ColourValue c = GetColorFromString(color);
        MaterialPtr newMat = mat->clone(matName);
        newMat->setDiffuse(c);
        newMat->setAmbient(c);
        matMap[color] = matName;
        LogManager::getSingleton().logMessage("Material " + matName + " created");
      }
      tempBlock.reset(new Block(currentColumn, currentRow, matName));
      mBlocks.push_back(tempBlock);
      outstream.str("");
      outstream << "Created block at (" << tempBlock->GetX() << ", 0.0, " << tempBlock->GetZ() << ") with material " << tempBlock->GetMaterial();
      LogManager::getSingleton().logMessage(outstream.str());
    }
  }
}

void Level::AddToScene(SceneManager* scene) {
  SceneNode* node;
  Entity* ent;
  tBlockPtr block;
  ostringstream s;
  Vector3 v;

  for(tBlockPtrList::const_iterator i = mBlocks.begin(); i != mBlocks.end(); ++i) {
    block = *i;

    s << "Block (" << block->GetX() << ", 0.0, " << block->GetZ() << ")";
    node = scene->getRootSceneNode()->createChildSceneNode();
    ent = scene->createEntity(s.str(), "Cube.mesh");
    ent->setMaterialName(block->GetMaterial());
    node->setPosition(block->GetX() * 2.0 - mWidth, 0.0, block->GetZ() * 2.0 - mHeight);
    node->attachObject(ent);

    v = node->getPosition();
    s.str("");
    s << "Created block node at " << v << ") with material " << ent->getSubEntity(0)->getMaterialName();
    s << " " << ent->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getDiffuse();
    LogManager::getSingleton().logMessage(s.str());
  }
}

ColourValue Level::GetColorFromString(const std::string color) {
  if (color.length() != 6) {
    throw string("color is not 6 characters");
  }
  int r, g, b;
  ostringstream os;
  istringstream s(color.substr(0, 2) + " " + color.substr(2, 2) + " " + color.substr(4, 2));
  s >> setbase(16) >> r >> g >> b;
  os << color << " " << (float) r << " (" << r / 255.0 << ") " << " " << g << " (" << (float) g / 255.0 << ") " << " " << b << " (" << (float) b / 255.0 << ") ";
  LogManager::getSingleton().logMessage(os.str());
  return ColourValue((float) r / 255.0, (float) g / 255.0, (float) b / 255.0, 1.0);
}
