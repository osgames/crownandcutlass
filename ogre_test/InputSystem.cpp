#include <sstream>
#include "OISInputManager.h"
#include "OISException.h"
#include "InputSystem.h"

using namespace std;
using namespace boost;
using namespace OIS;

InputSystem::InputSystem(Ogre::RenderWindow* win) {
  ParamList pl;
  size_t windowHnd = 0;
  std::ostringstream windowHndStr;

  if (win == NULL) {
    throw "RenderWindow is NULL";
  }

  win->getCustomAttribute("WINDOW", &windowHnd);
  windowHndStr << windowHnd;
  pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

  mInputManager = InputManager::createInputSystem(pl);
  if (mInputManager->numKeyBoards() > 0) {
    mKeyboard = static_cast<Keyboard*>(mInputManager->createInputObject(OISKeyboard, true));
  }
  if (mInputManager->numMice() > 0) {
    mMouse = static_cast<Mouse*>(mInputManager->createInputObject(OISMouse, true));
  }
}

InputSystem::~InputSystem() {
  if (mInputManager != NULL) {
    RemoveListener();

    mInputManager->destroyInputObject(mKeyboard);
    mInputManager->destroyInputObject(mMouse);

    InputManager::destroyInputSystem(mInputManager);
    mInputManager = NULL;
  }
}

void InputSystem::Capture() {
  if (mKeyboard != NULL) {
    mKeyboard->capture();
  }
  if (mMouse != NULL) {
    mMouse->capture();
  }
}

void InputSystem::AddListener(tEventHandlerPtr handler) {
  mHandler = handler;
  if (mKeyboard != NULL) {
    mKeyboard->setEventCallback(handler.get());
  }
  if (mMouse != NULL) {
    mMouse->setEventCallback(handler.get());
  }
}

void InputSystem::RemoveListener() {
  if (mKeyboard != NULL) {
    mKeyboard->setEventCallback(NULL);
  }
  if (mMouse != NULL) {
    mMouse->setEventCallback(NULL);
  }
  mHandler.reset();
}
