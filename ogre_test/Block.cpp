#include "Block.h"

using namespace std;

Block::Block(float x, float z, string material) {
  mX = x;
  mZ = z;
  mMaterial = material;
}

float Block::GetX() {
  return mX;
}

float Block::GetZ() {
  return mZ;
}

string Block::GetMaterial() {
  return mMaterial;
}
