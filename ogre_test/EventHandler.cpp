#include <iostream>
#include "EventHandler.h"

using namespace std;
using namespace OIS;

EventHandler::EventHandler(Ogre::SceneNode* paddle) {
  mRunning = true;
  mPaddle = paddle;
}

bool EventHandler::IsRunning() {
  return mRunning;
}

bool EventHandler::keyPressed( const KeyEvent &arg ) {
  return true;
}

bool EventHandler::keyReleased( const KeyEvent &arg ) {
  if (arg.key == KC_ESCAPE) {
    mRunning = false;
  }
  return true;
}

bool EventHandler::mouseMoved( const MouseEvent &arg ) {
  const OIS::MouseState& s = arg.state;
  if (mPaddle != NULL) {
    // This should check bounds based on the current level...
    mPaddle->translate((float) s.X.rel / 10.0, 0.0, 0.0);
  }
  return true;
}

bool EventHandler::mousePressed( const MouseEvent &arg, MouseButtonID id ) {
  return true;
}

bool EventHandler::mouseReleased( const MouseEvent &arg, MouseButtonID id ) {
  return true;
}
