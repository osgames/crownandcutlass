#ifndef _LEVEL_H_
#define _LEVEL_H_

#include <string>
#include <Ogre.h>
#include "Block.h"

class Level {
public:
  Level(const std::string filename);

  void AddToScene(Ogre::SceneManager* scene);

private:
  int mLevelNumber;
  std::string mTitle;
  int mWidth;
  int mHeight;
  tBlockPtrList mBlocks;

  Ogre::ColourValue GetColorFromString(const std::string color);
};

#endif
