#ifndef _EVENT_HANDLER_H_
#define _EVENT_HANDLER_H_

#include "Ogre.h"
#include "OISKeyboard.h"
#include "OISMouse.h"
#include "OISEvents.h"
#include <boost/shared_ptr.hpp>

class EventHandler: public OIS::KeyListener, public OIS::MouseListener {
public:
  EventHandler(Ogre::SceneNode* paddle);

  bool IsRunning();

  bool keyPressed( const OIS::KeyEvent &arg );
  bool keyReleased( const OIS::KeyEvent &arg );

  bool mouseMoved( const OIS::MouseEvent &arg );
  bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
  bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

private:
  bool mRunning;
  Ogre::SceneNode* mPaddle;
};

typedef boost::shared_ptr< EventHandler > tEventHandlerPtr;

#endif
