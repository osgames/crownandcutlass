#ifndef _GRAPHICS_SYSTEM_H_
#define _GRAPHICS_SYSTEM_H_

#include <memory>
#include <string>
#include "Ogre.h"

class GraphicsSystem {
public:
  GraphicsSystem();
  ~GraphicsSystem();

  void Initialize(int width, int height, bool fullscreen, std::string title);

  Ogre::SceneManager* NewSceneManager(Ogre::SceneType type);

  Ogre::RenderWindow* GetRenderWindow();

  void RenderFrame();

private:
  std::auto_ptr< Ogre::Root > mRoot;
  Ogre::RenderWindow* mWindow;
  bool mInitialized;
};

#endif
