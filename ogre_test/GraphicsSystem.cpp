#include "GraphicsSystem.h"

using namespace std;

GraphicsSystem::GraphicsSystem() {
  mRoot.reset(new Ogre::Root("", ""));
  mWindow = NULL;
  mInitialized = false;
}

GraphicsSystem::~GraphicsSystem() {
  mRoot.reset();
}

void GraphicsSystem::Initialize(int width, int height, bool fullscreen, std::string title) {
  if (mInitialized) {
    throw "GraphicsSystem already initialized";
  }

  mRoot->loadPlugin("RenderSystem_GL");

  Ogre::RenderSystemList *renderSystems = mRoot->getAvailableRenderers();
  if (renderSystems->size() == 0) {
    throw "No available render systems";
  }

  mRoot->setRenderSystem(renderSystems->front());
  if (mRoot->getRenderSystem() == NULL) {
    throw "Could not set render system";
  }
  
  // false because we are not using an autocreated window
  mRoot->initialise(false);
  mWindow = mRoot->createRenderWindow(title, width, height, fullscreen);

  mInitialized = true;
}

Ogre::RenderWindow* GraphicsSystem::GetRenderWindow() {
  if (!mInitialized) {
    throw "GraphicsSystem not initialized";
  }
  return mWindow;
}

void GraphicsSystem::RenderFrame() {
  mRoot->renderOneFrame();
  mWindow->update();
}

Ogre::SceneManager* GraphicsSystem::NewSceneManager(Ogre::SceneType type) {
  return mRoot->createSceneManager(type);
}
