#include <memory>
#include "Ogre.h"
#include "OIS.h"
#include "GraphicsSystem.h"
#include "EventHandler.h"
#include "InputSystem.h"
#include "Level.h"

using namespace std;
using namespace Ogre;

int main(int argc, char **argv) {
  auto_ptr< GraphicsSystem > graphics (new GraphicsSystem());

  try {
    const unsigned int w = 800;
    const unsigned int h = 600;
    const bool fullscreen = false;
    const string title = "Breakout";

    graphics->Initialize(w, h, fullscreen, title);

    ResourceGroupManager::getSingleton().addResourceLocation(
      "data/textures", "FileSystem", "General");
    ResourceGroupManager::getSingleton().addResourceLocation(
      "data", "FileSystem", "General");
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    // Initialize input
    LogManager::getSingleton().logMessage("Initializing input...");
    auto_ptr< InputSystem > input(new InputSystem(graphics->GetRenderWindow()));
    LogManager::getSingleton().logMessage("Input initialized");

    // Create scene
    SceneManager *sceneMgr = graphics->NewSceneManager(ST_GENERIC);
    Camera *camera = sceneMgr->createCamera("Camera");
    camera->setFixedYawAxis(false);
    camera->setPosition(0,50,0);
    camera->lookAt(0,0,0);
    camera->setNearClipDistance(10);

    Viewport* vp = graphics->GetRenderWindow()->addViewport(camera);
    vp->setBackgroundColour(ColourValue(0,0,0));

    // Alter the camera aspect ratio to match the viewport
    camera->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));

    // Set up the lighting
    sceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
    Light* light = sceneMgr->createLight("Light");
    light->setType(Light::LT_DIRECTIONAL);
    light->setDiffuseColour(1, 1, 1);
    //light->setSpecularColour(1, 1, 1);
    light->setDirection(camera->getDirection());

    // Create background material
    MaterialPtr material = MaterialManager::getSingleton().create("Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("Background.png");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);

    // Disable lighting on the background (it will show as fully lit)
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    // Create background rectangle
    Rectangle2D* rect = new Rectangle2D(true);
    // Cover the whole screen
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    // Hacky, but we need to set the bounding box to something big
    rect->setBoundingBox(
      AxisAlignedBox(-100000.0*Vector3::UNIT_SCALE, 100000.0*Vector3::UNIT_SCALE));
    rect->setMaterial("Background");
    // Render the background before everything else
    rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

    // Attach background to the scene
    SceneNode* node = sceneMgr->getRootSceneNode()->createChildSceneNode("Background");
    node->attachObject(rect);

    Entity* ent1 = sceneMgr->createEntity("Paddle", "Cube.mesh");
    SceneNode* node1 = sceneMgr->getRootSceneNode()->createChildSceneNode("PaddleNode");
    node1->attachObject(ent1);
    node1->scale(3, 1, 1);
    ent1->setNormaliseNormals(true);

    auto_ptr< Level > level(new Level("data/levels/LevelLayout.xml"));
    level->AddToScene(sceneMgr);

    LogManager::getSingleton().logMessage("Scene created");

    tEventHandlerPtr handler(new EventHandler(node1));
    input->AddListener(handler);

    while(handler->IsRunning()) {
      graphics->RenderFrame();
      input->Capture();
    }
    LogManager::getSingleton().logMessage("Shutdown started");
  } catch (Ogre::Exception &e) {
    cout << e.getFullDescription() << endl;
  } catch (OIS::Exception &e) {
    cout << "OIS Exception: " << e.eType << " " << e.eText << endl;
  } catch (const char *e) {
    cout << "Uncaught exception (" << e << ")"<< endl;
  } catch (string e) {
    cout << "Uncaught exception (" << e << ")" << endl;
  } catch (...) {
    cout << "Uncaught exception" << endl;;
  }
}
