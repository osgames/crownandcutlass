/* Crown and Cutlass
 * Environment Object Code
 *
 * This class is a parent to any object which may appear
 * in the game. Initially the things that come to mind which
 * this would be used as a parent class would be cities,
 * hidden treasures, and perhaps enemy pirate ships.
 */

#include <string>
#include "ccmath.h"
#include "Log.h"
#include "IEnvironmentObject.h"

using namespace std;

IEnvironmentObject::IEnvironmentObject() {
  locX = 0;
  locZ = 0;
  visible = false;
  name = "";
}

IEnvironmentObject::IEnvironmentObject(string objName, int objLocX, int objLocZ, bool objVisible) {
  locX = objLocX;
  locZ = objLocZ;
  visible = objVisible;
  name = objName;
}

string IEnvironmentObject::getName() {
  return name;
}

float IEnvironmentObject::calcDist(float x, float z) {
  return sqrt((x-locX)*(x-locX)+(z-locZ)*(z-locZ));
}

float IEnvironmentObject::calcAngle(float x, float z) {
  // This is weird, but because of the strange coords setup it works
  float angle = atan2(z - locZ, locX - x);

  // atan2 returns from -PI to PI, so move to 0 to 2*PI
  if (angle < 0) angle = 2*M_PI + angle;

  return angle;
}

int IEnvironmentObject::GetLocX() {
  return locX;
}

int IEnvironmentObject::GetLocZ() {
  return locZ;
}
