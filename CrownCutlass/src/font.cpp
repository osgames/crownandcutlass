/* Crown and Cutlass
 * Font Creation / Use Code
 */

#include <cstdio>
#include <string>
#include <cstdarg>
#include "GLee.h"
#include <GL/glu.h>
#include "Log.h"
#include "Texture.h"
#include "font.h"

using namespace std;

void EnterTextMode(int width, int height) {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluOrtho2D(0, width, height, 0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
}

void ExitTextMode() {
  glEnable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glDisable(GL_BLEND);
}
