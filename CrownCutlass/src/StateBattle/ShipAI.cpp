/* Crown and Cutlass
 * Ship AI Object Code
 */

#include "../Log.h"
#include "../ccmath.h"
#include "../Ship.h"
#include "ShipAI.h"

#define CHASE_RANGE 12

ShipAI::ShipAI(Ship *shipIn, Ship *playerShipIn, float bulletSpeedIn) {
  m_ship = shipIn;
  m_playerShip = playerShipIn;

  m_state = asFight;

  m_reloaded = true;

  m_bulletSpeed = bulletSpeedIn;

  m_shipMaxDamage = m_ship->GetMaxDamage();
}

ShipAI::~ShipAI() {
  // Don't delete the ships, the Battle object will delete them
}

void ShipAI::Update(unsigned int ticks, bool reloadedIn) {
  float dX, dZ;

  m_reloaded = reloadedIn;

  // It's weird that the order of these is reversed, but it works...
  dX = m_ship->m_x - m_playerShip->m_x;
  dZ = m_playerShip->m_z - m_ship->m_z;

  m_distToPlayer = sqrt((dX*dX) + (dZ*dZ));

  if ((m_state != asFlee) && (m_ship->m_damage < (m_shipMaxDamage / 2)) && (m_playerShip->m_damage > (m_ship->m_damage * 1.5))) {
    m_state = asFlee;
    m_ship->SetSails(FULLSAILS);
  }

  m_rotToPlayer = m_ship->m_rot - (atan2(dZ, dX) + M_PI);

  if (m_rotToPlayer < -M_PI) {
    m_rotToPlayer += TWO_PI;
  }
  if (m_rotToPlayer > M_PI) {
    m_rotToPlayer -= TWO_PI;
  }

  m_rotToHitPlayer = CalcAngleToHit();

  switch (m_state) {
    case asFight:
      UpdateFight(ticks);
      break;
    case asFlee:
      UpdateFlee(ticks);
      break;
    case asChase:
      UpdateChase(ticks);
      break;
  }
  //Log::logger->message("%d: ship %f, rot %f, rotHit %f, dist %f (%f)", state, ship->rot, rotToPlayer, rotToHitPlayer, distToPlayer, atan2(dZ, dX) + M_PI);
}

void ShipAI::UpdateFight(unsigned int ticks) {
  if (m_distToPlayer > CHASE_RANGE) {
    m_state = asChase;
    m_ship->SetSails(FULLSAILS);
  }

  if (m_reloaded) {
    if (fabs(-1*M_PI_2 - m_rotToHitPlayer) < degreesToRadians(10)) {
      m_fire = true;
      m_fireAngle = M_PI_2;
    } else if (fabs(M_PI_2 - m_rotToHitPlayer) < degreesToRadians(10)) {
      m_fire = true;
      m_fireAngle = -1*M_PI_2;
    }
  }

  if (((m_rotToHitPlayer < 0) && (m_rotToHitPlayer > -1*M_PI_2)) || ((m_rotToHitPlayer > M_PI_2) && (m_rotToHitPlayer < M_PI))) {
    m_ship->SetRudder(RIGHT);
  } else {
    m_ship->SetRudder(LEFT);
  }
}

void ShipAI::UpdateFlee(unsigned int ticks) {
  if ((m_rotToPlayer > 0) && (m_rotToPlayer < M_PI-0.03)) {
    m_ship->SetRudder(LEFT);
  } else if (m_rotToPlayer > 0.03-M_PI) {
    m_ship->SetRudder(RIGHT);
  }
}

void ShipAI::UpdateChase(unsigned int ticks) {
  if (m_distToPlayer < CHASE_RANGE-(CHASE_RANGE*.3)) {
    m_state = asFight;
    m_ship->SetSails(HALFSAILS);
  }

  if (m_rotToPlayer < -0.03) {
    m_ship->SetRudder(LEFT);
  } else if (m_rotToPlayer > 0.03) {
    m_ship->SetRudder(RIGHT);
  }
}

bool ShipAI::ShouldFire(float *angleOut) {
  if (m_fire) {
    *angleOut = m_fireAngle;
    m_reloaded = false;
    m_fire = false;
    return true;
  } else {
    return false;
  }
}

float ShipAI::CalcAngleToHit() {
  float dAngle;
  float drift = m_distToPlayer / m_bulletSpeed * m_ship->m_speed;

  dAngle = atan2(drift, m_distToPlayer);
  if (m_rotToPlayer < 0) {
    dAngle = -1 * dAngle;
  }

  return dAngle + m_rotToPlayer;
}
