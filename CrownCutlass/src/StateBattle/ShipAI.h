/* Crown and Cutlass
 * Ship AI Object Header
 */

#if !defined ( _SHIPAI_H_ )
#define _SHIPAI_H_

class Ship;

typedef enum {asFight, asFlee, asChase} AIState;

class ShipAI {
public:
  ShipAI(Ship *shipIn, Ship *playerShipIn, float bulletSpeedIn);
  ~ShipAI();

  void Update(unsigned int ticks, bool reloadedIn);

  bool ShouldFire(float *angleOut);

 private:
   Ship *m_ship;
   Ship *m_playerShip;

   AIState m_state;

   // Just so we don't have to dereference a pointer over and over
   int m_shipMaxDamage;

   float m_rotToPlayer; // Angle to player's ship
   float m_rotToHitPlayer; // Angle need to be to hit player's ship
   float m_distToPlayer;

   bool m_reloaded;

   bool m_fire;
   float m_fireAngle;

   // From the bullet class, just so the AI knows how fast it shoots
   float m_bulletSpeed;

   void UpdateFight(unsigned int ticks);
   void UpdateFlee(unsigned int ticks);
   void UpdateChase(unsigned int ticks);

   float CalcAngleToHit();
};

#endif
