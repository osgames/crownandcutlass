/* Crown and Cutlass
 * Naval Battle Done Header
 */

#if !defined( _STATEDONE_H_ )

#define _STATEDONE_H_

#include <vector>
#include <boost/shared_ptr.hpp>

#include "../Callback.h"
#include "../IGameState.h"

using boost::shared_ptr;

class Ship;
class Menu;
class Cargo;
class Texture;
class Product;
class CCTable;
class CCLabel;

typedef enum {sbVictory, sbDefeat, sbNone} BattleResult;

class StateDone: public IGameState {
 public:
  // Constructor
  StateDone(Ship *enemy, BattleResult result);

  // Destructor
  ~StateDone();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Callbacks
  void Exit(int ID);
  void Buy(int ID);
  void Sell(int ID);
  void TakeAll(int ID);
  void LeaveAll(int ID);

 private:
  // Background texture
  Texture *m_background;

  // Menu class
  CCTable* m_productTable;

  CCLabel* m_firstTitle;
  CCLabel* m_secondTitle;
  CCButton* m_doneButton;

  int m_enemyGold;
  shared_ptr<Cargo> m_enemyCargo;

  // Player object from state sailing, need to pass to StateBattle
  shared_ptr<Cargo> m_playerCargo;

  // List of buttons, to find out which was called.
  std::vector<int> m_buyList;
  std::vector<int> m_sellList;
  std::vector<int> m_takeList;
  std::vector<int> m_leaveList;
  //int GoldData;
  std::vector<Product*> m_productList;

  TCallback<StateDone> m_exitCallback;
  TCallback<StateDone> m_buyCallback;
  TCallback<StateDone> m_sellCallback;
  TCallback<StateDone> m_buyAllCallback;
  TCallback<StateDone> m_sellAllCallback;

  // Create the menu for moving cargo
  void SetupMenu();

  void SackPlayer();

  void SetMenuQtys();

  void CheckInput();

  // Font names
  static const std::string s_mainFontName;
  static const std::string s_highlightFontName;
  static const std::string s_disabledFontName;
};

#endif
