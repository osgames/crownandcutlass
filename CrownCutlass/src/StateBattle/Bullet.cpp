/* Crown and Cutlass
 * Bullet Header
 */

#include "../GLee.h"
#include "../ccmath.h"
#include "../Ship.h"
#include "Bullet.h"

#define BULLETSPEED .006
#define BULLETGRAVITY -.000002

GLuint Bullet::s_bulletList = 0;

Bullet::Bullet(Ship *ship, float angleIn) {
  float shipX = cos(-1*ship->m_rot) * ship->m_speed;
  float shipZ = sin(-1*ship->m_rot) * ship->m_speed;

  m_owner = ship;

  m_location[0] = m_owner->m_x;
  m_location[1] = .01;
  m_location[2] = m_owner->m_z;

  m_vector[0] = cos(angleIn) * BULLETSPEED + shipX;
  m_vector[1] = .0013;
  m_vector[2] = sin(angleIn) * BULLETSPEED + shipZ;

  // This really should be ship->m_rot, not angleIn
  m_angle = radiansToDegrees(m_owner->m_rot);
}

Bullet::~Bullet() {
}

void Bullet::Update(unsigned int ticks) {
  m_location[0] += m_vector[0] * ticks;
  m_location[1] += m_vector[1] * ticks;
  m_location[2] += m_vector[2] * ticks;

  m_vector[1] += BULLETGRAVITY * ticks;
}

bool Bullet::CheckCollision(Ship *ship) {
  if ((m_location[0]-ship->m_x)*(m_location[0]-ship->m_x) + (m_location[2]-ship->m_z)*(m_location[2]-ship->m_z) <= 1) {
    return true;
  } else {
    return false;
  }
}

void Bullet::Draw() {
  glPushMatrix();
  glTranslatef(m_location[0], m_location[1], m_location[2]);
  glRotatef(m_angle, 0, 1, 0);
  glCallList(s_bulletList);
  glPopMatrix();
}

void Bullet::GenerateList() {
  // Generate the bullet list
  s_bulletList = glGenLists(1);
  if (s_bulletList == 0) {
    // Should throw an exception
    return;
  }

  glNewList(s_bulletList, GL_COMPILE);

  glPushMatrix();
  DrawBullet();
  glTranslatef(0.3, 0, 0);
  DrawBullet();
  glTranslatef(-0.6, 0, 0);
  DrawBullet();
  glPopMatrix();

  glEndList();
}

void Bullet::DeleteList() {
  glDeleteLists(s_bulletList, 1);
}

void Bullet::DrawBullet() {
  glBegin(GL_TRIANGLES);
  glNormal3f(1, 0, 0);
  glVertex3f(.1, 0, 0);
  glNormal3f(0, 1, 0);
  glVertex3f(0, .1, 0);
  glNormal3f(0, 0, 1);
  glVertex3f(0, 0, .1);

  glNormal3f(0, 1, 0);
  glVertex3f(0, .1, 0);
  glNormal3f(-1, 0, 0);
  glVertex3f(-.1, 0, 0);
  glNormal3f(0, 0, 1);
  glVertex3f(0, 0, .1);

  glNormal3f(0, 1, 0);
  glVertex3f(0, .1, 0);
  glNormal3f(0, 0, -1);
  glVertex3f(0, 0, -.1);
  glNormal3f(-1, 0, 0);
  glVertex3f(-.1, 0, 0);

  glNormal3f(0, 1, 0);
  glVertex3f(0, .1, 0);
  glNormal3f(1, 0, 0);
  glVertex3f(.1, 0, 0);
  glNormal3f(0, 0, -1);
  glVertex3f(0, 0, -.1);

  glNormal3f(1, 0, 0);
  glVertex3f(.1, 0, 0);
  glNormal3f(0, 0, 1);
  glVertex3f(0, 0, .1);
  glNormal3f(0, -1, 0);
  glVertex3f(0, -.1, 0);

  glNormal3f(0, -1, 0);
  glVertex3f(0, -.1, 0);
  glNormal3f(0, 0, 1);
  glVertex3f(0, 0, .1);
  glNormal3f(-1, 0, 0);
  glVertex3f(-.1, 0, 0);

  glNormal3f(0, -1, 0);
  glVertex3f(0, -.1, 0);
  glNormal3f(-1, 0, 0);
  glVertex3f(-.1, 0, 0);
  glNormal3f(0, 0, -1);
  glVertex3f(0, 0, -.1);

  glNormal3f(0, -1, 0);
  glVertex3f(0, -.1, 0);
  glNormal3f(0, 0, -1);
  glVertex3f(0, 0, -.1);
  glNormal3f(1, 0, 0);
  glVertex3f(.1, 0, 0);
  glEnd();
}

float Bullet::GetBulletSpeed() {
  return BULLETSPEED;
}
