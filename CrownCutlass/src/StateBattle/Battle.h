/* Crown and Cutlass
 * Naval Battle GameState Header
 */

#if !defined( _BATTLE_H_ )

#define _BATTLE_H_

#include <list>
#include <guichan.hpp>
#include "SDL.h"
#include "../GLee.h"
#include "../IGameState.h"
#include "StateDone.h"

using namespace std;

// So I don't have to include the headers
class Camera;
class Bullet;
class ShipAI;
class StateDone;
class SkyDome;
class SoundEffect;
class Texture;
class Ship;
class CCTable;

class Battle: public IGameState {
 public:
  // Constructor
  Battle();

  // Destructor
  ~Battle();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

 private:
  // Handles processing SDL events
  void CheckInput();

  // Keyboard handler functions
  void HandleKeyUp(SDL_keysym* keysym);
  void HandleKeyDown(SDL_keysym* keysym);

  // Timer function to handle time-based movement
  void Timer(const unsigned int ticks);

  // Fire cannons
  void PlayerFire(float rotDiff);
  void EnemyFire(float rotDiff);

  // Record a collision
  void PlayerHit();
  void EnemyHit();

  void BattleFinished(BattleResult result);

  // Generate the water display list
  void GenerateWaterList();

  // Helper function for generateWaterList
  float CalcBottomDepth(float x, float z, float bottomSize);

  // Initialize the HUD
  void InitHUD();

  // Sets all of the HUD lables based on ship values
  void ResetLabelValues();

  // Current time of day (0 to 24)
  float m_time;

  // Ship model
  Ship *m_ship;
  Ship *m_enemyShip;

  // Object to hand AI for enemy ship
  ShipAI *m_enemyAI;

  // Sky dome object
  SkyDome *m_skyDome;

  int m_playerTicksToReload;
  int m_enemyTicksToReload;

  bool m_battleDone;
  BattleResult m_result;

  // List of cannon balls in the air
  list <Bullet*> m_bullets;

  // Camera location
  Camera *m_camera;
  GLfloat m_cameraX, m_cameraY, m_cameraZ;

  // State to go to after battle is done
  StateDone *m_leaveState;

  // Light info
  GLfloat m_ambientLight[4];
  GLfloat m_diffuseLight[4];
  GLfloat m_lightPosition[4];

  GLfloat m_waterMaterial[4];
  GLfloat m_bottomMaterial[4];
  GLfloat m_bulletMaterial[4];

  Texture *m_waterTexture;
  Texture *m_bottomTexture;

  // Display list for water and bottom of ocean
  GLuint m_waterList;

  SoundEffect *m_playerCannonSound;
  SoundEffect *m_enemyCannonSound;

  SoundEffect *m_playerHitSound;
  SoundEffect *m_enemyHitSound;

  CCTable* m_playerTable;
  CCLabel* m_playerSailsLabel;
  CCLabel* m_playerDamageLabel;
  CCLabel* m_playerReloadLabel;

  CCTable* m_enemyTable;
  CCLabel* m_enemySailsLabel;
  CCLabel* m_enemyDamageLabel;
  CCLabel* m_enemyReloadLabel;

  // Font names
  static const std::string s_mainFontName;
};

#endif
