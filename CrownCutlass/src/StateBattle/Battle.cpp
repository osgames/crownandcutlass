/* Crown and Cutlass
 * Naval Battle GameState Code
 */

#include "../GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include <string>
#include <list>
#include "SDL.h"
#include "../SoundEffect.h"
#include "../SoundSystem.h"
#include "../ccmath.h"
#include "../Log.h"
#include "../Config.h"
#include "../Player.h"
#include "../Camera.h"
#include "../Texture.h"
#include "../IGameState.h"
#include "../Ship.h"
#include "../SkyDome.h"
#include "Bullet.h"
#include "ShipAI.h"
#include "../Menu/CCGui.h"
#include "../Menu/CCLabel.h"
#include "../Menu/CCTable.h"
#include "StateDone.h"
#include "Battle.h"

#define RELOADTICKS 1500

#define WATER_TEXSCALE 6.0
#define WATER_SIZE 200.0

using namespace std;

const string Battle::s_mainFontName = "systemWhite.png";

Battle::Battle(): IGameState(NULL) {
  // Temp storage for window width & height
  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  // Just to be safe
  m_leaveState = NULL;

  // Start at 5am
  m_time = 5.0;

  // Get a copy of the player's ship
  m_ship = new Ship(Player::player->ship);
  //ship = ShipManager::list->newShip(3, "Test");
  m_ship->m_x = 0;
  m_ship->m_z = 0;
  m_ship->m_rot = rand0to1();
  m_ship->m_speed = 0.002;
  //ship->speed = 0;
  m_ship->SetSails(HALFSAILS);
  m_ship->SetRudder(CENTER);
  m_playerTicksToReload = 0;

  // Set up the enemy ship
  m_enemyShip = new Ship("Enemy");
  m_enemyShip->m_x = randInt(5);
  m_enemyShip->m_z = randInt(5);
  if (((m_enemyShip->m_x*m_enemyShip->m_x)+(m_enemyShip->m_z*m_enemyShip->m_z)) < 4) {
    if (m_enemyShip->m_x > 0) {
      m_enemyShip->m_x += 2;
    } else {
      m_enemyShip->m_x -= 2;
    }
  }
  m_enemyShip->m_rot = rand0to1();
  m_enemyShip->m_speed = 0.002;
  //enemyShip->speed = 0;
  m_enemyTicksToReload = RELOADTICKS;
  //enemyShip->damage = 1;
  // Start with half sails
  m_enemyShip->SetSails(HALFSAILS);

  m_enemyAI = new ShipAI(m_enemyShip, m_ship, Bullet::GetBulletSpeed());

  Bullet::GenerateList();

  // Set up the camera
  m_cameraX = 0;
  m_cameraY = 13;
  m_cameraZ = 5;
  m_camera = new Camera((float) width/(float) height, m_cameraX, m_cameraY, m_cameraZ);

  // Set the light color and position
  m_ambientLight[0] = 0.17;
  m_ambientLight[1] = 0.17;
  m_ambientLight[2] = 0.17;
  m_ambientLight[3] = 1.0;

  m_diffuseLight[0] = 1.0;
  m_diffuseLight[1] = 1.0;
  m_diffuseLight[2] = 1.0;
  m_diffuseLight[3] = 1.0;

  m_lightPosition[0] = 7.0;
  m_lightPosition[1] = 15.0;
  m_lightPosition[2] = 2.0;
  m_lightPosition[3] = 0.0;

  m_waterMaterial[0] = 0.23;
  m_waterMaterial[1] = 0.35;
  m_waterMaterial[2] = 0.7;
  m_waterMaterial[3] = 0.45;

  m_bottomMaterial[0] = 0.3;
  m_bottomMaterial[1] = 0.3;
  m_bottomMaterial[2] = 0.3;
  m_bottomMaterial[3] = 1.0;

  m_bulletMaterial[0] = 0.1;
  m_bulletMaterial[1] = 0.1;
  m_bulletMaterial[2] = 0.1;
  m_bulletMaterial[3] = 1.0;

  m_waterTexture = new Texture("water.png");
  m_bottomTexture = new Texture("detail.png");

  // Begin setting up the rendering

  glClearColor(0, 0, 0, 0);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  //glEnable(GL_NORMALIZE);
  glShadeModel(GL_SMOOTH);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);

  // Set up lighting
  // For some reason this messes up the lighting in StateSailing
  //glEnable(GL_LIGHTING);
  //glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
  //glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
  //glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
  //glEnable(GL_LIGHT0);

  GenerateWaterList();

  m_skyDome = new SkyDome(100, 16, 16, "skydome.png");

  m_playerCannonSound = new SoundEffect("cannon.ogg");
  m_enemyCannonSound = new SoundEffect("cannon.ogg");

  m_playerHitSound = new SoundEffect("hit.ogg");
  m_enemyHitSound = new SoundEffect("hit.ogg");

  m_battleDone = false;

  InitHUD();
}

Battle::~Battle() {
  Bullet *temp;

  while (!m_bullets.empty()) {
    temp = m_bullets.front();
    m_bullets.pop_front();
    delete temp;
  }

  m_enemyTable->clear();
  delete m_enemyTable;
  m_playerTable->clear();
  delete m_playerTable;

  delete m_enemyAI;

  delete m_ship;
  delete m_enemyShip;
  delete m_camera;
  delete m_skyDome;

  delete m_playerCannonSound;
  delete m_playerHitSound;
  delete m_enemyCannonSound;
  delete m_enemyHitSound;

  if (m_leaveState != NULL) {
    delete m_leaveState;
    m_leaveState = NULL;
  }

  Bullet::DeleteList();

  glDeleteLists(m_waterList, 1);
  delete m_bottomTexture;
  delete m_waterTexture;

  // Don't delete the player, StateSailing will do that

  Log::s_log->Message("Battle deleted");
}

void Battle::Update(const unsigned int ticks) {
  // Process incoming events
  CheckInput();

  // Update the scene
  Timer(ticks);
}

void Battle::Display() {
  float texShiftX = m_ship->m_x / WATER_TEXSCALE;
  float texShiftZ = m_ship->m_z / WATER_TEXSCALE;

  Bullet *temp;
  list< Bullet* >::iterator iter;
  list< Bullet* >::iterator iterEnd;

  glLoadIdentity();
  m_camera->SetCamera();

  glPushMatrix();
  glTranslatef(0, 0, 0);
  m_skyDome->Draw(m_time);
  glPopMatrix();

  // Draw the ship
  m_ship->Draw(false, 0, 0);

  // Draw the enemy ship
  m_enemyShip->Draw(true, -1*m_ship->m_x, -1*m_ship->m_z);

  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, m_bulletMaterial);

  glPushMatrix();
  glTranslatef(-1*m_ship->m_x, 0, -1*m_ship->m_z);
  glDisable(GL_TEXTURE_2D);

  iter = m_bullets.begin();
  iterEnd = m_bullets.end();
  while(iter != iterEnd) {
    temp = *iter;
    temp->Draw();

    ++iter;
  }
  glEnable(GL_TEXTURE_2D);
  glPopMatrix();

  glMatrixMode(GL_TEXTURE);
  glTranslatef(texShiftX, texShiftZ, 0);
  glCallList(m_waterList);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
}

void Battle::CheckInput() {
  /* Our SDL event placeholder. */
  SDL_Event event;

  /* Grab all the events off the queue. */
  while( SDL_PollEvent( &event ) ) {
    switch( event.type ) {
    case SDL_KEYDOWN:
      /* Handle key presses. */
      HandleKeyDown( &event.key.keysym );
      break;
    case SDL_KEYUP:
      HandleKeyUp(&event.key.keysym);
      break;
    case SDL_QUIT:
      /* Handle quit requests (like Ctrl-c). */
      DoExit();
      break;
    }
  }
}

void Battle::HandleKeyUp(SDL_keysym* keysym) {
  switch( keysym->sym ) {
  case SDLK_LEFT:
    m_ship->SetRudder(CENTER);
    break;
  case SDLK_RIGHT:
    m_ship->SetRudder(CENTER);
    break;
  case SDLK_ESCAPE:
    PrepareStateSwitch(s_mainMenu, this);
    break;
  default:
    break;
  }
}

void Battle::HandleKeyDown(SDL_keysym* keysym) {
  switch( keysym->sym ) {
  case SDLK_e:
    PlayerFire(-1*M_PI_2);
    break;
  case SDLK_q:
    PlayerFire(M_PI_2);
    break;
  case SDLK_d:
    EnemyFire(-1*M_PI_2);
    break;
  case SDLK_a:
    EnemyFire(M_PI_2);
    break;
  case SDLK_c:
    m_ship->m_rot += M_PI;
    break;
  case SDLK_x:
    BattleFinished(sbNone);
    break;
  case SDLK_z:
    TakeScreenshot();
    break;
  case SDLK_1:
    m_cameraX = 0;
    m_cameraY = 13;
    m_cameraZ = 9;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_2:
    m_cameraX = 0;
    m_cameraY = 150;
    m_cameraZ = 150;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_3:
    m_cameraX = 0;
    m_cameraY = 5;
    m_cameraZ = 9;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_4:
    m_cameraX = 0;
    m_cameraY = 26;
    m_cameraZ = 18;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_5:
    m_cameraX = 0;
    m_cameraY = 0;
    m_cameraZ = 10;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_6:
    m_cameraX = 0;
    m_cameraY = 30;
    m_cameraZ = .00001;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_7:
    m_cameraX = 0;
    m_cameraY = 2.6;
    m_cameraZ = 1.8;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_8:
    m_cameraX = 0;
    m_cameraY = 0;
    m_cameraZ = 3;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_9:
    m_cameraX = 0;
    m_cameraY = 0;
    m_cameraZ = 75;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_0:
    m_cameraX = 0;
    m_cameraY = 10;
    m_cameraZ = 15;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_LEFT:
    m_ship->SetRudder(LEFT);
    break;
  case SDLK_RIGHT:
    m_ship->SetRudder(RIGHT);
    break;
  case SDLK_UP:
    m_ship->RaiseSails();
    break;
  case SDLK_DOWN:
    m_ship->LowerSails();
    break;
  case SDLK_k:
    BattleFinished(sbDefeat);
    break;
  default:
    break;
  }
}

void Battle::Timer(const unsigned int ticks) {
  list< Bullet* >::iterator iter;
  list< Bullet* >::iterator iterEnd;
  Bullet *temp;
  Ship *tempShip;
  SoundEffect *hitSound;
  GLfloat distBetweenShips;
  float tempAngle;

  m_time += (float) ticks / 1000.0;
  if (m_time >= 24) {
    m_time -= 24;
  }
  //Log::s_log->Message("%f", time);

  if (m_playerTicksToReload > 0) {
    m_playerTicksToReload -= ticks;
  }

  if (m_enemyTicksToReload > 0) {
    m_enemyTicksToReload -= ticks;
  }

  m_enemyAI->Update(ticks, m_enemyTicksToReload <= 0);
  if (m_enemyAI->ShouldFire(&tempAngle)) {
    EnemyFire(tempAngle);
  }

  // Calculate the ships' speeds
  m_ship->Update(ticks);
  m_enemyShip->Update(ticks);
  //Log::s_log->Message("Speed: %f", ship->speed);

  temp = m_bullets.front();
  while (!m_bullets.empty() && temp->m_location[1] < -.01) {
    m_bullets.pop_front();
    delete temp;
    temp = m_bullets.front();
  }

  iter = m_bullets.begin();
  iterEnd = m_bullets.end();
  while(iter != iterEnd) {
    temp = *iter;

    if (temp->m_location[1] >= -.01) {
      temp->Update(ticks);

      if (temp->m_owner == m_ship) {
        tempShip = m_enemyShip;
        hitSound = m_enemyHitSound;
      } else {
        tempShip = m_ship;
        hitSound = m_playerHitSound;
      }

      if (temp->CheckCollision(tempShip)) {
        temp->m_location[1] = -10;
        --tempShip->m_damage;

        SoundSystem::s_soundSystem->Play(hitSound);

        Log::s_log->Message("Damage: %d (%f)", tempShip->m_damage, temp->m_location[1]);
      }
    }

    ++iter;
  }

  ResetLabelValues();

  if (!m_battleDone) {
    if (m_ship->m_damage <= 0) {
      m_ship->SinkShip();
      m_result = sbDefeat;
      m_battleDone = true;
    } else if (m_enemyShip->m_damage <= 0) {
      m_enemyShip->SinkShip();
      m_result = sbVictory;
      m_battleDone = true;
    }

    distBetweenShips = (m_ship->m_x - m_enemyShip->m_x)*(m_ship->m_x - m_enemyShip->m_x)+(m_ship->m_z - m_enemyShip->m_z)*(m_ship->m_z - m_enemyShip->m_z);
    if (distBetweenShips > 500) {
      BattleFinished(sbNone);
    }
  } else {
    if (m_ship->IsShipSunk() || m_enemyShip->IsShipSunk()) {
      BattleFinished(m_result);
    }
  }
}

void Battle::PlayerFire(float rotDiff) {
  // Change this to make sure reloaded
  if (m_playerTicksToReload <= 0) {
    float angle = m_ship->m_rot + rotDiff;
    m_bullets.push_back(new Bullet(m_ship, -1*angle));
    m_playerTicksToReload = RELOADTICKS;
    SoundSystem::s_soundSystem->Play(m_playerCannonSound);
  }
}

void Battle::EnemyFire(float rotDiff) {
  // Change this to make sure reloaded
  if (m_enemyTicksToReload <= 0) {
    float angle = m_enemyShip->m_rot + rotDiff;
    m_bullets.push_back(new Bullet(m_enemyShip, -1*angle));
    m_enemyTicksToReload = RELOADTICKS;
    SoundSystem::s_soundSystem->Play(m_enemyCannonSound);
  }
}

void Battle::PlayerHit() {
  Log::s_log->Message("Player hit");
}

void Battle::EnemyHit() {
  Log::s_log->Message("Enemy hit");
}

void Battle::BattleFinished(BattleResult result) {
  // Just to be safe
  if (m_leaveState != NULL) {
    delete m_leaveState;
  }

  Player::player->ship->m_damage = m_ship->m_damage;

  m_leaveState = new StateDone(m_enemyShip, result);
  PrepareStateSwitch(m_leaveState, m_previous);
}

void Battle::GenerateWaterList() {
  const int bottomSize = 10;
  int i, j;

  // Set up the display list
  m_waterList = glGenLists(1);
  glNewList(m_waterList, GL_COMPILE);

  // Actually draw the bottom and the water
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, m_bottomMaterial);
  m_bottomTexture->BindTexture();

  glNormal3f(0, 1, 0);
  for (j = 0; j <= bottomSize - 1; j++) {
    glBegin(GL_TRIANGLE_STRIP);
    for (i = 0; i <= bottomSize; i++) {
      glTexCoord2f((float) i / bottomSize * WATER_SIZE/WATER_TEXSCALE, (float) j / bottomSize * WATER_SIZE/WATER_TEXSCALE);
      glVertex3f((i*WATER_SIZE/bottomSize) - WATER_SIZE/2, CalcBottomDepth(i, j, bottomSize), j*WATER_SIZE/bottomSize - WATER_SIZE/2);
      glTexCoord2f((float) i / bottomSize * WATER_SIZE/WATER_TEXSCALE, (float) (j+1) / bottomSize * WATER_SIZE/WATER_TEXSCALE);
      glVertex3f(i*WATER_SIZE/bottomSize - WATER_SIZE/2, CalcBottomDepth(i, j+1, bottomSize), (j+1)*WATER_SIZE/bottomSize - WATER_SIZE/2);
    }
    glEnd();
  }

  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, m_waterMaterial);
  m_waterTexture->BindTexture();

  glEnable(GL_BLEND);

  glBegin(GL_QUADS);
  glNormal3f(0, 1, 0);
  glTexCoord2f(0, 0);
  glVertex3f(-WATER_SIZE/2, 0, -WATER_SIZE/2);
  glTexCoord2f(0, WATER_SIZE/WATER_TEXSCALE);
  glVertex3f(-WATER_SIZE/2, 0, WATER_SIZE/2);
  glTexCoord2f(WATER_SIZE/WATER_TEXSCALE, WATER_SIZE/WATER_TEXSCALE);
  glVertex3f(WATER_SIZE/2, 0, WATER_SIZE/2);
  glTexCoord2f(WATER_SIZE/WATER_TEXSCALE, 0);
  glVertex3f(WATER_SIZE/2, 0, -WATER_SIZE/2);
  glEnd();

  glDisable(GL_BLEND);

  // Done creating the list
  glEndList();
}

float Battle::CalcBottomDepth(float x, float z, float bottomSize) {
  const float bottom = -2.5;

  float dist;
  float mid = bottomSize / 2;

  // Note: This causes a funny corner triangle in the top-left and bottom-right, but it's not really an issue

  if (fabs(mid - x) < fabs(mid - z)) {
    // Need to deal with z
    dist = fabs(z - mid);
  } else {
    // Need to deal with x
    dist = fabs(x - mid);
  }

  return ((fabs(bottom)/mid) * dist * dist) / mid + bottom;
}

void Battle::InitHUD() {
  gcn::Widget *tempWidget;
  int columnSize;

  // Do this first so the table columns get sized correctly
  m_playerSailsLabel = new CCLabel("", s_mainFontName);
  m_playerDamageLabel = new CCLabel("", s_mainFontName);
  m_playerReloadLabel = new CCLabel("", s_mainFontName);
  m_enemySailsLabel = new CCLabel("", s_mainFontName);
  m_enemyDamageLabel = new CCLabel("", s_mainFontName);
  m_enemyReloadLabel = new CCLabel("", s_mainFontName);
  ResetLabelValues();

  m_playerTable = new CCTable(2, 4);
  m_playerTable->setColumnBuffer(8);
  m_playerTable->setRowBuffer(1);
  tempWidget = new CCLabel("Ship:", s_mainFontName);
  m_playerTable->add(0, 0, tempWidget);
  tempWidget = new CCLabel(m_ship->m_title, s_mainFontName);
  m_playerTable->add(1, 0, tempWidget);
  tempWidget = new CCLabel("Sails:", s_mainFontName);
  m_playerTable->add(0, 1, tempWidget);
  m_playerTable->add(1, 1, m_playerSailsLabel);
  tempWidget = new CCLabel("Damage:", s_mainFontName);
  m_playerTable->add(0, 2, tempWidget);
  m_playerTable->add(1, 2, m_playerDamageLabel);
  tempWidget = new CCLabel("Cannons:", s_mainFontName);
  m_playerTable->add(0, 3, tempWidget);
  m_playerTable->add(1, 3, m_playerReloadLabel);
  m_top->add(m_playerTable, 0, 0);

  m_enemyTable = new CCTable(2, 4);
  m_enemyTable->setColumnBuffer(8);
  m_enemyTable->setRowBuffer(1);
  tempWidget = new CCLabel("Ship:", s_mainFontName);
  m_enemyTable->add(0, 0, tempWidget);
  tempWidget = new CCLabel(m_enemyShip->m_title, s_mainFontName);
  m_enemyTable->add(1, 0, tempWidget);
  tempWidget = new CCLabel("Sails:", s_mainFontName);
  m_enemyTable->add(0, 1, tempWidget);
  m_enemyTable->add(1, 1, m_enemySailsLabel);
  tempWidget = new CCLabel("Damage:", s_mainFontName);
  m_enemyTable->add(0, 2, tempWidget);
  m_enemyTable->add(1, 2, m_enemyDamageLabel);
  tempWidget = new CCLabel("Cannons:", s_mainFontName);
  m_enemyTable->add(0, 3, tempWidget);
  m_enemyTable->add(1, 3, m_enemyReloadLabel);
  m_top->add(m_enemyTable, Config::s_config->GetWinWidth() - m_enemyTable->getWidth(), 0);

  // Just to make sure there's plenty of room...
  // The enemy always gets initialized with the cannons being reloaded
  columnSize = m_enemyTable->getColumnSize(1) + 2;
  m_playerTable->resizeColumn(1, columnSize);
  m_enemyTable->resizeColumn(1, columnSize);
}

void Battle::ResetLabelValues() {
  string reloadingStr = "Reloading...";
  string readyStr = "Ready!";

  m_playerSailsLabel->setCaption(m_ship->GetSailsStr());
  m_playerSailsLabel->adjustSize();
  m_playerDamageLabel->SetCaption(m_ship->m_damage);
  m_playerDamageLabel->adjustSize();
  if (m_playerTicksToReload > 0) {
    m_playerReloadLabel->setCaption(reloadingStr);
  } else {
    m_playerReloadLabel->setCaption(readyStr);
  }
  m_playerReloadLabel->adjustSize();

  m_enemySailsLabel->setCaption(m_enemyShip->GetSailsStr());
  m_enemySailsLabel->adjustSize();
  m_enemyDamageLabel->SetCaption(m_enemyShip->m_damage);
  m_enemyDamageLabel->adjustSize();
  if (m_enemyTicksToReload > 0) {
    m_enemyReloadLabel->setCaption(reloadingStr);
  } else {
    m_enemyReloadLabel->setCaption(readyStr);
  }
  m_enemyReloadLabel->adjustSize();
}

void Battle::SwitchTo(IGameState *oldState) {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  m_camera->SetProjection();

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);
}

void Battle::SwitchFrom() {
}
