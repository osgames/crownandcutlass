/* Crown and Cutlass
 * Bullet Header
 */

#if !defined ( _BULLET_H_ )
#define _BULLET_H_

class Ship;

class Bullet {
 public:
  Bullet(Ship *ship, float angleIn);
  ~Bullet();

  void Update(unsigned int ticks);

  bool CheckCollision(Ship *ship);

  void Draw();

  static void GenerateList();
  static void DeleteList();

  // Used to tell AI how fast a bullet is fired
  static float GetBulletSpeed();

  float m_location[3];
  float m_vector[3];

  Ship *m_owner;

 private:
  // Just used to draw the bullet (stored in degrees!)
  float m_angle;

  static GLuint s_bulletList;

  // Draws one cannon ball, used to generate list
  static void DrawBullet();
};

#endif
