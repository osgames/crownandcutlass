/* Crown and Cutlass
 * Naval Battle Done Code
 */

#include <string>
#include <sstream>
#include "../GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "SDL.h"
#include "../Menu/CCGui.h"
#include "../Menu/CCTable.h"
#include "../Menu/CCButton.h"
#include "../Menu/CCFont.h"
#include "../Menu/CCLabel.h"
#include "../Config.h"
#include "../Log.h"
#include "../Texture.h"
#include "../font.h"
#include "../IGameState.h"
#include "../ccmath.h"
#include "../Player.h"
#include "../Ship.h"
#include "../Economy.h"
#include "../Product.h"
#include "../Cargo.h"
#include "StateDone.h"

using namespace std;

const string StateDone::s_mainFontName = "smallFont.png";
const string StateDone::s_highlightFontName = "smallHighlight.png";
const string StateDone::s_disabledFontName = "smallDisabledFont.png";

StateDone::StateDone(Ship *enemy, BattleResult result): IGameState(NULL) {
  string backgroundName;

  m_enemyGold = (int) (randBellCurve() * 100.0);

  m_enemyCargo = enemy->GetCargo();
  m_enemyCargo->FillRandom();
  m_enemyCargo->Dump();

  m_playerCargo = Player::player->GetCargo();

  // Make sure this is large enough just so the player could leave everything if they wanted to
  m_enemyCargo->SetSize(m_enemyCargo->GetSize() + m_playerCargo->GetSize());

  m_exitCallback.SetCallback(this, &StateDone::Exit);
  m_buyCallback.SetCallback(this, &StateDone::Buy);
  m_sellCallback.SetCallback(this, &StateDone::Sell);
  m_buyAllCallback.SetCallback(this, &StateDone::TakeAll);
  m_sellAllCallback.SetCallback(this, &StateDone::LeaveAll);

  switch (result) {
    case sbVictory:
      backgroundName = "battlesuccess.png";
      break;
    case sbDefeat:
      backgroundName = "battledefeat.png";
      break;
    case sbNone:
      backgroundName = "battletie.png";
      break;
  }

  Log::s_log->Message("Creating background image");
  m_background = new Texture(backgroundName);

  /* Setting these to null keeps us from calling
   * delete on objects that don't exist. They don't
   * get created if we don't win the battle.
   */
  m_productTable = NULL;

  Log::s_log->Message("Setting up captions");
  switch (result) {
    case sbVictory:
      m_firstTitle = new CCLabel("Victory!", s_mainFontName);
      m_secondTitle = new CCLabel("You get the enemy's gold.", s_mainFontName);
      Log::s_log->Message("Setting up menu");
      SetupMenu();
      Player::player->gold += m_enemyGold;
      break;
    case sbDefeat:
      m_firstTitle = new CCLabel("You were defeated!", s_mainFontName);
      m_secondTitle = new CCLabel("The enemy sacks your ship but luckily you escape.", s_mainFontName);

      SackPlayer();
      break;
    case sbNone:
      m_firstTitle = new CCLabel("The battle draws to a close as the ships slip apart.", s_mainFontName);
      m_secondTitle = new CCLabel("", s_mainFontName);
      m_productTable = NULL;
      break;
  }

  Log::s_log->Message("Creating done button");
  m_doneButton = new CCButton("Done", "done_button", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_exitCallback);

  m_top->add(m_doneButton, 15, 150);

  m_top->add(m_firstTitle, 15, 50);
  m_top->add(m_secondTitle, 15, 80);

  Log::s_log->Message("Setting up menu");
}

StateDone::~StateDone() {
  // Don't delete either cargo, since the ships we got them from will do that
  if (m_productTable != NULL) {
    m_productTable->clear();
    delete m_productTable;
  }
  delete m_firstTitle;
  delete m_secondTitle;
  delete m_doneButton;

  delete m_background;

  Log::s_log->Message("State Done deleted");
}

void StateDone::SetupMenu() {
  stringstream tempStr;
  CCButton* tempButton;
  CCLabel* tempLabel;

  Log::s_log->Message("Creating new Table");
  m_productTable = new CCTable(5, Economy::s_economy->ProductCount() + 2);
  Log::s_log->Message("New Table Created");

  m_top->add(m_productTable, 300, 300);

  Log::s_log->Message("Created and set fonts");

  /*****************************************************
   * This section sets up the headers for the columns. *
   *****************************************************/
  tempLabel = new CCLabel("Products", s_mainFontName);
  m_productTable->add(0, 0, tempLabel);
  //widgetList.push_back(tempLabel);

  tempLabel = new CCLabel("Take", s_mainFontName);
  m_productTable->add(1, 0, tempLabel);
  m_productTable->resizeColumn(1, m_productTable->getColumnSize(0));

  tempLabel = new CCLabel("Leave", s_mainFontName);
  m_productTable->add(4, 0, tempLabel);
  m_productTable->resizeColumn(4, m_productTable->getColumnSize(0));
  /* End Headers */

  m_productTable->setColumnBuffer(10);

  Log::s_log->Message("Set table headers");
  /* Need to fill in the table with each product... */
  for (int i = 0; i < Economy::s_economy->ProductCount(); i++) {
    tempLabel = new CCLabel(Economy::s_economy->ProdList[i], s_mainFontName);
    m_productTable->add(0, i + 1, tempLabel);

    tempStr.str("");
    tempStr << m_playerCargo->GetQty(i);
    tempLabel = new CCLabel(tempStr.str(), s_mainFontName);
    m_productTable->add(1, i + 1, tempLabel);

    /* Each Take and Leave button gets the ID of
     * the product that it corresponds to. This is
     * represented in this loop by i. So if Cotton
     * is at Economy::s_economy->ProdList[0], then id
     * here would be zero. That way in our callbacks
     * it makes it real easy to figure which widgets
     * need to be changed.
     */
    tempButton = new CCButton("Take", "buy" + Economy::s_economy->ProdList[i], s_mainFontName, s_highlightFontName, s_disabledFontName, &m_buyCallback);
    tempButton->SetID(i);
    m_productTable->add(2, i + 1, tempButton);

    tempButton = new CCButton("Leave", "sell" + Economy::s_economy->ProdList[i], s_mainFontName, s_highlightFontName, s_disabledFontName, &m_sellCallback);
    tempButton->SetID(i);
    m_productTable->add(3, i + 1, tempButton);

    tempStr.str("");
    tempStr << m_enemyCargo->GetQty(i);
    tempLabel = new CCLabel(tempStr.str(), s_mainFontName);
    m_productTable->add(4, i + 1, tempLabel);
  }

  Log::s_log->Message("Setting up Take and Leave all buttons");
  tempButton = new CCButton("All", "sellAll", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_sellAllCallback);
  m_productTable->add(3, m_productTable->getNumRows() - 1, tempButton);
  tempButton = new CCButton("All", "BuyAll", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_buyAllCallback);
  m_productTable->add(2, m_productTable->getNumRows() - 1, tempButton);
}

void StateDone::Update(const unsigned int ticks) {
  CheckInput();
}

void StateDone::Display() {
  glLoadIdentity();
  DisplayBackground(m_background->GetTexture(), 1);
}

void StateDone::CheckInput() {
 SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    if (event.type == SDL_KEYDOWN)
    {
      /*
      if (event.key.keysym.sym == SDLK_ESCAPE)
      {
        done = false;
      }
      if (event.key.keysym.sym == SDLK_f)
      {
        if (event.key.keysym.mod & KMOD_CTRL)
        {
          // Works with X11 only
          //SDL_WM_ToggleFullScreen(screen);
        }
      }
      */
      if (event.key.keysym.sym == SDLK_z) {
        TakeScreenshot();
      }
    }

    /*
     * Now that we are done polling and using SDL events we pass
     * the leftovers to the SDLInput object to later be handled by
     * the Gui. (This example doesn't require us to do this 'cause a
     * label doesn't use input. But will do it anyway to show how to
     * set up an SDL application with Guichan.)
     */
    CCGui::s_CCGui->m_input->pushInput(event);
  }
}
void StateDone::Exit(int ID) {
  PrepareStateSwitch(m_previous, this);
}

void StateDone::Buy(int ID) {
  int productId = -1;
  stringstream tempStr;
  CCLabel* tempLabel;

  productId = ID;

  if ((productId > 0) && (productId >= Economy::s_economy->ProductCount())) {
    Log::s_log->Message("StateDone.sell: Unknown id (%d)", ID);
    return;
  }

  if ((m_enemyCargo->GetQty(productId) > 0) && (m_playerCargo->GetAvailQty() > 0)) {
    m_playerCargo->AddQty(productId, 1);
    m_enemyCargo->AddQty(productId, -1);
    tempStr.str("");
    tempStr << m_playerCargo->GetQty(productId);
    tempLabel = (CCLabel*)m_productTable->getWidget(1, productId + 1);
    tempLabel->setCaption(tempStr.str());
    tempLabel->adjustSize();
    tempStr.str("");
    tempStr << m_enemyCargo->GetQty(productId);
    tempLabel = (CCLabel*)m_productTable->getWidget(4, productId + 1);
    tempLabel->setCaption(tempStr.str());
    tempLabel->adjustSize();
  }

}

void StateDone::Sell(int ID) {
  int productId = -1;
  stringstream tempStr;
  CCLabel* tempLabel;

  productId = ID;

  if ((productId > 0) && (productId >= Economy::s_economy->ProductCount())) {
    Log::s_log->Message("StateDone.sell: Unknown id (%d)", ID);
    return;
  }

  if ((m_playerCargo->GetQty(productId) > 0) && (m_enemyCargo->GetAvailQty() > 0)) {
    m_playerCargo->AddQty(productId, -1);
    m_enemyCargo->AddQty(productId, 1);
    tempStr.str("");
    tempStr << m_playerCargo->GetQty(productId);
    tempLabel = (CCLabel*)m_productTable->getWidget(1, productId + 1);
    tempLabel->adjustSize();
    tempLabel->setCaption(tempStr.str());
    tempStr.str("");
    tempStr << m_enemyCargo->GetQty(productId);
    tempLabel = (CCLabel*)m_productTable->getWidget(4, productId + 1);
    tempLabel->setCaption(tempStr.str());
    tempLabel->adjustSize();
  }

}

void StateDone::TakeAll(int ID) {
  m_playerCargo->AddCargo(m_enemyCargo);

  SetMenuQtys();
}

void StateDone::LeaveAll(int ID) {
  m_enemyCargo->AddCargo(m_playerCargo);

  SetMenuQtys();
}

void StateDone::SackPlayer() {
  unsigned int i;

  Player::player->ship->m_damage = 1;
  Player::player->gold = (int) (Player::player->gold * randBellCurve());


  for (i = 0; i < (unsigned int) Economy::s_economy->ProductCount(); i++) {
    try {
      m_playerCargo->SetQty(i, (unsigned int) (m_playerCargo->GetQty(i) * randBellCurve()));
    } catch (const char *e) {
      Log::s_log->Message("Warning: Could not set qty in sackPlayer (%s)", e);
    }
  }
}

void StateDone::SetMenuQtys() {
  int i;
  stringstream tempStr;
  Label* tempLabel;

  Log::s_log->Message("setMenuQtys");

  for (i = 0; i < Economy::s_economy->ProductCount(); i++) {
    tempStr.str("");
    tempStr << m_playerCargo->GetQty(i);
    tempLabel = (Label*)m_productTable->getWidget(1, i + 1);
    tempLabel->setCaption(tempStr.str());
    tempLabel->adjustSize();
    Log::s_log->Message("Player %d: %s", i, tempStr.str().c_str());
    tempStr.str("");
    tempStr << m_enemyCargo->GetQty(i);
    Log::s_log->Message("Enemy %d: %s", i, tempStr.str().c_str());
    tempLabel = (Label*)m_productTable->getWidget(4, i + 1);
    tempLabel->setCaption(tempStr.str());
    tempLabel->adjustSize();
  }

}

void StateDone::SwitchTo(IGameState *oldState) {
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
}

void StateDone::SwitchFrom() {
}
