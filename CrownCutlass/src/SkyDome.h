/* Crown and Cutlass
 * Sky Dome Object Header
 */

#if !defined ( _SKYDOME_H_ )
#define _SKYDOMEI_H_

#include <string>
#include "GLee.h"

using namespace std;

class Texture;

class SkyDome {
public:
  SkyDome(float radius, int slices, int stacks, string textureName);

  ~SkyDome();

  // Draw the sky dome
  // time represents the time of day on a 24-clock (0 <= time < 24)
  void Draw(float time);

private:
  // Display list to draw the sky dome
  GLuint m_list;

  // Texture to use for sky color
  Texture *m_colorTexture;

  // Actually draw the geometry for the dome
  void DrawDome(float radius, int slices, int stacks);

  // Convert from polar coords
  float GetX(float angle, float radiusAngle, float domeRadius);
  float GetZ(float angle, float radiusAngle, float domeRadius);

  // Gets the height of the dome at that radius
  float GetHeight(float domeRadius, float pointRadiusAngle);
};

#endif
