/* Crown and Cutlass
 * ISubSystem Header
 */

#if !defined( _ISUBSYSTEM_H_ )

#define _ISUBSYSTEM_H_

class ISubSystem {
 public:
  virtual ~ISubSystem() { };
  virtual void Update(unsigned int ticks) = 0;
};

#endif
