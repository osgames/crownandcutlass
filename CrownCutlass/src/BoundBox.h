/* Crown and Cutlass
 * Quad-Tree Bounding Box Object Header
 * Note: For use with the terrain quadtree
 */

#if !defined( _BOUNDBOX_H_ )

#define _BOUNDBOX_H_

class Point;

class BoundBox {
 public:
  // Contructor
  BoundBox();

  // Pass in max/min x/y/z
  BoundBox(float minX, float maxX, float minY, float maxY, float minZ, float maxZ);

  // Destructor
  ~BoundBox();

  Point *m_b1, *m_b2, *m_b3, *m_b4;
  Point *m_t1, *m_t2, *m_t3, *m_t4;

  void SetHeight(float low, float high);

  // Good for debugging
  void Draw();
  void Dump();
};

#endif
