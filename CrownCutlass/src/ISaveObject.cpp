/* Crown and Cutlass
 * ISaveObject Code
 * Note: This is a base class for anything that will need to be saved in the save games
 */

#include <string>
#include "ISaveObject.h"

using namespace std;

ISaveObject::ISaveObject(string XMLName) {
  m_XMLName = XMLName;
}
