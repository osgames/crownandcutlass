/* Crown and Cutlass
 * Quad-Tree Node Object Header
 * Note: For use with the terrain quadtree
 */

#if !defined( _QUADNODE_H_ )

#define _QUADNODE_H_

// So I don't have to include the headers
class Terrain;
class BoundBox;
class Frustum;
class WaveEmitter;

class QuadNode {
 public:
  QuadNode(Terrain *terrain, BoundBox *box);
  ~QuadNode();

  // Check visibility and either draw children, or call list
  void Draw(Frustum *frustum);

  // Dump info
  void Dump();

  static int s_quadSize;

 private:
  // Child nodes
  QuadNode *m_child1;
  QuadNode *m_child2;
  QuadNode *m_child3;
  QuadNode *m_child4;

  // Coords of this node
  BoundBox *m_nodeBox;

  // Count of vertices in index buffer
  int m_vertexCount;

  // Range of indices in the index buffer
  unsigned int m_indexStartRange;
  unsigned int m_indexEndRange;

  // Actual index buffer used to draw vertex arrays
  unsigned int *m_indexArray;

  // Emitter for all waves in this box
  // Note: This should only be set if leaf node
  WaveEmitter *m_emitter;
};

#endif
