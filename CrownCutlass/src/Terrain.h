/* Crown and Cutlass
 * Terrain Object Header
 */

#if !defined( _TERRAIN_H_ )

#define _TERRAIN_H_

#define DETAIL_SCALE .15

#include <list>
#include <string>
#include "GLee.h"

// So I don't have to include the headers
class Ocean;
class QuadNode;
class BoundBox;
class Frustum;
class Point;
class Texture;

class Terrain {
  // Allow QuadNodes to be friends
  friend class QuadNode;
 public:
   Terrain(std::string file, GLfloat vScaleIn, int quadSizeIn);
  ~Terrain();

  void Update(unsigned int ticks);

  void Draw();

  // Check a line to see if it collided with the terrain around it
  bool CheckLineCollision(GLfloat xIn, GLfloat zIn, int size, double v[3], float mag);

  // Get the height of the terrain at a specific location
  GLfloat GetHeight(int x, int y);

  // Calc height at any point (doesn't need to be exactly on a vertex)
  GLfloat CalcHeight(GLfloat x, GLfloat z);

  // Return w or h
  int GetWidth();
  int GetHeight();

  // For use with the quadtree, generate the index array for this box.  min/maxHeight allow the return of min/max height of terrain in that box.
  // Note: It is the caller's responsibility to delete the array that is returned
  unsigned int *GenIndexArray(BoundBox *box, int *vertexCount, GLfloat *maxHeight, GLfloat *minHeight, unsigned int *minRange, unsigned int *maxRange);

  // Get min/max height of terrain in the box
  void GetMinMax(BoundBox *box, GLfloat *minHeight, GLfloat *maxHeight);

  void LocateShoreLine(BoundBox *box, std::list< Point* > *pointList);
  bool FindNearestShoreAngle(Point *p, float *angle);

 private:
  //GLfloat *terrain;  // Should be w*h
  //GLfloat *normals;  // Should be w*h*3

  // This is the array used to draw the terrain
  // Note: This array interleaves vertex (3), normal (3), texture coord 0 (2), and texture coord 1 (2) data,
  //   so it should be w*h*(3+3+2+2)
  GLfloat *m_terrainArray;

  // This is just a pointer to wherever the correct data for collisions, etc is located
  // Note: If VBO's are used, terrainArray gets deleted and this needs to be created as a separate array, if
  //   VA's are used, this can just point to terrainArray.
  GLfloat *m_vertexData;

  // This is used for the collision, etc code to index into the vertexData array correctly
  // Note: If VBO's are used, this will be 1.  Otherwise, it will be (3+3+2+2).
  int m_vertexStride;

  // This is used for the collision, etc code to index into the vertexData array correctly
  // Note: If VBO's are used, this will be 0.  With VA's, it will be 1.
  int m_vertexOffset;

  // Handle for the vbo
  GLuint m_buffer;

  // Ocean surfaced object
  Ocean *m_ocean;

  // Width (# of pixels) of heightmap
  int m_w;
  // Height (# of pixels) of heightmap
  int m_h;

  // For image
  Texture *m_texture;
  // For detail texture
  Texture *m_detailTex;

  // Material
  GLfloat m_material[4];

  // Root of quadtree
  QuadNode *m_root;

  // Frustum used for culling quadtree
  Frustum *m_frustum;

  bool NeedCollision(int x, int z);

  // Calculate the normals for the terrain and store them in terrainArray
  void CalcNormals();
  void CalcNormals2(std::string file);

// This function calls glVertexPointer, etc to initialize vertex arrays
  void SetGlArrayPointers();
};

#endif
