/* Crown and Cutlass
 * Sound Stream class
 *
 * This class streams music from disk so that
 * all the music doesn't need to be in memory.
 *
 * First writing is copied from DevMaster.net
 * http://www.devmaster.net/articles/openal-tutorials/lesson8.php
 */

#include <string>
#include <sstream>
#include <iostream>
using namespace std;

#include <AL/al.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisenc.h>
#include <vorbis/vorbisfile.h>
#include "Log.h"

#include "SoundStream.h"

void SoundStream::open(string path)
{
    int result;

    file = path;

    if(!(oggFile = fopen(path.c_str(), "rb")))
        throw string("Could not open ogg file ") + path;

    if((result = ov_open(oggFile, &oggStream, NULL, 0)) < 0)
    {
        fclose(oggFile);

        throw string("Could not open Ogg stream. ") + errorString(result);
    }

    vorbisInfo = ov_info(&oggStream, -1);
    vorbisComment = ov_comment(&oggStream, -1);

    if(vorbisInfo->channels == 1)
        format = AL_FORMAT_MONO16;
    else
        format = AL_FORMAT_STEREO16;

    alGenBuffers(2, buffers);
    check();
    alGenSources(1, &source);
    check();

    alSource3f(source, AL_POSITION,        0.0, 0.0, 0.0);
    alSource3f(source, AL_VELOCITY,        0.0, 0.0, 0.0);
    alSource3f(source, AL_DIRECTION,       0.0, 0.0, 0.0);
    alSourcef (source, AL_ROLLOFF_FACTOR,  0.0          );
    alSourcei (source, AL_SOURCE_RELATIVE, AL_TRUE      );
}

void SoundStream::release()
{
    alSourceStop(source);
    empty();
    alDeleteSources(1, &source);
    check();
    alDeleteBuffers(2, buffers);
    check();

    ov_clear(&oggStream);
}

void SoundStream::display()
{
    cout
        << "version         " << vorbisInfo->version         << "\n"
        << "channels        " << vorbisInfo->channels        << "\n"
        << "rate (hz)       " << vorbisInfo->rate            << "\n"
        << "bitrate upper   " << vorbisInfo->bitrate_upper   << "\n"
        << "bitrate nominal " << vorbisInfo->bitrate_nominal << "\n"
        << "bitrate lower   " << vorbisInfo->bitrate_lower   << "\n"
        << "bitrate window  " << vorbisInfo->bitrate_window  << "\n"
        << "\n"
        << "vendor " << vorbisComment->vendor << "\n";

    for(int i = 0; i < vorbisComment->comments; i++)
        cout << "   " << vorbisComment->user_comments[i] << "\n";

    cout << endl;
}

bool SoundStream::playback()
{
    if(playing())
        return true;

    if(!stream(buffers[0]))
        return false;

    if(!stream(buffers[1]))
        return false;

    alSourceQueueBuffers(source, 2, buffers);
    alSourcePlay(source);

    return true;
}

bool SoundStream::playing()
{
    ALenum state;

    alGetSourcei(source, AL_SOURCE_STATE, &state);

    return (state == AL_PLAYING);
}

bool SoundStream::update()
{
    int processed;
    bool active = true;

    alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);

    while(processed--)
    {
        ALuint buffer;

        alSourceUnqueueBuffers(source, 1, &buffer);
        check();

        active = stream(buffer);

        alSourceQueueBuffers(source, 1, &buffer);
        check();
    }

    return active;
}

bool SoundStream::stream(ALuint buffer)
{
    char data[BUFFER_SIZE];
    int  size = 0;
    int  section;
    int  result;

    while(size < BUFFER_SIZE)
    {
        result = ov_read(&oggStream, data + size, BUFFER_SIZE - size, 0, 2, 1, & section);

        if(result > 0)
            size += result;
        else
            if(result < 0)
                throw "SomeError";
            else
                break;
    }

    if(size == 0)
        return false;

    alBufferData(buffer, format, data, size, vorbisInfo->rate);
    check();

    return true;
}

void SoundStream::empty()
{
    int queued;

    alGetSourcei(source, AL_BUFFERS_QUEUED, &queued);

    while(queued--)
    {
        ALuint buffer;

        alSourceUnqueueBuffers(source, 1, &buffer);
        check();
    }
}

void SoundStream::check()
{
  ostringstream stream;
  int error = alGetError();

  if(error != AL_NO_ERROR) {
    stream.str("");
    stream << hex << error;

    throw string("OpenAL error: 0x" + stream.str());
  }
}

string SoundStream::errorString(int code)
{
  switch(code) {
    case OV_EREAD:
      return string("Read from media.");
    case OV_ENOTVORBIS:
      return string("Not Vorbis data.");
    case OV_EVERSION:
      return string("Vorbis version mismatch.");
    case OV_EBADHEADER:
      return string("Invalid Vorbis header.");
    case OV_EFAULT:
      return string("Internal logic fault (bug or heap/stack corruption.");
    default:
      return string("Unknown Ogg error.");
  }
}

void SoundStream::reload() {
  release();
  open(file);

  playback();

}
