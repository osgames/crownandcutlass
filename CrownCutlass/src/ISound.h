/* Crown and Cutlass
 * ISound Base Object Header
 */

#if !defined( _ISOUND_H_ )

#define _ISOUND_H_

class OpenALSource;

class ISound {
  friend class SoundSystem;

 public:
  ISound();
  virtual ~ISound() = 0;

  void SetLocked(bool lock);

 protected:
  OpenALSource *m_source;

  bool m_locked;

  void Prepare();

  virtual void Play() = 0;
  virtual void Pause() = 0;
  virtual void Stop() = 0;
  virtual void Update(unsigned int ticks) = 0;

  OpenALSource* GetSource();
  void RevokeSource();

  // Called when a source is aquired
  virtual void InitializeSource() = 0;
};

#endif
