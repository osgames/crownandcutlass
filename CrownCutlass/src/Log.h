/* Crown and Cutlass
 * Log Object Header
 */

#if !defined ( _LOG_H_ )
#define _LOG_H_

#include <fstream>
#include <string>

class Log {
 public:
  Log(bool logToFile);
  ~Log();

  // Log an error message
  void Message(std::string msg);
  void Message(const char *msg, ...);

  // Log an error message only if set to debug in config
  void DebugMessage(std::string msg);
  void DebugMessage(const char *msg, ...);

  // Enable or disable debug logging
  void SetDebugMode(bool debugMode);

  static bool OpenLog(bool logToFile);
  static void CloseLog();

  static Log *s_log;

 private:
  bool m_debugMode;

  std::ofstream m_logFile;
};

#endif
