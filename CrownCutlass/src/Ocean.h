/* Crown and Cutlass
 * Ocean Object Header
 */

#if !defined( _OCEAN_H_ )

#define _OCEAN_H_

#include <vector>

using namespace std;

class WaveEmitter;
class Texture;

class Ocean {
public:
  // Constructor
  Ocean(int sizeX, int sizeZ);

  // Destructor
  ~Ocean();

  // Draw the actual water
  void Draw();

  // Update the water's position
  void Update(unsigned int ticks);

  // Add a WaveEmitter to the master list
  void AddEmitter(WaveEmitter *emitter);

 private:
  // Size of water patch
  int m_width;
  int m_height;

  // Texture handle
  Texture *m_texture;

  // Texture handle for waves
  Texture *m_waveTexture;

  // Material
  GLfloat m_material[4];

  // For sin/cos to move texture
  GLfloat m_count;

  // For display list
  GLuint m_list;

  // Pointer to all of the wave emitters
  // Note: This makes update easier
  vector< WaveEmitter* > m_waveEmitters;
};

#endif
