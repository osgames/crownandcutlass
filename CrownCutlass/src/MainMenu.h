/* Crown and Cutlass
 * Main Menu Class Header
 */

#if !defined( _MAINMENU_H_ )

#define _MAINMENU_H_

#include <guichan.hpp>
#include "SDL.h"
#include "AL/al.h"
#include "IGameState.h"
#include "Callback.h"
#include "SoundStream.h"

// So I don't have to include the headers
class StateSailing;
class Texture;
class CCButton;
class CCTable;
class CCTextField;

class MainMenu: public IGameState {
 public:
  // Constructor
  MainMenu(bool *done);

  // Destructor
  ~MainMenu();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Override the exit function
  void DoExit();

  // Callback Functions
  void ContinueGame(int i);
  void NewGame(int i);
  void SaveGame(int i);
  void LoadGame(int i);
  void Exit(int i);
  void Accept(int i);
  void Cancel(int i);

  void SetMainGame(StateSailing *sailingState);

 private:
  // Set to false to exit
  bool *m_done;

  // Check sdl input
  void CheckInput();

  void ResetNewGameMenu();

  void DoMouseOut(CCButton* widget);

  // Main sailing game state
  StateSailing *m_mainGame;

  // Callback functions
  TCallback<MainMenu> m_contCallback;
  TCallback<MainMenu> m_newCallback;
  TCallback<MainMenu> m_saveCallback;
  TCallback<MainMenu> m_loadCallback;
  TCallback<MainMenu> m_exitCallback;

  // New game callback functions
  TCallback<MainMenu> m_acceptCallback;
  TCallback<MainMenu> m_cancelCallback;

  // Background image texture
  Texture *m_background;

  // OpenAL stuff
  SoundStream m_music;

  // Guichan widgets
  // For main menu
  gcn::Container *m_mainMenu;
  CCTable* m_mainMenuTable;
  CCButton* m_contButton;
  CCButton* m_exitButton;
  CCButton* m_loadButton;
  CCButton* m_newButton;
  CCButton* m_saveButton;

  // For new game menu
  gcn::Container *m_newGameMenu;
  CCTable* m_newGameTable;
  CCButton* m_acceptButton;
  CCButton* m_cancelButton;
  CCTextField* m_nameText;
  CCTextField* m_shipText;

  // Font names
  static const std::string s_mainFontName;
  static const std::string s_highlightFontName;
  static const std::string s_disabledFontName;
  static const std::string s_smallFontName;
};

#endif
