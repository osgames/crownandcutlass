/* Crown and Cutlass
 * Player Data Object Header
 */

#if !defined ( _PLAYER_H_ )
#define _PLAYER_H_

#include <vector>
#include <boost/shared_ptr.hpp>
#include "ISaveObject.h"

class Ship;
class Cargo;

using namespace std;
using boost::shared_ptr;

class Player: public ISaveObject {
 public:
  Player(const string playerNameIn, const string shipNameIn);
  ~Player();

  void Load(TiXmlElement *parent);
  void Save(TiXmlElement *parent);
  void Reset();

  void Dump();

  static Player *player;
  static void CreateNewPlayer(const string playerName, const string shipName);
  static void DeletePlayer();

  // This just returns the ship's cargo, so you can avoid going through the ship
  shared_ptr<Cargo> GetCargo();

  string GetPlayerName();

  // We'll need to work this out once we get fleets
  Ship *ship;

  int gold;

 private:
  string playerName;
};

#endif
