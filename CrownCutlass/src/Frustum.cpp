/* Crown and Cutlass
 * Frustum Code
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include "GLee.h"
#include "Point.h"
#include "BoundBox.h"
#include "Frustum.h"

void Frustum::GetFrustum() {
  float proj[16];
  float modl[16];
  float clip[16];
  //float t;

  // Get the current PROJECTION matrix
  glGetFloatv(GL_PROJECTION_MATRIX, proj);

  // Get the current MODELVIEW matrix
  glGetFloatv(GL_MODELVIEW_MATRIX, modl);

  // Combine the two matrices (multiply projection by modelview)
  clip[0] = modl[0]*proj[0] + modl[1]*proj[4] + modl[2]*proj[8] + modl[3]*proj[12];
  clip[1] = modl[0]*proj[1] + modl[1]*proj[5] + modl[2]*proj[9] + modl[3]*proj[13];
  clip[2] = modl[0]*proj[2] + modl[1]*proj[6] + modl[2]*proj[10] + modl[3]*proj[14];
  clip[3] = modl[0]*proj[3] + modl[1]*proj[7] + modl[2]*proj[11] + modl[3]*proj[15];

  clip[4] = modl[4]*proj[0] + modl[5]*proj[4] + modl[6]*proj[8] + modl[7]*proj[12];
  clip[5] = modl[4]*proj[1] + modl[5]*proj[5] + modl[6]*proj[9] + modl[7]*proj[13];
  clip[6] = modl[4]*proj[2] + modl[5]*proj[6] + modl[6]*proj[10] + modl[7]*proj[14];
  clip[7] = modl[4]*proj[3] + modl[5]*proj[7] + modl[6]*proj[11] + modl[7]*proj[15];

  clip[8] = modl[8]*proj[0] + modl[9]*proj[4] + modl[10]*proj[8] + modl[11]*proj[12];
  clip[9] = modl[8]*proj[1] + modl[9]*proj[5] + modl[10]*proj[9] + modl[11]*proj[13];
  clip[10] = modl[8]*proj[2] + modl[9]*proj[6] + modl[10]*proj[10] + modl[11]*proj[14];
  clip[11] = modl[8]*proj[3] + modl[9]*proj[7] + modl[10]*proj[11] + modl[11]*proj[15];

  clip[12] = modl[12]*proj[0] + modl[13]*proj[4] + modl[14]*proj[8] + modl[15]*proj[12];
  clip[13] = modl[12]*proj[1] + modl[13]*proj[5] + modl[14]*proj[9] + modl[15]*proj[13];
  clip[14] = modl[12]*proj[2] + modl[13]*proj[6] + modl[14]*proj[10] + modl[15]*proj[14];
  clip[15] = modl[12]*proj[3] + modl[13]*proj[7] + modl[14]*proj[11] + modl[15]*proj[15];

  // Extract the numbers for the RIGHT plane
  m_frustum[0][0] = clip[3] - clip[0];
  m_frustum[0][1] = clip[7] - clip[4];
  m_frustum[0][2] = clip[11] - clip[8];
  m_frustum[0][3] = clip[15] - clip[12];

  // Normalize the result (not needed for boxes)
  /*
    t = sqrt(frustum[0][0]*frustum[0][0] + frustum[0][1]*frustum[0][1] + frustum[0][2]*frustum[0][2]);
    frustum[0][0] /= t;
    frustum[0][1] /= t;
    frustum[0][2] /= t;
    frustum[0][3] /= t;
  */

  // Extract the numbers for the LEFT plane
  m_frustum[1][0] = clip[3] + clip[0];
  m_frustum[1][1] = clip[7] + clip[4];
  m_frustum[1][2] = clip[11] + clip[ 8];
  m_frustum[1][3] = clip[15] + clip[12];

  // Normalize the result (not needed for boxes)

  // Extract the BOTTOM plane
  m_frustum[2][0] = clip[3] + clip[1];
  m_frustum[2][1] = clip[7] + clip[5];
  m_frustum[2][2] = clip[11] + clip[9];
  m_frustum[2][3] = clip[15] + clip[13];

  // Normalize the result (not needed for boxes)

  // Extract the TOP plane
  m_frustum[3][0] = clip[3] - clip[1];
  m_frustum[3][1] = clip[7] - clip[5];
  m_frustum[3][2] = clip[11] - clip[ 9];
  m_frustum[3][3] = clip[15] - clip[13];

  // Normalize the result (not needed for boxes)

  // Extract the FAR plane
  m_frustum[4][0] = clip[3] - clip[2];
  m_frustum[4][1] = clip[7] - clip[6];
  m_frustum[4][2] = clip[11] - clip[10];
  m_frustum[4][3] = clip[15] - clip[14];

  // Normalize the result (not needed for boxes)

  // Extract the NEAR plane
  m_frustum[5][0] = clip[3] + clip[2];
  m_frustum[5][1] = clip[7] + clip[6];
  m_frustum[5][2] = clip[11] + clip[10];
  m_frustum[5][3] = clip[15] + clip[14];

  // Normalize the result (not needed for boxes)
}

bool Frustum::CheckBox(BoundBox *box) {
  // Check each frustum plane
  for (int p=0; p<6; p++) {
    // Check each point of the box
    // Bottom points
    if (m_frustum[p][0]*box->m_b1->m_x + m_frustum[p][1]*box->m_b1->m_y + m_frustum[p][2]*box->m_b1->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_b2->m_x + m_frustum[p][1]*box->m_b2->m_y + m_frustum[p][2]*box->m_b2->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_b3->m_x + m_frustum[p][1]*box->m_b3->m_y + m_frustum[p][2]*box->m_b3->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_b4->m_x + m_frustum[p][1]*box->m_b4->m_y + m_frustum[p][2]*box->m_b4->m_z + m_frustum[p][3] > 0)
      continue;

    // Top points
    if (m_frustum[p][0]*box->m_t1->m_x + m_frustum[p][1]*box->m_t1->m_y + m_frustum[p][2]*box->m_t1->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_t2->m_x + m_frustum[p][1]*box->m_t2->m_y + m_frustum[p][2]*box->m_t2->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_t3->m_x + m_frustum[p][1]*box->m_t3->m_y + m_frustum[p][2]*box->m_t3->m_z + m_frustum[p][3] > 0)
      continue;
    if (m_frustum[p][0]*box->m_t4->m_x + m_frustum[p][1]*box->m_t4->m_y + m_frustum[p][2]*box->m_t4->m_z + m_frustum[p][3] > 0)
      continue;

    // It's not in front of any plane, return false
    return false;
  }

  // Got through all checks, it passed
  return true;
}
