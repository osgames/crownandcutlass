/* Crown and Cutlass
 * Building List Object Header
 */

#if !defined( _BUILDINGLIST_H_ )

#define _BUILDINGLIST_H_

#include <vector>

using namespace std;

class Model;
class Point;
class Building;

// A point in 3d space
class BuildingList {
 public:
  // Constructor
  BuildingList();

  // Destructor
  // Note: This will delete the points that have been added, but not the models!
  ~BuildingList();

  // Add a new building to the list
  // Note: Since you probably want to give multiple buildings the same model, but each point should be unique, this point will be deleted in ~BuildingList but the model will not be
  void Add(Model *modelIn, Point *pointIn, float rotationIn);

  // Adds the cities location to the point's so it only has to happen once
  void AddCityLocation(float x, float y, float z);

  // Dump out building info
  void Dump();

  // Draw all of the models in the list
  void Draw();

 private:
  vector< Building* > m_list;
};

#endif
