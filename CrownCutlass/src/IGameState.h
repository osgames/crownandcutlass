/* Crown and Cutlass
 * IGameState Header
 */

#if !defined( _IGAMESTATE_H_ )

#define _IGAMESTATE_H_

#include <memory>
#include <string>
#undef DELETE
#include <guichan.hpp>

class IGameState {
 public:
  // Constructor
  IGameState(IGameState *prev);

  // Need a virtual destructor to make sure the proper destructor gets called
  virtual ~IGameState();

  // Initialize the mainMenu pointer
  // Note: This only needs to happen once per execution
  static void SetMainMenu(IGameState *menu);

  // Initialize the currentPtr
  // Note: This only needs to happen once per execution
  static void SetCurrentPtr(IGameState **current);

  // Main state loop
  void Loop(const unsigned int ticks);

  virtual void Update(const unsigned int ticks) = 0;
  virtual void Display() = 0;

  // Call this to exit
  virtual void DoExit();

  virtual void CheckInput() = 0;

  void PrepareTopContainer();

 protected:
  IGameState *m_previous;

  // All states share these pointers, so make them static
  static IGameState *s_mainMenu;
  static IGameState **s_currentPtr;

  // For screenshots
  static unsigned char s_nextScreen;

  // Save a screenshot to disk
  void TakeScreenshot();

  // Display a splash screen
  void DisplaySplashScreen(const std::string imageName, const std::string text, const std::string fontName, float shade);

  void DisplayBackground(GLuint imageTexture, float shade);

  // Set m_next to be switched to, sets next's previous pointer to prev
  void PrepareStateSwitch(IGameState *next, IGameState *prev);

 public:
  std::auto_ptr<gcn::Container> m_top;

 private:
  IGameState *m_next;

  // Actually perform switch to m_next
  void SwitchStates();

  // Stuff to do when switching to this state from oldState
  virtual void SwitchTo(IGameState *oldState) = 0;

  // Stuff to do when switching from this state
  virtual void SwitchFrom() = 0;
};

#endif
