/* Crown and Cutlass
 * City Economy Class
 * 
 * This class is a container for each of the city to keep track of the state
 * of its own economy. It also allows the Economy class to work on it.
 * Each instance is kept in the Economy class and the cities must access it through
 * the Economy interface.
 */

#if !defined( _CITYECONOMY_H_ )

#define _CITYECONOMY_H_

#include <vector>
#include <string>

class Product;

using namespace std;

class CityEconomy {
 public:
  //Constructor
  CityEconomy(const char* CityName, vector<Product*> ProductList, int size);
  
  // Destructor
  ~CityEconomy();
  
  // Methods
  string GetCityName();
  
  Product* GetProduct(char* ProductName);
  float GetPrice(char* ProductName);
  float GetPrice(string ProductName);
  
  void Print();
  
  vector<Product*> GetProductList();

	void UpdateEconomy(int steps);
	
	// City Size stuff
	int GetSize();
	/* These functions should have an immediate effect on the product's
	 * Production and Consumption values.
	 */
	void IncreaseSize();
	void DecreaseSize();
  
 private:
  
  vector<Product*> m_Products;
  string m_Name;
  int m_Size;
  int m_UnallocatedProductionPoints;
  
  void AllocateProductionPoints();
};

#endif
