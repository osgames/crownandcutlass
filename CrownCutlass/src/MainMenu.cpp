/* Crown and Cutlass
 * Menu Class Code
 */

#include <cstdlib>
#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "SDL.h"
#include "Menu/CCGui.h"
#include "Menu/CCButton.h"
#include "Menu/CCTable.h"
#include "Menu/CCTextField.h"
#include "Menu/CCLabel.h"
#include "Config.h"
#include "Texture.h"
#include "Log.h"
#include "font.h"
#include "IGameState.h"
#include "StateSailing.h"
#include "StateNewGame.h"
#include "Callback.h"
#include "SoundStream.h"
#include "tinyxml.h"
#include "MainMenu.h"

using namespace std;

const string k_defaultName = "John Smith";
const string k_defaultShip = "Jolly Smith";

const string MainMenu::s_mainFontName = "papyrus.png";
const string MainMenu::s_highlightFontName = "highlightPapyrus.png";
const string MainMenu::s_disabledFontName = "disabledPapyrus.png";
const string MainMenu::s_smallFontName = "smallFont.png";

MainMenu::MainMenu(bool *done): IGameState(NULL) {
  CCLabel *tempLabel;

  Log::s_log->Message("MainMenu");
  DisplaySplashScreen("title.png", "", s_mainFontName, 1);

  // Set the static GameState mainMenu pointer to this
  IGameState::SetMainMenu(this);

  // Bool value to set to quit
  m_done = done;

  // Set the pointer to the main sailing state to null
  m_mainGame = NULL;

  m_background = new Texture("background.png");

  m_contCallback.SetCallback(this, &MainMenu::ContinueGame);
  m_newCallback.SetCallback(this, &MainMenu::NewGame);
  m_saveCallback.SetCallback(this, &MainMenu::SaveGame);
  m_loadCallback.SetCallback(this, &MainMenu::LoadGame);
  m_exitCallback.SetCallback(this, &MainMenu::Exit);
  m_acceptCallback.SetCallback(this, &MainMenu::Accept);
  m_cancelCallback.SetCallback(this, &MainMenu::Cancel);

  Log::s_log->Message("\tMenu: Callbacks Set.");

  // Set up the main menu
  m_mainMenu = new gcn::Container();
  m_mainMenu->setDimension(gcn::Rectangle(0, 0,
                      Config::s_config->GetWinWidth(),
                      Config::s_config->GetWinHeight()));
  m_mainMenu->setOpaque(false);

  m_mainMenuTable = new CCTable(1, 5);

  m_contButton = new CCButton("Continue", "contButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_contCallback);
  m_contButton->setEnabled(false);
  m_contButton->setVisible(true);
  m_exitButton = new CCButton("Exit", "exitButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_exitCallback);
  m_exitButton->setVisible(true);
  m_newButton = new CCButton("New", "newButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_newCallback);
  m_newButton->setVisible(true);
  m_loadButton = new CCButton("Load", "loadButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_loadCallback);
  m_loadButton->setVisible(true);
  m_saveButton = new CCButton("Save", "saveButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_saveCallback);
  m_saveButton->setEnabled(false);
  m_saveButton->setVisible(true);

  m_mainMenuTable->add(0, 0, m_contButton);
  m_mainMenuTable->add(0, 1, m_newButton);
  m_mainMenuTable->add(0, 4, m_exitButton);
  m_mainMenuTable->add(0, 3, m_loadButton);
  m_mainMenuTable->add(0, 2, m_saveButton);

  m_mainMenuTable->setVisible(true);
  m_mainMenuTable->setOpaque(false);
  m_mainMenu->add(m_mainMenuTable, 10, 100);

  // Set up the new game menu
  m_newGameMenu = new gcn::Container();
  m_newGameMenu->setDimension(gcn::Rectangle(0, 0,
                           Config::s_config->GetWinWidth(),
                           Config::s_config->GetWinHeight()));
  m_newGameMenu->setOpaque(false);

  m_newGameTable = new CCTable(2, 3);
  m_newGameTable->setRowBuffer(10);
  m_newGameTable->setColumnBuffer(10);

  m_acceptButton = new CCButton("Accept", "acceptButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_acceptCallback);
  m_acceptButton->setVisible(true);
  m_cancelButton = new CCButton("Cancel", "cancelButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_cancelCallback);
  m_cancelButton->setVisible(true);

  tempLabel = new CCLabel("Your Name:", s_smallFontName);
  m_newGameTable->add(0, 0, tempLabel);
  m_nameText = new CCTextField(k_defaultName, s_smallFontName, 150);
  m_newGameTable->add(1, 0, m_nameText);

  tempLabel = new CCLabel("Ship Name:", s_smallFontName);
  m_newGameTable->add(0, 1, tempLabel);
  m_shipText = new CCTextField(k_defaultShip, s_smallFontName, 150);
  m_newGameTable->add(1, 1, m_shipText);
  m_newGameTable->add(0, 2, m_acceptButton);
  m_newGameTable->add(1, 2, m_cancelButton);

  m_newGameTable->setVisible(true);
  m_newGameTable->setOpaque(false);

  m_newGameMenu->add(m_newGameTable, 10, 100);

  Log::s_log->Message("\tGraphics and font initialized.");

  m_top->add(m_mainMenu, 0, 0);
  m_top->add(m_newGameMenu, 0, 0);
  m_newGameMenu->setVisible(false);

  m_mainMenu->requestMoveToTop();

  Log::s_log->Message("\tGuichan finished.");

  // Initialize the sound buffer and source
  try {
    m_music.open("./data/Music/title.ogg");
    m_music.display();

    if (!m_music.playback()) {
      Log::s_log->Message("Music Playback didn't work.");
    }
  } catch (string error) {
    Log::s_log->Message(error.c_str());
  }

  Log::s_log->Message("\tMusic loaded");

  Log::s_log->Message("MainMenu initialized");
}

MainMenu::~MainMenu() {
  if (m_previous == m_mainGame) m_previous = NULL;

  delete m_mainGame;

  // Delete Guichan stuff
  m_top->clear();
  m_mainMenu->clear();
  m_mainMenuTable->clear();
  m_newGameMenu->clear();
  m_newGameTable->clear();
  delete m_mainMenu;
  delete m_mainMenuTable;
  delete m_newGameMenu;
  delete m_newGameTable;

  // Delete buffer and source
  try {
    m_music.release();
  } catch (...) {
    Log::s_log->Message("Error releasing MainMenu music");
  }

  delete m_background;

  // Set the static GameState mainMenu pointer back to NULL
  IGameState::SetMainMenu(NULL);

  Log::s_log->Message("MainMenu deleted");
}

void MainMenu::Update(const unsigned int ticks) {
  if (!m_music.update())
    Log::s_log->Message("Audio Playback didn't work");

  CheckInput();
}

void MainMenu::Display() {
  glLoadIdentity();
  DisplayBackground(m_background->GetTexture(), 1);
}

void MainMenu::CheckInput()
{
  SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    if (event.type == SDL_KEYDOWN)
    {
      /*
      if (event.key.keysym.sym == SDLK_ESCAPE)
      {
        done = false;
      }
      if (event.key.keysym.sym == SDLK_f)
      {
        if (event.key.keysym.mod & KMOD_CTRL)
        {
          // Works with X11 only
          //SDL_WM_ToggleFullScreen(screen);
        }
      }
      */
      if (event.key.keysym.sym == SDLK_z) {
        TakeScreenshot();
      }
    }
    else if(event.type == SDL_QUIT)
    {
      *m_done = true;
    }

    /*
     * Now that we are done polling and using SDL events we pass
     * the leftovers to the SDLInput object to later be handled by
     * the Gui. (This example doesn't require us to do this 'cause a
     * label doesn't use input. But will do it anyway to show how to
     * set up an SDL application with Guichan.)
     */
    CCGui::s_CCGui->m_input->pushInput(event);
  }
}

void MainMenu::DoExit() {
  *m_done = true;
}

void MainMenu::SwitchTo(IGameState *oldState) {
  if (m_mainGame != NULL) {
    m_contButton->setEnabled(true);
    m_saveButton->setEnabled(true);
  } else {
    m_contButton->setEnabled(false);
    m_saveButton->setEnabled(false);
  }
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
  m_music.reload();
}

void MainMenu::SwitchFrom() {
}

void MainMenu::ContinueGame(int i) {
  if (m_previous != NULL) {
    DoMouseOut(m_contButton);
    PrepareStateSwitch(m_previous, NULL);
  }
}

void MainMenu::NewGame(int i) {
  ResetNewGameMenu();
  DoMouseOut(m_newButton);
  m_newGameMenu->setVisible(true);
  m_mainMenu->setVisible(false);
  m_newGameMenu->requestMoveToTop();
}

void MainMenu::SaveGame(int i) {
  TiXmlDocument doc;
  TiXmlElement rootNode("CCSaveGame");

  // Shouldn't try to save if the game hasn't even started
  if (m_mainGame != NULL) {
    m_mainGame->Save(&rootNode);
    doc.InsertEndChild(rootNode);
    // This will need to change once we can do multiple save games
    doc.SaveFile("CCSave.xml");
    PrepareStateSwitch(m_mainGame, NULL);
  }
}

void MainMenu::LoadGame(int i) {
  // This will need to change once we allow multiple save games
  TiXmlDocument doc("CCSave.xml");

  doc.LoadFile();
  if (doc.Error()) {
    // Should throw an exception
    Log::s_log->Message("Warning: Could not load game, %s (line %d)", doc.ErrorDesc(), doc.ErrorRow());
    return;
  }

  if (m_mainGame == NULL) {
    // Game hasn't been started yet, need to create it
    // The player/ship names don't matter since we are loading those values
    m_mainGame = new StateSailing("Default", "Default");
  }
  // Now we can load the game
  m_mainGame->Load(doc.FirstChildElement("CCSaveGame"));

  m_mainGame->Dump();

  // Switch to game state
  PrepareStateSwitch(m_mainGame, NULL);
}

void MainMenu::Exit(int ID) {
  DoExit();
}

void MainMenu::Accept(int i) {
  if (m_mainGame == NULL) {
    // New game
    m_mainGame = new StateSailing(m_nameText->getText(), m_shipText->getText());
  } else {
    // Just reset existing game
    m_mainGame->NewGame(m_nameText->getText(), m_shipText->getText());
  }

  DoMouseOut(m_acceptButton);
  m_mainMenu->setVisible(true);
  m_newGameMenu->setVisible(false);
  m_mainMenu->requestMoveToTop();

  PrepareStateSwitch(m_mainGame, NULL);
}

void MainMenu::Cancel(int i) {
  DoMouseOut(m_cancelButton);
  m_mainMenu->setVisible(true);
  m_newGameMenu->setVisible(false);
  m_mainMenu->requestMoveToTop();
}

void MainMenu::SetMainGame(StateSailing *sailingState) {
  m_mainGame = sailingState;
}

void MainMenu::ResetNewGameMenu() {
  m_nameText->setText(k_defaultName);
  m_shipText->setText(k_defaultShip);
}

void MainMenu::DoMouseOut(CCButton* widget) {
  gcn::MouseEvent me(
    widget,
    false,
    false,
    false,
    false,
    static_cast< unsigned int >(gcn::MouseEvent::MOVED),
    static_cast< unsigned int >(gcn::MouseEvent::EMPTY),
    -1,
    -1,
    0);
  widget->mouseExited(me);
}
