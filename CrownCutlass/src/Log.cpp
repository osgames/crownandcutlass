/* Crown and Cutlass
 * Log Object Code
 */

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdarg>
#include "Log.h"

using namespace std;

Log *Log::s_log = NULL;

Log::Log(bool logToFile) {
  time_t timestamp;

  m_debugMode = false;

  if (logToFile) {
    m_logFile.open("CrownCutlass.log", ios::out);
    if (!m_logFile) {
      // Can't open log file
      cerr << "Warning: Cannot open error.log for write, continuing without logging\n";
    } else {
      // Log file is open fine, give opening timestamp
      timestamp = time(NULL);
      m_logFile << "Log Started: " << ctime(&timestamp) << endl;
    }
  }
}

Log::~Log() {
  time_t timestamp;

  // Only need to close logFile if it's open
  if (m_logFile) {
    // Give closing timestamp
    timestamp = time(NULL);
    m_logFile << endl << "Log Closed: " << ctime(&timestamp) << endl;

    // Close log file
    m_logFile.close();
  }
}

void Log::Message(std::string msg) {
  if (m_logFile) {
    m_logFile << msg << endl << endl;
  }

  cerr << msg << endl << endl;
}

void Log::Message(const char *msg, ...) {
  va_list vargList; // This is to handle the variable arguments ...
  char outBuf[1024];
  unsigned short outLen;

  // This section takes the arguments and puts them into
  // the character array.
  va_start(vargList, msg);
#if defined (WIN32)
  outLen = _vsnprintf(outBuf, sizeof(outBuf), msg, vargList);
#else
  outLen = vsnprintf(outBuf, sizeof(outBuf), msg, vargList);
#endif
  //outLen = vsprintf(outBuf, msg, vargList);
  va_end(vargList);

  if (outLen >= sizeof(outBuf)) {
    // vsnprintf had to truncate the data, set outLen to correct value
    outLen = sizeof(outBuf);
  }

  if (m_logFile) {
    m_logFile << outBuf << endl;
  }

  cerr << outBuf << endl;
}

void Log::DebugMessage(std::string msg) {
  if (m_debugMode) {
    Message(msg);
  }
}

void Log::DebugMessage(const char *msg, ...) {
  /* WHY DOESN'T THIS WORK???
  va_list vargList; // This is to handle the variable arguments ...

  va_start(vargList, msg);
  if (debugMode) {
    message(msg, vargList);
  }
  va_end(vargList);
  */

  // Remove this once I figure out how to pass the variable args to the regular message
  va_list vargList; // This is to handle the variable arguments ...
  char outBuf[1024];
  unsigned short outLen;

  if (m_debugMode) {
    // This section takes the arguments and puts them into
    // the character array.
    va_start(vargList, msg);
#if defined (WIN32)
    outLen = _vsnprintf(outBuf, sizeof(outBuf), msg, vargList);
#else
    outLen = vsnprintf(outBuf, sizeof(outBuf), msg, vargList);
#endif
    //outLen = vsprintf(outBuf, msg, vargList);
    va_end(vargList);

    if (outLen >= sizeof(outBuf)) {
      // vsnprintf had to truncate the data, set outLen to correct value
      outLen = sizeof(outBuf);
    }

    if (m_logFile) {
      m_logFile << outBuf << endl;
    }

    cerr << outBuf << endl;
  }
}

void Log::SetDebugMode(bool debugMode) {
  m_debugMode = debugMode;
}

bool Log::OpenLog(bool logToFile) {
  // Make sure s_log hasn't already been initialized
  if (s_log != NULL) {
    s_log->Message("Warning: Multiple calls to OpenLog()");
    return false;
  }

  s_log = new Log(logToFile);
  return true;
}

void Log::CloseLog() {
  if (s_log == NULL) {
    cerr << "Warning: Call to closeLog with NULL s_log pointer" << endl;
    return;
  }

  delete s_log;
  s_log = NULL;
}
