/* Crown and Cutlass
 * Model Object Header
 */

#if !defined ( _MODEL_H_ )
#define _MODEL_H_

#include <string>
#include "GLee.h"

struct ObjModel;
struct ObjGroup;
class BoundBox;
class Texture;

class Model {
 public:
  // This basically just loads the texture
  // Still need to call a loadFORMAT method
  Model(std::string textureName);

  // Do whatever cleanup is necessary
  ~Model();

  // Draws the actual model
  void draw();

  // Add all file format dependent loadFORMAT functions here
  // Loads a .obj file
  void loadObj(const char *filename, float scale);

  BoundBox *modelBox;

 private:
  // Starts and sets up a display list, call from all LoadFORMAT functions
  void startList();

  // Texture for model
  Texture *texture;

  // Display list to display the model
  // Eventually this will probably be replace by a VBO/VA
  GLuint displayList;

  // Whether the model has been loaded or not
  bool loaded;

  // Add all loadFORMAT helper functions here
  ObjModel* objLoad(const char *filename);
  void objDraw(ObjModel *model);
  void objFirstPass(ObjModel *model, FILE *file);
  ObjGroup* objAddGroup(ObjModel *model, const char *name);
  ObjGroup* objFindGroup(ObjModel *model, const char *name);
  void objSecondPass(ObjModel* model, FILE* file);
  void objScale(ObjModel* model, float scale);
  BoundBox* objGenerateBox(ObjModel* model);
};

#endif
