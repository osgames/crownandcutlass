/* Crown and Cutlass
 * Naval Battle Entry Code
 */

#include <string>
#include <sstream>
#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "SDL.h"
#include "Menu/CCGui.h"
#include "Menu/CCButton.h"
#include "Menu/CCTable.h"
#include "Menu/CCLabel.h"
#include "Config.h"
#include "Log.h"
#include "Texture.h"
#include "font.h"
#include "IGameState.h"
#include "StateBattle/Battle.h"
#include "Player.h"
#include "Ship.h"
#include "StateBattle.h"

using namespace std;

const string StateBattle::s_mainFontName = "papyrus.png";
const string StateBattle::s_highlightFontName = "highlightPapyrus.png";
const string StateBattle::s_disabledFontName = "disabledPapyrus.png";

StateBattle::StateBattle(): IGameState(NULL) {
  m_actualBattle = NULL;

  // Stop the player, so when they leave they aren't moving
  Player::player->ship->m_speed = 0;
  Player::player->ship->SetSails(NOSAILS);

  m_exitCallback.SetCallback(this, &StateBattle::Exit);
  m_attackCallback.SetCallback(this, &StateBattle::Attack);

  Log::s_log->Message("Callbacks Set");

  m_background = new Texture("enterbattle.png");

  Log::s_log->Message("Background Texture built");

  Log::s_log->Message("Top Created");
  m_menuList = new CCTable(1, 2);

  m_attackButton = new CCButton("Attack!", "attack_button", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_attackCallback);
  m_sailAwayButton = new CCButton("Sail Away", "sailAway_button", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_exitCallback);

  m_title = new CCLabel("Ship sighted!", s_mainFontName);

  m_menuList->add(0, 0, m_sailAwayButton);
  m_menuList->add(0, 1, m_attackButton);

  m_menuList->setVisible(true);
  m_menuList->setOpaque(false);

  Log::s_log->Message("Table Set...Label Set");

  m_top->add(m_menuList, 10, 200);
  m_top->add(m_title, (Config::s_config->GetWinWidth() / 2) - (m_title->getWidth() / 2), 50);

  Log::s_log->Message("StateBattle initialization done.");

}

StateBattle::~StateBattle() {
  m_menuList->clear();
  delete m_menuList;
  delete m_title;

  delete m_actualBattle;

  delete m_background;

  Log::s_log->Message("State Battle deleted");
}

void StateBattle::Update(const unsigned int ticks) {
  CheckInput();
}

void StateBattle::Display() {
  glLoadIdentity();
  DisplayBackground(m_background->GetTexture(), 1);
}

void StateBattle::CheckInput() {
  SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    if (event.type == SDL_KEYDOWN)
    {
      /*
      if (event.key.keysym.sym == SDLK_ESCAPE)
      {
        done = false;
      }
      if (event.key.keysym.sym == SDLK_f)
      {
        if (event.key.keysym.mod & KMOD_CTRL)
        {
          // Works with X11 only
          //SDL_WM_ToggleFullScreen(screen);
        }
      }
      */
      if (event.key.keysym.sym == SDLK_z) {
        TakeScreenshot();
      }
    }


    /*
     * Now that we are done polling and using SDL events we pass
     * the leftovers to the SDLInput object to later be handled by
     * the Gui. (This example doesn't require us to do this 'cause a
     * label doesn't use input. But will do it anyway to show how to
     * set up an SDL application with Guichan.)
     */
    CCGui::s_CCGui->m_input->pushInput(event);
  }
}

void StateBattle::Exit(int ID) {
  PrepareStateSwitch(m_previous, this);
}

void StateBattle::Attack(int ID) {
  // Just to be on the safe side
  if (m_actualBattle != NULL) {
    delete m_actualBattle;
  }
  m_actualBattle = new Battle();
  PrepareStateSwitch(m_actualBattle, m_previous);
}

void StateBattle::SwitchTo(IGameState *oldState) {
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
}

void StateBattle::SwitchFrom() {
}
