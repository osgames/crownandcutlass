/* Crown and Cutlass
 * State New Game Class Code
 */

#include <cstdlib>
#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "SDL.h"
#include "Menu/CCGui.h"
#include "Menu/CCButton.h"
#include "Menu/CCTable.h"
#include "Menu/CCLabel.h"
#include "Menu/CCTextField.h"
#include "Config.h"
#include "Texture.h"
#include "font.h"
#include "Log.h"
#include "IGameState.h"
#include "StateSailing.h"
#include "MainMenu.h"
#include "Player.h"
#include "Callback.h"
#include "StateNewGame.h"

using namespace std;

const string g_defaultName = "John Smith";
const string g_defaultShip = "Jolly Smith";

const string StateNewGame::s_mainFontName = "papyrus.png";
const string StateNewGame::s_highlightFontName = "highlightPapyrus.png";
const string StateNewGame::s_disabledFontName = "disabledPapyrus.png";
const string StateNewGame::s_smallFontName = "smallFont.png";

StateNewGame::StateNewGame(): IGameState(NULL) {
  CCLabel *tempLabel;

  m_mainGame = NULL;

  m_acceptCallback.SetCallback(this, &StateNewGame::Accept);
  m_cancelCallback.SetCallback(this, &StateNewGame::Cancel);

  Log::s_log->Message("\tMenu: Callbacks Set.");

  m_menuList = new CCTable(2, 3);
  m_menuList->setRowBuffer(10);
  m_menuList->setColumnBuffer(10);

  m_acceptButton = new CCButton("Accept", "acceptButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_acceptCallback);
  m_acceptButton->setVisible(true);
  m_cancelButton = new CCButton("Cancel", "cancelButton", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_cancelCallback);
  m_cancelButton->setVisible(true);

  tempLabel = new CCLabel("Your Name:", s_smallFontName);
  m_menuList->add(0, 0, tempLabel);
  m_nameText = new CCTextField(g_defaultName, s_smallFontName, 150);
  m_menuList->add(1, 0, m_nameText);

  tempLabel = new CCLabel("Ship Name:", s_smallFontName);
  m_menuList->add(0, 1, tempLabel);
  m_shipText = new CCTextField(g_defaultShip, s_smallFontName, 150);
  m_menuList->add(1, 1, m_shipText);
  m_menuList->add(0, 2, m_acceptButton);
  m_menuList->add(1, 2, m_cancelButton);

  m_menuList->setVisible(true);
  m_menuList->setOpaque(false);

  Log::s_log->Message("\tGraphics and font initialized.");

  m_background = new Texture("background.png");

  m_top->add(m_menuList, 10, 100);

  Log::s_log->Message("\tGuichan finished.");

  Log::s_log->Message("NewGame initialized");
}

StateNewGame::~StateNewGame() {
  // Delete Guichan stuff
  // The table deletes all the buttons and labels that are in it
  m_menuList->clear();
  delete m_menuList;

  delete m_mainGame;

  delete m_background;

  Log::s_log->Message("StateNewGame deleted");
}

void StateNewGame::Reset() {
  m_nameText->setText(g_defaultName);
  m_shipText->setText(g_defaultShip);
}

void StateNewGame::Update(const unsigned int ticks) {
  CheckInput();
}

void StateNewGame::Display() {
  glLoadIdentity();
  DisplayBackground(m_background->GetTexture(), 1);
}

void StateNewGame::CheckInput()
{
  SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    if (event.type == SDL_KEYDOWN)
    {
      if (event.key.keysym.sym == SDLK_z) {
        TakeScreenshot();
      }
    }
    else if(event.type == SDL_QUIT)
    {
      DoExit();
    }

    CCGui::s_CCGui->m_input->pushInput(event);
  }
}

void StateNewGame::SwitchTo(IGameState *oldState) {
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
}

void StateNewGame::SwitchFrom() {
}

void StateNewGame::Accept(int i) {
  if (m_mainGame == NULL) {
    // New game
    m_mainGame = new StateSailing(m_nameText->getText(), m_shipText->getText());
    ((MainMenu*)s_mainMenu)->SetMainGame(m_mainGame);
  } else {
    // Just reset existing game
    m_mainGame->NewGame(m_nameText->getText(), m_shipText->getText());
  }
  PrepareStateSwitch(m_mainGame, NULL);
}

void StateNewGame::Cancel(int i) {
  PrepareStateSwitch(s_mainMenu, NULL);
}
