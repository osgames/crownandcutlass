/* Crown and Cutlass
 * Timer Header
 */

#if !defined ( _TIMER_H_ )
#define _TIMER_H_

class Timer {
 public:
  Timer();

  ~Timer();

  // Returns the ticks since the last call to ResetTimer
  // Note: This would be useful if you want to time something
  unsigned int GetTicks();

  // Resets the timer and returns ticks since last reset
  // Note: This should be called before every frame
  unsigned int ResetTimer();

  // Initializes and shutsdown the static timer object
  static void Initialize();
  static void Shutdown();

  static Timer *timer;

 private:
  unsigned int lastTicks;
};

#endif
