/* Map Class Code
 */

#include <guichan.hpp>
#include "StateSailing.h"
#include <cstdlib>
#include "SDL.h"
#include "GLee.h"
#include "Log.h"
#include "Config.h"
#include "Texture.h"
#include "font.h"
#include "IGameState.h"
#include "Ship.h"
#include "Terrain.h"
#include "CityManager.h"
#include "City.h"
#include "Menu/CCGui.h"
#include "Menu/CCFont.h"
#include "Menu/CCLabel.h"
#include "Map.h"

const string Map::s_mainFontName = "smallFont.png";

Map::Map(StateSailing *curGame, Ship *playerShip, Terrain *terrain):IGameState(curGame) {
  m_winWidth = Config::s_config->GetWinWidth();
  m_winHeight = Config::s_config->GetWinHeight();

  m_terrainWidth = terrain->GetWidth();
  m_terrainHeight = terrain->GetHeight();

  // Set the pointer to the main sailing state to null
  m_mainGame = curGame;

  m_playerShip = playerShip;

  // Load the background
  glEnable(GL_TEXTURE_2D);
  m_background = new Texture("mapscreen.png");
  m_background->BindTexture();

  m_curMousePosX = 0;
  m_curMousePosY = 0;

  m_transport = false;

  InitMenu();
}

Map::~Map() {
  gcn::Widget* tempWidget;
  unsigned int numWidgets;

  delete m_background;

  numWidgets = m_guichanWidgets.size();

  for (unsigned int i = 0; i < numWidgets; i++) {
    tempWidget = m_guichanWidgets[i];
    m_guichanWidgets[i] = NULL;
    delete tempWidget;
  }
  m_guichanWidgets.clear();

  delete m_shipIcon;

  delete m_cityIconImage;
  delete m_shipIconImage;
}

void Map::Update(const unsigned int ticks) {
  float screenX, screenY;

  // Process incoming events
  CheckInput();

  screenX = TerrainToScreenX((float)m_playerShip->m_x);
  screenY = TerrainToScreenZ((float)m_playerShip->m_z);
  m_shipIcon->setX((int) screenX - (m_shipIconImage->getWidth() / 2));
  m_shipIcon->setY((int) screenY - (m_shipIconImage->getHeight() / 2));
}

void Map::Display() {
  glLoadIdentity();

  // Draw the background
  DisplayBackground(m_background->GetTexture(), .6);
}


void Map::CheckInput() {
  /* Our SDL event placeholder. */
  SDL_Event event;

  HandleMouseMove();
  /* Grab all the events off the queue. */
  while( SDL_PollEvent( &event ) ) {
    switch( event.type ) {
    case SDL_KEYDOWN:
      /* Handle key presses. */
      HandleKeyDown( &event.key.keysym );
      break;
    case SDL_KEYUP:
      HandleKeyUp(&event.key.keysym);
      break;
    case SDL_QUIT:
      /* Handle quit requests (like Ctrl-c). */
      DoExit();
      break;
    }
  }
}

void Map::HandleMouseMove() {
  Uint8 buttons;

  buttons = SDL_GetMouseState(&m_curMousePosX, &m_curMousePosY);

  if ((buttons == SDL_BUTTON(1)) && m_transport) {
    m_playerShip->m_x = ScreenToTerrainX((float)m_curMousePosX);
    m_playerShip->m_z = ScreenToTerrainZ((float)m_curMousePosY);
  }
}

void Map::HandleKeyUp(SDL_keysym* keysym) {
  switch(keysym->sym) {
  case SDLK_ESCAPE:
    // Go back to the game, if it is loaded
    if (m_previous != NULL) {
      PrepareStateSwitch(m_previous, this);
    }
    break;
  case SDLK_m:
    // Go back to the game, if it is loaded
    if (m_previous != NULL) {
      PrepareStateSwitch(m_previous, this);
    }
    break;
  case SDLK_t:
    // Stop the transport function
    m_transport = false;
    break;

  default:
    break;
  }
}

void Map::HandleKeyDown(SDL_keysym* keysym) {
  switch( keysym->sym ) {
  case SDLK_z:
    TakeScreenshot();
    break;
  case SDLK_t:
    m_transport = true;
    break;
  default:
    break;
  }
}

void Map::SwitchTo(IGameState *oldState) {
  m_transport = false;
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
}

void Map::SwitchFrom() {
}

void Map::DrawGui() {
  glPushMatrix();
  glLoadIdentity();
  m_background->BindTexture();

  glColor3f(0, 0, 0);

  //Draw quad for map
  glBegin(GL_QUADS);
  glTexCoord2f(0,0);
  glVertex3f(10, m_winHeight - 250, 0);
  glTexCoord2f(0, 1);
  glVertex3f(10, m_winHeight - 10, 0);
  glTexCoord2f(1, 1);
  glVertex3f(370, m_winHeight - 10, 0);
  glTexCoord2f(1, 0);
  glVertex3f(370, m_winHeight - 250, 0);
  glEnd();

  glColor3f(1, 1, 1);
  glPopMatrix();
}

void Map::InitMenu() {
  gcn::Widget* tempWidget;
  unsigned int i, numCities;
  float screenX, screenY;

  m_cityIconImage = gcn::Image::load("data/Textures/cityicon.png");
  m_shipIconImage = gcn::Image::load("data/Textures/shipicon.png");

  numCities = m_mainGame->m_cities->NumberCities();

  m_guichanWidgets.reserve(numCities * 2);

  for (i = 0; i < numCities; i++) {
    screenX = TerrainToScreenX((float)m_mainGame->m_cities->GetCity(i)->GetLocX());
    screenY = TerrainToScreenZ((float)m_mainGame->m_cities->GetCity(i)->GetLocZ());

    tempWidget = new gcn::Icon(m_cityIconImage);
    m_guichanWidgets.push_back(tempWidget);
    m_top->add(tempWidget, (int) screenX - (m_cityIconImage->getWidth() / 2), (int) screenY - (m_cityIconImage->getHeight() / 2));

    tempWidget = new CCLabel(m_mainGame->m_cities->GetCity(i)->getName(), s_mainFontName);
    m_guichanWidgets.push_back(tempWidget);
    m_top->add(tempWidget, (int) screenX, (int) screenY);
  }

  screenX = TerrainToScreenX((float)m_playerShip->m_x);
  screenY = TerrainToScreenZ((float)m_playerShip->m_z);

  m_shipIcon = new gcn::Icon(m_shipIconImage);
  m_top->add(m_shipIcon, (int) screenX - (m_shipIconImage->getWidth() / 2), (int) screenY - (m_shipIconImage->getHeight() / 2));
}

float Map::TerrainToScreenX(float x) {
  return m_winWidth * (x / m_terrainWidth + 0.5);
}

float Map::TerrainToScreenZ(float z) {
  return m_winHeight * (z / m_terrainHeight + 0.5);
}

float Map::ScreenToTerrainX(float x) {
  return m_terrainWidth * ((float)x/m_winWidth - 0.5);
}

float Map::ScreenToTerrainZ(float z) {
  return m_terrainHeight * ((float)z/m_winHeight - 0.5);
}
