/*
 * Economy Screen Class
 * Crown and Cutlass
 */

#include <string>
#include "Menu/CCGui.h"
#include "Menu/CCButton.h"
#include "Menu/CCTable.h"
#include "Menu/CCListModel.h"
#include "Menu/CCListBox.h"
#include "Menu/CCTextField.h"
#include "Menu/CCLabel.h"
#include "Menu/CCFont.h"
#include "Callback.h"
#include "Economy.h"
#include "City.h"
#include "CityEconomy.h"
#include "CityManager.h"
#include "Product.h"
#include "EconomyScreen.h"

using namespace std;

EconomyScreen::EconomyScreen(CityManager* Manager, int Width, int Height) {
  unsigned int i;
  string CityName;
  CCLabel* tempLabel;

  m_CityManager = Manager;

  Log::s_log->DebugMessage("Setting Callbacks");
  m_HideCallback.SetCallback(this, &EconomyScreen::HideMe);
  m_StepCallback.SetCallback(this, &EconomyScreen::StepEconomy);
  m_ListBoxCallback.SetCallback(this, &EconomyScreen::ListBoxCallback);

  m_listModel = new CCListModel();
  m_top = new gcn::Container();
  m_top->setDimension(gcn::Rectangle(0,0,Width, Height));

  Log::s_log->DebugMessage("Adding Cities to the CCListModel");
  for (i = 0;
       i < m_CityManager->NumberCities();
       i++) {
    CityName = m_CityManager->GetCity(i)->getName();
    m_listModel->AddItem(CityName);
  }

  m_Cities = new CCListBox(m_listModel, &m_ListBoxCallback);
  m_CityName = new CCLabel("", "systemBlack.png");
  m_CityLabel = new CCLabel("City Name", "systemBlack.png");
  m_StepEconomy = new CCButton("Step Economy",
                               "Step",
                               "systemBlack.png",
                               "systemWhite.png",
                               "systemBlack.png",
                               &m_StepCallback);

  m_Table = new CCTable(2, Economy::s_economy->ProductCount() + 1);
  m_Table->setColumnBuffer(10);
  tempLabel = new CCLabel("Product", "systemBlack.png");
  m_Table->add(0, 0, tempLabel);
  tempLabel = new CCLabel("Price", "systemBlack.png");
  m_Table->add(1, 0, tempLabel);

  for (i = 0; i < Economy::s_economy->ProdList.size(); i++) {
    tempLabel = new CCLabel(Economy::s_economy->ProdList[i], "systemBlack.png");
    m_Table->add(0, i+1, tempLabel);
    tempLabel = new CCLabel("0", "systemBlack.png");
    m_Table->add(1, i+1, tempLabel);
  }

  m_top->setFrameSize(2);
  m_top->setOpaque(true);
  m_top->add(m_Cities, 0,0);
  m_top->add(m_Table, 200, 100);
  m_top->add(m_CityLabel, 200, 0);
  m_top->add(m_CityName, 200, 20);
  m_top->add(m_StepEconomy, m_top->getWidth() - m_StepEconomy->getWidth(),
    m_top->getHeight() - m_StepEconomy->getHeight());
  ShowScreen(false);

  Log::s_log->DebugMessage("EconomyScreen Created");
}

EconomyScreen::~EconomyScreen() {
  m_Table->clear();
  delete m_Table;
  //delete m_HideMenu;
  delete m_StepEconomy;
  delete m_Cities;
  delete m_listModel;
  delete m_CityLabel;
  delete m_CityName;
  delete m_top;
}

bool EconomyScreen::ToggleShowScreen() {
  ShowScreen(!m_top->isVisible());
  return m_top->isVisible();
}

void EconomyScreen::ShowScreen(bool Show) {
  m_top->setVisible(Show);
}

void EconomyScreen::StepEconomy(int n) {
  Economy::s_economy->Step();
  ListBoxCallback(n);
}

void EconomyScreen::HideMe(int n) {
  ShowScreen(false);
}

void EconomyScreen::ListBoxCallback(int n) {
  CCLabel* tempLabel;
  unsigned int i;
  CityEconomy* CE;
  Product* tempProd;
  string cityName;
  try {
    cityName = m_listModel->getElementAt(m_Cities->getSelected());
  } catch (...) {return;}
  m_CityName->setCaption(cityName);
  m_CityName->adjustSize();

  CE = m_CityManager->GetCity(cityName.c_str())->MyEconomy;
  for (i = 0; i < CE->GetProductList().size(); i++) {
    tempLabel = (CCLabel*)m_Table->getWidget(1, i+1);
    tempProd = CE->GetProductList()[i];
    tempLabel->SetCaption(CE->GetPrice(tempProd->GetName()), 2);
    tempLabel->adjustSize();
  }
}

gcn::Widget* EconomyScreen::GetWidget() {
  return m_top;
}
