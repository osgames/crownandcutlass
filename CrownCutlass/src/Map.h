/* Crown and Cutlass
 * Map Class Header (Mostly Stolen from MainMenu Header)
 */

#if !defined( _MAP_H_ )

#define _MAP_H_

#include "SDL.h"
#include "IGameState.h"

// So I don't have to include the headers
class StateSailing;
class Ship;
class Terrain;
class Texture;
class CCFont;

class Map: public IGameState {
 public:
  // Constructor
  Map(StateSailing *curGame, Ship *playerShip, Terrain *terrain);

  // Destructor
  ~Map();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // This will draw the map in the upper left corner of the screen while sailing
  void DrawGui();

 private:
  // Background texture
  Texture *m_background;

  // Guichan stuff
  gcn::Image* m_cityIconImage;
  gcn::Icon* m_shipIcon;
  gcn::Image* m_shipIconImage;
  vector<gcn::Widget*> m_guichanWidgets;

  void InitMenu();

  // Handles processing SDL events
  void CheckInput();

  // Main sailing game state
  StateSailing *m_mainGame;

  // Player...for position on the map
  Ship *m_playerShip;

  // Keyboard handler functions
  void HandleKeyUp(SDL_keysym* keysym);
  void HandleKeyDown(SDL_keysym* keysym);
  void HandleMouseMove();

  int m_winWidth;
  int m_winHeight;

  int m_terrainWidth;
  int m_terrainHeight;

  int m_curMousePosX;
  int m_curMousePosY;

  bool m_transport;

  // Converts from location on the terrain to map screen coord
  float TerrainToScreenX(float x);
  float TerrainToScreenZ(float z);

  // Converts from map screen coord to location on the terrain
  float ScreenToTerrainX(float x);
  float ScreenToTerrainZ(float z);

  // Font names
  static const std::string s_mainFontName;
};

#endif
