/* Crown and Cutlass
 * Main Code File
 */

#include <string>
#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "Menu/CCGui.h"
#include "SDL.h"
#include "Log.h"
#include "Config.h"
#include "IGameState.h"
#include "MainMenu.h"
#include "Economy.h"
#include "Timer.h"
#include "font.h"
#include "SoundSystem.h"
#include "ResourceManager/ResourceManager.h"
#include "ResourceManager/IResourceFactory.h"
#include "ResourceManager/SoundFactory.h"
#include "ResourceManager/TextureFactory.h"
#include "ResourceManager/ShipFactory.h"
#include "ResourceManager/FontFactory.h"

using namespace std;

// Set to true to exit program
bool done = false;

IGameState *current;
MainMenu *mainMenu;

SDL_Surface *screen;

void prepExit() {
  SDL_Quit();
  Log::s_log->Message("SDL shutdown");

  ResourceManager::Shutdown();

  Config::CloseConfig();

  Timer::Shutdown();

  Log::s_log->Message("Shutdown successful");
  Log::CloseLog();
}

void init() {
  auto_ptr<IResourceFactory> factory;

  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  glViewport(0, 0, width, height);

  IGameState::SetCurrentPtr(&current);

  Log::s_log->Message("Current state pointer set");

  Log::s_log->Message("Sound system initialized");
  SoundSystem::Initialize(Config::s_config->GetNumberALSources());

  factory.reset(new SoundFactory());
  ResourceManager::s_resourceManager->LoadXMLFile("data/SoundResources.xml", factory);

  factory.reset(new TextureFactory());
  ResourceManager::s_resourceManager->LoadXMLFile("data/TextureResources.xml", factory);

  factory.reset(new ShipFactory());
  ResourceManager::s_resourceManager->LoadXMLFile("data/ShipResources.xml", factory);

  factory.reset(new FontFactory());
  ResourceManager::s_resourceManager->LoadXMLFile("data/FontResources.xml", factory);

  if (!CCGui::CreateGui(width, height)) {
    throw new gcn::Exception("Gui creation error.");
  }

  Log::s_log->Message("Gui Created");

  // Set up the mainMenu to run first
  mainMenu = new MainMenu(&done);
  current = mainMenu;
  mainMenu->PrepareTopContainer();

  Log::s_log->Message("Main menu created");
}

int main(int argc, char** argv) {
  unsigned int ticks;
  int width, height;

  Log::OpenLog(true);
  Timer::Initialize();
  Config::OpenConfig("Config.xml");
  ResourceManager::Initialize();

  width = Config::s_config->GetWinWidth();
  height = Config::s_config->GetWinHeight();

  /* Information about the current video settings. */
  const SDL_VideoInfo* info = NULL;

  /* Color depth in bits of our window. */
  int bpp = 0;
  /* Flags we will pass into SDL_SetVideoMode. */
  int flags = 0;

  // Flags for SDL_Init
  int initFlags = 0;

  if (Config::s_config->CheckSDLParachute()) {
    initFlags = SDL_INIT_VIDEO;
  } else {
    initFlags = SDL_INIT_VIDEO|SDL_INIT_NOPARACHUTE;
  }

  /* First, initialize SDL's video subsystem. */
  if(SDL_Init(initFlags) < 0) {
    // Failed to initialize
    Log::s_log->Message("Video initialization failed: %s", SDL_GetError());

    prepExit();

    exit(1);
  }

  /* Let's get some video information. */
  info = SDL_GetVideoInfo();
  if(!info) {
    /* This should probably never happen. */
    Log::s_log->Message("Video query failed: %s", SDL_GetError());

    prepExit();

    exit(1);
  }

  Log::s_log->Message("Got video info");

  /*
   * We get the bpp we will request from
   * the display. On X11, VidMode can't change
   * resolution, so this is probably being overly
   * safe. Under Win32, ChangeDisplaySettings
   * can change the bpp.
   */
  bpp = info->vfmt->BitsPerPixel;

  /*
   * Now, we want to setup our requested
   * window attributes for our OpenGL window.
   * We want *at least* 5 bits of red, green
   * and blue. We also want at least a 16-bit
   * depth buffer.
   *
   * The last thing we do is request a double
   * buffered window. '1' turns on double
   * buffering, '0' turns it off.
   *
   * Note that we do not use SDL_DOUBLEBUF in
   * the flags to SDL_SetVideoMode. That does
   * not affect the GL attribute state, only
   * the standard 2D blitting setup.
   */
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  // Request an OpenGL window
  if (Config::s_config->CheckFullscreen()) {
    flags = SDL_OPENGL | SDL_FULLSCREEN;
  } else {
    flags = SDL_OPENGL;
  }

  // Set the window name
  SDL_WM_SetCaption("Crown and Cutlass", "Crown and Cutlass");
  //SDL_ShowCursor(0);

  /*
   * Set the video mode
   */
  screen = SDL_SetVideoMode(width, height, bpp, flags);
  if(screen == 0) {
    /*
     * This could happen for a variety of reasons,
     * including DISPLAY not being set, the specified
     * resolution not being available, etc.
     */
    Log::s_log->Message("Video mode set failed: %s", SDL_GetError());

    prepExit();

    exit(1);
  }

  SDL_EnableUNICODE(1);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

  Log::s_log->Message("Video mode set");

  // Make sure the video card supports the necessary extensions
  // Note: this needs to happen after we have a valid opengl rendering context
  if (!Config::s_config->CheckExtensions()) {
    Log::s_log->Message("Error: Your video card does not support the necessary extensions");
    prepExit();
    exit(1);
  }
  Log::s_log->Message("Extensions verified");

  try {
    init();

    Log::s_log->Message("Crown and Cutlass initialized sucessfully\n");

    while(!done && (current != NULL)) {
      ticks = Timer::timer->ResetTimer();

      SoundSystem::s_soundSystem->Update(ticks);

      current->Loop(ticks);
    }
  } catch (gcn::Exception& e) {
    Log::s_log->Message("Uncaught guichan exception in %s line %d (%s)", e.getFilename().c_str(), e.getLine(), e.getMessage().c_str());
  } catch (gcn::Exception* e) {
    Log::s_log->Message("Uncaught guichan exception in %s line %d (%s)", e->getFilename().c_str(), e->getLine(), e->getMessage().c_str());
  } catch (const char *e) {
    Log::s_log->Message("Uncaught exception (%s)", e);
  } catch (string e) {
    Log::s_log->Message("Uncaught exception (%s)", e.c_str());
  } catch (...) {
    Log::s_log->Message("Uncaught exception");
  }

  delete mainMenu;

  CCGui::DestroyGui();

  // Don't delete current!
  // We only created the main menu, whatever created current will delete it
  //delete current;

  SoundSystem::Shutdown();

  prepExit();

  return 0;
}
