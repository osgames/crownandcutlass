/* Crown and Cutlass
 * City Manager Class
 *
 * This class is a vector of cities. It will handle all the
 * appropriate actions to take on the cities. (Displaying,
 * checking collisions, etc)
 */

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include "GLee.h"
#include "SDL.h"
#include "ccmath.h"
#include "City.h"
#include "IEnvironmentObject.h"
#include "Terrain.h"
#include "Ship.h"
#include "Log.h"
#include "Product.h"
#include "Model.h"
#include "BuildingList.h"
#include "Point.h"
#include "tinyxml.h"
#include "CityManager.h"

#define MAX_CITIES 32 // This needs to get bigger probably...

using namespace std;

CityManager::CityManager(Terrain *land) {
  LoadCityConfig("data/City.xml", land);

  //WorldEcon = new Economy("data/Product.xml");

  Log::s_log->Message("Cities loaded");
}

CityManager::~CityManager() {
  unsigned int i;
  map<string, City*>::const_iterator j;

  for (j = CityList.begin();
       j != CityList.end();
       j++)
    delete j->second;
  CityList.clear();

  // Delete the cluster models
  for (i = 0; i < clusters.size(); i++) {
    delete clusters[i];
    clusters[i] = NULL;
  }
  clusters.clear();

  Log::s_log->Message("Cities deleted");
}

City* CityManager::CheckCollision(Ship *ship) {
  // Used to determine if the player is sailing towards the city
  float angleToCity;
  float diffAngles;

  map<string, City*>::iterator i;

  // No sense in checking if the player isn't moving
  if (ship->m_speed == 0) return NULL;

  for (i=CityList.begin();
       i!=CityList.end();
       i++) {
    // Check the distance to each city
    // Note: This isn't really efficient, but since there aren't many cities
    //   it's not really worth doing something fancier.
    if (i->second->calcDist(ship->m_x, ship->m_z) < 3) {
      // It's in range, now make sure the angle is close
      angleToCity = i->second->calcAngle(ship->m_x, ship->m_z);

      diffAngles = fabs(ship->m_rot - angleToCity);

      if (diffAngles > M_PI) diffAngles = 2*M_PI - diffAngles;

      if (diffAngles < degreesToRadians(15)) {
        // The player is close and headed +-5 degs of city
        return i->second;
      }
    }
  }

  // No collision
  return NULL;
}

void CityManager::DisplayCities() {
  map<string, City*>::iterator i;

  for (i = CityList.begin();
       i != CityList.end();
       i++)
    i->second->DisplayObject();
}

void CityManager::LoadCityConfig(const char *filename, Terrain *land) {
  City* TempCity;
  TiXmlElement* object;
  TiXmlElement* location;
  TiXmlElement* buildings;
  TiXmlNode* node;
  TiXmlNode* cities;
  TiXmlDocument doc( filename );
  TiXmlText* cityString;
  string cityName;
  vector<Product*> tempList;
  int locX, locZ;
  int objNum = 0;
  string modelName;
  string textureName;
  BuildingList *cityBuildings;

  doc.LoadFile();
  cities = doc.FirstChild("Cities");

  ParseClusters(cities->FirstChildElement("Clusters"));

  for (node = cities->FirstChild("object"); node; node = node->NextSibling()) {
    objNum++;
    try {
      // Get the name of the city
      object = node->FirstChild("name")->ToElement();
      if (object == NULL) throw 0;
      cityString = object->FirstChild()->ToText();
      if (cityString == NULL) throw 0;
      //cityString = object->ToText();
      cityName = cityString->Value();

      // Get the X location
      location = node->FirstChild("location")->ToElement();
      if (location == NULL) throw 1;
      object = location->FirstChild("x")->ToElement();
      if (object == NULL) throw 1;
      cityString = object->FirstChild()->ToText();
      if (cityString == NULL) throw 1;
      sscanf(cityString->Value(), "%d", &locX);

      // Get the Z location
      object = location->FirstChild("z")->ToElement();
      if (object == NULL) throw 2;
      cityString = object->FirstChild()->ToText();
      if (cityString == NULL) throw 2;
      sscanf(cityString->Value(), "%d", &locZ);

      // Get the buildings
      cityBuildings = new BuildingList();

      buildings = node->FirstChildElement("Buildings");
      if (buildings == NULL) throw 3;

      for (object = buildings->FirstChildElement("Building"); object; object = object->NextSiblingElement("Building")) {
        int tModel;
        float tX, tY, tZ, tRot;
        const char *tStr;

        if (object == NULL) throw 4;

        tStr = object->Attribute("index");
        if (tStr == NULL) throw 5;
        if (sscanf(tStr, "%d", &tModel) != 1) throw 5;

        tStr = object->Attribute("rotation");
        if (tStr == NULL) throw 6;
        if (sscanf(tStr, "%f", &tRot) != 1) throw 6;

        tStr = object->Attribute("x");
        if (tStr == NULL) throw 7;
        if (sscanf(tStr, "%f", &tX) != 1) throw 7;

        tStr = object->Attribute("y");
        if (tStr == NULL) throw 7;
        if (sscanf(tStr, "%f", &tY) != 1) throw 7;

        tStr = object->Attribute("z");
        if (tStr == NULL) throw 7;
        if (sscanf(tStr, "%f", &tZ) != 1) throw 7;

        cityBuildings->Add(clusters[tModel], new Point(tX, tY, tZ), tRot);
      }

      TempCity = new City(land, cityName, locX, locZ, cityBuildings);
      CityList[cityName] = TempCity;
      Keys.push_back(cityName);
    } catch(int j) {
      switch (j) {
      case 0:
        Log::s_log->Message("Warning: Problem with Object Name.");
        break;
      case 1:
        Log::s_log->Message("Warning: Problem with Object x Location.");
        break;
      case 2:
        Log::s_log->Message("Warning: Problem with Object y Location.");
        break;
      case 3:
        Log::s_log->Message("Warning: Problem with Object Buildings.");
        break;
      case 4:
        Log::s_log->Message("Warning: Problem with Object Buildings item.");
        break;
      case 5:
        Log::s_log->Message("Warning: Problem with Object Buildings index.");
        break;
      case 6:
        Log::s_log->Message("Warning: Problem with Object Buildings rotation.");
        break;
      case 7:
        Log::s_log->Message("Warning: Problem with Object Buildings x, y, z.");
        break;
      default:
        Log::s_log->Message("Warning: Problem with City Config file.");
        break;
      }
    }
  }
}

void CityManager::ParseClusters(TiXmlElement *clustersNode) {
  TiXmlElement *node;
  TiXmlElement *tempElement;
  TiXmlText* tempString;
  string file;
  string texture;
  string scaleStr;
  float scale;
  Model *model;

  if (clustersNode == NULL) {
    Log::s_log->Message("Warning: Clusters node NULL");
    return;
  }

  try {
    for (node = clustersNode->FirstChildElement("Cluster"); node; node = node->NextSiblingElement("Cluster")) {
      tempElement = node->FirstChildElement("Model");
      if ((tempElement == NULL) || (tempElement->FirstChild() == NULL)) throw "Model";
      tempString = tempElement->FirstChild()->ToText();
      if (tempString == NULL) throw "Model2";
      file = tempString->Value();

      tempElement = node->FirstChildElement("Texture");
      if ((tempElement == NULL) || (tempElement->FirstChild() == NULL)) throw "Texture";
      tempString = tempElement->FirstChild()->ToText();
      if (tempString == NULL) throw "Texture";
      texture = tempString->Value();

      tempElement = node->FirstChildElement("Scale");
      if ((tempElement == NULL) || (tempElement->FirstChild() == NULL)) throw "Scale";
      tempString = tempElement->FirstChild()->ToText();
      if (tempString == NULL) throw "Scale";
      if (sscanf(tempString->Value(), "%f", &scale) != 1) throw "Scale";

      model = new Model(texture);
      model->loadObj(file.c_str(), scale);

      clusters.push_back(model);
    }
  } catch (const char *e) {
    Log::s_log->Message("Warning: Could not parse %s in city clusters", e);
  }

  Log::s_log->Message("%d clusters loaded", clusters.size());
}

bool CityManager::CheckCity(const char *cityName) {
  if (CityList[cityName]) return true;
  return false;
}

unsigned int CityManager::NumberCities() {
  return CityList.size();
}

City* CityManager::GetCity(const char *cityName) {
  return CityList[cityName];
}

City* CityManager::GetCity(int CityIndex) {
  return GetCity(Keys[CityIndex].c_str());
}
