/*
 * Economy Screen Class
 * Crown and Cutlass
 */

#if !defined( _ECONOMYSCREEN_H_)

#define _ECONOMYSCREEN_H_

#include <guichan.hpp>
#include "SDL.h"
#include "Callback.h"

//class CCButton;
//class CCTable;
class CCListModel;
class CCListBox;
class CCTextField;
class CCLabel;
class StateSailing;

class EconomyScreen {
  public:
    EconomyScreen(CityManager* Manager, int Width, int Height);
    ~EconomyScreen();

    bool ToggleShowScreen();
    void ShowScreen(bool Show);
    
    gcn::Widget* GetWidget();

  private:
    void StepEconomy(int n);
    void HideMe(int n);
    void ListBoxCallback(int n);

    CCListModel* m_listModel;
    CCListBox* m_Cities;
    gcn::Container* m_top;
    CCLabel* m_CityName;
    CCLabel* m_CityLabel;
    CityManager* m_CityManager;

    CCTable* m_Table;
    //CCButton* m_HideMenu;
    CCButton* m_StepEconomy;

    TCallback<EconomyScreen> m_HideCallback;
    TCallback<EconomyScreen> m_StepCallback;
    TCallback<EconomyScreen> m_ListBoxCallback;
};

#endif
