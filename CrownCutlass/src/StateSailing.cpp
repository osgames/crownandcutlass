/* Crown and Cutlass
 * Sailing GameState Code
 */

#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include <string>
#include "SDL.h"
#include "ccmath.h"
#include "Log.h"
#include "Config.h"
#include "normals.h"
#include "Player.h"
#include "Terrain.h"
#include "Ocean.h"
#include "Camera.h"
#include "image.h"
#include "font.h"
#include "IGameState.h"
#include "Map.h"
#include "City.h"
#include "CityManager.h"
#include "StateCity.h"
#include "Economy.h"
#include "Ship.h"
#include "StateBattle.h"
#include "Menu/CCGui.h"
#include "EconomyScreen.h"
#include "Timer.h"
#include "ISaveObject.h"
#include "StateSailing.h"

using namespace std;

#define WATER_SIZE 2.5

#define TICKS_BETWEEN_BATTLES 15000

const string StateSailing::s_mainFontName = "papyrus.png";

StateSailing::StateSailing(string playerName, string shipName): IGameState(NULL), ISaveObject("StateSailing") {
  DisplaySplashScreen("loading.png", "Loading...", s_mainFontName, .4);

  Log::s_log->Message("Loading screen displayed");

  // Set the m_childState pointer to NULL
  m_childState = NULL;

  SetTicksToNextBattle();

  // Create the terrain
  m_land = new Terrain("data/heightmap.png", .04, 25);

  Log::s_log->Message("Terrain created");

  // Set the light color and position
  m_ambientLight[0] = 0.17;
  m_ambientLight[1] = 0.17;
  m_ambientLight[2] = 0.17;
  m_ambientLight[3] = 1.0;

  m_diffuseLight[0] = 1.0;
  m_diffuseLight[1] = 1.0;
  m_diffuseLight[2] = 1.0;
  m_diffuseLight[3] = 1.0;

  m_lightPosition[0] = 7.0;
  m_lightPosition[1] = 15.0;
  m_lightPosition[2] = 2.0;
  m_lightPosition[3] = 0.0;

  // Begin setting up the rendering

  glClearColor(0, 0, 0, 0);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  //glEnable(GL_NORMALIZE);
  glShadeModel(GL_SMOOTH);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);

  // Set up lighting
  glEnable(GL_LIGHTING);
  glLightfv(GL_LIGHT0, GL_AMBIENT, m_ambientLight);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, m_diffuseLight);
  glLightfv(GL_LIGHT0, GL_POSITION, m_lightPosition);
  glEnable(GL_LIGHT0);

  Initialize(playerName, shipName);

  Log::s_log->Message("StateSailing initialized\n");
}

StateSailing::~StateSailing() {
  Destroy();

  // Delete all the objects that don't get cleaned up by destroy
  delete m_land;

  Log::s_log->Message("StateSailing deleted");
}

void StateSailing::Initialize(const string playerName, const string shipName) {
  bool EconCreated;

  // Temp storage for window width & height
  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  // Initialize sway of the ship
  m_rotx = 0.0;

  // Set up the camera
  m_cameraX = 0;
  m_cameraY = 13;
  m_cameraZ = 9;
  m_camera = new Camera((float) width/(float) height, m_cameraX, m_cameraY, m_cameraZ);

  Log::s_log->Message("Camera created");

  EconCreated = Economy::CreateEconomy();

  if (EconCreated) Log::s_log->Message("Economy loaded");
  else Log::s_log->Message("***Economy loading failed***");

  // Create Cities
  m_cities = new CityManager(m_land);

  // Create the economy screen
  m_es = new EconomyScreen(m_cities, 600, 600);
  m_top->add(m_es->GetWidget(), 10, 10);
  m_top->moveToTop(m_es->GetWidget());

  Player::CreateNewPlayer(playerName, shipName);

  // Create the map
  m_map = new Map(this, Player::player->ship, m_land);

  Log::s_log->Message("Map created");

  Timer::timer->ResetTimer();
}

void StateSailing::Destroy() {
  // Delete the m_childState if it's active
  if (m_childState != NULL) {
    delete m_childState;
    m_childState = NULL;
  }

  Economy::DestroyEconomy();

  delete m_map;
  delete m_es;
  delete m_cities;
  delete m_camera;

  Player::DeletePlayer();
}

void StateSailing::NewGame(const string playerName, const string shipName) {
  Destroy();
  Initialize(playerName, shipName);
}

void StateSailing::Save(TiXmlElement *parent) {
  TiXmlElement selfNode(m_XMLName);

  Player::player->Save(&selfNode);
  parent->InsertEndChild(selfNode);
}

void StateSailing::Load(TiXmlElement *parent) {
  TiXmlElement *selfNode;

  selfNode = parent->FirstChildElement(m_XMLName);
  Player::player->Load(selfNode);
}

void StateSailing::Update(unsigned int ticks) {
  // Check for child class.  If it is there it means we switched to it and
  //   are now back, so it can be deleted.
  if (m_childState != NULL) {
    delete m_childState;
    m_childState = NULL;
  }

  // Process incoming events
  CheckInput();

  // Change the economy
  //Economy::s_economy->Step();

  // Update the scene
  Timer(ticks);
}

void StateSailing::Display() {
  glLoadIdentity();
  m_camera->SetCamera();

  // Draw the ship
  Player::player->ship->Draw(false, 0, 0);

  // Draw the terrain
  glPushMatrix();
  glTranslatef(-1*Player::player->ship->m_x, 0, -1*Player::player->ship->m_z);
  m_land->Draw();
  m_cities->DisplayCities();
  glPopMatrix();
}

void StateSailing::CheckInput() {
  /* Our SDL event placeholder. */
  SDL_Event event;

  /* Grab all the events off the queue. */
  while( SDL_PollEvent( &event ) ) {
    switch( event.type ) {
    case SDL_KEYDOWN:
      /* Handle key presses. */
      HandleKeyDown( &event.key.keysym );
      break;
    case SDL_KEYUP:
      HandleKeyUp(&event.key.keysym);
      break;
    case SDL_QUIT:
      /* Handle quit requests (like Ctrl-c). */
      DoExit();
      break;
    }
    CCGui::s_CCGui->m_input->pushInput(event);
  }
}

void StateSailing::HandleKeyUp(SDL_keysym* keysym) {
  switch( keysym->sym ) {
  case SDLK_LEFT:
    Player::player->ship->SetRudder(CENTER);
    break;
  case SDLK_RIGHT:
    Player::player->ship->SetRudder(CENTER);
    break;
  case SDLK_ESCAPE:
    PrepareStateSwitch(s_mainMenu, this);
    break;
  case SDLK_m:
    PrepareStateSwitch(m_map, this);
    break;
  default:
    break;
  }
}

void StateSailing::HandleKeyDown(SDL_keysym* keysym) {
  switch( keysym->sym ) {
  case SDLK_b:
    SetTicksToNextBattle();
    if (m_childState != NULL) {
      delete m_childState;
    }
    m_childState = new StateBattle();
    PrepareStateSwitch(m_childState, this);
    break;
  case SDLK_c:
    Player::player->ship->m_rot += M_PI;
    break;
  case SDLK_d:
    Dump();
    break;
  case SDLK_e:
    m_es->ToggleShowScreen();
    break;
  case SDLK_z:
    TakeScreenshot();
    break;
  case SDLK_u:
    delete m_cities;
    m_cities = new CityManager(m_land);
    break;
  case SDLK_1:
    m_cameraX = 0;
    m_cameraY = 13;
    m_cameraZ = 9;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_2:
    m_cameraX = 0;
    m_cameraY = 150;
    m_cameraZ = 150;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_3:
    m_cameraX = 0;
    m_cameraY = 5;
    m_cameraZ = 9;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_4:
    m_cameraX = 0;
    m_cameraY = 26;
    m_cameraZ = 18;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_5:
    m_cameraX = 0;
    m_cameraY = 0;
    m_cameraZ = 10;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_6:
    m_cameraX = 0;
    m_cameraY = 10;
    m_cameraZ = .00001;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_7:
    m_cameraX = 0;
    m_cameraY = 2.6;
    m_cameraZ = 1.8;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_8:
    m_cameraX = 0;
    m_cameraY = 0;
    m_cameraZ = 3;
    m_camera->SetPosition(m_cameraX, m_cameraY, m_cameraZ);
    break;
  case SDLK_LEFT:
    Player::player->ship->SetRudder(LEFT);
    break;
  case SDLK_RIGHT:
    Player::player->ship->SetRudder(RIGHT);
    break;
  case SDLK_UP:
    Player::player->ship->RaiseSails();
    break;
  case SDLK_DOWN:
    Player::player->ship->LowerSails();
  default:
    break;
  }
}

void StateSailing::Timer(const unsigned int ticks) {
  double shipLine[3];
  int maxX, maxZ;

  maxX = m_land->GetWidth() / 2 - 5;
  maxZ = m_land->GetHeight() / 2 - 5;

  // Used to check for a collision with a city
  City *town;

  // Check for hitting a city
  town = m_cities->CheckCollision(Player::player->ship);
  if (town != NULL) {
    Player::player->ship->m_speed = 0;
    Player::player->ship->SetSails(NOSAILS);
    Player::player->ship->m_rot += M_PI;
    if (Player::player->ship->m_rot > TWO_PI) Player::player->ship->m_rot -= TWO_PI;

    if (m_childState != NULL) {
      Log::s_log->Message("Warning: m_childState not null when entering town");
      delete m_childState;
      m_childState = NULL;
    }

    // Before we switch, update the economy
    Economy::s_economy->UpdateEconomy();

    m_childState = new StateCity(town);

    PrepareStateSwitch(m_childState, this);
  }

  Player::player->ship->Update(ticks);
  Player::player->ship->GetDirectionVector(shipLine);

  if (Player::player->ship->m_x > maxX) {
    Player::player->ship->m_x = maxX;
  } else if (Player::player->ship->m_x < -maxX) {
    Player::player->ship->m_x = -maxX;
  }
  if (Player::player->ship->m_z > maxZ) {
    Player::player->ship->m_z = maxZ;
  } else if (Player::player->ship->m_z < -maxZ) {
    Player::player->ship->m_z = -maxZ;
  }

  if((Player::player->ship->m_speed > 0) && (m_land->CheckLineCollision(Player::player->ship->m_x, Player::player->ship->m_z, 6, shipLine, 1.25))) {
    Player::player->ship->m_speed = 0;
    Player::player->ship->SetSails(NOSAILS);
  }

  m_land->Update(ticks);

  m_rotx += .001 * ticks;
  if (m_rotx > TWO_PI) m_rotx -= TWO_PI;

  m_ticksToNextBattle -= ticks;
  if (m_ticksToNextBattle <= 0) {
    SetTicksToNextBattle();

    if (m_childState != NULL) {
      Log::s_log->Message("Warning: m_childState not null when entering naval battle");
      delete m_childState;
      m_childState = NULL;
    }
    m_childState = new StateBattle();
    PrepareStateSwitch(m_childState, this);
  }
}

void StateSailing::Dump() {
  Player::player->Dump();
}

void StateSailing::SetTicksToNextBattle() {
  m_ticksToNextBattle = (int) (Config::s_config->GetTicksBetweenBattles() * (randBellCurve() + .5));
}

void StateSailing::SwitchTo(IGameState *oldState) {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  m_camera->SetProjection();

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);
  glLightfv(GL_LIGHT0, GL_AMBIENT, m_ambientLight);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, m_diffuseLight);
  glLightfv(GL_LIGHT0, GL_POSITION, m_lightPosition);
  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);
}

void StateSailing::SwitchFrom() {
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
}
