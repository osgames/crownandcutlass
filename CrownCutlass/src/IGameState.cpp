/* Crown and Cutlass
 * IGameState Code
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "SDL.h"
#include "Log.h"
#include "Config.h"
#include "image.h"
#include "Texture.h"
#undef DELETE
#include "MainMenu.h"
#include "Menu/CCGui.h"
#include "Menu/CCLabel.h"
#include "font.h"
#include "IGameState.h"

using namespace std;

IGameState *IGameState::s_mainMenu = NULL;
IGameState **IGameState::s_currentPtr = NULL;

unsigned char IGameState::s_nextScreen = 0;

IGameState::IGameState(IGameState *prev) {
  m_previous = prev;
  m_next = NULL;

  m_top.reset(new gcn::Container());
  // Set the dimension of the top container to match the screen.
  m_top->setDimension(gcn::Rectangle(0, 0,
                      Config::s_config->GetWinWidth(),
                      Config::s_config->GetWinHeight()));
}

IGameState::~IGameState() {
  CCGui::s_CCGui->SetTopContainer(NULL);
}

// Static function to set mainMenu pointer
void IGameState::SetMainMenu(IGameState *menu) {
  if ((menu != NULL) && (s_mainMenu != NULL)) {
    Log::s_log->Message("Warning: setMainMenu called with non-null mainMenu pointer");
  }
  s_mainMenu = menu;
}

// Static function to set currentPtr pointer
void IGameState::SetCurrentPtr(IGameState **current) {
  if (s_currentPtr != NULL) {
    Log::s_log->Message("Warning: setCurrentPtr called with non-null currentPtr pointer");
  }
  s_currentPtr = current;
}

void IGameState::Loop(const unsigned int ticks) {
  Update(ticks);

  // Don't need to clear color buffer, since every pixel is redrawn
  // Just do it anyway, while it's in development
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  Display();

  try {
    glPushAttrib(GL_ENABLE_BIT|GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    glLoadIdentity();
    EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
    CCGui::s_CCGui->UpdateGui();
    //ExitTextMode();
    glPopMatrix();
    glPopAttrib();
  } catch (gcn::Exception e) {
    Log::s_log->Message("Guichan exception: %s\n\t%s: line %i", e.getMessage().c_str(), e.getFilename().c_str(), e.getLine());
  }

  SDL_GL_SwapBuffers();

  if (m_next != NULL) {
    SwitchStates();
  }
}

void IGameState::DoExit() {
  // The MainMenu object needs to overload this
  if (s_mainMenu != NULL) {
    s_mainMenu->DoExit();
  }
}

void IGameState::TakeScreenshot() {
  string buffer;
  ifstream fileStream;

  bool loop = false;
  bool done = false;
  unsigned char count = s_nextScreen;

  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  while (!done) {
    if (count == 0) {
      if (loop == true) {
        Log::s_log->Message("Error: All 256 screenshot names are taken, could not take screenshot");
        return;
      }
      else {
        loop = true;
      }
    }

    // Create the file name
    ostringstream outputString;
    outputString << (int) count;
    buffer = "Screenshot" + outputString.str() + ".tga";

    // Check to see if the file already exists
    fileStream.open(buffer.c_str(), ios::in);
    if (!fileStream) {
      // The file doesn't exist, so this is the name to use
      done = true;
    } else {
      // A file by that name already exists, close the file & look for new name
      fileStream.close();
    }

    count++;
  }

  s_nextScreen = count;

  SaveScreenshot(buffer.c_str(), width, height);
  Log::s_log->Message((string) "Screenshot saved as " + buffer);
}

void IGameState::SwitchStates() {
  if (m_next == NULL) {
    Log::s_log->Message("Warning: SwitchState called with null m_next");
    return;
  }

  // Prepare to leave this state
  SwitchFrom();

  // Prepare to enter the next state
  m_next->SwitchTo(this);
  m_next->PrepareTopContainer();

  // Hand control over to the next state
  *s_currentPtr = m_next;
  m_next = NULL;
}

void IGameState::PrepareStateSwitch(IGameState *next, IGameState *prev) {
  if (m_next != NULL){
    Log::s_log->Message("Warning: SetNextState called on a non-null m_next");
  }

  // Set the next pointer, so at the end of the game loop, the state is changed
  m_next = next;

  // Set the next state's previous pointer, if it's not NULL
  // If it is null, just leave the previous pointer alone
  if (prev != NULL) {
    m_next->m_previous = prev;
  }
}

void IGameState::DisplaySplashScreen(const string imageName, const string text, const string fontName, float shade) {
  bool needsText = text.length() > 0;
  gcn::Widget* oldTop;
  gcn::Container* tempTop;
  auto_ptr<CCLabel> myLabel;

  // Temp storage for window width & height
  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  auto_ptr<Texture> texture(new Texture(imageName));

  // Get ready for text mode
  EnterTextMode(width, height);

  DisplayBackground(texture->GetTexture(), shade);

  // Only do all of the guichan work if there is text to display
  if (needsText) {
    tempTop = new gcn::Container();
    // Set the dimension of the top container to match the screen.
    tempTop->setDimension(gcn::Rectangle(0, 0,
                          Config::s_config->GetWinWidth(),
                          Config::s_config->GetWinHeight()));

    tempTop->setOpaque(false);
    tempTop->setVisible(true);

    myLabel.reset(new CCLabel(text, fontName));
    tempTop->add(myLabel.get(), (Config::s_config->GetWinWidth() / 2) - (myLabel->getWidth() / 2),
                 (Config::s_config->GetWinHeight() / 2) - (myLabel->getHeight() / 2));

    oldTop = CCGui::s_CCGui->GetTopContainer();
    CCGui::s_CCGui->SetTopContainer(tempTop);
    CCGui::s_CCGui->UpdateGui();
  }

  SDL_GL_SwapBuffers();

  if (needsText) {
    CCGui::s_CCGui->SetTopContainer(oldTop);
    delete tempTop;
  }
}



void IGameState::DisplayBackground(GLuint myLoadingTexture, float shade) {
  // Temp storage for window width & height
  int width = Config::s_config->GetWinWidth();
  int height = Config::s_config->GetWinHeight();

  // Clear the depth buffer
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // Set the color
  glColor3f(shade, shade, shade);

  glEnable(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, myLoadingTexture);

  // Draw the actual background quad
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex3f(0, 0, 0);
  glTexCoord2f(0, 1);
  glVertex3f(0, height, 0);
  glTexCoord2f(1, 1);
  glVertex3f(width, height, 0);
  glTexCoord2f(1, 0);
  glVertex3f(width, 0, 0);
  glEnd();
}

void IGameState::PrepareTopContainer() {
  if (m_top.get() != NULL) {
    m_top->setOpaque(false);
    m_top->setVisible(true);
  }
  CCGui::s_CCGui->SetTopContainer(m_top.get());
}
