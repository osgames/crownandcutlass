/* Crown and Cutlass
 * Economy Class
 *
 */

#include <string>
#include <boost/shared_ptr.hpp>
#include "SDL.h"
#include "GLee.h"
#include "Player.h"
#include "Cargo.h"
#include "Product.h"
#include "Log.h"
#include "tinyxml.h"
#include "CityManager.h"
#include "CityEconomy.h"
#include "Economy.h"

using namespace std;
using boost::shared_ptr;

Economy *Economy::s_economy = NULL;

Economy::Economy(const char* filename) {
  TiXmlNode* Products;
  TiXmlNode* ForProducts;
  TiXmlElement* Prod;
  TiXmlText* data;
  string prodName;
  int Id;
  int DefaultValue;

	// We load the products here from Product.xml->CandC->Products
  doc.LoadFile( filename );
  if (doc.Error()) {
    Log::s_log->Message("Error: Could not open Economy File.\n");
    exit(-1);
  }

  Products = doc.FirstChild("CandC")->FirstChild("Products");

  for(ForProducts = Products->FirstChildElement("Name");
      ForProducts;
      ForProducts = ForProducts->NextSibling("Name")) {

    if (ForProducts == NULL) throw -1;
    Prod = ForProducts->ToElement();
		if (Prod == NULL) throw -2;
		if (Prod->QueryIntAttribute("id", &Id) != TIXML_SUCCESS)
			throw string("Could not parse Product ID");
		if (Prod->QueryIntAttribute("default", &DefaultValue) != TIXML_SUCCESS)
			throw string("Could not parse Product Default Value");
    data = Prod->FirstChild()->ToText();
    prodName = data->Value();

    ProdList.push_back(prodName);
		ProductDefaults.push_back(DefaultValue);
  }

  num_steps = 0;
}

Economy::~Economy() {
  map<string, CityEconomy*>::iterator i;

  for (i = CityList.begin(); 
			 i != CityList.end(); 
			 i++)
		delete i->second;

  CityList.clear();

}

bool Economy::CreateEconomy() {
  if (s_economy != NULL) {
    Log::s_log->Message("Warning: Multiple calls to CreateEcon()");
    return false;
  }

  s_economy = new Economy("data/Product.xml");

  return true;
}

void Economy::DestroyEconomy() {
  if (s_economy == NULL) {
    Log::s_log->Message("Warning: Call to DestroyEconomy with NULL Econ");
    return;
  }
  delete s_economy;
  s_economy = NULL;
}

void Economy::Step() {
  UpdateEconomy();
}

void Economy::UpdateEconomy() {
  map<string, CityEconomy*>::iterator i;

  for (i = CityList.begin(); 
			 i != CityList.end(); 
			 i++)
		i->second->UpdateEconomy(num_steps);

  num_steps = 0;
}

void Economy::Print() {
  map<string, CityEconomy*>::iterator i;

  for (i = CityList.begin(); 
			 i != CityList.end(); 
			 i++)
		i->second->Print();
}

vector<Product*> Economy::ParseCityProducts(TiXmlElement* CityElement) {
	int Consumption;
	int Production;
	int Id;
	TiXmlElement* ProductNode;
	Product* ProductHolder;
	vector<Product*> Result;
	
	for (ProductNode = CityElement->FirstChildElement("Product");
			 ProductNode;
			 ProductNode = ProductNode->NextSiblingElement("Product")) {
		if (ProductNode->QueryIntAttribute("id", &Id) != TIXML_SUCCESS) {
			Log::s_log->Message("Could not parse Product ID");
			throw 0;
		}
		if (ProductNode->QueryIntAttribute("consumption", &Consumption) != TIXML_SUCCESS) {
			Log::s_log->Message("Could not parse Product Consumption");
			throw 1;
		}
		if (ProductNode->QueryIntAttribute("production", &Production) != TIXML_SUCCESS) {
			Log::s_log->Message("Could not parse Product Production");
			throw 2;
		}
		
		ProductHolder = new Product(Id, Consumption, Production);
		Result.push_back(ProductHolder);
	}
	return Result;
}

CityEconomy* Economy::CreateCity(const char* CityName){
  TiXmlNode* City;
  TiXmlElement* CityElement;
  vector<Product*> ProductList;
  CityEconomy* tempCE;
  string name;
  string cityName;
  int size;
	
  EconomyNode = doc.FirstChild("CandC")->FirstChild("Economy");

  for(City = EconomyNode->FirstChildElement("City");
      City;
      City = City->NextSibling("City")) {

		if (City == NULL) throw -1;
		CityElement = City->ToElement();
			
    cityName = CityElement->Attribute("name");
    size = atoi(CityElement->Attribute("size"));
    if (cityName == CityName) {
			ProductList = ParseCityProducts(CityElement);
      tempCE = new CityEconomy(cityName.c_str(), ProductList, size);

      CityList[tempCE->GetCityName()] = tempCE;

      ProductList.clear();
      break;
    } // if (cityName == CityName)

  } // for
  return tempCE;
}

bool Economy::CheckCities(CityManager* tempManager) {
  map<string, CityEconomy*>::iterator i;

  for (i = CityList.begin(); 
			 i != CityList.end(); 
			 i++) {
    if (!tempManager->CheckCity(i->second->GetCityName().c_str()))
      return false;
  }

  return true;
}

int Economy::ProductCount() {
  return (int)ProdList.size();
}

void Economy::BuyProduct(int ProdID, float price) {
  shared_ptr<Cargo> playerCargo = Player::player->GetCargo();

  try {
    // Make sure the player can afford it and has room for it
    if ((Player::player->gold < price) || (playerCargo->GetAvailQty() <= 0)) return;

    playerCargo->AddQty(ProdID, 1);
    Player::player->gold -= (int) price;
   } catch (const char *e) {
     Log::s_log->Message(e);
   }
}

void Economy::SellProduct(int ProdID, float price) {
  shared_ptr<Cargo> playerCargo = Player::player->GetCargo();

  try {
    if (playerCargo->GetQty(ProdID) <= 0) return;

    playerCargo->AddQty(ProdID, -1);
    Player::player->gold += (int) price;
  } catch (const char *e) {
    Log::s_log->Message(e);
  }
}

int Economy::FindID(string Name) {
  unsigned int i;

  for(i = 0; i < ProdList.size(); i++) {
    if (ProdList[i] == Name) return (int)i;
  }

  return -1;
}
