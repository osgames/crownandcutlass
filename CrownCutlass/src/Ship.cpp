/* Crown and Cutlass
 * Ship Code
 */

#include <string>
//#include <exception>
#include "ccmath.h"
#include "Log.h"
#include "tinyxml.h"
#include "ResourceManager/ResourceManager.h"
#include "ResourceManager/IResource.h"
#include "ResourceManager/ShipResource.h"
#include "Cargo.h"
#include "ISaveObject.h"
#include "Ship.h"

using namespace std;

string SailStateStrs[FULLSAILS + 1] = {"None", "Half", "Full"};

Ship::Ship(string name, string shipTitle): ISaveObject("Ship") {
  m_resource = NULL;
  AcquireResource(name);

  m_title = shipTitle;

  Reset();
}

Ship::Ship(string shipTitle): ISaveObject("Ship") {
  m_resource = (ShipResource *) ResourceManager::s_resourceManager->AcquireRandom(ShipResource::s_type);

  m_title = shipTitle;

  Reset();
}

Ship::Ship(Ship *copy): ISaveObject("Ship") {
  m_resource = NULL;
  AcquireResource(copy->m_resource->GetName());

  m_x = copy->m_x;
  m_z = copy->m_z;
  m_rot = copy->m_rot;
  m_speed = copy->m_speed;
  m_rotSpeed = copy->m_rotSpeed;

  m_title = copy->m_title;
  m_damage = copy->m_damage;
  m_cargo = copy->m_cargo;
  m_sails = copy->m_sails;
  m_rudder = copy->m_rudder;
  m_state = copy->m_state;
  m_shipSine = copy->m_shipSine;
  m_shipCos = copy->m_shipCos;
  m_rotX = copy->m_rotX;
  m_rotXAngle = copy->m_rotXAngle;
}

Ship::~Ship() {
  ReleaseResource();

  // Don't need to delete this, the shared_ptr takes care of that
  //delete cargo;
}

void Ship::Draw(bool doTranslate, float dX, float dZ) {
  glPushMatrix();
  if (doTranslate) {
    glTranslatef(m_x + dX, 0, m_z + dZ);
  }
  glRotatef(radiansToDegrees(m_rot), 0, 1, 0);
  glRotatef(m_rotX, 1, 0, 0);
  m_resource->Draw();
  glPopMatrix();
}

int Ship::GetMaxDamage() {
  return m_resource->GetMaxDamage();
}

shared_ptr<Cargo> Ship::GetCargo() {
  return m_cargo;
}

void Ship::Dump() {
  Log::s_log->Message("Ship \"%s\"", m_title.c_str());
  Log::s_log->Message("Damage: %d", m_damage);
  Log::s_log->Message("Rot: %f", m_rot);
  Log::s_log->Message("RotSpeed: %f", m_rotSpeed);
  Log::s_log->Message("Position: (%f, %f)", m_x, m_z);
  Log::s_log->Message("Speed: %f", m_speed);
  Log::s_log->Message("Sails: %d", m_sails);
  Log::s_log->Message("Rudder: %d", m_rudder);
  m_cargo->Dump();
}

void Ship::Reset() {
  unsigned int cargoSize;

  m_x = 0.0;
  m_z = 0.0;
  m_rot = 0.0;
  m_speed = 0.0;
  m_rotSpeed = 0.0;
  m_shipSine = 0.0;
  m_shipCos = 0.0;

  m_damage = m_resource->GetMaxDamage();

  cargoSize = m_resource->GetCargoSize();
  m_cargo = shared_ptr<Cargo>(new Cargo(m_title + " Cargo", cargoSize));

  m_sails = NOSAILS;
  m_rudder = CENTER;
  m_state = NORMAL;

  m_rotX = 0.0;
  m_rotXAngle = 0.0;
}

void Ship::Update(const unsigned int ticks) {
  const float rudderConst = 0.6;

  float sailMult = 0.0;
  float forceSails;
  float forceDrag;
  float accel;
  float rotAccel;
  float forceRudder;
  float rudderDragMult;

  float angleRudder = 0.0;

  switch (m_sails) {
    case NOSAILS:
      sailMult = 0.0;
      break;
    case HALFSAILS:
      sailMult = 0.5;
      break;
    case FULLSAILS:
      sailMult = 1.0;
      break;
  }

  forceSails = m_resource->GetSailArea() * sailMult * .002;
  forceDrag = m_resource->GetDrag() * ((100 * m_speed) * (100 * m_speed));
  accel = (forceSails + forceDrag) / m_resource->GetMass();

  m_speed = m_speed + (accel * ticks * .001);

  switch (m_rudder) {
    case LEFT:
      angleRudder = M_PI_2;
      break;
    case RIGHT:
      angleRudder = -1 * M_PI_2;
      break;
    case CENTER:
      angleRudder = 0.0;
  }

  // The rudder physics aren't quite right...

  // Note: This is opposite of what you might expect since info->getDrag is negative
  if (m_rotSpeed > 0) {
    rudderDragMult = 1;
  } else {
    rudderDragMult = -1;
  }

  forceRudder = rudderConst * m_resource->GetRudderArea() * m_speed * m_speed * angleRudder;
  forceDrag = rudderDragMult * m_resource->GetDrag() * 100 * m_rotSpeed * m_rotSpeed;

  rotAccel = (m_resource->GetDistToRudder() * (forceRudder + forceDrag)) / m_resource->GetMassMomentInertia();

  //Log::s_log->Message("%f = (%f) / (%f)", rotAccel, (forceRudder + forceDrag), (info->getMass() * info->getDistToRudder()));

  m_rotSpeed = m_rotSpeed + (rotAccel * ticks);

  m_rot = m_rot + (m_rotSpeed * ticks);

  if (m_rot < 0) {
    m_rot += TWO_PI;
  }
  if (m_rot >= TWO_PI) {
    m_rot -= TWO_PI;
  }
  //Log::s_log->Message("%f, %f", rotSpeed, rot);

  m_shipCos = cos(m_rot);
  m_shipSine = sin(m_rot);

  // Update the position
  m_x += m_shipCos * m_speed * ticks;
  m_z -= m_shipSine * m_speed * ticks;

  switch (m_state) {
    case NORMAL:
      m_rotXAngle += .001 * ticks;
      if (m_rotXAngle > TWO_PI) m_rotXAngle -= TWO_PI;
      m_rotX = 5 * sin(m_rotXAngle);
      break;
    case SINKING:
      m_rotX += .02 * ticks;

      if (m_rotX > 120) {
        m_state = SUNK;
      }
      break;
    case SUNK:
      break;
  }
}

void Ship::RaiseSails() {
  if ((m_state == NORMAL) && (m_sails != FULLSAILS)) {
    m_sails = SailState(m_sails + 1);
  }
}

void Ship::LowerSails() {
  if ((m_state == NORMAL) && (m_sails != NOSAILS)) {
    m_sails = SailState(m_sails - 1);
  }
}

void Ship::SetSails(SailState newSails) {
  if (m_state == NORMAL) {
    m_sails = newSails;
  }
}

SailState Ship::GetSails() {
  return m_sails;
}

const std::string& Ship::GetSailsStr() {
  return GetSailStateStr(m_sails);
}

const std::string& Ship::GetSailStateStr(SailState sails) {
  return SailStateStrs[sails];
}

void Ship::SetRudder(RudderState newRudder) {
  if (m_state == NORMAL) {
    m_rudder = newRudder;
  }
}

void Ship::SinkShip() {
  if (m_state == NORMAL) {
    m_state = SINKING;
    m_sails = NOSAILS;
  } else {
    Log::s_log->Message("Warning: Attempt to sink already sinking ship");
  }
}

ShipState Ship::GetShipState() {
  return m_state;
}

bool Ship::IsShipNormal() {
  return m_state == NORMAL;
}

bool Ship::IsShipSinking() {
  return m_state == SINKING;
}

bool Ship::IsShipSunk() {
  return m_state == SUNK;
}

void Ship::GetDirectionVector(double output[3]) {
  output[0] = m_shipCos;
  output[1] = 0;
  output[2] = m_shipSine;
}

void Ship::Save(TiXmlElement *parent) {
  TiXmlElement selfNode(m_XMLName);

  selfNode.SetAttribute("name", m_resource->GetName());
  selfNode.SetAttribute("title", m_title);
  selfNode.SetAttribute("damage", m_damage);
  selfNode.SetDoubleAttribute("speed", m_speed);
  selfNode.SetDoubleAttribute("rot", m_rot);
  selfNode.SetDoubleAttribute("rotSpeed", m_rotSpeed);
  selfNode.SetDoubleAttribute("x", m_x);
  selfNode.SetDoubleAttribute("z", m_z);
  selfNode.SetAttribute("sails", m_sails);
  selfNode.SetAttribute("rudder", m_rudder);

  m_cargo->Save(&selfNode);
  parent->InsertEndChild(selfNode);
}

void Ship::Load(TiXmlElement *parent) {
  TiXmlElement *selfNode;
  string name;

  selfNode = parent->FirstChildElement(m_XMLName);

  name = string(selfNode->Attribute("name"));
  if (name != m_resource->GetName()) {
    ReleaseResource();
    AcquireResource(name);
  }

  // Load the title
  m_title = string(selfNode->Attribute("title"));

  // Load the ship's damage
  if (selfNode->QueryIntAttribute("damage", &m_damage) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: damage attribute missing in ship node");
    return;
  }

  // Load the speed
  if (selfNode->QueryFloatAttribute("speed", &m_speed) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: speed attribute missing in ship node");
    return;
  }

  // Load the rot
  if (selfNode->QueryFloatAttribute("rot", &m_rot) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: rot attribute missing in ship node");
    return;
  }

  // Load the rotSpeed
  if (selfNode->QueryFloatAttribute("rotSpeed", &m_rotSpeed) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: rotSpeed attribute missing in ship node");
    return;
  }

  // Load the x
  if (selfNode->QueryFloatAttribute("x", &m_x) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: x attribute missing in ship node");
    return;
  }

  // Load the z
  if (selfNode->QueryFloatAttribute("z", &m_z) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: z attribute missing in ship node");
    return;
  }

  // Load the sails' state
  if (selfNode->QueryIntAttribute("sails", (int *) &m_sails) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: sails attribute missing in ship node");
    return;
  }

  // Load the rudder's state
  if (selfNode->QueryIntAttribute("rudder", (int *) &m_rudder) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: rudder attribute missing in ship node");
    return;
  }

  m_cargo->Load(selfNode);
}

void Ship::AcquireResource(string name) {
  if (m_resource != NULL) {
    Log::s_log->Message("Attempt to re-acquire ship resource");
    return;
  }
  m_resource = (ShipResource *) ResourceManager::s_resourceManager->Acquire(IResource::ConstructKey(ShipResource::s_type, name));
}

void Ship::ReleaseResource() {
  if (m_resource == NULL) {
    Log::s_log->Message("Attempt to re-release ship resource");
    return;
  }
  ResourceManager::s_resourceManager->Release(m_resource);
  m_resource = NULL;
}


// ShipInfo code
/*
ShipInfo::ShipInfo(string nameIn, string modelFile, string texFile, float scaleIn, int damageIn, unsigned int cargoSizeIn, float massIn, float sailAreaIn, float dragIn, float rudderAreaIn, float distToRudderIn) {
  modelFileName = modelFile;
  textureFileName = texFile;

  refCount = 0;

  model = NULL;

  // Set up ship stats
  name = nameIn;
  scale = scaleIn;
  maxDamage = damageIn;
  cargoSize = cargoSizeIn;
  mass = massIn;
  sailArea = sailAreaIn;
  drag = dragIn;
  rudderArea = rudderAreaIn;
  distToRudder = distToRudderIn;
  massMomentInertia = -1.0;
}

ShipInfo::~ShipInfo() {
  if (refCount != 0) {
    unload();
    Log::s_log->Message("Warning: ShipInfo destructor called with refCount != 0");
  }

}

void ShipInfo::load() {
  if (model != NULL) {
    Log::s_log->Message("Warning: Loading already loaded ship");
  }

  model = new Model(textureFileName.c_str());
  model->loadObj(modelFileName.c_str(), scale);

  if (massMomentInertia < 0.0) {
    calcMassMomentInertia();
  }
}

void ShipInfo::unload() {
  if (model == NULL) {
    Log::s_log->Message("Warning: Unloading already unloaded ship");
    return;
  }

  delete model;
  model = NULL;
}
*/
