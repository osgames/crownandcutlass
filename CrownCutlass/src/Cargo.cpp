/* Crown and Cutlass
 * Cargo Object Code
 */

#include <vector>
#include <string>
#include "Log.h"
#include "ccmath.h"
#include "Economy.h"
#include "ISaveObject.h"
#include "Cargo.h"

Cargo::Cargo(string nameIn, unsigned int sizeIn): ISaveObject("Cargo") {
  size = sizeIn;

  name = nameIn;

  Clear();
}

Cargo::~Cargo() {
  Log::s_log->Message("Deleting " + name);
  // Nothing really needs to happen
  products.clear();
}

void Cargo::Clear() {
  filled = 0;

  products.resize(Economy::s_economy->ProductCount());

  for (unsigned int i = 0; i < products.size(); i++) {
    products[i] = 0;
  }
}

unsigned int Cargo::GetFilled() {
  return filled;
}

unsigned int Cargo::GetSize() {
  return size;
}

void Cargo::SetSize(unsigned int sizeIn) {
  size = sizeIn;
}

unsigned int Cargo::GetAvailQty() {
  return size - filled;
}

unsigned int Cargo::GetQty(unsigned int product) throw ( const char *) {
  if (product >= products.size()) {
    throw "Invalid product";
  }

  return products[product];
}

void Cargo::SetQty(unsigned int product, unsigned int qty) throw ( const char *) {
  unsigned int oldQty;

  if (product >= products.size()) {
    throw "Invalid product";
  }

  oldQty = products[product];
  products[product] = qty;

  filled = filled + (qty - oldQty);
}

unsigned int Cargo::AddQty(unsigned int product, int qty) throw ( const char *) {
  if (product >= products.size()) {
    throw "Invalid product";
  }
  if ((qty < 0) && (-1*qty > products[product])) {
    throw "Result would be negative";
  }

  products[product] = products[product] + qty;
  filled += qty;

  return products[product];
}

void Cargo::AddCargo(shared_ptr<Cargo> cargo) throw ( const char *) {
  int i;
  unsigned int qty;

  if (cargo->products.size() != products.size()) {
    throw "Could not AddCargo (sizes do not match)";
  }

  Log::s_log->Message("Prod.size: %d", products.size());

  for (i = products.size() - 1; i >= 0; i--) {
    if (GetAvailQty() == 0) break;


    Log::s_log->Message("Avail: %d", GetAvailQty());
    Log::s_log->Message("i: %d", i);

    qty = min(cargo->GetQty(i), GetAvailQty());
    cargo->AddQty(i, -1 * qty);
    AddQty(i, qty);
  }
}

void Cargo::FillRandom() throw ( const char*) {
  FillRandom((int) (size * randBellCurve()));
}

void Cargo::FillRandom(unsigned int qty) throw ( const char *) {
  unsigned int avgQty;
  unsigned int tempQty;

  if (qty > size) {
    throw "Qty greater than size";
  }

  avgQty = qty / products.size();
  Log::s_log->Message("AvgQty: %u", avgQty);

  // Loop through all but last
  for (unsigned int i = 0; i < (products.size() - 1); i++) {
    tempQty = (int) ((randBellCurve() + .5) * avgQty);
    if (tempQty > GetAvailQty()) {
      tempQty = GetAvailQty();
    }

    products[i] = tempQty;

    filled += tempQty;

    // If it's full, there's no sense in keeping going
    if (GetAvailQty() == 0) {
      break;
    }
  }

  products[products.size()-1] = qty - filled;
  filled = qty;
}

void Cargo::Save(TiXmlElement *parent) {
  unsigned int i;
  TiXmlElement selfNode(m_XMLName);
  TiXmlElement prodsElem("Products");
  TiXmlElement tElement("Product");

  for (i = 0; i < products.size(); i++) {
    tElement.Clear();
    tElement.SetAttribute("name", Economy::s_economy->ProdList[i]);
    tElement.SetAttribute("qty", products[i]);
    prodsElem.InsertEndChild(tElement);
  }

  selfNode.SetAttribute("size", size);
  selfNode.SetAttribute("filled", filled);
  selfNode.InsertEndChild(prodsElem);

  parent->InsertEndChild(selfNode);
}

void Cargo::Load(TiXmlElement *parent) {
  TiXmlElement *selfNode;
  TiXmlElement *prodsElem;
  TiXmlElement *node;
  unsigned int sumQty = 0;
  unsigned int prodIndex = 0;
  unsigned int tempQty;

  Clear();

  selfNode = parent->FirstChildElement(m_XMLName);

  if (selfNode->QueryIntAttribute("size", (int *) &size) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: size attribute missing in cargo node");
    return;
  }

  if (selfNode->QueryIntAttribute("filled", (int *) &filled) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: filled attribute missing in cargo node");
    return;
  }

  if (filled > size) {
    // Should throw an exception
    Log::s_log->Message("Warning: filled > size");
    return;
  }

  // Load the actual products
  prodsElem = selfNode->FirstChildElement("Products");
  if (prodsElem == NULL) {
    // Should throw an exception
    Log::s_log->Message("Warning: Could not find Products node");
    return;
  }

  for (node = prodsElem->FirstChildElement("Product"); node; node = node->NextSiblingElement("Product")) {
    if (prodIndex >= products.size()) {
      // Should throw exception
      Log::s_log->Message("Warning: Too many products in cargo node");
      return;
    }

    if (string(node->Attribute("name")) != Economy::s_economy->ProdList[prodIndex]) {
      // It's not the product we expected
      Log::s_log->Message("Warning: expected product \"%s\", found \"%s\"", Economy::s_economy->ProdList[prodIndex].c_str(), node->Attribute("name"));
      return;
    }

    if (node->QueryIntAttribute("qty", (int *) &tempQty) != TIXML_SUCCESS) {
      // Should throw exception
      Log::s_log->Message("Warning: qty attribute missing in cargo product node");
      return;
    }

    products[prodIndex] = tempQty;

    // Increment the sum
    sumQty += tempQty;

    // Move on to the next product
    prodIndex++;
  }

  // Make sure the actual qty matches the expected qty
  if (sumQty != filled) {
    // Should throw an exception
    Log::s_log->Message("Warning: Filled qty (%d) does not equal sum of products (%d)", filled, sumQty);
    return;
  }
}

void Cargo::Dump() {
  Log::s_log->Message("%s", name.c_str());
  Log::s_log->Message("Size:\t%u", size);
  Log::s_log->Message("Filled:\t%u", filled);

  for (unsigned int i = 0; i < products.size(); i++) {
    Log::s_log->Message("%s:\t%u", Economy::s_economy->ProdList[i].c_str(), products[i]);
  }
}
