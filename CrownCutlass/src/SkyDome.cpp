/* Crown and Cutlass
 * Sky Dome Object Code
 */

#include <string>
#include "Log.h"
#include "ccmath.h"
#include "Texture.h"
#include "SkyDome.h"

using namespace std;

SkyDome::SkyDome(float radius, int slices, int stacks, string textureName) {
  m_colorTexture = new Texture(textureName);

  m_list = glGenLists(1);
  glNewList(m_list, GL_COMPILE);

  DrawDome(radius, slices, stacks);

  glEndList();
}

SkyDome::~SkyDome() {
  // Free the list
  glDeleteLists(m_list, 1);

  delete m_colorTexture;
}

void SkyDome::DrawDome(float radius, int slices, int stacks) {
  const float centerRad = degreesToRadians(5);
  int i;

  GLfloat material[4];
  float tempX, tempY, tempZ;

  float dSlice = TWO_PI / slices;
  float dStack = (M_PI_2 - centerRad) / (stacks - 1);
  //float maxY = getHeight(radius, 0);

  material[0] = 1.0;
  material[1] = 1.0;
  material[2] = 1.0;
  material[3] = 1.0;

  glDepthMask(GL_FALSE);

  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
  m_colorTexture->BindTexture();

  glNormal3f(0, 1, 0);

  for (i = 0; i < slices; ++i) {
    glBegin(GL_TRIANGLE_STRIP);
    for (int j = 0; j < stacks; ++j) {
      tempX = GetX((i+1) * dSlice, (j * dStack) + centerRad, radius);
      tempY = GetHeight(radius, (j * dStack) + centerRad);
      tempZ = GetZ((i+1) * dSlice, (j * dStack) + centerRad, radius);
      //glTexCoord2f(tempY / maxY, 0);
      glTexCoord2f((tempX/radius + 1) / 2, 0);
      glVertex3f(tempX, tempY, tempZ);

      tempX = GetX(i * dSlice, (j * dStack) + centerRad, radius);
      // TempY doesn't change
      tempZ = GetZ(i * dSlice, (j * dStack) + centerRad, radius);
      //glTexCoord2f(tempY / maxY, 0);
      glTexCoord2f((tempX/radius + 1) / 2, 0);
      glVertex3f(tempX, tempY, tempZ);
    }
    glEnd();
  }

  glBegin(GL_TRIANGLE_FAN);
  // This is the very center of the dome, so it's tex coord is .5, .5
  glTexCoord2f(0.5, 0);
  //glTexCoord2f(1, 0);
  glVertex3f(0, GetHeight(radius, 0), 0);
  for (i = 0; i <= slices; ++i) {
    tempX = GetX(i * dSlice, centerRad, radius);
    tempY = GetHeight(radius, centerRad);
    tempZ = GetZ(i * dSlice, centerRad, radius);
    //glTexCoord2f(tempY / maxY, 0);
    glTexCoord2f((tempX/radius + 1) / 2, 0);
    glVertex3f(tempX, tempY, tempZ);
  }
  glEnd();

  glDepthMask(GL_TRUE);
}

void SkyDome::Draw(float time) {
  if (time >= 24) {
    Log::s_log->Message("Sky dome drawn with time (%f) >= 24", time);
    time = 0;
  }

  glMatrixMode(GL_TEXTURE);
  glTranslatef(0, time/24.0, 0);

  glCallList(m_list);

  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
}

float SkyDome::GetX(float angle, float radiusAngle, float domeRadius) {
  return sin(radiusAngle) * cos(angle) * domeRadius;
}

float SkyDome::GetZ(float angle, float radiusAngle, float domeRadius) {
  return sin(radiusAngle) * sin(angle) * domeRadius;
}

float SkyDome::GetHeight(float domeRadius, float pointRadiusAngle) {
  return (cos(pointRadiusAngle) * domeRadius) / 2;
}
