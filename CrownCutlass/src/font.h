/* Crown and Cutlass
 * Font Creation / Use Header
 */

#if !defined( _FONT_H_ )

#define _FONT_H_

void EnterTextMode(int width, int height);

void ExitTextMode();

#endif
