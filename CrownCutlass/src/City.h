/* Crown and Cutlass
 * City Class
 *
 * This is inherited from the Environment Object. It will keep
 * the location, name, and economic information of the city.
 */

#if !defined (_CITY_H_)

#define _CITY_H_

#include <vector>
#include "IEnvironmentObject.h"

class Product;
class Terrain;
class CityEconomy;
class BuildingList;

using namespace std;

class City: public IEnvironmentObject {
 public:
  // Constructor
  City(Terrain *landIn, string cityName, int cityLocX, int cityLocY, BuildingList *buildingsIn);

  // Destructor
  ~City();

  // Method
  void DisplayObject();

  bool CheckName(const char* cityName);

  CityEconomy* MyEconomy;

 private:
  // Will probably need some product information here.
  float height;
  Terrain *land;

  // List of buildings to draw this city
  BuildingList *buildings;
};

#endif
