/* Crown and Cutlass
 * Texture Code
 */

#include "GLee.h"
#include "ResourceManager/ResourceManager.h"
#include "ResourceManager/IResource.h"
#include "ResourceManager/TextureResource.h"
#include "Texture.h"

Texture::Texture(std::string name) {
  m_resource = (TextureResource *) ResourceManager::s_resourceManager->Acquire(IResource::ConstructKey(TextureResource::s_type, name));
}

Texture::~Texture() {
  ResourceManager::s_resourceManager->Release(m_resource);
}

GLuint Texture::GetTexture() {
  return m_resource->GetTexture();
}

void Texture::BindTexture() {
  glBindTexture(GL_TEXTURE_2D, m_resource->GetTexture());
}
