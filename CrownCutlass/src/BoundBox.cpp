/* Crown and Cutlass
 * Quad-Tree Bounding Box Object Code
 * Note: For use with the terrain quadtree
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <cstdio>
#include "GLee.h"
#include "BoundBox.h"
#include "Point.h"

BoundBox::BoundBox() {
  m_b1 = NULL;
  m_b2 = NULL;
  m_b3 = NULL;
  m_b4 = NULL;

  m_t1 = NULL;
  m_t2 = NULL;
  m_t3 = NULL;
  m_t4 = NULL;
}

BoundBox::BoundBox(float minX, float maxX, float minY, float maxY, float minZ, float maxZ) {
  m_b1 = new Point(minX, minY, minZ);
  m_b2 = new Point(maxX, minY, minZ);
  m_b3 = new Point(minX, minY, maxZ);
  m_b4 = new Point(maxX, minY, maxZ);

  m_t1 = new Point(minX, maxY, minZ);
  m_t2 = new Point(maxX, maxY, minZ);
  m_t3 = new Point(minX, maxY, maxZ);
  m_t4 = new Point(maxX, maxY, maxZ);
}

BoundBox::~BoundBox() {
  delete m_b1;
  delete m_b2;
  delete m_b3;
  delete m_b4;

  delete m_t1;
  delete m_t2;
  delete m_t3;
  delete m_t4;
}

void BoundBox::SetHeight(float low, float high) {
  if ((m_b1 == NULL) || (m_b2 == NULL) || (m_b3 == NULL) || (m_b4 == NULL)) {
    printf("NULL!\n");
  }
  m_b1->m_y = low;
  m_b2->m_y = low;
  m_b3->m_y = low;
  m_b4->m_y = low;

  delete m_t1;
  delete m_t2;
  delete m_t3;
  delete m_t4;

  m_t1 = new Point(m_b1->m_x, high, m_b1->m_z);
  m_t2 = new Point(m_b2->m_x, high, m_b2->m_z);
  m_t3 = new Point(m_b3->m_x, high, m_b3->m_z);
  m_t4 = new Point(m_b4->m_x, high, m_b4->m_z);
}

void BoundBox::Draw() {
  glBegin(GL_LINES);
  glNormal3f(0, 1, 0);
  m_b1->Draw();
  m_b2->Draw();

  m_b2->Draw();
  m_b4->Draw();

  m_b4->Draw();
  m_b3->Draw();

  m_b3->Draw();
  m_b1->Draw();

  m_b1->Draw();
  m_t1->Draw();

  m_b2->Draw();
  m_t2->Draw();

  m_b3->Draw();
  m_t3->Draw();

  m_b4->Draw();
  m_t4->Draw();

  m_t1->Draw();
  m_t2->Draw();

  m_t2->Draw();
  m_t4->Draw();

  m_t4->Draw();
  m_t3->Draw();

  m_t3->Draw();
  m_t1->Draw();
  glEnd();
}

void BoundBox::Dump() {
  m_b1->Dump();
  m_b2->Dump();
  m_b3->Dump();
  m_b4->Dump();

  m_t1->Dump();
  m_t2->Dump();
  m_t3->Dump();
  m_t4->Dump();
}
