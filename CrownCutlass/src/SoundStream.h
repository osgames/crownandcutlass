/* Crown and Cutlass
 * Sound Stream header
 *
 * This class streams music from disk so that
 * all the music doesn't need to be in memory.
 *
 * First writing is copied from DevMaster.net
 * http://www.devmaster.net/articles/openal-tutorials/lesson8.php
 */

#if !defined( _SOUNDSTREAM_H_ )

#define _SOUNDSTREAM_H_

#include <string>
#include <iostream>
using namespace std;

#include <AL/al.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisenc.h>
#include <vorbis/vorbisfile.h>


#define BUFFER_SIZE (4096 * 8)

class SoundStream
{
  public:
    void open(string path); // obtain a handle to the file
    void release();         // release the file handle
    void display();         // display some info on the Ogg
    bool playback();        // play the Ogg stream
    bool playing();         // check if the source is playing
    bool update();          // update the stream if necessary

    void reload();          // Starts the file from the beginning

  protected:
    bool stream(ALuint buffer);   // reloads a buffer
    void empty();                 // empties the queue
    void check();                 // checks OpenAL error state
    string errorString(int code); // stringify an error code

  private:
    FILE*           oggFile;       // file handle
    OggVorbis_File  oggStream;     // stream handle
    vorbis_info*    vorbisInfo;    // some formatting data
    vorbis_comment* vorbisComment; // user comments

    ALuint buffers[2]; // front and back buffers
    ALuint source;     // audio source
    ALenum format;     // internal format

    string file;      // used to store where the file is
};

#endif
