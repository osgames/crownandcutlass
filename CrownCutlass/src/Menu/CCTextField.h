/* Crown and Cutlass
 * Crown and Cutlass TextField Header
 */

#if !defined( _CCTEXTFIELD_H_ )
#define _CCTEXTFIELD_H_

#include <guichan.hpp>
#include <string>
#include <memory>

class CCFont;

class CCTextField: public gcn::TextField {
  public:
    CCTextField(const std::string& caption, const std::string mainFontName, int width);
    ~CCTextField();

    void SetMainFont(std::string fontName);
  private:
    std::auto_ptr<CCFont> m_mainFont;
};

#endif
