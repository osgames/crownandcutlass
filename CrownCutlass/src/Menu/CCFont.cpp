/* Crown and Cutlass
 * Font Header
 */

#include <string>
#include <guichan.hpp>
#include "../ResourceManager/ResourceManager.h"
#include "../ResourceManager/IResource.h"
#include "../ResourceManager/FontResource.h"
#include "CCFont.h"

using namespace std;

CCFont::CCFont(string name) {
  m_resource = (FontResource *) ResourceManager::s_resourceManager->Acquire(IResource::ConstructKey(FontResource::s_type, name));
}

CCFont::~CCFont() {
  ResourceManager::s_resourceManager->Release(m_resource);
}

gcn::ImageFont* CCFont::GetImageFont() {
  return m_resource->GetImageFont();
}
