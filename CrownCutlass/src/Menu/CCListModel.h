/*
 * CCListModel Class
 * Crown and Cutlass
 */
 
#if !defined( _CCLISTMODEL_H_)

#define _CCLISTMODEL_H_

#include <guichan.hpp>
#include <string>
#include <vector>

using namespace std;

class CCListModel: public gcn::ListModel {
	public:
		CCListModel();
		virtual ~CCListModel();
	
		void AddItem(std::string Item);
		void AddItem(std::string Item, int Position);
	
		void RemoveItem(std::string Item);
		void RemoveItem(int Position);
		
		int getNumberOfElements();
	
		std::string getElementAt(int i);
	private:
		vector<const char*> m_List;
};

#endif
