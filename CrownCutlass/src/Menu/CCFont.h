/* Crown and Cutlass
 * CCFont Header
 */

#if !defined ( _CCFONT_H_ )
#define _CCFONT_H_

#include <guichan.hpp>

class FontResource;

class CCFont {
  public:
    CCFont(std::string name);
    ~CCFont();

    gcn::ImageFont *GetImageFont();

  private:
    FontResource *m_resource;
};

#endif
