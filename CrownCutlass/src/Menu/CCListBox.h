/* Crown and Cutlass
 * Crown and Cutlass ListBox Header
 */
 
#if !defined( _CCLISTBOX_H_ )
#define _CCLISTBOX_H_

#include <guichan.hpp>
#include <string>

using namespace std;

class cCallback;

class CCListBox :
  public gcn::ListBox
{
  public:
    CCListBox(gcn::ListModel *listModel, cCallback *recallFunction);
    
    virtual void action(const gcn::ActionEvent& actionEvent);
  private:
    cCallback *m_callback;
};

#endif
