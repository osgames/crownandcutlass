/* Crown and Cutlass
 * Crown and Cutlass Table Class
 *
 * This is essentially a matrix of guichan widgets. While I could create
 * a matrix class or an array of arrays or whatever, I've decided to make
 * the matrix a single vector and keep track of the number of columns
 * and number of rows. That way I can index them in the following way:
 *    cell = (row * Number_Of_Columns) + column
 * It presents some limitations, but they shouldn't have any effect on
 * the performance of the class.
 */

#include <guichan.hpp>
#include "../Log.h"
#include "CCTable.h"


CCTable::CCTable(): gcn::Container() {
  numRows = 0;
  numColumns = 0;
}

CCTable::CCTable(int columns, int rows): gcn::Container() {
  int i;
  numRows = rows;
  numColumns = columns;
  columnBuffer = 0;
  rowBuffer = 0;

  setOpaque(false);

  rowSize.resize(numRows, 5);
  columnSize.resize(numColumns , 5);

  for (i = 0; i < numRows; i++) {
    resizeRow(i, 5);
  }
  for (i = 0; i < numColumns; i++) {
    resizeColumn(i, 5);
  }

  cellMatrix.resize(columns * rows, NULL);

  recheckTable = false;
}

CCTable::~CCTable() {
  int i;
  gcn::Container* tempContainer;

  for (i = 0; i < numRows; i++) {
    for (int j = 0; j < numColumns; j++) {
       tempContainer = (gcn::Container*)cellMatrix[(i * numColumns) + j];
       delete tempContainer;
    }
  }
}

void CCTable::draw(Graphics* graphics)
{
  /* This is so that if something is changed (something gets a widget)
   * we can adapt the table properly
   */
  if (recheckTable) {
    resetTable();
    recheckTable = false;
  }
  if (isOpaque())
  {
    graphics->setColor(getBaseColor());
    graphics->fillRectangle(Rectangle(0, 0, getWidth(), getHeight()));
  }
  drawChildren(graphics);
}

void CCTable::resizeColumn(int columnNum, int size) {
  // Should probably throw an error...
  if ((columnNum >= numColumns) || (columnNum < 0)) return;

  columnSize[columnNum] = size + columnBuffer;

  resetTable();
}

void CCTable::resizeRow(int rowNum, int size) {
  // Should probably throw an error...
  if ((rowNum >= numRows) || (rowNum < 0)) return;

  rowSize[rowNum] = size + rowBuffer;

  resetTable();
}

int CCTable::getRowSize(int rowNum) {
  if ((rowNum >= numRows) || (rowNum < 0)) return -1;

  return rowSize[rowNum];
}

int CCTable::getColumnSize(int columnNum) {
  if ((columnNum >= numColumns) || (columnNum < 0)) return -1;

  return columnSize[columnNum];
}

int CCTable::getRowPosition(int rowNum) {
  int returnValue = 0;
  int i;
  if ((rowNum >= numRows) || (rowNum < 0)) return -1;

  for (i = 0; i < rowNum; i++) {
    returnValue += getRowSize(i);
  }

  return returnValue;
}

int CCTable::getColumnPosition(int columnNum) {
  int returnValue = 0;
  int i;
  if ((columnNum >= numColumns) || (columnNum < 0)) return -1;

  for (i = 0; i < columnNum; i++) {
    returnValue += getColumnSize(i);
  }

  return returnValue;
}

int CCTable::getNumColumns() {
  return numColumns;
}

int CCTable::getNumRows() {
  return numRows;
}

void CCTable::add(int columnNum, int rowNum, Widget* widget) {
  if ((columnNum >= numColumns) || (columnNum < 0)) return;
  if ((rowNum >= numRows) || (rowNum < 0)) return;

  widget->setPosition(getColumnPosition(columnNum), getRowPosition(rowNum));
  cellMatrix[(rowNum * numColumns) + columnNum] = widget;

  // Resize the column and row if necessary
  if (getColumnSize(columnNum) < widget->getWidth()) {
    resizeColumn(columnNum, widget->getWidth());
  }
  if (getRowSize(rowNum) < widget->getHeight()) {
    resizeRow(rowNum, widget->getHeight());
  }

  //resetTable();

  mWidgets.push_back(widget);
  widget->_setFocusHandler(_getFocusHandler());
  widget->_setParent(this);
}

/* resize()
 * This function finds out the sizes of the columns and the rows
 * and makes sure that the container is big enough to display
 * the all.
 */
void CCTable::resize() {
  int i;
  int sizeX = 0;
  int sizeY = 0;

  for (i = 0; i < numColumns; i++) {
    sizeX += getColumnSize(i);
  }
  for (i = 0; i < numRows; i++) {
    sizeY += getRowSize(i);
  }

  setSize(sizeX, sizeY);

}

/* resetTable()
 * This function adjusts where each widget is when a row or column
 * is resized.
 */
void CCTable::resetTable() {
  unsigned int i;
  int row;
  int column;
  gcn::Widget* tempWidget;

  for (i = 0; i < cellMatrix.size(); i++) {
    row = (i / numColumns);
    column =(i % numColumns);
    tempWidget = (gcn::Widget*)cellMatrix[i];
    if (tempWidget != NULL)
      tempWidget->setPosition(getColumnPosition(column), getRowPosition(row));
  }

  resize();
}

gcn::Widget* CCTable::getWidget(int columnNum, int rowNum) {
  if ((columnNum >= numColumns) || (columnNum < 0)) return NULL;
  if ((rowNum >= numRows) || (rowNum < 0)) return NULL;
  recheckTable = true;
  return (Widget*)cellMatrix[(rowNum * numColumns) + columnNum];
}

void CCTable::setColumnBuffer(int size) {
  columnBuffer = size;
}

void CCTable::setRowBuffer(int size) {
  rowBuffer = size;
}
