/* Crown and Cutlass
 * Crown and Cutlass Button Header
 */

#if !defined( _CCBUTTON_H_ )
#define _CCBUTTON_H_

#include <guichan.hpp>
#include <string>
#include <memory>

class cCallback;
class CCFont;

class CCButton :
    public gcn::Button,
    public gcn::ActionListener
{
  public:
    CCButton(const std::string& caption,
             const std::string& eventId,
             const std::string mainFontName,
             const std::string highlightFontName,
             const std::string disabledFontName,
             cCallback *recallFunction);
    ~CCButton();

   void SetMainFont(std::string fontName);
   void SetHighlightFont(std::string fontName);
   void SetDisabledFont(std::string fontName);

  /*
   * Inherited from Widget
   */
   void draw(gcn::Graphics* graphics);
   void setEnabled(bool enable);
   virtual void mouseEntered(gcn::MouseEvent& mouseEvent);
   virtual void mouseExited(gcn::MouseEvent& mouseEvent);

   // This is inherited from ActionListener
   virtual void action(const gcn::ActionEvent& actionEvent);

   void SetRecallFunction(cCallback *recallFunc);

   void SetID(int id);
   int GetID();

  private:
    std::auto_ptr<CCFont> m_mainFont;
    std::auto_ptr<CCFont> m_highlightFont;
    std::auto_ptr<CCFont> m_disabledFont;
    cCallback *m_recallFunction;
    int m_id;
};

#endif
