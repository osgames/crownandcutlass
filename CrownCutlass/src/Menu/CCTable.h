/* Crown and Cutlass
 * Gui, table header.
 */

#if !defined( _CCTABLE_H_ )
#define _CCTABLE_H_

#include <guichan.hpp>
#include <string>
#include <vector>
#include "SDL.h"

using namespace gcn;

class CCTable :
    public gcn::Container
{
  public:
    CCTable();
    CCTable(int columns, int rows);
    ~CCTable();

    void draw(Graphics* graphics);

    int getNumColumns();
    int getNumRows();

    /* While resizing the Table is theoretically
     * possible, it is beyond the scope of this
     * class and so is not implemented.
     *
     * void resizeTable(int columns, int rows);
     */

    void resizeColumn(int columnNum, int size);
    void resizeRow(int rowNum, int size);

    int getColumnSize(int columnNum);
    int getColumnPosition(int columnNum);
    int getRowSize(int rowNum);
    int getRowPosition(int rowNum);

    //virtual void add(Widget* widget);
    void add(int columnNum, int rowNum, Widget* widget);

    Widget* getWidget(int columnNum, int rowNum);

    void setColumnBuffer(int size);
    void setRowBuffer(int size);

  private:
    std::vector<int> columnSize;
    std::vector<int> rowSize;

    std::vector<gcn::Widget*> cellMatrix;

    void resetTable();
    void resize();

    int numRows;
    int numColumns;

    int rowBuffer;
    int columnBuffer;

    bool recheckTable;
};

#endif
