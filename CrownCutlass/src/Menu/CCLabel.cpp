/* Crown and Cutlass
 * Crown and Cutlass Label Code
 */

#include <guichan.hpp>
#include <string>
#include <sstream>
#include "CCFont.h"
#include "CCLabel.h"

using namespace std;

CCLabel::CCLabel(const string& caption, const string mainFontName): gcn::Label(caption) {
  SetMainFont(mainFontName);

  adjustSize();

  stream.setf(ios::fixed, ios::floatfield);
}

CCLabel::~CCLabel() {
}

void CCLabel::SetMainFont(string fontName) {
  m_mainFont.reset(new CCFont(fontName));
  setFont(m_mainFont->GetImageFont());
}

void CCLabel::SetCaption(int i) {
  stream.str("");
  stream << i;
  setCaption(stream.str());
}

void CCLabel::SetCaption(unsigned int i) {
  stream.str("");
  stream << i;
  setCaption(stream.str());
}

void CCLabel::SetCaption(double f, int places) {
  stream.str("");
  stream.precision(places);
  stream << f;
  setCaption(stream.str());
}
