/* Crown and Cutlass
 * Gui Helper Function header
 */

#if !defined ( _GUI_H_ )
#define _GUI_H_

#include "../GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include <memory>
#include <string>
#include "SDL.h"

class CCFont;
class CCButton;
class cCallback;

class CCGui {
public:
  CCGui(int width, int height);

  ~CCGui();

  void UpdateGui();

  static bool CreateGui(int width, int height);
  static bool DestroyGui();

  void SetTopContainer(gcn::Widget* top);
  gcn::Widget* GetTopContainer();

  // Returns true if the widget is the top container
  bool IsTopContainer(gcn::Widget* top);

  static CCGui *s_CCGui;

  void SetTargetPlane(int width, int height);

  //gcn::Input* globalInput;
  gcn::SDLInput* m_input;

private:
  // Guichan Menu Globals
  gcn::OpenGLGraphics* m_graphics;
  gcn::ImageLoader* m_imageLoader;

  gcn::Gui* m_gui;

  std::auto_ptr<CCFont> m_globalFont;

  int m_width;
  int m_height;

  void CheckInputGlobal();
};

#endif
