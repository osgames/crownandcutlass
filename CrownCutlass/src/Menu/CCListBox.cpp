/* Crown and Cutlass
 * Crown and Cutlass ListBox Class
 */
 
#include "../Callback.h"
#include "../Log.h"
#include <guichan.hpp>
#include "CCListBox.h"

CCListBox::CCListBox(gcn::ListModel *listModel, cCallback *recallFunction):
  gcn::ListBox(listModel) {
  Log::s_log->DebugMessage("Creating CCListBox");
  m_callback = recallFunction;
  
  setEnabled(true);
}

void CCListBox::action(const gcn::ActionEvent& actionEvent) {
  m_callback->Execute(0);
}
