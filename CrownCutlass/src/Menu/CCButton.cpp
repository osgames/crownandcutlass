/* Crown and Cutlass
 * Crown and Cutlass Button Class
 */

#include "../Callback.h"
#include "../Log.h"
#include "CCFont.h"
#include "CCButton.h"

using namespace std;

CCButton::CCButton(const std::string& caption,
                   const std::string& eventId,
                   const std::string mainFontName,
                   const std::string highlightFontName,
                   const std::string disabledFontName,
                   cCallback *recallFunction): gcn::Button(caption) {
  // Set up the button
  SetMainFont(mainFontName);
  SetHighlightFont(highlightFontName);
  SetDisabledFont(disabledFontName);

  setActionEventId(eventId);
  SetRecallFunction(recallFunction);
  addActionListener(this);
  SetID(0);
  setFrameSize(0);

  setEnabled(true);
  adjustSize();
}

CCButton::~CCButton() {

}

void CCButton::draw(gcn::Graphics* graphics) {
  graphics->setFont(getFont());
  graphics->drawText(getCaption(),0,0);
}

void CCButton::setEnabled(bool enable) {
  gcn::Button::setEnabled(enable);

  if (!enable) {
    setFont(m_disabledFont->GetImageFont());
  } else if (mHasMouse) {
    setFont(m_highlightFont->GetImageFont());
  } else {
    setFont(m_mainFont->GetImageFont());
  }
}

void CCButton::mouseEntered(gcn::MouseEvent& mouseEvent) {
  gcn::Button::mouseEntered(mouseEvent);
  if (isEnabled()) {
    setFont(m_highlightFont->GetImageFont());
  }
}

void CCButton::mouseExited(gcn::MouseEvent& mouseEvent) {
  gcn::Button::mouseExited(mouseEvent);
  if (isEnabled()) {
    setFont(m_mainFont->GetImageFont());
  }
}

void CCButton::SetMainFont(string fontName) {
  m_mainFont.reset(new CCFont(fontName));
}

void CCButton::SetHighlightFont(string fontName) {
  m_highlightFont.reset(new CCFont(fontName));
}

void CCButton::SetDisabledFont(string fontName) {
  m_disabledFont.reset(new CCFont(fontName));
}

void CCButton::SetRecallFunction(cCallback *recallFunc) {
  m_recallFunction = recallFunc;
}

void CCButton::SetID(int id) {
  m_id = id;
}

int CCButton::GetID() {
  return m_id;
}

void CCButton::action(const gcn::ActionEvent& actionEvent) {
  if ((actionEvent.getId().compare(getActionEventId()) == 0) && (m_recallFunction != NULL) && (isEnabled())) {
    m_recallFunction->Execute(m_id);
  }
}
