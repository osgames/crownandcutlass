/* Crown and Cutlass
 * Crown and Cutlass Label Header
 */

#if !defined( _CCLABEL_H_ )
#define _CCLABEL_H_

#include <guichan.hpp>
#include <memory>
#include <string>
#include <sstream>

class CCFont;

class CCLabel: public gcn::Label {
  public:
    CCLabel(const std::string& caption, const std::string mainFontName);
    ~CCLabel();

    void SetMainFont(std::string fontName);

    // These SetCaption functions make it easier to display numbers
    void SetCaption(int i);
    void SetCaption(unsigned int i);
    void SetCaption(double f, int places);
  private:
    std::auto_ptr<CCFont> m_mainFont;

    // Used to convert from int/float to string
    std::stringstream stream;
};

#endif
