/* Crown and Cutlass
 * GUI Helper Monkies...I mean Functions
 */

#include "../GLee.h"
#include <guichan.hpp>
#include <guichan/opengl/openglsdlimageloader.hpp>
#include "SDL.h"
#include "../Log.h"
#include "CCButton.h"
#include "../Callback.h"
#include "CCFont.h"
#include "CCGui.h"

using namespace std;

CCGui *CCGui::s_CCGui = NULL;

CCGui::CCGui(int width, int height) {
  m_imageLoader = new gcn::OpenGLSDLImageLoader();

  // The ImageLoader in use is static and must be set to be
  // able to load images
  gcn::Image::setImageLoader(m_imageLoader);
  m_graphics = new gcn::OpenGLGraphics();
  // We need to tell OpenGL graphics how big the screen is.
  m_graphics->setTargetPlane(width, height);

  m_input = new gcn::SDLInput();

  m_globalFont.reset(new CCFont("systemWhite.png"));

  m_gui = new gcn::Gui();
  m_gui->setTabbingEnabled(false);
  m_gui->setInput(m_input);
  m_gui->setGraphics(m_graphics);

  gcn::Widget::setGlobalFont(m_globalFont->GetImageFont());

  m_width = width;
  m_height = height;
}

CCGui::~CCGui() {
  gcn::Widget::setGlobalFont(NULL);
  // This seems to have to happen before we delete m_gui...
  m_globalFont.reset(NULL);
  gcn::Image::setImageLoader(NULL);
  delete m_gui;
  delete m_imageLoader;
  delete m_graphics;
  delete m_input;

  Log::s_log->Message("CCGui destroyed.");
}

void CCGui::UpdateGui() {
  if (m_gui->getTop() != NULL) {
    m_gui->logic();
    m_gui->draw();
  }
}


void CCGui::SetTopContainer(gcn::Widget* top) {
  m_gui->setTop(top);
}

gcn::Widget* CCGui::GetTopContainer() {
  return m_gui->getTop();
}

bool CCGui::IsTopContainer(gcn::Widget* top) {
  return m_gui->getTop() == top;
}

void CCGui::CheckInputGlobal() {
  SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    m_input->pushInput(event);
    //globalInput->pushInput(event);
  }
}

bool CCGui::CreateGui(int width, int height) {
  if (s_CCGui != NULL) {
    Log::s_log->Message("Warning: Multiple calls to createGui()");
    return false;
  }

  try {
    s_CCGui = new CCGui(width, height);
  } catch (...) {
    return false;
  }

  return true;
}

bool CCGui::DestroyGui() {
  if (s_CCGui == NULL) {
    Log::s_log->Message("Warning: Call to destroyGui() with NULL gui pointer");
    return false;
  }

  delete s_CCGui;
  s_CCGui = NULL;
  return true;
}
