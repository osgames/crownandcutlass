/* Crown and Cutlass
 * Crown and Cutlass TextField Code
 */

#include <guichan.hpp>
#include <string>
#include "CCFont.h"
#include "CCTextField.h"

using namespace std;

CCTextField::CCTextField(const string& caption, const string mainFontName, int width): gcn::TextField(caption) {
  SetMainFont(mainFontName);

  adjustHeight();
  setWidth(width);
}

CCTextField::~CCTextField() {
}

void CCTextField::SetMainFont(string fontName) {
  m_mainFont.reset(new CCFont(fontName));
  setFont(m_mainFont->GetImageFont());
}
