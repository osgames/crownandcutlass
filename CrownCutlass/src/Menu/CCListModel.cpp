/*
 * CCListModel Class
 * Crown and Cutlass
 */
 
#include <guichan.hpp>
#include <string>
#include <vector>
#include "../Log.h"
#include "CCListModel.h"

using namespace std;

CCListModel::CCListModel() {
    Log::s_log->DebugMessage("Creating CCListModel!");
	m_List.reserve(20);
}

CCListModel::~CCListModel() {
  Log::s_log->DebugMessage("Deleting CCListModel!");
}

void CCListModel::AddItem(std::string Item) {
	Log::s_log->DebugMessage("Adding %s", Item.c_str());
	m_List.push_back(Item.c_str());
}

void CCListModel::AddItem(std::string Item, int Position) {
	vector<const char*>::iterator i;
	i = m_List.begin();
	i = i + Position;
	m_List.insert(i, Item.c_str());
}
	
void CCListModel::RemoveItem(std::string Item) {
	std::vector<const char*>::iterator i;
	
	for (i = m_List.begin();
			 i != m_List.end();
			 i++)
		if (*i == Item) {
			m_List.erase(i);
			break;
		}
}

void CCListModel::RemoveItem(int Position) {
	if ((Position < 0) || (Position >= (int)m_List.size())) {
		Log::s_log->DebugMessage("CCListModel: Index out of bounds in RemoveItem");
		return;
	}
	RemoveItem(m_List[Position]);
}
		
int CCListModel::getNumberOfElements() {
	return (int)m_List.size();
}
	
std::string CCListModel::getElementAt(int i) {
  string Result;
	if ((i < 0) || (i >= (int)m_List.size())) {
		Log::s_log->Message("CCListModel: Index out of bounds in getElementAt");
		throw 10000;
	}
	Result = m_List[i];
	return Result;
}
