/* Crown and Cutlass
 * Config Object Header
 */

#if !defined ( _CONFIG_H_ )
#define _CONFIG_H_

#include <string>

class Config {
public:
  // Constructor
  Config(const std::string filename);

  // Destructor
  ~Config();

  // Make sure the necesary extensions are supported
  // Note: this needs to happen after we have an opengl context
  bool CheckExtensions();

  // Check to see whether or not to use VBOs
  bool CheckVBO();

  // Returns winWidth/Height
  int GetWinWidth();
  int GetWinHeight();

  // Check to see whether or not to go fullscreen
  bool CheckFullscreen();

  // Check to see whether or not to enable the SDL parachute
  bool CheckSDLParachute();

  // Check whether or not to compress textures
  bool CheckCompressTextures();

  // Get the average number of ticks that should go by between battles
  unsigned int GetTicksBetweenBattles();

  // Get the number of OpenAL sources to use
  unsigned int GetNumberALSources();

  // Get the debug log mode
  bool DebugLogEnabled();

  std::string GetStartingShipType();

  static bool OpenConfig(const std::string filename);
  static void CloseConfig();

  static Config *s_config;

private:
  // Window dimensions
  int m_winWidth;
  int m_winHeight;

  // True is fullscreen, false is windowedd
  bool m_fullscreen;

  // Bool for VBO usage
  bool m_useVBO;

  // Bool for whether or not to enable the SDL parachute
  // Note: This should probably be true unless you want to get a core file
  bool m_useSDLParachute;

  // Bool for whether or not to compress textures on load
  // Note: This should probably always be true, unless your video card
  //   can't handle it
  bool m_compressTextures;

  // Get the average number of ticks that should go by between battles
  // Note: 20000 is a decent default
  unsigned int m_ticksBetweenBattles;

  // Number of OpenAL sources to use
  // 16 is way more than enough for now
  unsigned int m_numberALSources;

  // Whether or not to enable debug logging
  bool m_debugLogMode;

  // Ship type to start with
  std::string m_startingShipType;

  // Send config options to logger
  void LogConfig();
};

#endif
