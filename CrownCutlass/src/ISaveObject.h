/* Crown and Cutlass
 * ISaveObject Header
 * Note: This is a base class for anything that will need to be saved in the save games
 */

#if !defined ( _ISAVEOBJECT_H_ )
#define _ISAVEOBJECT_H_

#include <string>

class TiXmlElement;

class ISaveObject {
 public:
   ISaveObject(std::string XMLName);

   virtual ~ISaveObject() { };

  // Saves this object to XML, pass parent element
  virtual void Save(TiXmlElement *parent) = 0;

  // Loads from XML, pass the element of this object
  virtual void Load(TiXmlElement *parent) = 0;

 protected:
  std::string m_XMLName;
};

#endif
