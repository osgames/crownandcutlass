/* Crown and Cutlass
 * Ocean Object Code
 */

#include <cstdio>
#include "GLee.h"
#include "ccmath.h"
#include <vector>
#include "Log.h"
#include "Texture.h"
#include "normals.h"
#include "WaveEmitter.h"
#include "Ocean.h"

#define OCEAN_TEXSCALE 6

using namespace std;

Ocean::Ocean(int sizeX, int sizeZ) {
  m_width = sizeX;
  m_height = sizeZ;

  m_count = 0;

  // Set up the texture
  glEnable(GL_TEXTURE_2D);
  m_texture = new Texture("water.png");

  // Set up the waves texture
  m_waveTexture = new Texture("surf.png");

  // Set up the material;
  /**/
  m_material[0] = 0.23;
  m_material[1] = 0.35;
  m_material[2] = 0.7;
  m_material[3] = 0.45;
  /**//*
  material[0] = 1.0;
  material[1] = 1.0;
  material[2] = 1.0;
  material[3] = 0.45;
  */

  // Generate the display list
  m_list = glGenLists(1);
  if (m_list == 0) {
    printf("Could not generate waves list\n");
  }
  glNewList(m_list, GL_COMPILE);

  // Draw the actual water
  // Use the material
  glMaterialfv(GL_FRONT, GL_AMBIENT, m_material);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, m_material);

  // Enable the texture
  m_texture->BindTexture();

  glNormal3f(0, 1, 0);

  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex3f(-1*m_width/2, 0, -1*m_height/2);
  glTexCoord2f(0, m_height/OCEAN_TEXSCALE);
  glVertex3f(-1*m_width/2, 0, m_height/2);
  glTexCoord2f(m_width/OCEAN_TEXSCALE, m_height/OCEAN_TEXSCALE);
  glVertex3f(m_width/2, 0, m_height/2);
  glTexCoord2f(m_width/OCEAN_TEXSCALE, 0);
  glVertex3f(m_width/2, 0, -1*m_height/2);
  glEnd();

  // Done with list
  glEndList();

  // Generate the wave list
  WaveEmitter::GenerateWaveList();
}

Ocean::~Ocean() {
  // The wave emitters will be deleted by the quad tree
  m_waveEmitters.clear();

  WaveEmitter::DeleteActiveWaves();

  WaveEmitter::DeleteWaveList();

  // Delete the display list
  glDeleteLists(m_list, 1);

  // Delete the texture
  delete m_texture;
  delete m_waveTexture;

  Log::s_log->Message("Ocean deleted");
}

void Ocean::Draw() {
  float mult = sin(m_count);
  float mult2 = cos(m_count);

  GLfloat shift = (.25 * mult) / OCEAN_TEXSCALE;
  GLfloat shift2 = (.25 * mult2 * (m_width/m_height)) / OCEAN_TEXSCALE;

  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  m_waveTexture->BindTexture();
  WaveEmitter::DrawActiveWaves();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Get ready to manipulate the texture matrix
  glMatrixMode(GL_TEXTURE);
  glPushMatrix();

  // Move around the texture
  glTranslatef(shift, shift2, 0);

  // Draw the actual waves
  glCallList(m_list);

  // Reset the matrix
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

void Ocean::Update(unsigned int ticks) {
  unsigned int numEmitters = m_waveEmitters.size();

  m_count += .002 * ticks;
  if (m_count > TWO_PI) m_count -= TWO_PI;

  for(unsigned int i = 0; i < numEmitters; i++) {
    m_waveEmitters[i]->Update(ticks);
  }

  WaveEmitter::UpdateActiveWaves(ticks);
}

void Ocean::AddEmitter(WaveEmitter *emitter) {
  // This just puts a pointer to a wave emitter into the vector
  m_waveEmitters.push_back(emitter);
}
