/* Crown and Cutlass
 * SoundSystem Code
 */

#include <list>
#include <vector>
#include <algorithm>
#include "Log.h"
#include <AL/al.h>
#include <AL/alut.h>
#include "OpenALSource.h"
#include "ISound.h"
#include "ISubSystem.h"
#include "SoundSystem.h"

using namespace std;

SoundSystem *SoundSystem::s_soundSystem = NULL;

void SoundSystem::Initialize(unsigned int numSources) {
  if (s_soundSystem != NULL) {
    Log::s_log->Message("Warning: Attempt to re-initialize sound subsystem");
    return;
  }

  s_soundSystem = new SoundSystem(numSources);
}

void SoundSystem::Shutdown() {
  if (s_soundSystem == NULL) {
    Log::s_log->Message("Warning: Attempt to re-shutdown sound subsystem");
    return;
  }
  delete s_soundSystem;
}

SoundSystem::SoundSystem(unsigned int numSources) {
  alutInit(0, NULL);

  SetListener();

  m_sources.resize(numSources);
  for (unsigned int i = 0; i < numSources; i++) {
    m_sources[i] = new OpenALSource();
  }
  m_availSources = m_sources;

  Log::s_log->DebugMessage("Post-initialization...");
  LogDebugInfo();

  Log::s_log->Message("Audio initialized");
}

SoundSystem::~SoundSystem() {
  Log::s_log->DebugMessage("Pre-shutdown...");
  LogDebugInfo();
  StopAll();
  if (!m_playingSounds.empty()) {
    Log::s_log->Message("Sound system shutdown with sounds still active (likely memory leak!)");
    m_playingSounds.clear();
  }

  m_availSources.clear();

  for (unsigned int i = 0; i < m_sources.size(); i++) {
    delete m_sources[i];
  }
  m_sources.clear();

  Log::s_log->DebugMessage("Post-shutdown...");
  LogDebugInfo();

  alutExit();
  alGetError();
  Log::s_log->Message("Audio shutdown");
}

void SoundSystem::LogDebugInfo() {
  Log::s_log->DebugMessage("%d sources", m_sources.size());
  Log::s_log->DebugMessage("%d sources available", m_availSources.size());
  Log::s_log->DebugMessage("%d sounds playing", m_playingSounds.size());
}

void SoundSystem::SetListener() {
  ALfloat listenerPos[] = { 0.0, 0.0, 0.0 };
  ALfloat listenerVel[] = { 0.0, 0.0, 0.0 };

  // Orientation of the listener. (first 3 elements are "at", second 3 are "up")
  ALfloat listenerOri[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };

  alListenerfv(AL_POSITION, listenerPos);
  alListenerfv(AL_VELOCITY, listenerVel);
  alListenerfv(AL_ORIENTATION, listenerOri);
}

void SoundSystem::Update(unsigned int ticks) {
  list< ISound* >::iterator p;
  list< ISound* >::iterator endIterator;

  endIterator = m_playingSounds.end();
  for (p = m_playingSounds.begin(); p != endIterator; ++p) {
    (*p)->Update(ticks);
  }
}

void SoundSystem::Play(ISound *sound) {
  sound->Prepare();
  MoveToFrontOfQueue(sound);
  sound->Play();
}

OpenALSource* SoundSystem::AquireSource() {
  OpenALSource *source;
  list< ISound* >::reverse_iterator p;
  list< ISound* >::reverse_iterator endIterator;
  ISound *victim;

  if (!m_availSources.empty()) {
    // We have a free source
    source = m_availSources.back();
    m_availSources.pop_back();

    return source;
  } else {
    // There isn't a free source, so we have to take the least recently used source
    p = m_playingSounds.rbegin();
    endIterator = m_playingSounds.rend();
    while (p != endIterator) {
      victim = (*p);
      if ((!victim->m_locked) && (victim->m_source != NULL)) {
        source = victim->m_source;
        victim->RevokeSource();

        return source;
      }
      ++p;
    }
  }
  throw string("Could not aquire source");
}

void SoundSystem::ReleaseSound(ISound *sound) {
  OpenALSource *source;

  sound->Stop();
  m_playingSounds.remove(sound);

  source = sound->GetSource();
  if (source != NULL) {
    m_availSources.push_back(sound->GetSource());
  }
}

void SoundSystem::PauseAll() {
  list< ISound* >::iterator p;
  list< ISound* >::const_iterator endIterator = m_playingSounds.end();

  for (p = m_playingSounds.begin(); p != endIterator; ++p) {
    (*p)->Pause();
  }
}

void SoundSystem::StopAll() {
  list< ISound* >::const_iterator p;
  list< ISound* >::const_iterator endIterator = m_playingSounds.end();

  for (p = m_playingSounds.begin(); p != endIterator; ++p) {
    (*p)->Stop();
  }
}

void SoundSystem::MoveToFrontOfQueue(ISound *sound) {
  m_playingSounds.remove(sound);
  m_playingSounds.push_front(sound);
}
