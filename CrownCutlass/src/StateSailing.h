/* Crown and Cutlass
 * Sailing GameState Header
 */

#if !defined( _SAILINGSTATE_H_ )

#define _SAILINGSTATE_H_

#include "GLee.h"
#include <guichan.hpp>
#include <guichan/sdl.hpp>
#include <guichan/opengl.hpp>
#include "SDL.h"
#include "IGameState.h"
#include "ISaveObject.h"

// So I don't have to include the headers
class Terrain;
class Ocean;
class Camera;
class Map;
class CityManager;
class Economy;
class StateCity;
class TiXmlElement;
class EconomyScreen;

class StateSailing: public IGameState, public ISaveObject {
 public:
  // Constructor
  StateSailing(const std::string playerName, const std::string shipName);

  // Decontructor
  ~StateSailing();

  // Reset the game, used when "New Game" is chosen from the menu
  void NewGame(const std::string playerName, const std::string shipName);

  // From SaveObject
  void Save(TiXmlElement *parent);
  void Load(TiXmlElement *parent);

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Dump some debug info
  void Dump();

  // City Manager - Needs to be public for Map call
  CityManager* m_cities;


 private:
  // Called from the constructor/destructor and by newGame
  void Initialize(const std::string playerName, const std::string shipName);
  void Destroy();

  // Handles processing SDL events
  void CheckInput();

  // Keyboard handler functions
  void HandleKeyUp(SDL_keysym* keysym);
  void HandleKeyDown(SDL_keysym* keysym);

  // Timer function to handle time-based movement
  void Timer(const unsigned int ticks);

  void SetTicksToNextBattle();

  int m_ticksToNextBattle;

  // For sway of ship
  GLfloat m_rotx;

  // Terrain object
  Terrain *m_land;

  // Map Object
  Map *m_map;

  // Economy
  //Economy* economy;

  // City State
  IGameState *m_childState;

  // Camera location
  Camera *m_camera;
  GLfloat m_cameraX, m_cameraY, m_cameraZ;

  // Light info
  GLfloat m_ambientLight[4];
  GLfloat m_diffuseLight[4];
  GLfloat m_lightPosition[4];

  //CCLabel *m_label;
  EconomyScreen *m_es;

  // Font names
  static const std::string s_mainFontName;
};

#endif
