/* Crown and Cutlass
 * Sound Effect Code
 */

#include <AL/al.h>
#include "Log.h"
#include "soundfile.h"
#include "OpenALSource.h"
#include "SoundSystem.h"
#include "ISound.h"
#include "ResourceManager/ResourceManager.h"
#include "ResourceManager/IResource.h"
#include "ResourceManager/SoundResource.h"
#include "SoundEffect.h"

using namespace std;

SoundEffect::SoundEffect(string name): ISound() {
  m_resource = (SoundResource *) ResourceManager::s_resourceManager->Acquire(IResource::ConstructKey(SoundResource::s_type, name));
}

SoundEffect::~SoundEffect() {
  Stop();
  SoundSystem::s_soundSystem->ReleaseSound(this);
  ResourceManager::s_resourceManager->Release(m_resource);
}

void SoundEffect::Play() {
  if (m_source != NULL) {
    alSourcePlay(m_source->GetSource());
  } else {
    Log::s_log->Message("Warning: SoundEffect Play without source set");
  }
}

void SoundEffect::Pause() {
  if (m_source != NULL) {
    alSourcePause(m_source->GetSource());
  }
}

void SoundEffect::Stop() {
  if (m_source != NULL) {
    alSourceStop(m_source->GetSource());
  }
}

void SoundEffect::Update(unsigned int ticks) {
  // Do nothing
}

void SoundEffect::InitializeSource() {
  if (m_source == NULL) {
    Log::s_log->Message("Warning: Attempt to initialize NULL source");
    return;
  }
  alSourcei(m_source->GetSource(), AL_BUFFER, m_resource->GetBuffer());
}
