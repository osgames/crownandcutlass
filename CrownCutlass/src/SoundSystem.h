/* Crown and Cutlass
 * SoundSystem Header
 */

#if !defined( _SOUNDSYSTEM_H_ )

#define _SOUNDSYSTEM_H_

#include <vector>
#include <list>
#include "ISubSystem.h"

using namespace std;

class ISound;
class OpenALSource;

class SoundSystem: public ISubSystem {
 public:
  SoundSystem(unsigned int numSources);

  ~SoundSystem();

  void LogDebugInfo();

  void SetListener();

  void Update(unsigned int ticks);

  void Play(ISound *sound);

  void PauseAll();
  void StopAll();

  // Lock a sound, preventing its source from getting replaced
  // Use this feature carefully.  It should probably only be used for background music
  void LockSound(ISound *sound);

  OpenALSource* AquireSource();
  void ReleaseSound(ISound *sound);

  static void Initialize(unsigned int numSources);
  static void Shutdown();

  static SoundSystem *s_soundSystem;

 private:
  vector< OpenALSource* > m_sources;
  vector< OpenALSource* > m_availSources;
  list< ISound* > m_playingSounds;

  void MoveToFrontOfQueue(ISound *sound);
};

#endif
