/* Callback function class
 * Borrowed from CodeGuru.com
 * http://www.codeguru.com/Cpp/Cpp/cpp_mfc/callbacks/article.php/c4129/
 */

#if !defined( _CCALLBACK_H_ )

#define _CCALLBACK_H_

#include "Log.h"

class cCallback
{
    public:
        virtual void Execute(int ButtonID) const =0;
  virtual ~cCallback() { };
};

template <class cInstance>
class TCallback : public cCallback
{
    public:
        TCallback()    // constructor
        {
            pFunction = 0;
        }

        typedef void (cInstance::*tFunction)(int Param);

        virtual void Execute(int ButtonID) const
        {
            if (pFunction) (cInst->*pFunction)(ButtonID);
            else Log::s_log->Message("Error: Attempt to call uninitialized callback function");
        }

        void SetCallback (cInstance  *cInstancePointer,
                          tFunction   pFunctionPointer)
        {
            cInst     = cInstancePointer;
            pFunction = pFunctionPointer;
        }

    private:
        cInstance  *cInst;
        tFunction  pFunction;
};


#endif
