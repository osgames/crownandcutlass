/* Crown and Cutlass
 * City GameState Code
 */

#include <guichan.hpp>
#include <string>
#include <sstream>
#include "SDL.h"
#include "Menu/CCGui.h"
#include "Menu/CCTable.h"
#include "Menu/CCLabel.h"
#include "StateSailing.h"
#include "GLee.h"
#include "Config.h"
#include "Log.h"
#include "Texture.h"
#include "font.h"
#include "IGameState.h"
#include "City.h"
#include "Economy.h"
#include "CityEconomy.h"
#include "Player.h"
#include "Ship.h"
#include "Cargo.h"
#include "Product.h"
#include "StateCity.h"

using namespace std;

const string StateCity::s_mainFontName = "smallFont.png";
const string StateCity::s_highlightFontName = "smallHighlight.png";
const string StateCity::s_disabledFontName = "smallDisabledFont.png";

StateCity::StateCity(City *townIn): IGameState(NULL) {
  m_cargo = Player::player->GetCargo();

  m_exitCallback.SetCallback(this, &StateCity::Exit);
  m_buyCallback.SetCallback(this, &StateCity::Buy);
  m_sellCallback.SetCallback(this, &StateCity::Sell);
  m_repairCallback.SetCallback(this, &StateCity::Repair);

  // Save the location where the player is
  m_town = townIn;

  m_productList = m_town->MyEconomy->GetProductList();

  Log::s_log->Message("Starting to create City Menu");
  try {
    InitMenu();
  } catch (gcn::Exception e) {
    Log::s_log->Message("Guichan error: %s\n\t%s line: %i", e.getMessage().c_str(), e.getFilename().c_str(), e.getLine());
  }

}

StateCity::~StateCity() {
  delete m_background;

  m_table->clear();
  delete m_table;
  delete m_returnToSailing;
  delete m_repairShip;
  delete m_gold;
}

void StateCity::Update(const unsigned int ticks) {
  CheckInput();
}

void StateCity::Display() {
  glLoadIdentity();
  DisplayBackground(m_background->GetTexture(), 1);
}

void StateCity::CheckInput() {
  SDL_Event event;
  /*
   * Poll SDL events
   */
  while(SDL_PollEvent(&event))
  {
    if (event.type == SDL_KEYDOWN)
    {
      /*
      if (event.key.keysym.sym == SDLK_ESCAPE)
      {
        done = false;
      }
      if (event.key.keysym.sym == SDLK_f)
      {
        if (event.key.keysym.mod & KMOD_CTRL)
        {
          // Works with X11 only
          //SDL_WM_ToggleFullScreen(screen);
        }
      }
      */
      if (event.key.keysym.sym == SDLK_z) {
        TakeScreenshot();
      }
    }

    /*
     * Now that we are done polling and using SDL events we pass
     * the leftovers to the SDLInput object to later be handled by
     * the Gui. (This example doesn't require us to do this 'cause a
     * label doesn't use input. But will do it anyway to show how to
     * set up an SDL application with Guichan.)
     */
    CCGui::s_CCGui->m_input->pushInput(event);
  }
}

void StateCity::Exit(int ID) {
  PrepareStateSwitch(m_previous, this);
}

void StateCity::Buy(int ID)  {
  int prodID = -1;
  unsigned int i;
  stringstream tempStr;
  Label* tempLabel;

  /* This finds which product we're referring to.
   * in other words its which row its in.
   */
  for (i = 0; i < Economy::s_economy->ProdList.size(); i++) {
    if (m_buyList[i] == ID) {
      prodID = i;
      break;
    }
  }

  if (prodID == -1 ) {
    Log::s_log->Message("StateCity.buy: Unknown id (%d)", ID);
    return;
  }

  Economy::s_economy->BuyProduct(prodID, 
    m_productList[prodID]->GetPrice(m_town->MyEconomy->GetSize()));
  tempStr << m_cargo->GetQty(prodID);
  tempLabel = (Label*)m_table->getWidget(4, ID);
  tempLabel->setCaption(tempStr.str());
  //menu->ChangeText(CargoList[prodID], tempStr.str());
  tempStr.str("");
  tempStr << Player::player->gold;
  m_gold->setCaption("Gold: " + tempStr.str());
  //menu->ChangeText(GoldData, tempStr.str());

}

void StateCity::Sell(int ID) {
  int prodID = -1;
  unsigned int i;
  stringstream tempStr;
  Label* tempLabel;

  /* This finds which product we're referring to.
   * in other words its which row its in.
   */
  for (i = 0; i < Economy::s_economy->ProdList.size(); i++) {
    if (m_sellList[i] == ID) {
      prodID = i;
      break;
    }
  }

  if (prodID == -1 ) {
    Log::s_log->Message("StateCity.sell: Unknown id (%d)", ID);
    return;
  }

  Economy::s_economy->SellProduct(prodID,
                             m_productList[prodID]->GetPrice(m_town->MyEconomy->GetSize()));
  tempStr << m_cargo->GetQty(prodID);
  tempLabel = (Label*)m_table->getWidget(4, ID);
  tempLabel->setCaption(tempStr.str());
  tempStr.str("");
  tempStr << Player::player->gold;
  m_gold->setCaption("Gold: " + tempStr.str());
}

void StateCity::Repair(int ID) {
  stringstream tempStr;
  int cost = Player::player->ship->GetMaxDamage() - Player::player->ship->m_damage;

  if (Player::player->gold >= cost) {
    Player::player->ship->m_damage = Player::player->ship->GetMaxDamage();
    Player::player->gold -= cost;

    tempStr.str("");
    tempStr << Player::player->gold;
    m_gold->setCaption("Gold: " + tempStr.str());

    m_repairShip->setCaption("Repair");
    m_repairShip->setEnabled(false);
  }
}

void StateCity::SwitchTo(IGameState *oldState) {
  EnterTextMode(Config::s_config->GetWinWidth(), Config::s_config->GetWinHeight());
}

void StateCity::SwitchFrom() {
}

void StateCity::InitMenu() {
  int i;
  int ID = 1;
  CCLabel* tempLabel;
  CCButton* tempButton;
  stringstream tempStr;
  bool needRepair;

  m_background = new Texture("city.png");
  m_table = new CCTable(6, Economy::s_economy->ProductCount() + 1);

  m_top->add(m_table, 300, 300);

  m_table->setColumnBuffer(10);
  m_table->setRowBuffer(1);

  /*****************************************************
   * This section sets up the headers for the columns. *
   *****************************************************/
  tempLabel = new CCLabel("Product", s_mainFontName);
  m_table->add(0, 0, tempLabel);
  //widgetList.push_back(tempLabel);

  tempLabel = new CCLabel("Price", s_mainFontName);
  m_table->add(1, 0, tempLabel);
  //widgetList.push_back(tempLabel);

  /*
  tempLabel = new gcn::Label("");
  m_table->add(2, 0, tempLabel);
  widgetList.push_back(tempLabel);

  tempLabel = new gcn::Label("");
  m_table->add(3, 0, tempLabel);
  widgetList.push_back(tempLabel);
  */
  tempLabel = new CCLabel("Cargo", s_mainFontName);
  m_table->add(4, 0, tempLabel);
  //widgetList.push_back(tempLabel);

  tempLabel = new CCLabel("Demand", s_mainFontName);
  m_table->add(5, 0, tempLabel);
  //widgetList.push_back(tempLabel);
  /* End Headers */

  /* Need to fill in the table with each product... */
  for (i = 0; i < Economy::s_economy->ProductCount(); i++) {
    tempLabel = new CCLabel(m_productList[i]->GetName(), s_mainFontName);
    m_table->add(0, i + 1, tempLabel);
    //widgetList.push_back(tempLabel);

    tempStr.str("");
    tempStr << m_productList[i]->GetPrice(m_town->MyEconomy->GetSize());
    tempLabel = new CCLabel(tempStr.str(), s_mainFontName);
    m_table->add(1, i + 1, tempLabel);
    //widgetList.push_back(tempLabel);

    tempButton = new CCButton("Buy", "buy" + m_productList[i]->GetName(), s_mainFontName, s_highlightFontName, s_disabledFontName, &m_buyCallback);
    tempButton->SetID(ID);
    m_table->add(2, i + 1, tempButton);
    //widgetList.push_back(tempButton);
    m_buyList.push_back(ID);
    //ID++;

    tempButton = new CCButton("Sell", "sell" + m_productList[i]->GetName(), s_mainFontName, s_highlightFontName, s_disabledFontName, &m_sellCallback);
    tempButton->SetID(ID);
    m_table->add(3, i + 1, tempButton);
    //widgetList.push_back(tempButton);
    m_sellList.push_back(ID);
    //ID++;

    tempStr.str("");
    tempStr << m_cargo->GetQty(i);
    tempLabel = new CCLabel(tempStr.str(), s_mainFontName);
    m_table->add(4, i + 1, tempLabel);
    m_cargoList.push_back(ID);
    ID++;

    tempStr.str(""); // We have to empty the sstream
    tempStr << m_productList[i]->GetConsumption();
    tempLabel = new CCLabel(tempStr.str(), s_mainFontName);
    m_table->add(5, i + 1, tempLabel);

    tempStr.str(""); // We have to empty the sstream

  }

  m_returnToSailing = new CCButton("Return", "exit", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_exitCallback);
  m_top->add(m_returnToSailing, 10, 300);

  needRepair = Player::player->ship->m_damage != Player::player->ship->GetMaxDamage();
  tempStr.str("");
  if (needRepair) {
    tempStr << "Repair Your Ship (" << Player::player->ship->GetMaxDamage() - Player::player->ship->m_damage << " gold)";
  } else {
    tempStr << "Repair Your Ship";
  }
  m_repairShip = new CCButton(tempStr.str(), "repair", s_mainFontName, s_highlightFontName, s_disabledFontName, &m_repairCallback);
  m_repairShip->setEnabled(needRepair);
  m_top->add(m_repairShip, 10, 320);

  tempStr.str("");
  tempStr << "Gold: " << Player::player->gold;
  m_gold = new CCLabel(tempStr.str(), s_mainFontName);
  Log::s_log->Message("Player's Gold: %i", Player::player->gold);
  m_top->add(m_gold, 100, 200);
}
