/* Crown and Cutlass
 * OpenAL Source Header
 */

#if !defined( _OPENAL_SOURCE_H_ )

#define _OPENAL_SOURCE_H_

#include <AL/al.h>

class OpenALSource {
 public:
  OpenALSource();
  ~OpenALSource();

  ALuint GetSource();

 private:
  ALuint source;
};

#endif
