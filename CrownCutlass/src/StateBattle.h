/* Crown and Cutlass
 * Naval Battle Entry Header
 */

#if !defined( _STATEBATTLE_H_ )

#define _STATEBATTLE_H_

#include <guichan.hpp>
#include "Callback.h"
#include "IGameState.h"

class Battle;
class Texture;
class CCButton;
class CCTable;
class CCLabel;

class StateBattle: public IGameState {
 public:
  // Constructor
  StateBattle();

  // Destructor
  ~StateBattle();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Callbacks
  void Exit(int ID);
  void Attack(int ID);

  void CheckInput();

 private:
  Texture *m_background;

  CCTable* m_menuList;
  CCButton* m_sailAwayButton;
  CCButton* m_attackButton;
  CCLabel* m_title;

  // Actual StateBattle
  Battle *m_actualBattle;

  TCallback<StateBattle> m_exitCallback;
  TCallback<StateBattle> m_attackCallback;

  // Font names
  static const std::string s_mainFontName;
  static const std::string s_highlightFontName;
  static const std::string s_disabledFontName;
};

#endif
