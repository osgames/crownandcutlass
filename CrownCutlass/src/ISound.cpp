/* Crown and Cutlass
 * Sound Base Object Code
 */

#include <cstdlib>
#include "Log.h"
#include "SoundSystem.h"
#include "OpenALSource.h"
#include "ISound.h"

ISound::ISound() {
  m_source = NULL;
  m_locked = false;
}

ISound::~ISound() {
  // Don't delete the source, the sound system does that
  m_source = NULL;
}

OpenALSource* ISound::GetSource() {
  return m_source;
}

void ISound::Prepare() {
  if (m_source == NULL) {
    m_source = SoundSystem::s_soundSystem->AquireSource();
    InitializeSource();
  }
}

void ISound::RevokeSource() {
  if (m_source != NULL) {
    Log::s_log->Message("Source revoked");
    alSourceStop(m_source->GetSource());
    m_source = NULL;
  } else {
    Log::s_log->Message("NULL source revoked");
  }
}

void ISound::SetLocked(bool lock) {
  m_locked = lock;
}
