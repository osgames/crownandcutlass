/* Crown and Cutlass
 * Product Class
 *
 * This is a very simple class to keep of each product.
 */

#ifndef _PRODUCT_H_
#define _PRODUCT_H_

#include <string>

using namespace std;

class Product {
 public:
  // Constructors
  Product();
  Product(int myID, int Consumption, int Production);
  
  // Destructor
  ~Product();
  
  // Methods
  string GetName();
  int GetConsumption();
	void SetConsumption(int Consumption);
	int GetProduction();
	void SetProduction(int Production);
	int GetProductionPoints();
	void SetProductionPoints(int PP);
	int AddProductionPoints(int PP);

	int GetPrice(int CitySize);

  void Print();
  
 private:
  int m_ID;
	int m_ConsumptionRate; // Per Size Unit
	int m_ProductionRate; // Per Production Point
	int m_ProductionPoints;
};

#endif
