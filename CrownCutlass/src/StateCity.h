/* Crown and Cutlass
 * City GameState Header
 */

#if !defined( _STATECITY_H_ )

#define _STATECITY_H_

#include <boost/shared_ptr.hpp>
#include <guichan.hpp>
#include "Menu/CCButton.h"
#include "SDL.h"
#include "Menu/CCTable.h"
#include "Callback.h"
#include "IGameState.h"

using boost::shared_ptr;

class StateSailing;
class City;
class Cargo;
class Menu;
class Texture;

class StateCity: public IGameState {
 public:
  // Constructor
  StateCity(City *townIn);

  // Destructor
  ~StateCity();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Callbacks
  void Exit(int ID);
  void Buy(int ID);
  void Sell(int ID);
  void Repair(int ID);

  void CheckInput();

 private:
  void InitMenu();

  Texture *m_background;

  // StateCity fonts
  CCTable* m_table;
  CCButton* m_returnToSailing;
  CCButton* m_repairShip;
  Label* m_gold;

  // Pointer to current city object
  City *m_town;

  // Menu class
  Menu *m_menu;

  // Cargo information
  shared_ptr<Cargo> m_cargo;

  // List of buttons, to find out which was called.
  vector<int> m_buyList;
  vector<int> m_sellList;
  vector<int> m_cargoList;
  int m_goldData;
  int m_repairButtonId;

  vector<Product*> m_productList;

  TCallback<StateCity> m_exitCallback;
  TCallback<StateCity> m_buyCallback;
  TCallback<StateCity> m_sellCallback;
  TCallback<StateCity> m_repairCallback;

  // Font names
  static const std::string s_mainFontName;
  static const std::string s_highlightFontName;
  static const std::string s_disabledFontName;
};

#endif
