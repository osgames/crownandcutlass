/* Crown and Cutlass
 * Texture Header
 */

#if !defined ( _TEXTURE_H_ )
#define _TEXTURE_H_

#include "GLee.h"

class TextureResource;

class Texture {
 public:
  Texture(std::string name);
  ~Texture();

  GLuint GetTexture();
  void BindTexture();

 private:
  TextureResource *m_resource;
};

#endif
