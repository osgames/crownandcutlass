/* Crown and Cutlass
 * OpenAL Source Code
 */

#include <AL/al.h>
#include <AL/alut.h>
#include "soundfile.h"
#include "OpenALSource.h"

OpenALSource::OpenALSource() {
  source = GenSource();
}

OpenALSource::~OpenALSource() {
  alDeleteSources(1, &source);
}

ALuint OpenALSource::GetSource() {
  return source;
}
