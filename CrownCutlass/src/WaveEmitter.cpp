/* Crown and Cutlass
 * Wave Emitter Object Code
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <cstdlib>
#include <iostream>
#include "GLee.h"
#include <vector>
#include <list>
#include "Log.h"
#include "image.h"
#include "Point.h"
#include "Terrain.h"
#include "BoundBox.h"
#include "Wave.h"
#include "WaveEmitter.h"

using namespace std;

#define WAVE_RATE .002
#define EMIT_RATE 3300
#define EMIT_DIFF_PERCENT .2
#define EMIT_RATIO .2

class WaveLoc {
 public:
  float m_x;
  float m_z;
  float m_angle;
};

GLuint WaveEmitter::s_waveList = 0;
list <Wave*> WaveEmitter::s_activeWaves;

WaveEmitter::WaveEmitter(Terrain *terrain, BoundBox *box){
  // List of points on shore
  list< Point* > shorePoints;
  Point *temp;
  float tAngle = 0.0;
  WaveLoc *tLoc;

  m_ticksToNextEmit = (int) (EMIT_RATE*(rand()/(RAND_MAX+1.0)));

  //counter = 0.0;

  m_isVisible = false;

  // Populate the shore line list
  terrain->LocateShoreLine(box, &shorePoints);

  while (!shorePoints.empty()) {
    // First, need to read the front element of list
    temp = shorePoints.front();
    // Next, remove that first element
    shorePoints.pop_front();

    // Find shore angle and move point to proper distance
    if (terrain->FindNearestShoreAngle(temp, &tAngle)) {
      // Set tLoc from point and add it to inactive queue
      tLoc = new WaveLoc();
      tLoc->m_x = temp->m_x;
      tLoc->m_z = temp->m_z;
      tLoc->m_angle = tAngle;
      m_waveLocations.push_back(tLoc);
    }

    // Finally, delete the Point the list element pointed to
    delete temp;
  }
}

WaveEmitter::~WaveEmitter(){
  // Delete the wave locations in vector
  WaveLoc *temp;
  while (!m_waveLocations.empty()) {
    temp = m_waveLocations.back();
    m_waveLocations.pop_back();
    delete temp;
  }
}

void WaveEmitter::Update(unsigned int ticks) {
  int numToGenerate;

  m_ticksToNextEmit -= ticks;

  if (m_ticksToNextEmit <= 0) {
    // Update ticksToNextEmit
    // NOTE: I add EMIT_RATE and ticksToNextEmit so that it keeps the emit rate exact (I hope)
    m_ticksToNextEmit = GetNextEmit() + m_ticksToNextEmit;

    // Add a new wave (only if visible???)
    if (m_isVisible) {
      numToGenerate = 1 + (int) (m_waveLocations.size() * EMIT_RATIO * (rand()/(RAND_MAX+1.0)));
      for (int i = 0; i < numToGenerate; i++) {
        GenerateWave();
      }
    }
  }

  m_isVisible = false;
}

void WaveEmitter::SetVisible() {
  m_isVisible = true;
}

int WaveEmitter::GetNextEmit() {
  int diff = (int) (EMIT_DIFF_PERCENT * EMIT_RATE);
  float randNum = (2.0 * (rand() / (RAND_MAX + 1.0))) - 1;

  return EMIT_RATE + (int) (diff * randNum);
}

void WaveEmitter::GenerateWaveList() {
  // Generate the surf list
  s_waveList = glGenLists(1);
  if (s_waveList == 0) {
    // Should throw an exception
    return;
  }

  glNewList(s_waveList, GL_COMPILE);

  // Draw the actual surf
  glBegin(GL_QUADS);
  glTexCoord2f(0, 1);
  glVertex3f(1, 0, -.3);
  glTexCoord2f(1, 1);
  glVertex3f(-1, 0, -.3);
  glTexCoord2f(1, 0);
  glVertex3f(-1, 0, .3);
  glTexCoord2f(0, 0);
  glVertex3f(1, 0, .3);
  glEnd();

  glEndList();
}

void WaveEmitter::DeleteWaveList() {
  glDeleteLists(s_waveList, 1);
}

void WaveEmitter::GenerateWave() {
  WaveLoc *temp;
  int index;

  index = (int) (m_waveLocations.size() * (rand() / (RAND_MAX + 1.0)));

  temp = m_waveLocations[index];

  s_activeWaves.push_back(new Wave(temp->m_x, temp->m_z, temp->m_angle));
}

int WaveEmitter::NumWaveLocations() {
  return m_waveLocations.size();
}

void WaveEmitter::DrawActiveWaves() {
  Wave *temp;
  list< Wave* >::iterator iter;
  list< Wave* >::iterator listEnd;

  if (s_activeWaves.size() <= 0) return;

  glEnable(GL_COLOR_MATERIAL);
  // Disable depth writes
  glDepthMask(GL_FALSE);

  iter = s_activeWaves.begin();
  listEnd = s_activeWaves.end();
  while(iter != listEnd) {
    temp = *iter;
    glPushMatrix();
    temp->PrepDraw();
    glCallList(s_waveList);
    glPopMatrix();

    ++iter;
  }

  // Reenable depth testing
  glDepthMask(GL_TRUE);
  glDisable(GL_COLOR_MATERIAL);
}

void WaveEmitter::UpdateActiveWaves(unsigned int ticks) {
  Wave *temp;
  list< Wave* >::iterator iter;
  list< Wave* >::iterator listEnd;

  if (s_activeWaves.size() <= 0) return;

  temp = s_activeWaves.front();
  while (!s_activeWaves.empty() && !temp->IsActive()) {
    s_activeWaves.pop_front();
    delete temp;
    temp = s_activeWaves.front();
  }

  iter = s_activeWaves.begin();
  listEnd = s_activeWaves.end();
  while(iter != listEnd) {
    temp = *iter;
    temp->Update(ticks);

    ++iter;
  }
}

void WaveEmitter::DeleteActiveWaves() {
  Wave *temp;

  while (!s_activeWaves.empty()) {
    temp = s_activeWaves.front();
    s_activeWaves.pop_front();
    delete temp;
  }
}
