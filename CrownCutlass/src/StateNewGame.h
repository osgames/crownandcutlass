/* Crown and Cutlass
 * State New Game Class Header
 */

#if !defined( _STATENEWGAME_H_ )

#define _STATENEWGAME_H_

#include "SDL.h"
#include "GLee.h"
#include "IGameState.h"
#include "Callback.h"

// So I don't have to include the headers
class StateSailing;
class Texture;
class CCTextField;
class CCButton;
class CCTable;

class StateNewGame: public IGameState {
 public:
  // Constructor
  StateNewGame();

  // Destructor
  ~StateNewGame();

  // Resets for another use
  void Reset();

  // Need to implement the Update and Display functions
  void Update(const unsigned int ticks);
  void Display();

  // Need to implement the switchTo function
  void SwitchTo(IGameState *oldState);

  // Need to implement the switchFrom function
  void SwitchFrom();

  // Callback Functions
  void Accept(int i);
  void Cancel(int i);

 private:
  // Check sdl input
  void CheckInput();

  // Main sailing game state
  StateSailing *m_mainGame;

  // Callback functions
  TCallback<StateNewGame> m_acceptCallback;
  TCallback<StateNewGame> m_cancelCallback;

  /*
   * All of the default widgets
   */
  CCTable* m_menuList;
  CCButton* m_acceptButton;                 // A button
  CCButton* m_cancelButton;
  CCTextField* m_nameText;
  CCTextField* m_shipText;

  Texture *m_background;

  // Font names
  static const std::string s_mainFontName;
  static const std::string s_highlightFontName;
  static const std::string s_disabledFontName;
  static const std::string s_smallFontName;
};

#endif
