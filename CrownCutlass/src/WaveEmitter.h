/* Crown and Cutlass
 * Wave Emitter Object Header
 */

#ifndef _WAVEEMITTER_H_
#define _WAVEEMITTER_H_

#include <vector>
#include <list>

using namespace std;

#define NUM_WAVES 5

class Wave;
class Terrain;
class BoundBox;
class Point;
struct WaveLoc;

class WaveEmitter {
public:
  WaveEmitter(Terrain *terrain, BoundBox *box);
  ~WaveEmitter();

  void Update(unsigned int ticks);

  void SetVisible();

  int NumWaveLocations();

  static void DrawActiveWaves();
  static void UpdateActiveWaves(unsigned int ticks);
  static void DeleteActiveWaves();

  static void GenerateWaveList();
  static void DeleteWaveList();

private:
  // Surf display list
  static GLuint s_waveList;

  // Static list of all the waves
  static list < Wave* > s_activeWaves;

  // Number of ticks until next emit
  int m_ticksToNextEmit;

  // Active waves
  vector< WaveLoc* > m_waveLocations;

  // Whether this area is visible or not
  bool m_isVisible;

  // Generate a ticks to next emit using the random percent difference
  int GetNextEmit();

  // Pick a random wave location and add it to the static wave array
  void GenerateWave();
};

#endif
