/* Crown and Cutlass
 * City Class
 *
 * This is inherited from the Environment Object. It will keep
 * the location, name, and economic information of the city.
 */

#include <string>
#include "GLee.h"
#include "IEnvironmentObject.h"
#include "image.h"
#include "font.h"
#include "Terrain.h"
#include "Economy.h"
#include "Product.h"
#include "CityEconomy.h"
#include "Config.h"
#include "Log.h"
#include "BuildingList.h"
#include "City.h"

using namespace std;

City::City(Terrain *landIn, string cityName, int cityLocX, int cityLocZ, BuildingList *buildingsIn):
      IEnvironmentObject(cityName, cityLocX, cityLocZ, true) {
  int xDiff = (int) landIn->GetWidth() / 2;
  int zDiff = (int) landIn->GetHeight() / 2;

  height = landIn->GetHeight(cityLocX + xDiff, cityLocZ + zDiff);

  land = landIn;

  MyEconomy = Economy::s_economy->CreateCity(cityName.c_str());

  buildings = buildingsIn;
  buildings->AddCityLocation(locX, height, locZ);
}

City::~City() {
  delete buildings;
}

void City::DisplayObject() {
  buildings->Draw();
}

bool City::CheckName(const char *cityName) {
  if (name.compare(cityName)) return true;

  return false;
}
