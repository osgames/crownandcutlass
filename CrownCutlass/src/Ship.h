/* Crown and Cutlass
 * Ships Header
 */

#if !defined ( _SHIPS_H_ )
#define _SHIPS_H_

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include "ISaveObject.h"

using boost::shared_ptr;

class Cargo;
class ShipResource;

enum SailState { NOSAILS, HALFSAILS, FULLSAILS };
enum RudderState { LEFT, CENTER, RIGHT };
enum ShipState { NORMAL, SINKING, SUNK };

// This is the public class that other programs use to handle a ship.  It
//   wraps up the ShipResource class
class Ship: public ISaveObject {
 public:
   Ship(std::string name, std::string shipTitle);

   // Create a ship of a random type
   Ship(std::string shipTitle);

  // Copy constructor
  // Note: The ships will share the cargo object
  Ship(Ship *copy);

  ~Ship();

  void Draw(bool doTranslate, float dX, float dZ);

  int GetMaxDamage();

  shared_ptr<Cargo> GetCargo();

  void Update(const unsigned int ticks);

  void RaiseSails();
  void LowerSails();
  void SetSails(SailState newSails);
  SailState GetSails();
  const std::string& GetSailsStr();

  void SetRudder(RudderState newRudder);

  void SinkShip();
  ShipState GetShipState();
  bool IsShipNormal();
  bool IsShipSinking();
  bool IsShipSunk();

  void GetDirectionVector(double output[3]);

  // Dump some debug info
  void Dump();

  // Reset the speed, damage and rot
  void Reset();

  // For SaveObject
  void Save(TiXmlElement *parent);
  void Load(TiXmlElement *parent);

  static const std::string& GetSailStateStr(SailState sails);

  float m_x, m_z;
  float m_rot;
  float m_speed;
  int m_damage;
  std::string m_title;

 private:
  void AcquireResource(std::string name);
  void ReleaseResource();

  ShipResource *m_resource;

  shared_ptr<Cargo> m_cargo;

  SailState m_sails;
  RudderState m_rudder;

  ShipState m_state;

  float m_rotSpeed;

  float m_shipSine;
  float m_shipCos;
  float m_rotX;
  float m_rotXAngle;
};

#endif
