/* Crown and Cutlass
 * Economy Class Header
 *
 * The Economy Class is the top level of all the
 * economies present in Crown and Cutlass. It
 * is the authority on how products and prices
 * change.
 *
 * After the creation of the Economy, the economy
 * doesn't interact much with other classes. Each
 * City will have a pointer to an element within
 * the Economy so the city can access and change the
 * information if it wants to. The only thing that
 * needs to be called is Step() once and a while
 * so that the Economy will actually change.
 */

#if !defined( _ECONOMY_H_ )

#define _ECONOMY_H_

#include <vector>
#include <string>
#include <map>

#include "tinyxml.h"

class Product;
class CityManager;
class CityEconomy;

using namespace std;

class Economy {
 public:
  /* Constructor
   * The constructor creates the Economy and opens
   * the file in filename. It does not create the
   * CityEconomies. They are created using CreateCity.
   * This allows a basic error checking system so that
   * there aren't cities without Economies and no
   * Economies without cities.
   */
	Economy(const char* filename);

	// Destructor
	~Economy();

  // Static methods
  static bool CreateEconomy(); // This creates the Economy for the Static Economy
  static void DestroyEconomy();
  static Economy* s_economy;

	// Methods
  // This changes the state of the economy
	void Step();
	void UpdateEconomy();

	void Print();

  CityEconomy* CreateCity(const char* CityName);

  bool CheckCities(CityManager* tempManager);

  int ProductCount();

  void BuyProduct(int ProdID, float price);
  void SellProduct(int ProdID, float price);

  vector<string> ProdList;
	vector<int> ProductDefaults;

 private:

  int FindID(string Name);
	map<string, CityEconomy*> CityList;

  TiXmlDocument doc;
  TiXmlNode* EconomyNode;

	int num_steps;
 
	vector<Product*> ParseCityProducts(TiXmlElement* CityElement);
};

#endif
