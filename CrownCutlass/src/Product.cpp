/* Crown and Cutlass
 * Product Class
 */

#include <string>
#include <iostream>

#include "ccmath.h"
#include "Log.h"
#include "Economy.h"
#include "Product.h"

using namespace std;

Product::Product() {
	m_ID = -1;
	m_ConsumptionRate = 0;
	m_ProductionRate = 0;
	m_ProductionPoints = 0;
}

Product::Product(int myID, int Consumption, int Production) {
	m_ID = myID;
	if (Consumption < 0) {
		Log::s_log->Message("Illegal Consumption Rate %i", Consumption);
		throw string("Illegal Consumption Rate");
	}
	m_ConsumptionRate = Consumption;
	if (Production < 0) {
		Log::s_log->Message("Illegal Production Rate %i", Production);
		throw string("Illegal Production Rate");
	}
	m_ProductionRate = Production;
}

Product::~Product() {

}

int Product::GetConsumption() {
	return m_ConsumptionRate;
}

void Product::SetConsumption(int Consumption) {
	if (Consumption < 0) throw string("Illegal Consumption Rate");
	m_ConsumptionRate = Consumption;
}

int Product::GetProduction() {
	return m_ProductionRate;
}

void Product::SetProduction(int Production) {
	if (Production < 0) throw string("Illegal Production Rate");
	m_ProductionRate = Production;
}

int Product::GetPrice(int CitySize) {
	/* This isn't quite right...
	 * Its more complicated...Production - Consumption gives you the rate
	 * of change in the storage. So then the amount stored of the product
	 * and the amount changed then looks at a comfort level for the product
   * to determine the price.
	 */
	int Price;
  Price = ((m_ProductionPoints * m_ProductionRate) - (m_ConsumptionRate * CitySize)) 
					+ Economy::s_economy->ProductDefaults[m_ID];
  Log::s_log->DebugMessage("Price= %i", Price);
	return Price;
}

string Product::GetName() {
  return Economy::s_economy->ProdList[m_ID - 1];
}

void Product::Print() {
	cout << "Product Name: " << GetName() 
		<< "\n\tProduction: " << m_ProductionRate 
		<< "\n\tConsumption: " << m_ConsumptionRate 
		<< endl;
}

int Product::GetProductionPoints() {
  return m_ProductionPoints;
}
void Product::SetProductionPoints(int PP) {
	if (PP < 0) return;
	m_ProductionPoints = PP;
}
int Product::AddProductionPoints(int PP) {
  if (m_ProductionPoints - PP < 0) return m_ProductionPoints;
  m_ProductionPoints += PP;
  Log::s_log->DebugMessage("%s's PPs: %i", GetName().c_str(), m_ProductionPoints);
  return m_ProductionPoints;
}
