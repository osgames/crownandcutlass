/* Crown and Cutlass
 * Camera Code
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include "GLee.h"
#include <GL/glu.h>
#include "ccmath.h"
#include "Camera.h"

#if !defined (MSVC6)
using namespace std;
#endif

Camera::Camera(float winRatio, float x, float y, float z) {
  ChangeRatio(winRatio);

  // Set the camera position
  SetPosition(x, y, z);
}

void Camera::SetPosition(float x, float y, float z) {
  m_position[0] = x;
  m_position[1] = y;
  m_position[2] = z;

  CalcValues();
  SetCamera();
}

void Camera::ChangeRatio(float winRatio) {
  m_ratio = winRatio;
}

void Camera::SetProjection() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0, m_ratio, 1.0, 512.0);

  glMatrixMode(GL_MODELVIEW);
}

void Camera::SetCamera() {
  SetProjection();

  glRotatef(m_angle, 1.0, 0.0, 0.0);
  glRotatef(m_angle2, 0.0, 1.0, 0.0);
  glTranslatef(m_position[0], -1*m_position[1], -1*m_position[2]);
}

void Camera::CalcValues() {
  m_angle = radiansToDegrees(atan2(m_position[1], fabs(m_position[2])));
  m_angle2 = radiansToDegrees(atan2(m_position[0], m_position[2]));
}
