/* Crown and Cutlass
 * Config Object Code
 */

#include <cstring>
#include <string>
#include <sstream>
#include <iostream>
#include "Log.h"
#include "Config.h"
#include "GLee.h"
#include "tinyxml.h"

using namespace std;

Config *Config::s_config = NULL;

Config::Config(const std::string filename) {
  TiXmlDocument doc( filename );
  TiXmlElement* config;
  TiXmlElement* resolution;
  TiXmlElement* temp;
  TiXmlText* data;
  int n;

  m_winWidth = 640;
  m_winHeight = 480;
  m_fullscreen = false;
  m_useVBO = false;
  m_useSDLParachute = true;
  m_compressTextures = true;
  m_ticksBetweenBattles = 20000;
  m_numberALSources = 16;
  m_debugLogMode = false;
  m_startingShipType = "Sloop";

  doc.LoadFile();
  if (doc.Error()) {
    Log::s_log->Message("Warning: %s (line %d)  Using defaults", doc.ErrorDesc(), doc.ErrorRow());
    LogConfig();
    return;
  }

  config = doc.FirstChildElement("GameConfiguration"); // This gets the <GameConfiguration> tag
  if (config == NULL) {
    Log::s_log->Message("Warning: <GameConfiguration> did not parse correctly.");
    LogConfig();
    return;
  }

  try {
    resolution = config->FirstChildElement("Resolution"); // This gets <Resolution>
    if (resolution == NULL) throw 0;

    // Deal with Resolution
    temp = resolution->FirstChildElement("Width");
    if (temp == NULL) throw 0;
    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    n = sscanf(data->Value(), "%d", &m_winWidth);
    if (n != 1) throw 1;

    temp = resolution->FirstChildElement("Height");
    if (temp == NULL) throw 0;
    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 2;
    n = sscanf(data->Value(), "%d", &m_winHeight);
    if (n != 1) throw 2;
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <Resolution> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid <Width> data, using defaults");
      break;
    case 2:
      Log::s_log->Message("Warning: Invalid <Height> data, using defaults");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <Resolution> error");
      break;
    }

    // Set it back to the defaults
    m_winWidth = 640;
    m_winHeight = 480;
  }

  try {
    // Deal with Fullscreen
    temp = config->FirstChildElement("Fullscreen");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    if (strcmp(data->Value(), "true") == 0) {
      m_fullscreen = true;
    } else if (strcmp(data->Value(), "false") == 0) {
      m_fullscreen = false;
    } else {
      throw 1;
    }
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <Fullscreen> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid Fullscreen data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <Fullscreen> error");
      break;
    }
  }

  try {
    // Deal with VBOs
    temp = config->FirstChildElement("VBO");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    if (strcmp(data->Value(), "true") == 0) {
      m_useVBO = true;
    } else if (strcmp(data->Value(), "false") == 0) {
      m_useVBO = false;
    } else {
      throw 1;
    }
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <VBO> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid VBO data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <VBO> error");
      break;
    }
  }

  try {
    // Deal with SDLParachute
    temp = config->FirstChildElement("SDLParachute");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    if (strcmp(data->Value(), "true") == 0) {
      m_useSDLParachute = true;
    } else if (strcmp(data->Value(), "false") == 0) {
      m_useSDLParachute = false;
    } else {
      throw 1;
    }
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <SDLParachute> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid SDLParachute data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <SDLParachute> error");
      break;
    }
  }

  try {
    // Deal with CompressTextures
    temp = config->FirstChildElement("CompressTextures");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    if (strcmp(data->Value(), "true") == 0) {
      m_compressTextures = true;
    } else if (strcmp(data->Value(), "false") == 0) {
      m_compressTextures = false;
    } else {
      throw 1;
    }
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <CompressTextures> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid CompressTexture data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <CompressTexture> error");
      break;
    }
  }

  try {
    // Deal with TicksBetweenBattles
    temp = config->FirstChildElement("TicksBetweenBattles");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    n = sscanf(data->Value(), "%u", &m_ticksBetweenBattles);
    if (n != 1) throw 1;
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <TicksBetweenBattles> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid TicksBetweenBattles data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <TicksBetweenBattles> error");
      break;
    }
  }

  try {
    // Deal with NumberALSources
    temp = config->FirstChildElement("NumberALSources");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    n = sscanf(data->Value(), "%u", &m_numberALSources);
    if (n != 1) throw 1;
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <NumberALSources> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid NumberALSources data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <NumberALSources> error");
      break;
    }
  }

  try {
    // Deal with debug log mode
    temp = config->FirstChildElement("DebugLogMode");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    if (strcmp(data->Value(), "true") == 0) {
      m_debugLogMode = true;
    } else if (strcmp(data->Value(), "false") == 0) {
      m_debugLogMode = false;
    } else {
      throw 1;
    }
  } catch (int i) {
    switch (i) {
    case 0:
      Log::s_log->Message("Warning: <DebugLogMode> did not parse correctly");
      break;
    case 1:
      Log::s_log->Message("Warning: Invalid DebugLogMode data, using default");
      break;
    default:
      // Shouldn't get here
      Log::s_log->Message("Warning: Unknown <DebugLogMode> error");
      break;
    }
  }
  Log::s_log->SetDebugMode(m_debugLogMode);

  try {
    // Deal with StartingShipType
    temp = config->FirstChildElement("StartingShipType");
    if (temp == NULL) throw 0;

    data = temp->FirstChild()->ToText();
    if (data == NULL) throw 1;
    m_startingShipType = data->Value();
  } catch (int i) {
    switch (i) {
      case 0:
        Log::s_log->Message("Warning: <StartingShipType> did not parse correctly");
        break;
      case 1:
        Log::s_log->Message("Warning: Invalid StartingShipType data, using default");
        break;
      default:
      // Shouldn't get here
        Log::s_log->Message("Warning: Unknown <StartingShipType> error");
        break;
    }
  }

  LogConfig();
}

// Doesn't really need to do anything
Config::~Config() {
}

// Note: this needs to happen after we have an opengl context, so it can't
//   happen in the constructor
bool Config::CheckExtensions() {
  // Check multi-texturing functions
  if (!GLEE_ARB_multitexture) {
    // Probably should just throw an exception
    Log::s_log->Message("Error: Your video card and/or drivers do not support multitexturing");
    return false;
  }

  // Check vertex array drawing function
  if (!GLEE_EXT_draw_range_elements) {
    // Probably should just throw an exception
    Log::s_log->Message("Error: Your video card and/or drivers do not support glDrawRangeElements");
    return false;
  }

  // Check VBOs if enabled
  if (Config::s_config->CheckVBO() && !GLEE_ARB_vertex_buffer_object) {
    m_useVBO = false;
    Log::s_log->Message("Warning: Your video card and/or drivers do not support VBOs, falling back on VAs");
  }

  // We have multitexturing, so we can continue
  return true;
}

bool Config::CheckVBO() {
  return m_useVBO;
}

int Config::GetWinWidth() {
  return m_winWidth;
}

int Config::GetWinHeight() {
  return m_winHeight;
}

bool Config::CheckFullscreen() {
  return m_fullscreen;
}

bool Config::CheckSDLParachute() {
  return m_useSDLParachute;
}

bool Config::CheckCompressTextures() {
  return m_compressTextures;
}

bool Config::OpenConfig(const std::string filename) {
  if (s_config != NULL) {
    Log::s_log->Message("Warning: Open config on non-NULL configuration");
    return false;
  }

  s_config = new Config(filename);
  return true;
}

void Config::CloseConfig() {
  if (s_config == NULL) {
    Log::s_log->Message("Warning: Close config on NULL configuration");
    return;
  }

  delete s_config;
  s_config = NULL;
}

void Config::LogConfig() {
  ostringstream outputString;

  outputString << "Configuration:" << endl;
  outputString << "Resolution: " << m_winWidth << " x " << m_winHeight << endl;

  outputString << "Fullscreen: ";
  if (m_fullscreen) outputString << "true";
  else outputString << "false";
  outputString << endl;

  outputString << "Use VBOs: ";
  if (m_useVBO) outputString << "true";
  else outputString << "false";
  outputString << endl;

  outputString << "Enable SDL Parachute: ";
  if (m_useSDLParachute) outputString << "true";
  else outputString << "false";
  outputString << endl;

  outputString << "Compress Textures: ";
  if (m_compressTextures) outputString << "true";
  else outputString << "false";
  outputString << endl;

  outputString << "TicksBetweenBattles: ";
  outputString << m_ticksBetweenBattles;
  outputString << endl;

  outputString << "NumberALSources: ";
  outputString << m_numberALSources;
  outputString << endl;

  outputString << "DebugLogMode: ";
  if (m_debugLogMode) outputString << "true";
  else outputString << "false";
  outputString << endl;

  outputString << "Starting Ship Type: \"" << m_startingShipType << "\"";

  Log::s_log->Message(outputString.str());
}

unsigned int Config::GetTicksBetweenBattles() {
  return m_ticksBetweenBattles;
}

unsigned int Config::GetNumberALSources() {
  return m_numberALSources;
}

bool Config::DebugLogEnabled() {
  return m_debugLogMode;
}

string Config::GetStartingShipType() {
  return m_startingShipType;
}
