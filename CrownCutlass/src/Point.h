/* Crown and Cutlass
 * Point Object Header
 * Note: For use with the terrain quadtree
 */

#if !defined( _POINT_H_ )

#define _POINT_H_

#include "GLee.h"

// A point in 3d space
class Point {
 public:
  // Generic constructor
  Point();

  // Copy contrustor
  Point(Point *copy);

  // Construct a point at (xIn, yIn, zIn);
  Point(GLfloat xIn, GLfloat yIn, GLfloat zIn);

  GLfloat m_x;
  GLfloat m_y;
  GLfloat m_z;

  // Dump out coords
  void Dump();

  // Just give the glVertex3f call for this point
  void Draw();

  // Calls glTranslatef with these coords
  void Translatef();

  // Average two points, store in result
  static void Average(Point *a, Point *b, Point *result);
};

#endif
