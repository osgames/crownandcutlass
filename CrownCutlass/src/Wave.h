/* Crown and Cutlass
 * Wave Object Header
 */

#ifndef _WAVE_H_
#define _WAVE_H_

#include <GL/gl.h>

class Wave {
public:
  Wave(float x, float z, float angle);

  ~Wave();

  void Update(int ticks);

  void PrepDraw();

  bool IsActive();

private:
  float m_x;
  float m_z;
  float m_angle;

  bool m_active;

  float m_counter;
};

#endif
