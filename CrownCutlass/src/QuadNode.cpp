/* Crown and Cutlass
 * Quad-Tree Node Object Code
 * Note: For use with the terrain quadtree
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <iostream>
#include "Terrain.h"
#include "BoundBox.h"
#include "Point.h"
#include "Frustum.h"
#include "WaveEmitter.h"
#include "Ocean.h"
#include "QuadNode.h"

int QuadNode::s_quadSize = 40;

using namespace std;

QuadNode::QuadNode(Terrain *terrain, BoundBox *box) {
  GLfloat xSize, zSize;
  GLfloat tHigh, tLow;

  m_indexStartRange = 0;
  m_indexEndRange = 0;

  m_child1 = NULL;
  m_child2 = NULL;
  m_child3 = NULL;
  m_child4 = NULL;

  m_nodeBox = box;

  xSize = m_nodeBox->m_b2->m_x - m_nodeBox->m_b1->m_x;
  zSize = m_nodeBox->m_b3->m_z - m_nodeBox->m_b1->m_z;

  if ((xSize < s_quadSize) && (zSize < s_quadSize)) {
    // Small enough to be a leaf
    m_indexArray = terrain->GenIndexArray(m_nodeBox, &m_vertexCount, &tLow, &tHigh, &m_indexStartRange, &m_indexEndRange);

    m_nodeBox->SetHeight(tLow, tHigh);

    m_emitter = new WaveEmitter(terrain, m_nodeBox);
    if (m_emitter->NumWaveLocations() > 0) {
      // No sense in bothering registering it with the ocean object if it
      //   has no shore in it
      terrain->m_ocean->AddEmitter(m_emitter);
    }
  } else {
    // Need to break into children

    // Make sure indexArray and emitter are NULL
    m_indexArray = NULL;
    m_emitter = NULL;

    // Set up its max/min height
    terrain->GetMinMax(m_nodeBox, &tLow, &tHigh);
    m_nodeBox->SetHeight(tLow, tHigh);

    // Child boxes
    BoundBox *child1Box;
    BoundBox *child2Box;
    BoundBox *child3Box;
    BoundBox *child4Box;

    // Set up child1
    child1Box = new BoundBox();
    child1Box->m_b1 = new Point(m_nodeBox->m_b1);

    child1Box->m_b2 = new Point();
    Point::Average(m_nodeBox->m_b1, m_nodeBox->m_b2, child1Box->m_b2);

    child1Box->m_b3 = new Point();
    Point::Average(m_nodeBox->m_b1, m_nodeBox->m_b3, child1Box->m_b3);

    child1Box->m_b4 = new Point();
    Point::Average(m_nodeBox->m_b1, m_nodeBox->m_b4, child1Box->m_b4);

    m_child1 = new QuadNode(terrain, child1Box);

    // Set up child2
    child2Box = new BoundBox();
    child2Box->m_b1 = new Point(child1Box->m_b2);
    child2Box->m_b2 = new Point(m_nodeBox->m_b2);
    child2Box->m_b3 = new Point(child1Box->m_b4);

    child2Box->m_b4 = new Point();
    Point::Average(m_nodeBox->m_b2, m_nodeBox->m_b4, child2Box->m_b4);

    m_child2 = new QuadNode(terrain, child2Box);

    // Set up child3
    child3Box = new BoundBox();
    child3Box->m_b1 = new Point(child1Box->m_b3);
    child3Box->m_b2 = new Point(child1Box->m_b4);
    child3Box->m_b3 = new Point(m_nodeBox->m_b3);

    child3Box->m_b4 = new Point();
    Point::Average(m_nodeBox->m_b3, m_nodeBox->m_b4, child3Box->m_b4);

    m_child3 = new QuadNode(terrain, child3Box);

    // Set up child4
    child4Box = new BoundBox();
    child4Box->m_b1 = new Point(child1Box->m_b4);
    child4Box->m_b2 = new Point(child2Box->m_b4);
    child4Box->m_b3 = new Point(child3Box->m_b4);
    child4Box->m_b4 = new Point(m_nodeBox->m_b4);

    m_child4 = new QuadNode(terrain, child4Box);

    // All of those child boxes get handled (ie deleted) by the children
  }
}

QuadNode::~QuadNode() {
  delete m_nodeBox;

  // If it's a leaf, delete the index array
  if (m_indexArray != NULL) {
    delete []m_indexArray;
    m_indexArray = NULL;
  }

  if (m_emitter != NULL) {
    delete m_emitter;
  }

  delete m_child1;
  delete m_child2;
  delete m_child3;
  delete m_child4;
}

void QuadNode::Draw(Frustum *frustum) {
  bool visible;

  // Check to see if it's visible
  visible = frustum->CheckBox(m_nodeBox);
  if (!visible) return;

  if (m_indexArray != NULL) {
    // This is a leaf, draw it
    glDrawRangeElements(GL_TRIANGLE_STRIP, m_indexStartRange, m_indexEndRange, m_vertexCount, GL_UNSIGNED_INT, m_indexArray);

    // Set this emitter as visible
    m_emitter->SetVisible();

    //glDisable(GL_FOG);
    //nodeBox->draw();
    //glEnable(GL_FOG);
    return;
  } else {
    // It must be visible and not a leaf, so try the children
    //nodeBox->draw();

    if (m_child1 != NULL) m_child1->Draw(frustum);
    if (m_child2 != NULL) m_child2->Draw(frustum);
    if (m_child3 != NULL) m_child3->Draw(frustum);
    if (m_child4 != NULL) m_child4->Draw(frustum);
  }
}

void QuadNode::Dump() {
  if (m_indexArray != NULL) {
    cout << "Leaf Node (Vertex Count  " << m_vertexCount << ", " << m_indexStartRange << "-" << m_indexEndRange << "):" << endl;
  } else {
    cout << "Non-leaf Node:" << endl;
  }
  m_nodeBox->Dump();
}
