/* Crown and Cutlass
 * City Economy Class
 */

#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "ccmath.h"
#include "Product.h"
#include "Log.h"
#include "CityEconomy.h"

using namespace std;

#define PPSPERSIZE 5

CityEconomy::CityEconomy(const char* CityName, vector<Product*> ProductList, int CitySize) {
	unsigned int i;
	int EvenlyDividedPPs;
  m_Products = ProductList;
  m_Name = CityName;
  m_Size = CitySize;
  m_UnallocatedProductionPoints = CitySize * PPSPERSIZE;
  EvenlyDividedPPs = m_UnallocatedProductionPoints / m_Products.size();
  for (i = 0; i < m_Products.size(); i++) 
  	m_Products[i]->SetProductionPoints(EvenlyDividedPPs);
}

CityEconomy::~CityEconomy() {
  unsigned int i;
  Product* tempProduct;

  for (i = 0; i < m_Products.size(); i++) {
    tempProduct = (Product*)m_Products[i];
    delete tempProduct;
  }

  m_Products.clear();

}

string CityEconomy::GetCityName() {
  return m_Name;
}


Product* CityEconomy::GetProduct(char* ProductName) {
  string compare;
  Product* tempProduct = NULL;
  unsigned int i;

  compare = ProductName;

  for (i = 0; i < m_Products.size(); i++) {
    tempProduct = (Product*)m_Products[i];
    if (compare == tempProduct->GetName()) break;
  }

  return tempProduct;

}

float CityEconomy::GetPrice(char* ProductName) {
  string compare;
  Product* tempProduct = NULL;
  unsigned int i;

  compare = ProductName;

  for (i = 0; i < m_Products.size(); i++) {
    tempProduct = (Product*)m_Products[i];
    if (compare == tempProduct->GetName()) break;
  }

  if (tempProduct == NULL) {
    // This should probably throw an exception
    return 0;
  }

  return tempProduct->GetPrice(m_Size);

}


float CityEconomy::GetPrice(string ProductName) {
  string compare;
  Product* tempProduct = NULL;
  unsigned int i;

  compare = ProductName;

  for (i = 0; i < m_Products.size(); i++) {
    tempProduct = (Product*)m_Products[i];
    if (compare == tempProduct->GetName()) break;
  }

  if (tempProduct == NULL) {
    // This should probably throw an exception
    return 0;
  }

  return tempProduct->GetPrice(m_Size);

}

void CityEconomy::Print() {
  unsigned int i;
  Product* temp;

  cout << m_Name << ":\n";
  for (i = 0; i < m_Products.size(); i++) {
    temp = (Product*)m_Products[i];
    temp->Print();
  }
}

void CityEconomy::UpdateEconomy(int steps) {
  AllocateProductionPoints();
}

vector<Product*> CityEconomy::GetProductList() {
  return m_Products;
}

int CityEconomy::GetSize() {
  return m_Size;
}

void CityEconomy::IncreaseSize() {
  if (m_Size >= 10) {
    Log::s_log->Message("City has reached its maximum size");
    return;
  }
  m_Size += 1;
}

void CityEconomy::DecreaseSize() {
  if (m_Size <= 1) {
    Log::s_log->Message("City has reached its minimum size");
    return;
  }
  m_Size -= 1;
}

void CityEconomy::AllocateProductionPoints() {
	unsigned int i;
	int TotalBasketOfGoods = 0;
	float Percent;
	int PPsToMove;
	/*
    Not exactly sure how this will work, but get Total value for all the
    products. Then get the percentage of the value of the total value.
    Then distribute the production points based on those.
	*/
  for (i = 0; i < m_Products.size(); i++) {
    TotalBasketOfGoods += m_Products[i]->GetPrice(m_Size);
  }
  
  for (i = 0; i < m_Products.size(); i++) {
  	Percent = ((float)m_Products[i]->GetPrice(m_Size) / (float)TotalBasketOfGoods);
  	// We want to take away from the lowest producers
  	if (Percent < (1/m_Products.size())) {
  		++m_UnallocatedProductionPoints;
  		m_Products[i]->AddProductionPoints(-1);
  	} else if (m_UnallocatedProductionPoints > 0) {
  		// and give to the highest producers
  		PPsToMove = (int) ((float) m_UnallocatedProductionPoints * Percent);
  		Log::s_log->DebugMessage("Moving %i PPs", PPsToMove);
  		m_Products[i]->AddProductionPoints(PPsToMove);
  		m_UnallocatedProductionPoints -= PPsToMove;
  	}
  }
}
