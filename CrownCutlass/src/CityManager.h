/* Crown and Cutlass
 * City Manager Class
 *
 * This class is a vector of cities. It will handle all the
 * appropriate actions to take on the cities. (Displaying,
 * checking collisions, etc)
 */

#ifndef _CITYMANAGER_H_

#define _CITYMANAGER_H_

#include <vector>
#include <map>
#include <string>

class Ship;
class City;
class Terrain;
class Model;
class TiXmlElement;

using namespace std;

class CityManager {
public:
  // Constructor
  CityManager(Terrain *land);

  // Destructor
  ~CityManager();

  // Methods
  City* CheckCollision(Ship *ship);

  void DisplayCities();

  bool CheckCity(const char *cityName);

  unsigned int NumberCities();

  City* GetCity(const char *cityName);
  City* GetCity(int CityIndex);

private:
  vector<string> Keys;
  map<string, City*> CityList;

  void LoadCityConfig(const char *filename, Terrain *land);

  // ParseClusters reads the building clusters from the xml file and loads the models into the clusters vector
  void ParseClusters(TiXmlElement *clustersNode);
  vector<Model*> clusters;

  Model *tempModel;

};

#endif
