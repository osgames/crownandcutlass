/* Crown and Cutlass
 * Camera Header
 */

#if !defined( _CAMERA_H_ )

#define _CAMERA_H_

class Camera {
 public:
  // Contructor, pass in window width & height
  Camera(float winRatio, float x, float y, float z);

  // Set the camera location
  void SetPosition(float x, float y, float z);

  // Update the ratio
  void ChangeRatio(float winRatio);

  // Set the projection matrix
  void SetProjection();

  void SetCamera();

 private:
  // Calculate the dist, angles based on position
  void CalcValues();

  float m_ratio;
  float m_angle;
  float m_angle2;
  float m_position[3];
};

#endif
