/* Crown and Cutlass
 * Image Manipulating and Saving Header
 * Note: Based on Chapter 7 of OpenGL Game Programming and NeHe lesson 6
 */

#if !defined( _IMAGE_H_ )

#define _IMAGE_H_

#include "GLee.h"

typedef struct {
  unsigned char imageTypeCode;
  short int     imageWidth;
  short int     imageHeight;
  unsigned char bitCount;
  unsigned char *imageData;
} TGAFILE;

// Uses SDL_image library
bool BuildTexture(const char *filename, GLuint *texId, GLint param, bool genMips, bool tryCompress, bool tryAlpha);

// Uses the TGAFILE struct, so use BuildTexture instead
int LoadTGAFile(const char *filename, TGAFILE *tgaFile);

// Uses the TGAFILE struct, should rewrite to use SDL
int WriteTGAFile(const char *filename, short int width, short int height, unsigned char* imageData);

void SaveScreenshot(const char *filename, int x, int y);

#endif
