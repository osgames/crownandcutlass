/* Crown and Cutlass
 * Point Object Code
 * Note: For use with the terrain quadtree
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <iostream>
#include "GLee.h"
#include "Log.h"
#include "Point.h"

using namespace std;

// Generic constructor
Point::Point() {
  m_x = 0;
  m_y = 0;
  m_z = 0;
}

// Copy constructor
Point::Point(Point *copy) {
  m_x = copy->m_x;
  m_y = copy->m_y;
  m_z = copy->m_z;
}

// Construct point at (xIn, yIn, zIn)
Point::Point(GLfloat xIn, GLfloat yIn, GLfloat zIn) {
  m_x = xIn;
  m_y = yIn;
  m_z = zIn;
}

void Point::Dump() {
  Log::s_log->Message("(%f, %f, %f)", m_x, m_y, m_z);
}

// Average two points, store in result
void Point::Average(Point *a, Point *b, Point *result) {
  result->m_x = (int) (a->m_x + b->m_x)/2;
  result->m_y = (int) (a->m_y + b->m_y)/2;
  result->m_z = (int) (a->m_z + b->m_z)/2;
}

void Point::Draw() {
  glVertex3f(m_x, m_y, m_z);
}

void Point::Translatef() {
  glTranslatef(m_x, m_y, m_z);
}
