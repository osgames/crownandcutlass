/* Crown and Cutlass
 * Ogg file loading header
 */

#if !defined( _SOUNDFILE_H_ )

#define _SOUNDFILE_H_

#include <AL/al.h>

// Load an ogg file into buffer
bool LoadOgg(const char *filename, ALuint *buffer);

// Load a wave file into buffer
//bool LoadWav(const char *filename, ALuint *buffer);

ALuint GenSource();
ALuint GenSource(ALuint buffer);

#endif
