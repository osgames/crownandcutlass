/* Crown and Cutlass
 * Wave Object Code
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <iostream>
#include "ccmath.h"
#include "SDL.h"
#include "GLee.h"
#include "Log.h"
#include "Wave.h"

using namespace std;

#define WAVE_END -.33
#define WAVE_RATE .002

Wave::Wave(float x, float z, float angle){
  m_counter = 0.0;

  m_x = x;
  m_z = z;
  m_angle = angle;

  m_active = true;
}

Wave::~Wave(){
}

void Wave::Update(int ticks){
  m_counter += WAVE_RATE * ticks;

  if (m_counter > 10) m_active = false;
}

void Wave::PrepDraw() {
  float dZ;
  float alpha;

  if (m_counter > 8) {
    dZ = WAVE_END;
    alpha = (10 - m_counter) / 2;
  } else {
    dZ = -1+WAVE_END+(m_counter/8);
    alpha = m_counter/7;
  }

  glTranslatef(m_x, 0, m_z);
  glRotatef(m_angle, 0, 1, 0);
  glTranslatef(0, 0, dZ);

  glColor4f(1.0, 1.0, 1.0, alpha);
}

bool Wave::IsActive() {
  return m_active;
}
