/* Crown and Cutlass
 * Environment Object
 *
 * This class is a parent to any object which may appear
 * in the game. Initially the things that come to mind which
 * this would be used as a parent class would be cities,
 * hidden treasures, and perhaps enemy pirate ships.
 */

#if !defined ( _IENVIRONMENTOBJECT_H_ )

#define _IENVIRONMENTOBJECT_H_

#include <string>

using namespace std;

class IEnvironmentObject {
public:
  // Constructors
  IEnvironmentObject();
  IEnvironmentObject(string objName, int objLocX, int objLocZ, bool objVisible);

  virtual ~IEnvironmentObject() { };

  // Methods

  // Returns the name of the object
  string getName();

  int GetLocX();
  int GetLocZ();

  // Returns distance from (x, z) to object
  float calcDist(float x, float z);

  // Return angle in radians from (x, z) to object
  float calcAngle(float x, float z);

  virtual void DisplayObject() = 0;

protected:
  int locX;
  int locZ;
  bool visible;
  string name;

private:
};

#endif
