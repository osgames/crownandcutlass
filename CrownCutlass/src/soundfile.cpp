/* Crown and Cutlass
 * Ogg file loading code
 */

#include <cstdio>
#include <vector>
#include <vorbis/vorbisfile.h>
#include <AL/al.h>
#include <AL/alut.h>
#include "Log.h"
#include "soundfile.h"

using namespace std;

#define BUFFER_SIZE   32768     // 32 KB buffers

bool LoadOgg(const char *fileName, ALuint *buffer) {
  int endian = 0;             // 0 for Little-Endian, 1 for Big-Endian
  int bitStream;
  long bytes;
  char *array;
  vector <char> data;
  FILE *f;
  ALenum format;
  ALsizei freq;

  vorbis_info *pInfo;
  OggVorbis_File oggFile;

  // Open for binary reading
  f = fopen(fileName, "rb");
  if (!f) {
    return false;
  }

  ov_open(f, &oggFile, NULL, 0);

 // Get some information about the OGG file
  pInfo = ov_info(&oggFile, -1);

  // Check the number of channels... always use 16-bit samples
  if (pInfo->channels == 1)
    format = AL_FORMAT_MONO16;
  else
    format = AL_FORMAT_STEREO16;

  // The frequency of the sampling rate
  freq = pInfo->rate;

  array = new char[BUFFER_SIZE];

  do {
    // Read up to a buffer's worth of decoded sound data
    bytes = ov_read(&oggFile, array, BUFFER_SIZE, endian, 2, 1, &bitStream);
    // Append to end of buffer
    data.insert(data.end(), array, array + bytes);
  } while (bytes > 0);

  delete []array;

  ov_clear(&oggFile);

  // Don't call fclose(f), ogg vorbis does it
  alGenBuffers(1, buffer);
  if (alGetError() != AL_NO_ERROR) {
    return false;
  }
  alBufferData(*buffer, format, &data[0], data.size(), freq);

  return true;
}

/*
bool LoadWav(const char *filename, ALuint *buffer) {
  ALenum format;
  ALsizei size;
  ALvoid* data;
  ALsizei freq;
  ALboolean loop;

  // Load wav data into a buffer.
  alGenBuffers(1, buffer);
  if (alGetError() != AL_NO_ERROR) {
    return false;
  }

  alutLoadWAVFile((ALbyte*)filename, &format, &data, &size, &freq, &loop);
  if (alGetError() != AL_NO_ERROR) {
    alDeleteBuffers(1, buffer);
    return false;
  }

  alBufferData(*buffer, format, data, size, freq);
  alutUnloadWAV(format, data, size, freq);

  return true;
}
*/

ALuint GenSource() {
  ALuint source;
  bool looping = false;
  ALfloat sourcePos[] = {0.0, 0.0, 0.0};
  ALfloat sourceVel[] = {0.0, 0.0, 0.0};

  // Bind buffer with a source.
  alGenSources(1, &source);

  if (alGetError() != AL_NO_ERROR)
    return 0;

  alSourcef(source, AL_PITCH, 1.0);
  alSourcef(source, AL_GAIN, 1.0);
  alSourcefv(source, AL_POSITION, sourcePos);
  alSourcefv(source, AL_VELOCITY, sourceVel);
  alSourcei(source, AL_LOOPING, looping);

  return source;
}

ALuint GenSource(ALuint buffer) {
  ALuint source = GenSource();

  if (source == 0) {
    return source;
  }

  alSourcei(source, AL_BUFFER, buffer);

  return source;
}
