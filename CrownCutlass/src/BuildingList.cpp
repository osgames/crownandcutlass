/* Crown and Cutlass
 * Building List Object Code
 */

#include <vector>
#include "Log.h"
#include "Point.h"
#include "Model.h"
#include "BuildingList.h"

class Building {
 public:
  Building(Model *modelIn, Point *pointIn, float rotationIn);
  ~Building();

  void Dump();

  void Draw();

  void AddToPoint(float x, float y, float z);
 private:
  Model *m_model;
  Point *m_point;
  float m_rotation;
};

Building::Building(Model *modelIn, Point *pointIn, float rotationIn) {
  m_model = modelIn;
  m_point = pointIn;
  m_rotation = rotationIn;
}

Building::~Building() {
  delete m_point;
}

void Building::Draw() {
  glPushMatrix();
  m_point->Translatef();
  glRotatef(m_rotation, 0, 1, 0);
  glRotatef(90, 0, 0, 1); // Until the model gets fixed...
  m_model->draw();
  glPopMatrix();
}

void Building::Dump() {
  Log::s_log->Message("Building");
  Log::s_log->Message("Rot: %f", m_rotation);
  m_point->Dump();
}

void Building::AddToPoint(float x, float y, float z) {
  m_point->m_x += x;
  m_point->m_y += y;
  m_point->m_z += z;
}


// Building list code

BuildingList::BuildingList() {
  // Does anything need to happen here?
}

BuildingList::~BuildingList() {
  unsigned int i;

  for (i = 0; i < m_list.size(); i++) {
    delete m_list[i];
    m_list[i] = NULL;
  }
  m_list.clear();
}

void BuildingList::Add(Model *modelIn, Point *pointIn, float rotationIn) {
  Building *temp;

  temp = new Building(modelIn, pointIn, rotationIn);
  m_list.push_back(temp);
}

void BuildingList::AddCityLocation(float x, float y, float z) {
  unsigned int i;

  for (i = 0; i < m_list.size(); i++) {
    m_list[i]->AddToPoint(x, y, z);
  }
}

void BuildingList::Dump() {
  unsigned int i;

  Log::s_log->Message("Building list");
  for (i = 0; i < m_list.size(); i++) {
    m_list[i]->Dump();
  }
  Log::s_log->Message("");
}

void BuildingList::Draw() {
  unsigned int i;

  for (i = 0; i < m_list.size(); i++) {
    m_list[i]->Draw();
  }
}
