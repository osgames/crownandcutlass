/* Crown and Cutlass
 * Sound Effect Header
 */

#if !defined( _SOUND_EFFECT_H_ )

#define _SOUND_EFFECT_H_

#include <string>
#include "ISound.h"

class SoundResource;

class SoundEffect: public ISound {
 public:
  SoundEffect(std::string name);
  ~SoundEffect();

 private:
  SoundResource *m_resource;

  void Play();
  void Pause();
  void Stop();
  void Update(unsigned int ticks);

  // Called when a source is aquired
  void InitializeSource();
};

#endif
