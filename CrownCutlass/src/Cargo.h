/* Crown and Cutlass
 * Cargo Object Header
 */

#if !defined ( _CARGO_H_ )
#define _CARGO_H_

#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include "ISaveObject.h"

class Product;

using namespace std;
using boost::shared_ptr;

class Cargo: public ISaveObject {
public:
  Cargo(string nameIn, unsigned int sizeIn);
  ~Cargo();

  unsigned int GetFilled();
  unsigned int GetSize();
  void SetSize(unsigned int sizeIn);
  unsigned int GetAvailQty();

  unsigned int GetQty(unsigned int product) throw ( const char *);
  void SetQty(unsigned int product, unsigned int qty) throw ( const char *);
  unsigned int AddQty(unsigned int product, int qty) throw ( const char *);
  void AddCargo(shared_ptr<Cargo> cargo) throw ( const char *);

  void FillRandom() throw ( const char*);
  void FillRandom(unsigned int qty) throw ( const char *);

  void Save(TiXmlElement *parent);
  void Load(TiXmlElement *parent);

  void Dump();

 private:
  // Clear out the cargo
  void Clear();

  vector<int> products;

  unsigned int size;
  unsigned int filled;

  string name;
};

#endif
