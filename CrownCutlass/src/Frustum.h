/* Crown and Cutlass
 * Frustum Code
 */

#if !defined( _FRUSTUM_H_ )

#define _FRUSTUM_H_

class BoundBox;

// Frustum used for culling quadtree
class Frustum {
 public:
  // Extracts projection data
  void GetFrustum();

  bool CheckBox(BoundBox *box);

 private:
  float m_frustum[6][4];
};

#endif
