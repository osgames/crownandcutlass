/* Crown and Cutlass
 * Player Data Object Code
 */
/*
#if defined (WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
*/
#include <cstdio>
#include <vector>
#include "GLee.h"
#include "ISaveObject.h"
#include "Player.h"
#include "Log.h"
#include "Config.h"
#include "Ship.h"
#include "tinyxml.h"

using namespace std;

Player* Player::player = NULL;

Player::Player(const string playerNameIn, const string shipNameIn): ISaveObject("Player") {
  playerName = playerNameIn;

  gold = 100;

  ship = new Ship(Config::s_config->GetStartingShipType(), shipNameIn);
}

Player::~Player() {
  delete ship;
}

void Player::Load(TiXmlElement *parent) {
  TiXmlElement *selfNode;

  selfNode = parent->FirstChildElement(m_XMLName);
  if (selfNode->QueryIntAttribute("gold", &gold) != TIXML_SUCCESS) {
    // Should throw exception
    Log::s_log->Message("Warning: Gold attribute missing in player node");
    return;
  }

  // Load the player name
  playerName = string(selfNode->Attribute("playerName"));

  ship->Load(selfNode);
}

void Player::Save(TiXmlElement *parent) {
  TiXmlElement selfNode(m_XMLName);

  selfNode.SetAttribute("gold", gold);
  selfNode.SetAttribute("playerName", playerName);

  ship->Save(&selfNode);
  parent->InsertEndChild(selfNode);
}

void Player::Reset() {
  gold = 0;
  ship->Reset();
}

void Player::Dump() {
  Log::s_log->Message("Player: %s", playerName.c_str());
  ship->Dump();

  Log::s_log->Message("Gold:\t%i\n", gold);
}

shared_ptr<Cargo> Player::GetCargo() {
  return ship->GetCargo();
}

string Player::GetPlayerName() {
  return playerName;
}

void Player::CreateNewPlayer(const string playerName, const string shipName) {
  if (player != NULL) {
    DeletePlayer();
  }
  player = new Player(playerName, shipName);
}

void Player::DeletePlayer() {
  delete player;
  player = NULL;
}
