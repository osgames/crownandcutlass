/* Crown and Cutlass
 * Timer Code
 */

#include "SDL.h"
#include "Log.h"
#include "Timer.h"

Timer *Timer::timer = NULL;

Timer::Timer() {
  lastTicks = SDL_GetTicks();
}

Timer::~Timer() {
  // Does anything need to happen here?
}

unsigned int Timer::GetTicks() {
  unsigned int currentTicks = SDL_GetTicks();

  if (currentTicks > lastTicks) {
    return currentTicks - lastTicks;
  } else {
    return 0;
  }
}

unsigned int Timer::ResetTimer() {
  unsigned int currentTicks = SDL_GetTicks();
  unsigned int ticks;

  // Calculate time since last reset
  if (currentTicks > lastTicks) {
    ticks = currentTicks - lastTicks;
  } else {
    ticks = 0;
  }

  lastTicks = currentTicks;

  return ticks;
}

void Timer::Initialize() {
  if (timer != NULL) {
    Log::s_log->Message("Warning: Attempt to reinitialize timer");
    return;
  }

  timer = new Timer();
  Log::s_log->Message("Timer initialized");
}

void Timer::Shutdown() {
  if (timer == NULL) {
    Log::s_log->Message("Warning: Multiple calls to shutdown timer");
    return;
  }

  delete timer;
  timer = NULL;
  Log::s_log->Message("Timer shutdown");
}
