/* Crown and Cutlass
 * TextureResource Header
 */

#if !defined ( _SHIP_RESOURCE_H_ )
#define _SHIP_RESOURCE_H_

#include <string>
#include "../GLee.h"
#include "IResource.h"

class Model;
class Cargo;

class ShipResource: public IResource {
 public:
   ShipResource(std::string name, std::string modelFileName, std::string textureName, float scale, int damage, unsigned int cargoSize, float mass, float sailArea, float drag, float rudderArea, float distToRudder);
   ~ShipResource();

  std::string GetType();

  void Load();
  void Unload();

  void Draw();

  int GetMaxDamage();

  unsigned int GetCargoSize();

  float GetMass();
  float GetSailArea();
  float GetDrag();
  float GetRudderArea();
  float GetDistToRudder();
  float GetMassMomentInertia();

  static const std::string s_type;

 private:
  // Ship name
  std::string m_name;

  // File names
  std::string m_modelFileName;
  std::string m_textureName;

  // Data pointers
  Model *m_model;

  // Scale to draw ship
  float m_scale;

  // Max amount of damage the ship can take before sinking
  int m_maxDamage;

  // Ship mass
  float m_mass;

  // Sail area
  float m_sailArea;

  // Drag coefficient
  float m_drag;

  // Used for rudder physics
  float m_rudderArea;
  float m_distToRudder;
  float m_massMomentInertia;

  // Size of cargo hold
  unsigned int m_cargoSize;

  void CalcMassMomentInertia();
};

#endif
