/* Crown and Cutlass
 * FontResource Code
 */

#include <string>
#include <guichan.hpp>
#include "../Log.h"
#include "IResource.h"
#include "FontResource.h"

using namespace std;

const string FontResource::s_type = string("Font");

FontResource::FontResource(string name, string filename, string characters): IResource(name) {
  m_filename = filename;
  m_characters = characters;
  m_font = NULL;

  Log::s_log->Message("FontResource %s created (Key: %s) \"%s\"", m_name.c_str(), GetKey().c_str(), characters.c_str());
}

FontResource::~FontResource() {
  if (m_font != NULL) {
    Log::s_log->Message("Warning: %s font resource deleted before unload", m_name.c_str());
    Unload();
  }
  Log::s_log->Message("FontResource %s destroyed", m_name.c_str());
}

string FontResource::GetType() {
  return s_type;
}

void FontResource::Load() {
  m_font = new gcn::ImageFont(m_filename, m_characters);
  Log::s_log->Message("FontResource %s loaded", m_name.c_str());
}

void FontResource::Unload() {
  delete m_font;
  m_font = NULL;
  Log::s_log->Message("FontResource %s unloaded", m_name.c_str());
}

gcn::ImageFont* FontResource::GetImageFont() {
  return m_font;
}
