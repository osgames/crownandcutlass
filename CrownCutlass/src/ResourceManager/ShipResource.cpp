/* Crown and Cutlass
 * TextureResource Code
 */

#include <string>
#include "../ccmath.h"
#include "../Log.h"
#include "../GLee.h"
#include "../Model.h"
#include "../BoundBox.h"
#include "../Point.h"
#include "IResource.h"
#include "ShipResource.h"

using namespace std;

const string ShipResource::s_type = string("Ship");

ShipResource::ShipResource(string name, string modelFileName, string textureName, float scale, int damage, unsigned int cargoSize, float mass, float sailArea, float drag, float rudderArea, float distToRudder): IResource(name) {
  m_name = name;
  m_modelFileName = modelFileName;
  m_textureName = textureName;
  m_model = NULL;
  m_scale = scale;
  m_maxDamage = damage;
  m_cargoSize = cargoSize;
  m_mass = mass;
  m_sailArea = sailArea;
  m_drag = drag;
  m_rudderArea = rudderArea;
  m_distToRudder = distToRudder;
  m_massMomentInertia = -1.0;

  Log::s_log->Message("ShipResource %s created (Key: %s)", m_name.c_str(), GetKey().c_str());
}

ShipResource::~ShipResource() {
  Log::s_log->Message("ShipResource %s destroyed", m_name.c_str());
}

string ShipResource::GetType() {
  return s_type;
}

void ShipResource::Load() {
  if (m_model != NULL) {
    Log::s_log->Message("Warning: Loading already loaded ship resource");
  }

  m_model = new Model(m_textureName);
  m_model->loadObj(m_modelFileName.c_str(), m_scale);

  if (m_massMomentInertia < 0.0) {
    CalcMassMomentInertia();
  }
  Log::s_log->Message("ShipResource %s loaded", m_name.c_str());
}

void ShipResource::Unload() {
  if (m_model == NULL) {
    Log::s_log->Message("Warning: Unloading already unloaded ship resource");
    return;
  }

  delete m_model;
  m_model = NULL;
  Log::s_log->Message("ShipResource %s unloaded", m_name.c_str());
}

void ShipResource::Draw() {
  m_model->draw();
}

int ShipResource::GetMaxDamage() {
  return m_maxDamage;
}

unsigned int ShipResource::GetCargoSize() {
  return m_cargoSize;
}

float ShipResource::GetMass() {
  return m_mass;
}

float ShipResource::GetSailArea() {
  return m_sailArea;
}

float ShipResource::GetDrag() {
  return m_drag;
}

float ShipResource::GetRudderArea() {
  return m_rudderArea;
}

float ShipResource::GetDistToRudder() {
  return m_distToRudder;
}

float ShipResource::GetMassMomentInertia() {
  return m_massMomentInertia;
}

void ShipResource::CalcMassMomentInertia() {
  float length, width;

  if (m_model == NULL) {
    Log::s_log->Message("Call to calcMassMomentInertia before model loaded");
    return;
  }

  length = fabs(m_model->modelBox->m_b1->m_x - m_model->modelBox->m_b4->m_x);
  width = fabs(m_model->modelBox->m_b1->m_z - m_model->modelBox->m_b4->m_z);

  m_massMomentInertia = (m_mass * (length * length + width * width)) / 12;
}
