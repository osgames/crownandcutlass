/* Crown and Cutlass
 * FontFactory Header
 */

#if !defined ( _FONT_FACTORY_H_ )
#define _FONT_FACTORY_H_

#include "IResourceFactory.h"

class IResource;
class TiXmlElement;

class FontFactory: public IResourceFactory {
 public:
  ~FontFactory();

  IResource* NewResource(TiXmlElement *XmlElement);
};

#endif
