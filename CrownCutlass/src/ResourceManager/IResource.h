/* Crown and Cutlass
 * IResource Header
 */

#if !defined ( _IRESOURCE_H_ )
#define _IRESOURCE_H_

#include <string>

class IResource {
 public:
  IResource(std::string name) { m_name = name; };
  virtual ~IResource() { };

  std::string GetKey() { return ConstructKey(GetType(), m_name); };
  std::string GetName() { return m_name; };
  virtual std::string GetType() = 0;

  virtual void Load() = 0;
  virtual void Unload() = 0;

  static std::string ConstructKey(std::string type, std::string name) { return type + ":" + name; };

 protected:
  std::string m_name;

 private:
  // Disallow anyone from using the standard constructor
  IResource();
};

#endif
