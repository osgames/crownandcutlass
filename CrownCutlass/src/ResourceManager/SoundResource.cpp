/* Crown and Cutlass
 * SoundResource Code
 */

#include <string>
#include <AL/al.h>
#include "../soundfile.h"
#include "../Log.h"
#include "IResource.h"
#include "SoundResource.h"

using namespace std;

const string SoundResource::s_type = string("Sound");

SoundResource::SoundResource(string name, string filename): IResource(name) {
  m_filename = filename;
  m_buffer = AL_NONE;

  Log::s_log->Message("SoundResource %s created (Key: %s)", m_filename.c_str(), GetKey().c_str());
}

SoundResource::~SoundResource() {
  if (m_buffer != AL_NONE) {
    Log::s_log->Message("Warning: %f sound resource deleted before unload", m_filename.c_str());
    Unload();
  }
  Log::s_log->Message("SoundResource %s destroyed", m_filename.c_str());
}

string SoundResource::GetType() {
  return s_type;
}

void SoundResource::Load() {
  LoadOgg(m_filename.c_str(), &m_buffer);
  Log::s_log->Message("SoundResource %s loaded", m_filename.c_str());
}

void SoundResource::Unload() {
  alDeleteBuffers(1, &m_buffer);
  m_buffer = AL_NONE;
  Log::s_log->Message("SoundResource %s unloaded", m_filename.c_str());
}

ALuint SoundResource::GetBuffer() {
  return m_buffer;
}
