/* Crown and Cutlass
 * TextureResource Header
 */

#if !defined ( _TEXTURE_RESOURCE_H_ )
#define _TEXTURE_RESOURCE_H_

#include <string>
#include "../GLee.h"
#include "IResource.h"

class TextureResource: public IResource {
 public:
   TextureResource(std::string name, std::string filename, GLuint tileMode, bool genMipMaps, bool tryCompress, bool tryAlpha);
   ~TextureResource();

  std::string GetType();

  void Load();
  void Unload();

  GLuint GetTexture();

  static const std::string s_type;

 private:
  bool m_loaded;
  std::string m_filename;

  GLuint m_texture;
  GLuint m_tileMode;  // eg GL_REPEAT, GL_CLAMP
  bool m_genMipMaps;
  bool m_tryCompress;
  bool m_tryAlpha;
};

#endif
