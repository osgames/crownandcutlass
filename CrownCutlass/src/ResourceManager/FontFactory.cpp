/* Crown and Cutlass
 * FontFactory Code
 */

#include "../tinyxml.h"
#include "../Log.h"
#include "IResource.h"
#include "FontResource.h"
#include "FontFactory.h"

using namespace std;

FontFactory::~FontFactory() {
  Log::s_log->Message("Font factory deleted");
}

IResource* FontFactory::NewResource(TiXmlElement *XmlElement) {
  string name, filename, characters;

  if (string(XmlElement->Attribute("type")) != FontResource::s_type) {
    throw string("Invalid resource type");
  }

  name = string(XmlElement->Attribute("name"));
  filename = string(XmlElement->Attribute("filename"));
  characters = string(XmlElement->Attribute("characters"));

  return new FontResource(name, filename, characters);
}
