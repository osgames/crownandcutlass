/* Crown and Cutlass
 * TextureFactory Header
 */

#if !defined ( _SHIP_FACTORY_H_ )
#define _SHIP_FACTORY_H_

#include "IResourceFactory.h"

class IResource;
class TiXmlElement;

class ShipFactory: public IResourceFactory {
 public:
   ~ShipFactory();

  IResource* NewResource(TiXmlElement *XmlElement);
};

#endif
