/* Crown and Cutlass
 * TextureResource Code
 */

#include <string>
#include "../Log.h"
#include "../GLee.h"
#include "../image.h"
#include "IResource.h"
#include "TextureResource.h"

using namespace std;

const string TextureResource::s_type = string("Texture");

TextureResource::TextureResource(string name, string filename, GLuint tileMode, bool genMipMaps, bool tryCompress, bool tryAlpha): IResource(name) {
  m_filename = filename;
  m_texture = 0;
  m_tileMode = tileMode;
  m_genMipMaps = genMipMaps;
  m_tryCompress = tryCompress;
  m_tryAlpha = tryAlpha;
  m_loaded = false;

  Log::s_log->Message("TextureResource %s created (Key: %s)", m_filename.c_str(), GetKey().c_str());
}

TextureResource::~TextureResource() {
  if (m_loaded) {
    Log::s_log->Message("Warning: %s texture resource deleted before unload", m_filename.c_str());
    Unload();
  }
  Log::s_log->Message("TextureResource %s destroyed", m_filename.c_str());
}

string TextureResource::GetType() {
  return s_type;
}

void TextureResource::Load() {
  if (m_loaded) {
    Log::s_log->Message("Warning: Load called on already loaded texture");
    Unload();
  }

  m_loaded = BuildTexture(m_filename.c_str(), &m_texture, m_tileMode, m_genMipMaps, m_tryCompress, m_tryAlpha);
  if (m_loaded) {
    Log::s_log->Message("TextureResource %s loaded", m_filename.c_str());
  } else {
    Log::s_log->Message("Warning: Could not load TextureResource %s", m_filename.c_str());
  }
}

void TextureResource::Unload() {
  if (!m_loaded) {
    Log::s_log->Message("Warning: Unload called on unloaded texture");
    return;
  }

  glDeleteTextures(1, &m_texture);
  m_texture = 0;
  m_loaded = false;
  Log::s_log->Message("TextureResource %s unloaded", m_filename.c_str());
}

GLuint TextureResource::GetTexture() {
  return m_texture;
}
