/* Crown and Cutlass
 * FontResource Header
 */

#if !defined ( _FONT_RESOURCE_H_ )
#define _FONT_RESOURCE_H_

#include <string>
#include <guichan.hpp>
#include "IResource.h"

class FontResource: public IResource {
 public:
   FontResource(std::string name, std::string filename, std::string characters);
   ~FontResource();

  std::string GetType();

  void Load();
  void Unload();

  gcn::ImageFont *GetImageFont();

  static const std::string s_type;

 private:
  std::string m_filename;
  std::string m_characters;

  gcn::ImageFont *m_font;
};

#endif
