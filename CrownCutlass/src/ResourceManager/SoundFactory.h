/* Crown and Cutlass
 * SoundFactory Header
 */

#if !defined ( _SOUND_FACTORY_H_ )
#define _SOUND_FACTORY_H_

#include "IResourceFactory.h"

class IResource;
class TiXmlElement;

class SoundFactory: public IResourceFactory {
 public:
  ~SoundFactory();

  IResource* NewResource(TiXmlElement *XmlElement);
};

#endif
