/* Crown and Cutlass
 * SoundResource Header
 */

#if !defined ( _SOUND_RESOURCE_H_ )
#define _SOUND_RESOURCE_H_

#include <string>
#include <AL/al.h>
#include "IResource.h"

class SoundResource: public IResource {
 public:
   SoundResource(std::string name, std::string filename);
  ~SoundResource();

  std::string GetType();

  void Load();
  void Unload();

  ALuint GetBuffer();

  static const std::string s_type;

 private:
  std::string m_filename;
  ALuint m_buffer;
};

#endif
