/* Crown and Cutlass
 * Resource Manager Header
 */

#if !defined ( _RESOURCEMANAGER_H_ )
#define _RESOURCEMANAGER_H_

#include <string>
#include <map>
#include <vector>
#include <memory>

class IResource;
class IResourceFactory;

struct ResourceEntry;
typedef std::map< std::string, ResourceEntry* > ResourceMap;
typedef std::vector< ResourceEntry* > ResourceEntryVector;
typedef std::map< std::string, ResourceEntryVector > ResourceTypeMap;

class ResourceManager {
 public:
  ResourceManager();
  ~ResourceManager();

  // Actual resource handling routines
  // Acquire a resource, loading it if necessary
  IResource* Acquire(std::string key);
  // Acquire a random resource of the specified type, only really useful for ships at the moment
  IResource* AcquireRandom(std::string type);
  // Release a resource, unloading it if necessary
  void Release(IResource *resource);

  void LoadXMLFile(std::string filename, std::auto_ptr<IResourceFactory> factory);

  void LogDebugInfo();

  static ResourceManager *s_resourceManager;
  static void Initialize();
  static void Shutdown();

 private:
  ResourceMap m_resources;
  ResourceTypeMap m_typeVectors;

  IResource* DoAcquireResource(ResourceEntry *entry);
};

#endif
