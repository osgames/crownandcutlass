/* Crown and Cutlass
 * IResourceFactory Header
 */

#if !defined ( _IRESOURCE_FACTORY_H_ )
#define _IRESOURCE_FACTORY_H_

#include <string>

class IResource;
class TiXmlElement;

class IResourceFactory {
 public:
  virtual ~IResourceFactory() { };

  virtual IResource* NewResource(TiXmlElement *XmlElement) = 0;
};

#endif
