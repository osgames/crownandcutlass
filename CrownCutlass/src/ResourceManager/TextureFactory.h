/* Crown and Cutlass
 * TextureFactory Header
 */

#if !defined ( _TEXTURE_FACTORY_H_ )
#define _TEXTURE_FACTORY_H_

#include "IResourceFactory.h"

class IResource;
class TiXmlElement;

class TextureFactory: public IResourceFactory {
 public:
   ~TextureFactory();

  IResource* NewResource(TiXmlElement *XmlElement);
};

#endif
