/* Crown and Cutlass
 * TextureFactory Code
 */

#include "../tinyxml.h"
#include "../Log.h"
#include "../GLee.h"
#include "IResource.h"
#include "TextureResource.h"
#include "TextureFactory.h"

using namespace std;

TextureFactory::~TextureFactory() {
  Log::s_log->Message("Texture factory deleted");
}

IResource* TextureFactory::NewResource(TiXmlElement *XmlElement) {
  string name, filename;
  GLuint tileMode;
  string tileModeStr;
  int genMipMaps, tryCompress, tryAlpha;

  if (string(XmlElement->Attribute("type")) != TextureResource::s_type) {
    throw string("Invalid resource type");
  }

  name = string(XmlElement->Attribute("name"));
  filename = string(XmlElement->Attribute("filename"));

  tileModeStr = string(XmlElement->Attribute("tileMode"));
  if (tileModeStr == string("GL_REPEAT")) {
    tileMode = GL_REPEAT;
  } else if (tileModeStr == string("GL_CLAMP")) {
    tileMode = GL_CLAMP;
  } else if (tileModeStr == string("GL_CLAMP_TO_EDGE")) {
    tileMode = GL_CLAMP_TO_EDGE;
  } else {
    throw ("Unknown tile mode " + tileModeStr);
  }

  if (XmlElement->QueryIntAttribute("genMipMaps", &genMipMaps) != TIXML_SUCCESS) {
    throw string("Could not read genMipMaps attribute");
  }

  if (XmlElement->QueryIntAttribute("tryCompress", &tryCompress) != TIXML_SUCCESS) {
    throw string("Could not read tryCompress attribute");
  }

  if (XmlElement->QueryIntAttribute("tryAlpha", &tryAlpha) != TIXML_SUCCESS) {
    throw string("Could not read tryAlpha attribute");
  }

  return new TextureResource(name, filename, tileMode, genMipMaps != 0, tryCompress != 0, tryAlpha != 0);
}
