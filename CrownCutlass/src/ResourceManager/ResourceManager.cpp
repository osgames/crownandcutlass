/* Crown and Cutlass
 * Resource Manager Code
 */

#include <string>
#include <map>
#include <vector>
#include "../Log.h"
#include "../tinyxml.h"
#include "../ccmath.h"
#include "IResource.h"
#include "IResourceFactory.h"
#include "ResourceManager.h"

using namespace std;

struct ResourceEntry {
  IResource *resource;
  unsigned int refCount;
};

ResourceManager* ResourceManager::s_resourceManager = NULL;

ResourceManager::ResourceManager() {
  // Does anything need to happen here?
  Log::s_log->Message("Resource manager initialized");
}

ResourceManager::~ResourceManager() {
  ResourceEntry *entry;
  string key;

  m_typeVectors.clear();

  // Loop through all the resources, free them and the ResourceEntry the map was using
  while (!m_resources.empty()) {
    entry = m_resources.begin()->second;
    key = m_resources.begin()->first;
    delete entry->resource;
    m_resources.erase(key);
    delete entry;
  }

  if (!m_resources.empty()) {
    Log::s_log->Message("Warning: Could not empty resource map");
  }

  Log::s_log->Message("Resource manager shut down");
}

IResource* ResourceManager::Acquire(string key) {
  ResourceEntry *entry;

  entry = m_resources[key];
  if (entry == NULL) {
    throw string("Key not found in resource map (" + key + ")");
  }

  return DoAcquireResource(entry);
}

IResource* ResourceManager::AcquireRandom(string type) {
  ResourceEntryVector resources;
  unsigned int index;
  ResourceEntry *entry;

  resources = m_typeVectors[type];
  if (resources.empty()) {
    throw string("Zero resources of type " + type + " found");
  }

  index = (unsigned int) randInt(resources.size() - 1);
  entry = resources[index];
  Log::s_log->Message("Got random resource: %s", entry->resource->GetKey().c_str());

  return DoAcquireResource(entry);
}

IResource* ResourceManager::DoAcquireResource(ResourceEntry *entry) {
  // Check to see if this is the first use of this resource, load it if it is
  if (entry->refCount == 0) {
    entry->resource->Load();
  }
  // Increment the reference count
  ++entry->refCount;

  return entry->resource;
}

void ResourceManager::Release(IResource *resource) {
  ResourceEntry *entry;

  entry = m_resources[resource->GetKey()];
  if (entry == NULL) {
    throw string("Key not found in resource map (" + resource->GetKey() + ")");
  }

  // Decrement the reference count
  --entry->refCount;
  // Check to see if we are releasing the last reference to this resource, and unload it if we are
  if (entry->refCount == 0) {
    entry->resource->Unload();
  }
}

void ResourceManager::LoadXMLFile(std::string filename, auto_ptr<IResourceFactory> factory) {
  TiXmlElement *elem;
  TiXmlDocument doc( filename.c_str() );

  doc.LoadFile();
  if (doc.Error()) {
    // Should throw an exception
    Log::s_log->Message("Warning: Could not load %s. %s (line %d, column %d)", filename.c_str(), doc.ErrorDesc(), doc.ErrorRow(), doc.ErrorCol());
    return;
  }

  for (elem = doc.FirstChildElement("Resources")->FirstChildElement("Resource"); elem; elem = elem->NextSiblingElement("Resource")) {
    ResourceEntry *entry = new ResourceEntry;
    entry->refCount = 0;
    try {
      entry->resource = factory->NewResource(elem);
      if (entry->resource == NULL) {
        throw string("Factory returned null pointer");
      }
      m_resources[entry->resource->GetKey()] = entry;

      m_typeVectors[entry->resource->GetType()].push_back(entry);
    } catch (string e) {
      delete entry;
      throw e;
    }
  }

  Log::s_log->Message("Resource file %s loaded", filename.c_str());
}

void ResourceManager::LogDebugInfo() {
  Log::s_log->Message("%d total resources", m_resources.size());
}

void ResourceManager::Initialize() {
  if (s_resourceManager != NULL) {
    Log::s_log->Message("Warning: Attempt to re-initialize resource manager");
    return;
  }

  s_resourceManager = new ResourceManager();
}

void ResourceManager::Shutdown() {
  if (s_resourceManager == NULL) {
    Log::s_log->Message("Warning: Attempt to re-shutdown resource manager");
    return;
  }
  delete s_resourceManager;
  s_resourceManager = NULL;
}

