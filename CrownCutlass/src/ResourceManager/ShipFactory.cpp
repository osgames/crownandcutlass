/* Crown and Cutlass
 * TextureFactory Code
 */

#include "../tinyxml.h"
#include "../Log.h"
#include "../GLee.h"
#include "IResource.h"
#include "IResourceFactory.h"
#include "ShipResource.h"
#include "ShipFactory.h"

using namespace std;

ShipFactory::~ShipFactory() {
  Log::s_log->Message("Ship factory deleted");
}

IResource* ShipFactory::NewResource(TiXmlElement *XmlElement) {
  TiXmlElement *generalElement;
  TiXmlElement *displayElement;
  TiXmlElement *physicsElement;
  string name, modelFileName, textureName;
  float scale;
  int damage, cargoSize;
  float mass, sailArea, drag, rudderArea, distToRudder;

  if (string(XmlElement->Attribute("type")) != ShipResource::s_type) {
    throw string("Invalid resource type");
  }

  name = string(XmlElement->Attribute("name"));

  generalElement = XmlElement->FirstChildElement("General");
  if (generalElement == NULL) throw string("Could not read General element");
  if (generalElement->QueryIntAttribute("damage", &damage) != TIXML_SUCCESS) {
    throw string("Could not read damage attribute");
  }
  if (generalElement->QueryIntAttribute("cargoSize", &cargoSize) != TIXML_SUCCESS) {
    throw string("Could not read cargoSize attribute");
  }
  if (cargoSize <= 0) {
    throw string("Invalid cargoSize attribute");
  }

  displayElement = XmlElement->FirstChildElement("Display");
  if (displayElement == NULL) throw string("Could not read Display element");
  modelFileName = string(displayElement->Attribute("modelFileName"));
  textureName = string(displayElement->Attribute("textureName"));
  if (displayElement->QueryFloatAttribute("scale", &scale) != TIXML_SUCCESS) {
    throw string("Could not read scale attribute");
  }

  physicsElement = XmlElement->FirstChildElement("Physics");
  if (physicsElement == NULL) throw string("Could not read Physics element");
  if (physicsElement->QueryFloatAttribute("mass", &mass) != TIXML_SUCCESS) {
    throw string("Could not read mass attribute");
  }
  if (physicsElement->QueryFloatAttribute("sailArea", &sailArea) != TIXML_SUCCESS) {
    throw string("Could not read sailArea attribute");
  }
  if (physicsElement->QueryFloatAttribute("drag", &drag) != TIXML_SUCCESS) {
    throw string("Could not read drag attribute");
  }
  if (physicsElement->QueryFloatAttribute("rudderArea", &rudderArea) != TIXML_SUCCESS) {
    throw string("Could not read rudderArea attribute");
  }
  if (physicsElement->QueryFloatAttribute("distToRudder", &distToRudder) != TIXML_SUCCESS) {
    throw string("Could not read distToRudder attribute");
  }

  return new ShipResource(name, modelFileName, textureName, scale, damage, (unsigned int) cargoSize, mass, sailArea, drag, rudderArea, distToRudder);
}
