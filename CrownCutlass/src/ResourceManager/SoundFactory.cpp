/* Crown and Cutlass
 * SoundFactory Code
 */

#include "../tinyxml.h"
#include "../Log.h"
#include "IResource.h"
#include "SoundResource.h"
#include "SoundFactory.h"

using namespace std;

SoundFactory::~SoundFactory() {
  Log::s_log->Message("Sound factory deleted");
}

IResource* SoundFactory::NewResource(TiXmlElement *XmlElement) {
  string name, filename;

  if (string(XmlElement->Attribute("type")) != SoundResource::s_type) {
    throw string("Invalid resource type");
  }

  name = string(XmlElement->Attribute("name"));
  filename = string(XmlElement->Attribute("filename"));

  return new SoundResource(name, filename);
}
