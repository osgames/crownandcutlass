Crown and Cutlass
Alpha-1.5

Crown and Cutlass is a 3d pirate action/adventure game in the spirit of the old Pirates! game.  It is being written in C++, using OpenGL and SDL with an emphasis on cross-platform compatibility.  The project is still early on in development, although it is just beginning to look like an actual game.

Please visit http://crownandcutlass.sourceforge.net/ for more information.

Alpha 1.5 Notes:
We have implemented a new resource manager for this release.  It makes handling images, sounds, and other game resources much easier.  It also allows different parts of the game to share copies of textures or sound files in memory with no extra effort.  Although it's not a 

We have also fixed several memory leaks.  In order to easily fix one problem, we started using Boost's shared_ptr.  It helps us automatically free cargo objects when nothing is using them anymore.  We also fixed a few other issues found with Valgrind.

Alpha 1.4 Notes:
One of our primary reasons for this release is to just move beyond the Alpha 1.3/1.31 situation.  Instead of worrying about downloading the right file, and making sure you are running the right exe, now you should be able to just use the 1.4 installer (for windows users) or source (for linux users or people who want to compile themselves).

Perhaps most notably for this release, we have started using GLee (by Ben Woodhouse, http://elf-stone.com) to handle OpenGL extensions.  I was getting tired of trying to keep up with NVidia's gl headers, while not breaking linux/windows compatibility.  Also, I expect that non-NVidia (e.g. Mesa) headers will be easier to get to work as well.  See the "GLee.readme" file for more information about GLee.  Please let me know of any issues you have with extensions.

We have finished our transition to Guichan.  We are still learning how to use it though, so look for improved menus in the future.  I also added a new sound system, which should help make it easier to add audio to various parts of the game.  It still needs some work, but it has already made things easier for us.

Alpha 1.31 Notes:
We are now using Guichan 0.4, which allows for font images on multiple lines.  That allowed us to avoid the image font size issues we had in Alpha 1.3.

Alpha 1.3 Notes:
This is a fairly major release for us.  We have begun to rework our menus using Guichan (http://guichan.sourceforge.net).  It would be nice if we had gotten more menu work done, but we decided that we just needed to release.  As a result, only about half of the menus are converted.  Expect additional menu improvement in coming versions.

The naval battles have been tweaked quite a bit.  We have the first shot at ship physics implemented, which really changes the feel of sailing significantly.  Additional tweaking needs to be done, but we have a start.  The enemy ship AI has also been significantly improved.  There is now a sky dome in the naval battles.  However, you need to use the 1-9 keys to change the view to see it.  We are planning future work on the camera, which would make this feature more visible.  Finally, it is also now possible to repair your ship in cities.

Thanks to Aleksandar Kovac for new city models.  We still need to do a lot of work to really make them look at home on the heightmap, but they are a huge improvement over the old giant yellow boxes.  Thanks to Ralph Hogaboom for the help rotating and placing the models we have!

We have reworked how the saved games are stored.  The end user shouldn't really be able to tell a difference, but it is an important foundation for future saved game work.

We now have a web updater to automatically keep files updated.  However, we do not yet have a host for the data.  Please contact us if you could provide server space.  Unfortunately, we need perl and would prefer shell access to be able to manually regenerate the updater files when necessary.  Until we get a host settled, the updater may not be extremely useful.

We also have a new background from Aleksandar Kovac, as well as new title music from Derek Mounce.  Gary Luck also provided us with a new image for entering naval battles  I'm sure there have been other contributions, but I'm sorry I haven't tracked that as carefully as I should.  Thanks to all of you for your contributions!


Alpha 1.2 Notes:
We have added audio in this release using Ogg Vorbis files and OpenAL.  Music is streamed off of the disk, while the sound effects are loaded directly into memory.  We still have some tweaking to do, and currently do not take advantage of OpenAL's 3d capabilities.  We are in the process of finding music for use in-game.  If you are interested in helping, please see our Help Wanted forum at: http://sourceforge.net/forum/forum.php?forum_id=409786

We have fairly complete naval battles now, although the speed and turning radius of the ships still needs to be tweaked and there is no HUD yet.  The AI also needs a significant amount of work still.  It current will not lead it's shots or even account for it's own speed at all.  However, it is a decent start for now.  Players periodically see ships and have the choice to attack or sail away.  After the battle (assuming victory), the player may take the captured cargo and gold.  More work will be done as we add additional gameplay features such as nationalities and ship crews.

Basic trading capabilities are present in Alpha-1.2.  You can now buy and sell goods in towns, as well as capture cargo in naval battles.  We are making progress towards a more functional (and dynamic) economy.

In order to allow for the trading and the naval battles, we reworked how ships are handled and added a few new ships.  The ship settings are loaded in from the Ships.xml file.

Thanks to Aleksandar Kovac for supplying us with a new logo.  It has been incorperated into this release in several places.

Our menu system has been reworked to make creating menus easier for the developers, and to allow us to create more useful menus.  It also now allows for mouse input.


Alpha-1.1 Notes:
Once Jeff Koppe provided the new model, we had to rewrite the model loading code so we could use it.  The new code is somewhat of a hybrid between the old system and what is planned for the future.  For now it still uses display lists, although we may move towards VA/VBO's in the future.  Eventually, we'll probably write our own model format, but for now it just loads .obj files.

There is a new automatic menu generation system, so we don't have to recode a basic menu everytime we need to create a new menu in for the game.  Rather than copying and pasting code, we can now just pass this menu class a few lists and it will generate the menus automatically.

A preliminary economy is in this release as well.  Currently it just loads the data and displays it when you enter a city.  However, future releases will build on this to allow the player to interact with the economy and trade in the cities.

The ocean rendering has been updated.  The actual shore line no longer moves, which will allow us to improve the heightmap, since low elevation land won't spend half of the time underwater.  There are also waves breaking on the shore lines.  The actual wave motion may change some, but the effect is pretty good in general.


Requirements:
SDL library
SDL_image library
OpenAL
Guichan (http://guichan.sourceforge.net)
Boost libraries
Videocard capable of multi-texturing

Config.xml:
This is the configuration file.  It is now fairly straight-forward xml, loaded with the tinyxml library.  The Width and Height values should be integers and they determine the resolution of the window.  Fullscreen should be either "true" or "false" and change whether Crown and Cutlass runs fullscreen or in a window.  VBO should also be either "true" or "false" and specifies whether to use VBOs to draw the terrain or just vertex arrays.  If VBOs are enabled, but not supported by the video card, Crown and Cutlass logs a warning and falls back on vertex arrays.  The configuration loading code is now much more flexible, simpler, and more robust.  The SDLParachute should be set to "true" unless you are trying to get a core dump for debugging (then set it to "false").  CompressTextures enables ("true") or disables ("false") compressing the textures when they are stored in video memory.  Almost everyone should be able to enable this setting, but if you have trouble, you can try disabling compressed textures.  The TicksBetweenBattles is the average number of milliseconds between naval battles.  It should be fine at "20000" although for testing it is nice to use a much larger number.  "NumberALSources" determines how many sounds can play at one time.  All soundcards/drivers have an actual hard limit for this value.  16 is the current default, which should be safe on almost all soundcards and is currently more than enough to handle all of the audio in Crown and Cutlass.  "DebugLogMode" should either be "true" or "false".  It enables some extra log messages that many people may not need to see, but may be helpful in finding bugs.  Most users can probably leave this off ("false"), although if you experience problems you may want to turn it on to give us a more complete log.


Game Controls:
up - speed up
down - slow down
left - turn left
right - turn right
'm' - display the map
'q' - fire port (left) cannons (in naval battles)
'e' - fire starboard (right) cannons (in naval batles)
'x' - abort battle (in naval batles)
'c' - abrupt turn-around (for testing)
'd' - dump player info (for testing)
'b' - force a naval battle (for testing)
'z' - take screenshot
1-8 - change view (this is mostly just for development)
esc - go to the menu

When looking at the map, hold 't' and click the mouse to teleport to that location.  This is just a development feature, but it sure is useful for testing.  Also, 'i' will display some debugging information.  'b' forces a naval battle to begin when sailing around, and 'x' immediately exits a naval battle in progress.


Support:
If you are having trouble compiling or running Crown and Cutlass, please visit http://crownandcutlass.sourceforge.net/ for assistance.

Known issues:
- Errors loading files are not handled gracefully
- Some Cities are difficult or impossible to enter
- VBO's do not work on ATI 8500 video cards
- Closing the game window directly (without choosing "Exit" from the main menu) can cause crashes
- Occasionally, viewing the main menu causes a crash


Developers:
David Thulson - Project Admin, Co-Lead Developer
Collin Sanford - Co-Lead Developer
Ben Coppock - Web Design


Code Contributors for Alpha 1.2:
Ed Mack - Additional Makefile help

Code Contributors for Alpha 1.1:
Michael Verchenko - Menu bug fix
Ed Mack - Improved Makefile


Thanks:
Jeff Koppe provided us with the 3D models of the ships.  He has agreed to provide us with additional models as well.  Needless to say, it is really nice to have such a good ship model in the game.

Wes Attaway provided the heightmap.  It is modified from a scan of the original Pirates! map which you can find on any number of classic gaming websites.

Thanks to Aleksandar Kovac for designing the Crown and Cutlass logo and all his contributions of artwork.

Thanks to Gary Luck for his sound and image contributions.

Crown and Cutlass uses TinyXML for handling XML files.  More information can be found in the TinyXML.readme file or at http://www.grinninglizard.com/tinyxml/index.html

The obj model loading code is slightly modified from the code found in Nate Robins' "Smooth Normal Generation with Preservation of Edges" project.  It can be found at: http://www.xmission.com/~nate/smooth.html

The collision detection code is used with permission from "Fast, Minimum Storage Ray-Triangle Intersection" by Ben Trumbore and Tomas Mller.  This code can be found at: http://www.acm.org/jgt/papers/MollerTrumbore97/  Their entire paper can be found at: http://www.graphics.cornell.edu/pubs/1997/MT97.html

The screenshot and tga handling code was almost directly copied from chapter 7 of "OpenGL Game Programming".  Slight modifications were necessary to get rid of Windows specific code.

The frustum culling code is slightly edited from a tutorial by Mark Morley.  I found it at www.markmorley.com/opengl/frustumculling.html, but since that page seems to be unavailable, you can find a copy (saved from the google cache) at http://crownandcutlass.sourceforge.net/frustum.html

The BuildTexture function is fairly heavily modified from NeHe Tutorial 6.

The water, snow, and beach textures are modified from images found at http://www.vb3d.com/Textures.html

Thanks to Corey O'Connor for endless help with opengl.
